-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 14, 2017 at 04:08 AM
-- Server version: 5.6.35
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `kess`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ms_achievment`
--

CREATE TABLE `ms_achievment` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_achievment`
--

INSERT INTO `ms_achievment` (`id`, `name`) VALUES
(1, '*1st Winner 2015 February – contest '),
(2, '*2nd Winner 2015 February – contest '),
(3, 'Group Unit Machine Sales ');

-- --------------------------------------------------------

--
-- Table structure for table `ms_aduan`
--

CREATE TABLE `ms_aduan` (
  `id` int(11) NOT NULL,
  `ms_kategori_aduan_id` int(11) NOT NULL,
  `nama_lengkap` text NOT NULL,
  `email` text NOT NULL,
  `hp` text NOT NULL,
  `alamat` text NOT NULL,
  `isi` text NOT NULL,
  `balas` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_aduan`
--

INSERT INTO `ms_aduan` (`id`, `ms_kategori_aduan_id`, `nama_lengkap`, `email`, `hp`, `alamat`, `isi`, `balas`, `created_at`, `updated_at`) VALUES
(1, 1, 'Muhammad Ghozie Manggala', 'user@email.com', '08123456789', 'Keputih Gg Makam D11, Sukoliloo, Surabaya', 'Kok nominal tilang banyak banget sih??? ', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 3, 'Ahmad Mustofa', 'ahmad@mustofa.com', '081234567890', 'Sumenep, Madura', 'Hebat sekali polisi Sumenep, membantu kakek-kakek untuk menyeberang jalan', 0, '2016-10-24 04:45:33', '2016-10-24 04:45:33');

-- --------------------------------------------------------

--
-- Table structure for table `ms_categories`
--

CREATE TABLE `ms_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `ms_user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_categories`
--

INSERT INTO `ms_categories` (`id`, `title`, `parent_id`, `description`, `ms_user_id`, `created_at`, `updated_at`) VALUES
(1, 'Berita', 0, 'kategori artikel berita/news gemarsehati', 0, '2015-04-14 07:38:21', '2016-02-16 21:20:53'),
(2, 'Event', 0, 'kategori artikel yang bersifat informasi detail tentang event gemar sehati\r\n', 0, '2015-04-14 07:38:21', '2016-02-16 21:21:18'),
(3, 'Company', 0, 'kategori artikel yg merupakan artikel tentang komunitas, seperti about us, visi, misi dll', 0, '2015-04-18 15:20:13', '2016-02-16 21:22:05'),
(5, 'Download', 0, 'Berisi post tentang link download aplikasi', 0, '2016-01-19 02:54:43', '2016-01-19 02:54:43'),
(7, 'Testimoni', 0, 'Testimoni pengguna', 0, '2016-01-21 07:35:35', '2016-01-21 07:35:35'),
(8, 'Kelebihan', 0, '4 kelebihan dari gemarsehati/kangen water', 0, '2016-01-21 20:32:32', '2016-01-21 20:48:48'),
(9, 'Sekilas', 0, 'Sekilas ttg gemarsehati', 0, '2016-01-21 20:48:33', '2016-01-21 20:48:33'),
(15, 'Sambutan', 0, 'Sambutan ', 0, '2016-10-18 01:27:51', '2016-10-18 01:27:51'),
(23, 'kessnews', 0, '', 0, '2017-03-09 01:23:05', '2017-03-09 01:23:05');

-- --------------------------------------------------------

--
-- Table structure for table `ms_cctv`
--

CREATE TABLE `ms_cctv` (
  `id` int(11) NOT NULL,
  `ms_media_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `embed` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_cctv`
--

INSERT INTO `ms_cctv` (`id`, `ms_media_id`, `title`, `description`, `embed`, `created_at`, `updated_at`) VALUES
(3, 646, 'Jalan besar', 'Jalan besar sekali ', 'the Embed', '2016-10-29 18:50:11', '2016-10-29 18:57:03');

-- --------------------------------------------------------

--
-- Table structure for table `ms_cities`
--

CREATE TABLE `ms_cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ms_province_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_cities`
--

INSERT INTO `ms_cities` (`id`, `code`, `name`, `ms_province_id`) VALUES
(1, NULL, 'Aceh Barat', 21),
(2, NULL, 'Aceh Barat Daya', 21),
(3, NULL, 'Aceh Besar', 21),
(4, NULL, 'Aceh Jaya', 21),
(5, NULL, 'Aceh Selatan', 21),
(6, NULL, 'Aceh Singkil', 21),
(7, NULL, 'Aceh Tamiang', 21),
(8, NULL, 'Aceh Tengah', 21),
(9, NULL, 'Aceh Tenggara', 21),
(10, NULL, 'Aceh Timur', 21),
(11, NULL, 'Aceh Utara', 21),
(12, NULL, 'Agam', 32),
(13, NULL, 'Alor', 23),
(14, NULL, 'Ambon', 19),
(15, NULL, 'Asahan', 34),
(16, NULL, 'Asmat', 24),
(17, NULL, 'Badung', 1),
(18, NULL, 'Balangan', 13),
(19, NULL, 'Balikpapan', 15),
(20, NULL, 'Banda Aceh', 21),
(21, NULL, 'Bandar Lampung', 18),
(22, NULL, 'Bandung', 9),
(24, NULL, 'Bandung Barat', 9),
(25, NULL, 'Banggai', 29),
(26, NULL, 'Banggai Kepulauan', 29),
(27, NULL, 'Bangka', 2),
(28, NULL, 'Bangka Barat', 2),
(29, NULL, 'Bangka Selatan', 2),
(30, NULL, 'Bangka Tengah', 2),
(31, NULL, 'Bangkalan', 11),
(32, NULL, 'Bangli', 1),
(33, NULL, 'Banjar', 13),
(35, NULL, 'Banjarbaru', 13),
(36, NULL, 'Banjarmasin', 13),
(37, NULL, 'Banjarnegara', 10),
(38, NULL, 'Bantaeng', 28),
(39, NULL, 'Bantul', 5),
(40, NULL, 'Banyuasin', 33),
(41, NULL, 'Banyumas', 10),
(42, NULL, 'Banyuwangi', 11),
(43, NULL, 'Barito Kuala', 13),
(44, NULL, 'Barito Selatan', 14),
(45, NULL, 'Barito Timur', 14),
(46, NULL, 'Barito Utara', 14),
(47, NULL, 'Barru', 28),
(48, NULL, 'Batam', 17),
(49, NULL, 'Batang', 10),
(50, NULL, 'Batang Hari', 8),
(51, NULL, 'Batu', 11),
(52, NULL, 'Batu Bara', 34),
(53, NULL, 'Bau-Bau', 30),
(54, NULL, 'Bekasi', 9),
(56, NULL, 'Belitung', 2),
(57, NULL, 'Belitung Timur', 2),
(58, NULL, 'Belu', 23),
(59, NULL, 'Bener Meriah', 21),
(60, NULL, 'Bengkalis', 26),
(61, NULL, 'Bengkayang', 12),
(62, NULL, 'Bengkulu', 4),
(63, NULL, 'Bengkulu Selatan', 4),
(64, NULL, 'Bengkulu Tengah', 4),
(65, NULL, 'Bengkulu Utara', 4),
(66, NULL, 'Berau', 15),
(67, NULL, 'Biak Numfor', 24),
(68, NULL, 'Bima', 22),
(70, NULL, 'Binjai', 34),
(71, NULL, 'Bintan', 17),
(72, NULL, 'Bireuen', 21),
(73, NULL, 'Bitung', 31),
(74, NULL, 'Blitar', 11),
(76, NULL, 'Blora', 10),
(77, NULL, 'Boalemo', 7),
(79, NULL, 'Bogor', 9),
(80, NULL, 'Bojonegoro', 11),
(81, NULL, 'Bolaang Mongondow (Bolmong)', 31),
(82, NULL, 'Bolaang Mongondow Selatan', 31),
(83, NULL, 'Bolaang Mongondow Timur', 31),
(84, NULL, 'Bolaang Mongondow Utara', 31),
(85, NULL, 'Bombana', 30),
(86, NULL, 'Bondowoso', 11),
(87, NULL, 'Bone', 28),
(88, NULL, 'Bone Bolango', 7),
(89, NULL, 'Bontang', 15),
(90, NULL, 'Boven Digoel', 24),
(91, NULL, 'Boyolali', 10),
(92, NULL, 'Brebes', 10),
(93, NULL, 'Bukittinggi', 32),
(94, NULL, 'Buleleng', 1),
(95, NULL, 'Bulukumba', 28),
(96, NULL, 'Bulungan (Bulongan)', 16),
(97, NULL, 'Bungo', 8),
(98, NULL, 'Buol', 29),
(99, NULL, 'Buru', 19),
(100, NULL, 'Buru Selatan', 19),
(101, NULL, 'Buton', 30),
(102, NULL, 'Buton Utara', 30),
(103, NULL, 'Ciamis', 9),
(104, NULL, 'Cianjur', 9),
(105, NULL, 'Cilacap', 10),
(106, NULL, 'Cilegon', 3),
(107, NULL, 'Cimahi', 9),
(108, NULL, 'Cirebon', 9),
(110, NULL, 'Dairi', 34),
(111, NULL, 'Deiyai (Deliyai)', 24),
(112, NULL, 'Deli Serdang', 34),
(113, NULL, 'Demak', 10),
(114, NULL, 'Denpasar', 1),
(115, NULL, 'Depok', 9),
(116, NULL, 'Dharmasraya', 32),
(117, NULL, 'Dogiyai', 24),
(118, NULL, 'Dompu', 22),
(119, NULL, 'Donggala', 29),
(120, NULL, 'Dumai', 26),
(121, NULL, 'Empat Lawang', 33),
(122, NULL, 'Ende', 23),
(123, NULL, 'Enrekang', 28),
(124, NULL, 'Fakfak', 25),
(125, NULL, 'Flores Timur', 23),
(126, NULL, 'Garut', 9),
(127, NULL, 'Gayo Lues', 21),
(128, NULL, 'Gianyar', 1),
(129, NULL, 'Gorontalo', 7),
(131, NULL, 'Gorontalo Utara', 7),
(132, NULL, 'Gowa', 28),
(133, NULL, 'Gresik', 11),
(134, NULL, 'Grobogan', 10),
(135, NULL, 'Gunung Kidul', 5),
(136, NULL, 'Gunung Mas', 14),
(137, NULL, 'Gunungsitoli', 34),
(138, NULL, 'Halmahera Barat', 20),
(139, NULL, 'Halmahera Selatan', 20),
(140, NULL, 'Halmahera Tengah', 20),
(141, NULL, 'Halmahera Timur', 20),
(142, NULL, 'Halmahera Utara', 20),
(143, NULL, 'Hulu Sungai Selatan', 13),
(144, NULL, 'Hulu Sungai Tengah', 13),
(145, NULL, 'Hulu Sungai Utara', 13),
(146, NULL, 'Humbang Hasundutan', 34),
(147, NULL, 'Indragiri Hilir', 26),
(148, NULL, 'Indragiri Hulu', 26),
(149, NULL, 'Indramayu', 9),
(150, NULL, 'Intan Jaya', 24),
(151, NULL, 'Jakarta Barat', 6),
(152, NULL, 'Jakarta Pusat', 6),
(153, NULL, 'Jakarta Selatan', 6),
(154, NULL, 'Jakarta Timur', 6),
(155, NULL, 'Jakarta Utara', 6),
(156, NULL, 'Jambi', 8),
(157, NULL, 'Jayapura', 24),
(159, NULL, 'Jayawijaya', 24),
(160, NULL, 'Jember', 11),
(161, NULL, 'Jembrana', 1),
(162, NULL, 'Jeneponto', 28),
(163, NULL, 'Jepara', 10),
(164, NULL, 'Jombang', 11),
(165, NULL, 'Kaimana', 25),
(166, NULL, 'Kampar', 26),
(167, NULL, 'Kapuas', 14),
(168, NULL, 'Kapuas Hulu', 12),
(169, NULL, 'Karanganyar', 10),
(170, NULL, 'Karangasem', 1),
(171, NULL, 'Karawang', 9),
(172, NULL, 'Karimun', 17),
(173, NULL, 'Karo', 34),
(174, NULL, 'Katingan', 14),
(175, NULL, 'Kaur', 4),
(176, NULL, 'Kayong Utara', 12),
(177, NULL, 'Kebumen', 10),
(178, NULL, 'Kediri', 11),
(180, NULL, 'Keerom', 24),
(181, NULL, 'Kendal', 10),
(182, NULL, 'Kendari', 30),
(183, NULL, 'Kepahiang', 4),
(184, NULL, 'Kepulauan Anambas', 17),
(185, NULL, 'Kepulauan Aru', 19),
(186, NULL, 'Kepulauan Mentawai', 32),
(187, NULL, 'Kepulauan Meranti', 26),
(188, NULL, 'Kepulauan Sangihe', 31),
(189, NULL, 'Kepulauan Seribu', 6),
(190, NULL, 'Kepulauan Siau Tagulandang Biaro (Sitaro)', 31),
(191, NULL, 'Kepulauan Sula', 20),
(192, NULL, 'Kepulauan Talaud', 31),
(193, NULL, 'Kepulauan Yapen (Yapen Waropen)', 24),
(194, NULL, 'Kerinci', 8),
(195, NULL, 'Ketapang', 12),
(196, NULL, 'Klaten', 10),
(197, NULL, 'Klungkung', 1),
(198, NULL, 'Kolaka', 30),
(199, NULL, 'Kolaka Utara', 30),
(200, NULL, 'Konawe', 30),
(201, NULL, 'Konawe Selatan', 30),
(202, NULL, 'Konawe Utara', 30),
(203, NULL, 'Kotabaru', 13),
(204, NULL, 'Kotamobagu', 31),
(205, NULL, 'Kotawaringin Barat', 14),
(206, NULL, 'Kotawaringin Timur', 14),
(207, NULL, 'Kuantan Singingi', 26),
(208, NULL, 'Kubu Raya', 12),
(209, NULL, 'Kudus', 10),
(210, NULL, 'Kulon Progo', 5),
(211, NULL, 'Kuningan', 9),
(212, NULL, 'Kupang', 23),
(214, NULL, 'Kutai Barat', 15),
(215, NULL, 'Kutai Kartanegara', 15),
(216, NULL, 'Kutai Timur', 15),
(217, NULL, 'Labuhan Batu', 34),
(218, NULL, 'Labuhan Batu Selatan', 34),
(219, NULL, 'Labuhan Batu Utara', 34),
(220, NULL, 'Lahat', 33),
(221, NULL, 'Lamandau', 14),
(222, NULL, 'Lamongan', 11),
(223, NULL, 'Lampung Barat', 18),
(224, NULL, 'Lampung Selatan', 18),
(225, NULL, 'Lampung Tengah', 18),
(226, NULL, 'Lampung Timur', 18),
(227, NULL, 'Lampung Utara', 18),
(228, NULL, 'Landak', 12),
(229, NULL, 'Langkat', 34),
(230, NULL, 'Langsa', 21),
(231, NULL, 'Lanny Jaya', 24),
(232, NULL, 'Lebak', 3),
(233, NULL, 'Lebong', 4),
(234, NULL, 'Lembata', 23),
(235, NULL, 'Lhokseumawe', 21),
(236, NULL, 'Lima Puluh Koto/Kota', 32),
(237, NULL, 'Lingga', 17),
(238, NULL, 'Lombok Barat', 22),
(239, NULL, 'Lombok Tengah', 22),
(240, NULL, 'Lombok Timur', 22),
(241, NULL, 'Lombok Utara', 22),
(242, NULL, 'Lubuk Linggau', 33),
(243, NULL, 'Lumajang', 11),
(244, NULL, 'Luwu', 28),
(245, NULL, 'Luwu Timur', 28),
(246, NULL, 'Luwu Utara', 28),
(247, NULL, 'Madiun', 11),
(248, NULL, 'Madiun', 11),
(249, NULL, 'Magelang', 10),
(250, NULL, 'Magelang', 10),
(251, NULL, 'Magetan', 11),
(252, NULL, 'Majalengka', 9),
(253, NULL, 'Majene', 27),
(254, NULL, 'Makassar', 28),
(255, NULL, 'Malang', 11),
(257, NULL, 'Malinau', 16),
(258, NULL, 'Maluku Barat Daya', 19),
(259, NULL, 'Maluku Tengah', 19),
(260, NULL, 'Maluku Tenggara', 19),
(261, NULL, 'Maluku Tenggara Barat', 19),
(262, NULL, 'Mamasa', 27),
(263, NULL, 'Mamberamo Raya', 24),
(264, NULL, 'Mamberamo Tengah', 24),
(265, NULL, 'Mamuju', 27),
(266, NULL, 'Mamuju Utara', 27),
(267, NULL, 'Manado', 31),
(268, NULL, 'Mandailing Natal', 34),
(269, NULL, 'Manggarai', 23),
(270, NULL, 'Manggarai Barat', 23),
(271, NULL, 'Manggarai Timur', 23),
(272, NULL, 'Manokwari', 25),
(273, NULL, 'Manokwari Selatan', 25),
(274, NULL, 'Mappi', 24),
(275, NULL, 'Maros', 28),
(276, NULL, 'Mataram', 22),
(277, NULL, 'Maybrat', 25),
(278, NULL, 'Medan', 34),
(279, NULL, 'Melawi', 12),
(280, NULL, 'Merangin', 8),
(281, NULL, 'Merauke', 24),
(282, NULL, 'Mesuji', 18),
(283, NULL, 'Metro', 18),
(284, NULL, 'Mimika', 24),
(285, NULL, 'Minahasa', 31),
(286, NULL, 'Minahasa Selatan', 31),
(287, NULL, 'Minahasa Tenggara', 31),
(288, NULL, 'Minahasa Utara', 31),
(289, NULL, 'Mojokerto', 11),
(290, NULL, 'Mojokerto', 11),
(291, NULL, 'Morowali', 29),
(292, NULL, 'Muara Enim', 33),
(293, NULL, 'Muaro Jambi', 8),
(294, NULL, 'Muko Muko', 4),
(295, NULL, 'Muna', 30),
(296, NULL, 'Murung Raya', 14),
(297, NULL, 'Musi Banyuasin', 33),
(298, NULL, 'Musi Rawas', 33),
(299, NULL, 'Nabire', 24),
(300, NULL, 'Nagan Raya', 21),
(301, NULL, 'Nagekeo', 23),
(302, NULL, 'Natuna', 17),
(303, NULL, 'Nduga', 24),
(304, NULL, 'Ngada', 23),
(305, NULL, 'Nganjuk', 11),
(306, NULL, 'Ngawi', 11),
(307, NULL, 'Nias', 34),
(308, NULL, 'Nias Barat', 34),
(309, NULL, 'Nias Selatan', 34),
(310, NULL, 'Nias Utara', 34),
(311, NULL, 'Nunukan', 16),
(312, NULL, 'Ogan Ilir', 33),
(313, NULL, 'Ogan Komering Ilir', 33),
(314, NULL, 'Ogan Komering Ulu', 33),
(315, NULL, 'Ogan Komering Ulu Selatan', 33),
(316, NULL, 'Ogan Komering Ulu Timur', 33),
(317, NULL, 'Pacitan', 11),
(318, NULL, 'Padang', 32),
(319, NULL, 'Padang Lawas', 34),
(320, NULL, 'Padang Lawas Utara', 34),
(321, NULL, 'Padang Panjang', 32),
(322, NULL, 'Padang Pariaman', 32),
(323, NULL, 'Padang Sidempuan', 34),
(324, NULL, 'Pagar Alam', 33),
(325, NULL, 'Pakpak Bharat', 34),
(326, NULL, 'Palangka Raya', 14),
(327, NULL, 'Palembang', 33),
(328, NULL, 'Palopo', 28),
(329, NULL, 'Palu', 29),
(330, NULL, 'Pamekasan', 11),
(331, NULL, 'Pandeglang', 3),
(332, NULL, 'Pangandaran', 9),
(333, NULL, 'Pangkajene Kepulauan', 28),
(334, NULL, 'Pangkal Pinang', 2),
(335, NULL, 'Paniai', 24),
(336, NULL, 'Parepare', 28),
(337, NULL, 'Pariaman', 32),
(338, NULL, 'Parigi Moutong', 29),
(339, NULL, 'Pasaman', 32),
(340, NULL, 'Pasaman Barat', 32),
(341, NULL, 'Paser', 15),
(342, NULL, 'Pasuruan', 11),
(344, NULL, 'Pati', 10),
(345, NULL, 'Payakumbuh', 32),
(346, NULL, 'Pegunungan Arfak', 25),
(347, NULL, 'Pegunungan Bintang', 24),
(348, NULL, 'Pekalongan', 10),
(349, NULL, 'Pekalongan', 10),
(350, NULL, 'Pekanbaru', 26),
(351, NULL, 'Pelalawan', 26),
(352, NULL, 'Pemalang', 10),
(353, NULL, 'Pematang Siantar', 34),
(354, NULL, 'Penajam Paser Utara', 15),
(355, NULL, 'Pesawaran', 18),
(356, NULL, 'Pesisir Barat', 18),
(357, NULL, 'Pesisir Selatan', 32),
(358, NULL, 'Pidie', 21),
(359, NULL, 'Pidie Jaya', 21),
(360, NULL, 'Pinrang', 28),
(361, NULL, 'Pohuwato', 7),
(362, NULL, 'Polewali Mandar', 27),
(363, NULL, 'Ponorogo', 11),
(364, NULL, 'Pontianak', 12),
(366, NULL, 'Poso', 29),
(367, NULL, 'Prabumulih', 33),
(368, NULL, 'Pringsewu', 18),
(369, NULL, 'Probolinggo', 11),
(371, NULL, 'Pulang Pisau', 14),
(372, NULL, 'Pulau Morotai', 20),
(373, NULL, 'Puncak', 24),
(374, NULL, 'Puncak Jaya', 24),
(375, NULL, 'Purbalingga', 10),
(376, NULL, 'Purwakarta', 9),
(377, NULL, 'Purworejo', 10),
(378, NULL, 'Raja Ampat', 25),
(379, NULL, 'Rejang Lebong', 4),
(380, NULL, 'Rembang', 10),
(381, NULL, 'Rokan Hilir', 26),
(382, NULL, 'Rokan Hulu', 26),
(383, NULL, 'Rote Ndao', 23),
(384, NULL, 'Sabang', 21),
(385, NULL, 'Sabu Raijua', 23),
(386, NULL, 'Salatiga', 10),
(387, NULL, 'Samarinda', 15),
(388, NULL, 'Sambas', 12),
(389, NULL, 'Samosir', 34),
(390, NULL, 'Sampang', 11),
(391, NULL, 'Sanggau', 12),
(392, NULL, 'Sarmi', 24),
(393, NULL, 'Sarolangun', 8),
(394, NULL, 'Sawah Lunto', 32),
(395, NULL, 'Sekadau', 12),
(396, NULL, 'Selayar (Kepulauan Selayar)', 28),
(397, NULL, 'Seluma', 4),
(398, NULL, 'Semarang', 10),
(399, NULL, 'Semarang', 10),
(400, NULL, 'Seram Bagian Barat', 19),
(401, NULL, 'Seram Bagian Timur', 19),
(402, NULL, 'Serang', 3),
(403, NULL, 'Serang', 3),
(404, NULL, 'Serdang Bedagai', 34),
(405, NULL, 'Seruyan', 14),
(406, NULL, 'Siak', 26),
(407, NULL, 'Sibolga', 34),
(408, NULL, 'Sidenreng Rappang/Rapang', 28),
(409, NULL, 'Sidoarjo', 11),
(410, NULL, 'Sigi', 29),
(411, NULL, 'Sijunjung (Sawah Lunto Sijunjung)', 32),
(412, NULL, 'Sikka', 23),
(413, NULL, 'Simalungun', 34),
(414, NULL, 'Simeulue', 21),
(415, NULL, 'Singkawang', 12),
(416, NULL, 'Sinjai', 28),
(417, NULL, 'Sintang', 12),
(418, NULL, 'Situbondo', 11),
(419, NULL, 'Sleman', 5),
(420, NULL, 'Solok', 32),
(422, NULL, 'Solok Selatan', 32),
(423, NULL, 'Soppeng', 28),
(424, NULL, 'Sorong', 25),
(426, NULL, 'Sorong Selatan', 25),
(427, NULL, 'Sragen', 10),
(428, NULL, 'Subang', 9),
(429, NULL, 'Subulussalam', 21),
(430, NULL, 'Sukabumi', 9),
(432, NULL, 'Sukamara', 14),
(433, NULL, 'Sukoharjo', 10),
(434, NULL, 'Sumba Barat', 23),
(435, NULL, 'Sumba Barat Daya', 23),
(436, NULL, 'Sumba Tengah', 23),
(437, NULL, 'Sumba Timur', 23),
(438, NULL, 'Sumbawa', 22),
(439, NULL, 'Sumbawa Barat', 22),
(440, NULL, 'Sumedang', 9),
(441, NULL, 'Sumenep', 11),
(442, NULL, 'Sungaipenuh', 8),
(443, NULL, 'Supiori', 24),
(444, NULL, 'Surabaya', 11),
(445, NULL, 'Surakarta (Solo)', 10),
(446, NULL, 'Tabalong', 13),
(447, NULL, 'Tabanan', 1),
(448, NULL, 'Takalar', 28),
(449, NULL, 'Tambrauw', 25),
(450, NULL, 'Tana Tidung', 16),
(451, NULL, 'Tana Toraja', 28),
(452, NULL, 'Tanah Bumbu', 13),
(453, NULL, 'Tanah Datar', 32),
(454, NULL, 'Tanah Laut', 13),
(455, NULL, 'Tangerang', 3),
(456, NULL, 'Tangerang', 3),
(457, NULL, 'Tangerang Selatan', 3),
(458, NULL, 'Tanggamus', 18),
(459, NULL, 'Tanjung Balai', 34),
(460, NULL, 'Tanjung Jabung Barat', 8),
(461, NULL, 'Tanjung Jabung Timur', 8),
(462, NULL, 'Tanjung Pinang', 17),
(463, NULL, 'Tapanuli Selatan', 34),
(464, NULL, 'Tapanuli Tengah', 34),
(465, NULL, 'Tapanuli Utara', 34),
(466, NULL, 'Tapin', 13),
(467, NULL, 'Tarakan', 16),
(468, NULL, 'Tasikmalaya', 9),
(470, NULL, 'Tebing Tinggi', 34),
(471, NULL, 'Tebo', 8),
(472, NULL, 'Tegal', 10),
(473, NULL, 'Tegal', 10),
(474, NULL, 'Teluk Bintuni', 25),
(475, NULL, 'Teluk Wondama', 25),
(476, NULL, 'Temanggung', 10),
(477, NULL, 'Ternate', 20),
(478, NULL, 'Tidore Kepulauan', 20),
(479, NULL, 'Timor Tengah Selatan', 23),
(480, NULL, 'Timor Tengah Utara', 23),
(481, NULL, 'Toba Samosir', 34),
(482, NULL, 'Tojo Una-Una', 29),
(483, NULL, 'Toli-Toli', 29),
(484, NULL, 'Tolikara', 24),
(485, NULL, 'Tomohon', 31),
(486, NULL, 'Toraja Utara', 28),
(487, NULL, 'Trenggalek', 11),
(488, NULL, 'Tual', 19),
(489, NULL, 'Tuban', 11),
(490, NULL, 'Tulang Bawang', 18),
(491, NULL, 'Tulang Bawang Barat', 18),
(492, NULL, 'Tulungagung', 11),
(493, NULL, 'Wajo', 28),
(494, NULL, 'Wakatobi', 30),
(495, NULL, 'Waropen', 24),
(496, NULL, 'Way Kanan', 18),
(497, NULL, 'Wonogiri', 10),
(498, NULL, 'Wonosobo', 10),
(499, NULL, 'Yahukimo', 24),
(500, NULL, 'Yalimo', 24),
(501, NULL, 'Yogyakarta', 5);

-- --------------------------------------------------------

--
-- Table structure for table `ms_clubs`
--

CREATE TABLE `ms_clubs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ms_type_club_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reward` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ms_contacts`
--

CREATE TABLE `ms_contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `ms_user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_contacts`
--

INSERT INTO `ms_contacts` (`id`, `ms_user_id`, `name`, `email`, `telephone`, `mobile`, `fax`, `website`, `facebook`, `twitter`, `instagram`, `address`, `city`, `province`, `country`, `description`, `file`, `featured`, `active`, `created_at`, `updated_at`) VALUES
(2, 1, 'KESS', 'info@kessby.com', '0123456789', '9119', '', 'korlantas.polri.go.id', 'https://www.facebook.com/', 'https://twitter.com/ntmclantaspolri', '', 'Jl. Kapas Madya 1 F no. 99 Surabaya', '', '', '', '', '', '1', '', '2015-04-17 14:20:50', '2017-03-06 01:43:27');

-- --------------------------------------------------------

--
-- Table structure for table `ms_country`
--

CREATE TABLE `ms_country` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_country`
--

INSERT INTO `ms_country` (`id`, `country_id`, `name`) VALUES
(1, 1, 'Acores Et Madere'),
(2, 2, 'Afghanistan'),
(3, 3, 'Afrika Barat  Rep'),
(4, 4, 'Albania'),
(5, 5, 'Algeria'),
(6, 6, 'Angola'),
(7, 7, 'Antigua and Barbuda'),
(8, 8, 'Argentina'),
(9, 9, 'Australia'),
(10, 10, 'Austria'),
(11, 11, 'Azerbaijan'),
(12, 12, 'Bahamas'),
(13, 13, 'Bahrain'),
(14, 14, 'Bangladesh'),
(15, 15, 'Barbados'),
(16, 16, 'Belgium'),
(17, 17, 'Belize'),
(18, 18, 'Benin'),
(19, 19, 'Bermuda'),
(20, 20, 'Bhutan'),
(21, 21, 'Bolivia'),
(22, 22, 'Botswana'),
(23, 23, 'Brazil'),
(24, 24, 'Brunei Darussalam'),
(25, 25, 'Bulgaria (rep)'),
(26, 26, 'Burkina Faso'),
(27, 27, 'Burundi'),
(28, 28, 'Cambodia'),
(29, 29, 'Cameroon'),
(30, 30, 'Canada'),
(31, 31, 'Cayman Islands'),
(32, 32, 'Ceko'),
(33, 33, 'Central African Rep'),
(34, 34, 'Chad'),
(35, 35, 'Chile'),
(36, 36, 'China (people_s rep)'),
(37, 37, 'Colombia'),
(38, 38, 'Comoros'),
(39, 39, 'Congo (rep)'),
(40, 40, 'Cook islands'),
(41, 41, 'Costa rica'),
(42, 42, 'Cote D_Ivoire (rep)- Pantai Gading'),
(43, 43, 'Cuba'),
(44, 44, 'Cyprus'),
(45, 45, 'Czech Rep'),
(46, 46, 'Dem People_s Rep of Korea'),
(47, 47, 'Denmark'),
(48, 48, 'Djibouti'),
(49, 49, 'Dominica'),
(50, 50, 'Dominican Republic'),
(51, 51, 'Ecuador'),
(52, 52, 'Egypt'),
(53, 53, 'El Salvador'),
(54, 54, 'Equatorial Guinea'),
(55, 55, 'Eritrea'),
(56, 56, 'Ethiopia'),
(57, 57, 'Faroe Islands'),
(58, 58, 'Fiji'),
(59, 59, 'Finland'),
(60, 60, 'Former Yugoslavian  Rep. of Macedonia'),
(61, 61, 'France'),
(62, 62, 'French Guiana'),
(63, 63, 'French Polynesia'),
(64, 64, 'Gabon'),
(65, 65, 'Gambia'),
(66, 66, 'Germany'),
(67, 67, 'Ghana'),
(68, 68, 'Gibraltar'),
(69, 69, 'Great Britain (Inggris)'),
(70, 70, 'Greece'),
(71, 71, 'Greenland'),
(72, 72, 'Grenada'),
(73, 73, 'Guadeloupe'),
(74, 74, 'Guam'),
(75, 75, 'Guatemala'),
(76, 76, 'Guinea'),
(77, 77, 'Guinea-Bissau'),
(78, 78, 'Guyana'),
(79, 79, 'Haiti'),
(80, 80, 'Honduras (rep)'),
(81, 81, 'Hongkong'),
(82, 82, 'Hungary (rep)'),
(83, 83, 'Iceland'),
(84, 84, 'India'),
(85, 85, 'Iran (Islamic rep)'),
(86, 86, 'Iraq'),
(87, 87, 'Ireland'),
(88, 88, 'Israel'),
(89, 89, 'Italy'),
(90, 90, 'Jamaica'),
(91, 91, 'Japan'),
(92, 92, 'Jordan'),
(93, 93, 'Kazakhstan'),
(94, 94, 'Kenya'),
(95, 95, 'Kiribati'),
(96, 96, 'Korea (rep)'),
(97, 97, 'Kuwait'),
(98, 98, 'Lao People_s Dem Rep'),
(99, 99, 'Lebanon'),
(100, 100, 'Lesotho'),
(101, 101, 'Liberia'),
(102, 102, 'Libyan Jamahiriya'),
(103, 103, 'Luxembourg'),
(104, 104, 'Macao'),
(105, 105, 'Madagascar'),
(106, 106, 'Madeira'),
(107, 107, 'Malawi'),
(108, 108, 'Malaysia'),
(109, 109, 'Maldives'),
(110, 110, 'Mali'),
(111, 111, 'Malta'),
(112, 112, 'Mariana Islands'),
(113, 113, 'Martinique'),
(114, 114, 'Mauritania'),
(115, 115, 'Mauritius'),
(116, 116, 'Mexico'),
(117, 117, 'Mongolia'),
(118, 118, 'Montserrat'),
(119, 119, 'Morocco'),
(120, 120, 'Mozambique'),
(121, 121, 'Myanmar'),
(122, 122, 'Namibia'),
(123, 123, 'Nauru'),
(124, 124, 'Nepal'),
(125, 125, 'Netherlands'),
(126, 126, 'Netherlands Antilles'),
(127, 127, 'New Caledonia'),
(128, 128, 'New Zealand'),
(129, 129, 'Nicaragua'),
(130, 130, 'Niger'),
(131, 131, 'Nigeria'),
(132, 132, 'Norway'),
(133, 133, 'Oman'),
(134, 134, 'Pakistan'),
(135, 135, 'Panama'),
(136, 136, 'Papua New Guinea'),
(137, 137, 'Paraguay'),
(138, 138, 'Peru'),
(139, 139, 'Philippines'),
(140, 140, 'Poland (rep)'),
(141, 141, 'Portugal'),
(142, 142, 'Qatar'),
(143, 143, 'Romania'),
(144, 144, 'Russian Federation'),
(145, 145, 'Rwanda'),
(146, 146, 'Samoa Barat'),
(147, 147, 'Saudi Arabia'),
(148, 148, 'Scattered Islands, Reunion'),
(149, 149, 'Senegal'),
(150, 150, 'Seychelles'),
(151, 151, 'Sierra Leone'),
(152, 152, 'Singapore'),
(153, 153, 'Slovak Rep'),
(154, 154, 'Solomon Islands'),
(155, 155, 'South Africa'),
(156, 156, 'Spain'),
(157, 157, 'Sri Lanka'),
(158, 158, 'St Helena'),
(159, 159, 'Sudan'),
(160, 160, 'Suriname'),
(161, 161, 'Swaziland'),
(162, 162, 'Sweden'),
(163, 163, 'Switzerland'),
(164, 164, 'Syrian Arab Rep.'),
(165, 165, 'Taiwan'),
(166, 166, 'Tanzania (United Rep)'),
(167, 167, 'Thailand'),
(168, 168, 'Timor-Leste'),
(169, 169, 'Togo'),
(170, 170, 'Tonga'),
(171, 171, 'Tortola'),
(172, 172, 'Trinidad and Tobago'),
(173, 173, 'Tunisia'),
(174, 174, 'Turkey'),
(175, 175, 'Turks Cay Islands'),
(176, 176, 'Tuvalu'),
(177, 177, 'Uganda'),
(178, 178, 'United Arab Emirates'),
(179, 179, 'United States of America'),
(180, 180, 'Uruguay'),
(181, 181, 'Vanuatu'),
(182, 182, 'Vatican'),
(183, 183, 'Venezuela'),
(184, 184, 'Viet nam'),
(185, 185, 'Western Samoa'),
(186, 186, 'Yemen'),
(187, 187, 'Zambia'),
(188, 188, 'Zimbabwe'),
(189, 189, 'Andora'),
(190, 190, 'Armenia'),
(191, 191, 'Aruba'),
(192, 192, 'Belarus'),
(193, 193, 'Bonaire'),
(194, 194, 'Bosnia & Herzegovina'),
(195, 195, 'Canary Islands, The'),
(196, 196, 'Cape Verde'),
(197, 197, 'Channel Islands'),
(198, 198, 'Croatia'),
(199, 199, 'Curacao'),
(200, 200, 'Estonia'),
(201, 201, 'Etrea'),
(202, 202, 'Falkand Islands'),
(203, 203, 'Georgia'),
(204, 204, 'Guerensey'),
(205, 205, 'Jersey'),
(206, 206, 'Kyrgysztan'),
(207, 207, 'Latvia'),
(208, 208, 'Liechtenstein'),
(209, 209, 'Lithuania'),
(210, 210, 'Moldova, Rep. of'),
(211, 211, 'Monaco'),
(212, 212, 'Montenegro Rep The'),
(213, 213, 'Nevis'),
(214, 214, 'Niue'),
(215, 215, 'Puerto Rico'),
(216, 216, 'Reunion, Island of'),
(217, 217, 'Saipan'),
(218, 218, 'Serbia Rep The'),
(219, 219, 'Slovenia'),
(220, 220, 'Somalia'),
(221, 221, 'St. Barthelemy'),
(222, 222, 'St. Eustatius'),
(223, 223, 'St. Kitts'),
(224, 224, 'St. Lucia'),
(225, 225, 'St. Maarten'),
(226, 226, 'St. Vincent'),
(227, 227, 'Tahiti'),
(228, 228, 'Tajikistan'),
(229, 229, 'Turkmenistan'),
(230, 230, 'Ukraine'),
(231, 231, 'Uzbekistan'),
(232, 232, 'Virgin Islands (British)'),
(233, 233, 'Virgin Islands (USA)'),
(234, 234, 'Yugoslavia'),
(235, 235, 'Zaire');

-- --------------------------------------------------------

--
-- Table structure for table `ms_download`
--

CREATE TABLE `ms_download` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_download`
--

INSERT INTO `ms_download` (`id`, `name`, `link`) VALUES
(1, 'IOS', 'itunes.apple.com/us/app/kangen-gemarsehati/id1135881438?mt=8'),
(2, 'Android', 'play.google.com/store/apps/details?id=kangen.gemarsehati.enagic');

-- --------------------------------------------------------

--
-- Table structure for table `ms_event`
--

CREATE TABLE `ms_event` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(600) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_event`
--

INSERT INTO `ms_event` (`id`, `name`, `date`, `description`) VALUES
(14, 'Wellness Seminar SAMARINDA', '2016-02-20', 'Enagic mengadakan WELLNESS SEMINAR di SAMARINDA\r\n\r\nTanggal : 20 Februari 2016\r\nVenue     : Swiss Bell Hotel Borneo Samarinda, Jl. Mulawarman No. 6 Samarinda\r\nTelp.       : 0521-200888\r\nWaktu    : 14.00 PM - 17.00 PM\r\nPIC          : Candra Sosiawan'),
(15, 'BOOTCAMP-4 GEMARSEHATI SURABAYA ', '2016-08-06', 'Ikuti Gemarsehati Bootcamp sessi-4 di Surabaya, Tanggal      :  6-7 Agustus 2016'),
(17, 'KanGen Water Seminar di Blitar', '2016-03-12', 'KanGen Water Seminar di Blitar\r\nSabtu, 12 Maret 2016 \r\nStart pk 13.00 WIB - selesai \r\nTempat: LEC ( Local Education Centre) \r\nJl.Raya Sawahan Pojok Garum Blitar\r\nRuang anggrek (lewat depan gedung KPPN Blitar)\r\n\r\nPresenter : \r\nYussa Marulli ST (6A3) \r\n(Pengusaha - Surabaya ) \r\nPangeran Chandra, SE  (4A)\r\n(Pengusaha - Surabaya )\r\nInvestasi Rp 20.000 !!! \r\nHub Hp/ WA : 0857.06101792 / 0896.04817038 '),
(18, 'KanGen Water Seminar di Padang panjang', '2016-04-23', ' Gemar Sehati mempersembahkan: \r\nKanGen Water Seminar di Padang panjang\r\nSabtu, 23 april 2016  \r\nsesi 1 pk 09.00 WIB - selesai | sesi 2 pk 13.00-selesai\r\nTempat: Aula A Hotel Pangeran - Jl. KH.A.DAHLAN No 7 Padang panjang\r\nInvestasi 1org: Rp 20.000 \r\n2org: Rp 30.000!!!\r\nHub Hp/ WA : 081363930799\r\n\r\nAyo  Ajak keluarga, teman, dan orang-orang yang Anda pedulikan untuk hadir dan utk melihat kehebatan demo air ajaib KanGen water \r\nSalam Kangen.'),
(19, 'Seminar 501 System Indonesia', '2016-05-14', 'Sebuah seminar yang mengulas bisnis mudah dari rumah, dan bonusnya sehat badan - sehat pikiran - sehat keuangan\r\nsabtu, 14 Mei 2016 - Pukul 13.00 WIB\r\nKYNU EAT and CHAT Cafe\r\nJl. Bratang Binangun I/23 - Surabaya\r\nEducator :\r\nIbu Dini & Ibu Sofia\r\nGuest Speaker :\r\nIr. Yussa Marulli (6A3 Enagic Indonesia)\r\nInfo :\r\n0853.1234.5959\r\nHTM :\r\n25K'),
(20, 'Ikutilah TALKSHOW RADIO ttg REVOLUSI KESEHATAN', '2016-09-14', ' oleh RS Rumah Sakit Wiyung Sejahtera dan Gemarsehati pada Hari Rabu Besok Malam 14 Sep 2016 Jam 19.00-21.00 di SINDO TRIJAYA FM Surabaya Frek 104.7 Mhz. \r\nAnda juga bisa mengikuti secara live di www.sindotrijayasurabaya.com dan telp interaktif di 031-596.1047'),
(21, 'Gemarsehati di Industri Bahari Expo 2016 Grand City Surabaya', '2016-09-22', 'Gemarsehati di Industri Bahari Expo 2016 Grand City Surabaya\r\nTgl : 22 - 25 September 2016'),
(22, 'Razia Kendaraan Bermotor', '2016-10-27', 'Razia Kendaraan Bermotor ');

-- --------------------------------------------------------

--
-- Table structure for table `ms_files`
--

CREATE TABLE `ms_files` (
  `id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `mime` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isumum` tinyint(4) NOT NULL,
  `ms_media_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_files`
--

INSERT INTO `ms_files` (`id`, `file`, `mime`, `title`, `description`, `created_at`, `updated_at`, `isumum`, `ms_media_id`) VALUES
(1, '5112100092-Imaduddin-Al-Fikri-POMITS_20160126_074537.pdf', '', '', '', '2016-01-28 14:36:21', '0000-00-00 00:00:00', 0, 474),
(2, 'Alur_PublikasiMakalah_20160126_070038.pdf', '', '', '', '2016-01-28 14:36:21', '0000-00-00 00:00:00', 127, 0),
(3, 'logo-gemarsehati_20160201_112533.png', 'png', 'Logo Gemarsehati', 'Logo Gemarsehati format PNG', '2016-02-01 11:33:25', '2016-02-01 04:33:25', 1, 500),
(17, 'APLIKASI_20160201_130748.pdf', 'pdf', 'Form Aplikasi Terbaru', 'Form Aplikasi Terbaru. format PDF', '2016-02-02 07:38:25', '2016-02-02 00:38:25', 0, 514),
(5, 'Logo Kangen_20160201_112036.pdf', 'pdf', 'Logo kangenwater', 'Logo kangenwater', '2016-02-02 07:38:06', '2016-02-02 00:38:06', 0, 514),
(9, 'logo-gemarsehati_20160201_110435.pdf', 'pdf', 'Logo Gemarsehati', 'Logo Gemarsehati format PNG', '2016-02-02 07:38:36', '2016-02-02 00:38:35', 0, 514),
(18, 'SURAT PERNYATAAN PENJAMIN 6A KOSONGAN_20160201_133253.pdf', 'pdf', 'Surat Pernyataan Penjamin', 'Surat Pernyataan Pengikatan Diri Sebagai Penjamin\r\n', '2016-02-02 07:38:17', '2016-02-02 00:38:17', 0, 514),
(19, '53_Permenkes 492_20160309_055203.pdf', 'pdf', 'Peraturan Menkes tentang baku mutu air minum ', 'Peraturan Menkes tentang baku mutu air minum ', '2016-03-09 05:03:56', '2016-03-08 22:03:56', 1, 514);

-- --------------------------------------------------------

--
-- Table structure for table `ms_gender`
--

CREATE TABLE `ms_gender` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_gender`
--

INSERT INTO `ms_gender` (`id`, `name`) VALUES
(1, 'Laki-laki'),
(2, 'Perempuan');

-- --------------------------------------------------------

--
-- Table structure for table `ms_general`
--

CREATE TABLE `ms_general` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tagline` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_credit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `about_community` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_general`
--

INSERT INTO `ms_general` (`id`, `site_title`, `tagline`, `language`, `file`, `file_credit`, `meta_description`, `meta_keywords`, `active`, `about_community`, `created_at`, `updated_at`) VALUES
(1, 'Komunitas Ekonomi Syariah Surabaya', 'KESS', 'id', 'img_logo.jpg', 'credit-logo.png', 'Sebuah komunitas ekonomi syariah untuk kemajuan tatanan dan tuntunan  demi membangun Negara Kesatuan Republik Indonesia', 'kess, syariah, ekonomi, 212, kes 212, ekonomi syariah, surabaya ekonomi syariah', '1', 'Sebuah komunitas ekonomi syariah untuk kemajuan tatanan dan tuntunan  demi membangun Negara Kesatuan Republik Indonesia', '2015-04-14 07:38:20', '2017-03-06 01:46:23');

-- --------------------------------------------------------

--
-- Table structure for table `ms_gerai`
--

CREATE TABLE `ms_gerai` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `address` text NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_gerai`
--

INSERT INTO `ms_gerai` (`id`, `title`, `description`, `address`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 'Korlantas Polri', 'Korlantas Polri', 'Jl. M.T. Haryono Kav. 37-38, Jakarta', '-7.279334', '112.7954033', '2016-10-29 09:49:35', '2016-10-29 09:58:09');

-- --------------------------------------------------------

--
-- Table structure for table `ms_kategori_aduan`
--

CREATE TABLE `ms_kategori_aduan` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_kategori_aduan`
--

INSERT INTO `ms_kategori_aduan` (`id`, `title`) VALUES
(1, 'Pengaduan'),
(2, 'Pertanyaan'),
(3, 'Penghargaan');

-- --------------------------------------------------------

--
-- Table structure for table `ms_leaders`
--

CREATE TABLE `ms_leaders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `rank` int(11) DEFAULT NULL,
  `rank_name` varchar(255) NOT NULL,
  `video_profile` varchar(1000) NOT NULL,
  `ms_media_id` int(11) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_leaders`
--

INSERT INTO `ms_leaders` (`id`, `name`, `rank`, `rank_name`, `video_profile`, `ms_media_id`, `facebook`, `twitter`, `skype`) VALUES
(2, 'HR.Ahmad', 2, 'Barangsiapa ingin doanya terkabul dan kesulitan -kesulitannya  teratasihendaklah dia menolong orang yang dalam kesempitan', '#', 648, '#', '#', '#'),
(3, 'HR.Ath-Thabrani ', 3, 'Dari Aisyah ra. ia berkata :Rasulullah saw bersabda: \" Barangsiapa yang mengisi shaff didalam shalat  yang kosong,niscaya ALLAH akan mengangkat satu derajatnya & membangunkan sebuah rumah di surga untuknya\"', '#', 650, '#', '#', '#'),
(4, 'HR.Muslim', 7, 'Islam datang dengan asing & kelak  akan kembali asing  sebagaimana  kedatangannya,maka beruntunglah bagi orang-orang asing. ', '#', 650, '#', '#', '#');

-- --------------------------------------------------------

--
-- Table structure for table `ms_links`
--

CREATE TABLE `ms_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ms_medias`
--

CREATE TABLE `ms_medias` (
  `id` int(10) UNSIGNED NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alternate_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isumum` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_medias`
--

INSERT INTO `ms_medias` (`id`, `file`, `mime`, `caption`, `alternate_text`, `description`, `created_at`, `updated_at`, `isumum`) VALUES
(438, 'img_20160119_065921.jpg', 'image/jpeg', '', '', '', '2016-01-18 23:21:59', '2016-01-18 23:21:59', 0),
(439, 'img_20160119_063822.jpg', 'image/jpeg', '', '', '', '2016-01-18 23:22:38', '2016-01-18 23:22:38', 0),
(440, 'img_20160119_063023.jpg', 'image/jpeg', '', '', '', '2016-01-18 23:23:30', '2016-01-18 23:23:30', 0),
(441, 'img_20160119_064423.jpg', 'image/jpeg', '', '', '', '2016-01-18 23:23:44', '2016-01-18 23:23:44', 0),
(442, 'img_20160119_072005.jpg', 'image/jpeg', '', '', '', '2016-01-19 00:05:20', '2016-01-19 00:05:20', 0),
(443, 'img_20160119_074205.jpg', 'image/jpeg', '', '', '', '2016-01-19 00:05:42', '2016-01-19 00:05:42', 0),
(444, 'img_20160119_094208.jpg', 'image/jpeg', '', '', '', '2016-01-19 02:08:42', '2016-01-19 02:08:42', 0),
(445, 'img_20160121_044625.png', 'image/png', '', '', '', '2016-01-20 21:25:47', '2016-01-20 21:25:47', 0),
(462, 'img_20160125_120902.jpg', 'image/jpeg', '', '', '', '2016-01-25 05:02:09', '2016-01-25 05:02:09', 0),
(463, 'img_20160125_123607.jpg', 'image/jpeg', '', '', '', '2016-01-25 05:07:36', '2016-01-25 05:07:36', 0),
(464, 'img_20160125_125315.jpg', 'image/jpeg', '', '', '', '2016-01-25 05:15:53', '2016-01-25 05:15:53', 0),
(465, 'img_20160125_121216.jpg', 'image/jpeg', '', '', '', '2016-01-25 05:16:12', '2016-01-25 05:16:12', 0),
(466, 'img_20160125_232920.jpg', 'image/jpeg', '', '', '', '2016-01-25 16:20:29', '2016-01-25 16:20:29', 0),
(467, 'img_20160126_025213.png', 'image/png', '', '', '', '2016-01-25 19:13:52', '2016-01-25 19:13:52', 0),
(468, 'img_20160126_022518.jpg', 'image/jpeg', '', '', '', '2016-01-25 19:18:25', '2016-01-25 19:18:25', 0),
(469, 'img_20160126_022921.jpg', 'image/jpeg', '', '', '', '2016-01-25 19:21:29', '2016-01-25 19:21:29', 0),
(470, 'img_20160126_021024.jpg', 'image/jpeg', '', '', '', '2016-01-25 19:24:10', '2016-01-25 19:24:10', 0),
(471, 'img_20160126_021526.jpg', 'image/jpeg', '', '', '', '2016-01-25 19:26:15', '2016-01-25 19:26:15', 0),
(472, 'img_20160126_020233.jpg', 'image/jpeg', '', '', '', '2016-01-25 19:33:02', '2016-01-25 19:33:02', 0),
(473, 'img_20160126_035412.jpg', 'image/jpeg', '', '', '', '2016-01-25 20:12:54', '2016-01-25 20:12:54', 0),
(481, 'img_20160127_122804.jpg', 'image/jpeg', 'Foto MemberTes', '', 'Foto MemberTes', '2016-01-27 05:04:28', '2016-01-27 05:04:28', 0),
(483, 'img_20160128_070805.png', 'image/png', '', '', '', '2016-01-28 00:05:08', '2016-01-28 00:05:08', 0),
(484, 'img_20160128_113218.jpg', 'image/jpeg', '', '', '', '2016-01-28 04:18:32', '2016-01-28 04:18:32', 0),
(488, 'img_20160128_120737.jpg', 'image/jpeg', '', '', '', '2016-01-28 05:37:07', '2016-01-28 05:37:07', 0),
(489, 'img_20160128_140138.png', 'image/png', '', '', '', '2016-01-28 07:38:02', '2016-01-28 07:38:02', 0),
(490, 'img_20160218_161612.jpg', 'image/jpeg', '', '', '', '2016-01-29 03:23:06', '2016-02-18 09:12:18', 0),
(491, 'img_20160129_104323.jpg', 'image/jpeg', '', '', '', '2016-01-29 03:23:43', '2016-01-29 03:23:43', 0),
(492, 'img_20160129_101024.jpg', 'image/jpeg', '', '', '', '2016-01-29 03:24:10', '2016-01-29 03:24:10', 0),
(493, 'img_20160129_101837.jpg', 'image/jpeg', '', '', '', '2016-01-29 03:37:18', '2016-01-29 03:37:18', 0),
(494, 'img_20160129_172525.png', 'image/png', 'Foto Imaduddin Al Fikri', '', 'Foto Imaduddin Al Fikri', '2016-01-29 10:25:26', '2016-01-29 10:25:26', 0),
(495, 'img_20160130_141917.jpg', 'image/jpeg', 'Foto ', '', 'Foto ', '2016-01-30 07:17:19', '2016-01-30 07:17:19', 0),
(497, 'img_20160131_122303.JPG', 'image/jpeg', 'Foto namates', '', 'Foto namates', '2016-01-31 05:03:23', '2016-01-31 05:03:23', 0),
(499, 'img_20160131_120407.JPG', 'image/jpeg', 'Foto namates', '', 'Foto namates', '2016-01-31 05:07:05', '2016-01-31 05:07:05', 0),
(500, 'img_20160201_110532.jpg', 'image/jpeg', '', '', '', '2016-02-01 04:32:05', '2016-02-01 04:32:05', 0),
(501, 'img_20160201_182825.jpg', 'image/jpeg', 'Foto a', '', 'Foto a', '2016-02-01 11:25:29', '2016-02-01 11:25:29', 0),
(502, 'img_20160201_194202.jpg', 'image/jpeg', 'Foto a', '', 'Foto a', '2016-02-01 12:02:42', '2016-02-01 12:02:42', 0),
(503, 'img_20160201_195102.jpg', 'image/jpeg', 'Foto a', '', 'Foto a', '2016-02-01 12:02:51', '2016-02-01 12:02:51', 0),
(504, 'img_20160202_061140.jpg', 'image/jpeg', '', '', '', '2016-02-01 23:40:11', '2016-02-01 23:40:11', 0),
(505, 'img_20160202_064340.jpg', 'image/jpeg', '', '', '', '2016-02-01 23:40:44', '2016-02-01 23:40:44', 0),
(506, 'img_20160202_060441.jpg', 'image/jpeg', '', '', '', '2016-02-01 23:41:04', '2016-02-01 23:41:04', 0),
(507, 'img_20160202_061941.jpg', 'image/jpeg', '', '', '', '2016-02-01 23:41:19', '2016-02-01 23:41:19', 0),
(508, 'img_20160202_063741.jpg', 'image/jpeg', '', '', '', '2016-02-01 23:41:37', '2016-02-01 23:41:37', 0),
(509, 'img_20160202_060042.jpg', 'image/jpeg', '', '', '', '2016-02-01 23:42:00', '2016-02-01 23:42:00', 0),
(510, 'img_20160202_062642.jpg', 'image/jpeg', '', '', '', '2016-02-01 23:42:26', '2016-02-01 23:42:26', 0),
(511, 'img_20160202_071721.jpg', 'image/jpeg', '', '', '', '2016-02-02 00:21:17', '2016-02-02 00:21:17', 0),
(512, 'img_20160202_075522.jpg', 'image/jpeg', '', '', '', '2016-02-02 00:22:55', '2016-02-02 00:22:55', 0),
(513, 'img_20160202_073125.jpg', 'image/jpeg', '', '', '', '2016-02-02 00:25:31', '2016-02-02 00:25:31', 0),
(514, 'img_20160202_075237.jpg', 'image/jpeg', '', '', '', '2016-02-02 00:37:53', '2016-02-02 00:37:53', 0),
(515, 'img_20160202_143243.pdf', 'images/jpeg', 'Foto Ikrom aulua fahdi', '', 'Foto Ikrom aulua fahdi', '2016-02-02 07:43:32', '2016-02-02 07:43:32', 0),
(516, 'img_20160202_145448.pdf', 'images/jpeg', 'Foto adsf', '', 'Foto adsf', '2016-02-02 07:48:54', '2016-02-02 07:48:54', 0),
(517, 'img_20160202_145358.pdf', 'images/jpeg', 'Foto adsf', '', 'Foto adsf', '2016-02-02 07:58:53', '2016-02-02 07:58:53', 0),
(518, 'img_20160202_154922.pdf', 'images/jpeg', 'Foto asdf', '', 'Foto asdf', '2016-02-02 08:22:49', '2016-02-02 08:22:49', 0),
(519, 'img_20160202_171044.jpg', 'image/jpeg', '', '', '', '2016-02-02 10:44:10', '2016-02-02 10:44:10', 0),
(520, 'img_20160202_231450.jpg', 'image/jpeg', '', '', '', '2016-02-02 16:50:14', '2016-02-02 16:50:14', 0),
(521, 'img_20160202_233258.jpg', 'image/jpeg', '', '', '', '2016-02-02 16:58:32', '2016-02-02 16:58:32', 0),
(522, 'img_20160203_022053.pdf', 'images/jpeg', 'Foto Ikrom', '', 'Foto Ikrom', '2016-02-02 19:53:20', '2016-02-02 19:53:20', 0),
(523, 'img_20160203_024953.pdf', 'images/jpeg', 'Foto Ikrom', '', 'Foto Ikrom', '2016-02-02 19:53:49', '2016-02-02 19:53:49', 0),
(525, 'img_20160203_072249.jpg', 'image/jpeg', '', '', '', '2016-02-03 00:49:23', '2016-02-03 00:49:23', 0),
(526, 'img_20160205_141358.jpg', 'image/jpeg', '', '', '', '2016-02-05 07:58:14', '2016-02-05 07:58:14', 0),
(527, 'img_20160207_034944.jpg', 'image/jpeg', '', '', '', '2016-02-06 20:44:49', '2016-02-06 20:44:49', 0),
(528, 'img_20160207_032547.jpg', 'image/jpeg', '', '', '', '2016-02-06 20:47:25', '2016-02-06 20:47:25', 0),
(529, 'img_20160207_035449.jpg', 'image/jpeg', '', '', '', '2016-02-06 20:49:54', '2016-02-06 20:49:54', 0),
(530, 'img_20160207_032051.jpg', 'image/jpeg', '', '', '', '2016-02-06 20:51:20', '2016-02-06 20:51:20', 0),
(531, 'img_20160207_031452.jpg', 'image/jpeg', '', '', '', '2016-02-06 20:52:14', '2016-02-06 20:52:14', 0),
(532, 'img_20160207_032653.jpg', 'image/jpeg', '', '', '', '2016-02-06 20:53:26', '2016-02-06 20:53:26', 0),
(533, 'img_20160207_032454.jpg', 'image/jpeg', '', '', '', '2016-02-06 20:54:25', '2016-02-06 20:54:25', 0),
(534, 'img_20160207_032855.jpg', 'image/jpeg', '', '', '', '2016-02-06 20:55:28', '2016-02-06 20:55:28', 0),
(535, 'img_20160207_032356.jpg', 'image/jpeg', '', '', '', '2016-02-06 20:56:23', '2016-02-06 20:56:23', 0),
(536, 'img_20160207_033957.jpg', 'image/jpeg', '', '', '', '2016-02-06 20:57:39', '2016-02-06 20:57:39', 0),
(537, 'img_20160207_033658.jpg', 'image/jpeg', '', '', '', '2016-02-06 20:58:36', '2016-02-06 20:58:36', 0),
(538, 'img_20160207_034459.jpg', 'image/jpeg', '', '', '', '2016-02-06 20:59:44', '2016-02-06 20:59:44', 0),
(539, 'img_20160207_044100.jpg', 'image/jpeg', '', '', '', '2016-02-06 21:00:41', '2016-02-06 21:00:41', 0),
(540, 'img_20160207_042901.jpg', 'image/jpeg', '', '', '', '2016-02-06 21:01:29', '2016-02-06 21:01:29', 0),
(541, 'img_20160207_042302.jpg', 'image/jpeg', '', '', '', '2016-02-06 21:02:23', '2016-02-06 21:02:23', 0),
(542, 'img_20160207_042403.jpg', 'image/jpeg', '', '', '', '2016-02-06 21:03:24', '2016-02-06 21:03:24', 0),
(543, 'img_20160207_042104.jpg', 'image/jpeg', '', '', '', '2016-02-06 21:04:21', '2016-02-06 21:04:21', 0),
(544, 'img_20160207_045504.jpg', 'image/jpeg', '', '', '', '2016-02-06 21:04:55', '2016-02-06 21:04:55', 0),
(545, 'img_20160207_044505.jpg', 'image/jpeg', '', '', '', '2016-02-06 21:05:45', '2016-02-06 21:05:45', 0),
(546, 'img_20160207_044406.jpg', 'image/jpeg', '', '', '', '2016-02-06 21:06:45', '2016-02-06 21:06:45', 0),
(547, 'img_20160207_040108.jpg', 'image/jpeg', '', '', '', '2016-02-06 21:08:01', '2016-02-06 21:08:01', 0),
(548, 'img_20160207_042709.jpg', 'image/jpeg', '', '', '', '2016-02-06 21:09:27', '2016-02-06 21:09:27', 0),
(549, 'img_20160207_042411.jpg', 'image/jpeg', '', '', '', '2016-02-06 21:11:24', '2016-02-06 21:11:24', 0),
(550, 'img_20160207_040413.jpg', 'image/jpeg', '', '', '', '2016-02-06 21:13:04', '2016-02-06 21:13:04', 0),
(552, 'img_20160209_121605.jpg', 'image/jpeg', 'Foto Andri Hartono ', '', 'Foto Andri Hartono ', '2016-02-09 05:05:16', '2016-02-09 05:05:16', 0),
(553, 'img_20160209_165407.jpg', 'image/jpeg', 'Foto ANANG SULIS SETIYO AWAN', '', 'Foto ANANG SULIS SETIYO AWAN', '2016-02-09 09:07:55', '2016-02-09 09:07:55', 0),
(554, 'img_20160210_062251.jpg', 'image/jpeg', 'Foto Ichsan Santoso', '', 'Foto Ichsan Santoso', '2016-02-09 23:51:22', '2016-02-09 23:51:22', 0),
(556, 'img_20160212_131617.jpeg', 'image/jpeg', 'Foto Rafika Nurbaeni', '', 'Foto Rafika Nurbaeni', '2016-02-12 06:17:16', '2016-02-12 06:17:16', 0),
(557, 'img_20160212_130756.jpg', 'image/jpeg', '', '', '', '2016-02-12 06:56:07', '2016-02-12 06:56:07', 0),
(560, 'img_20160213_092745.jpg', 'image/jpeg', 'Foto Agung Bangun S, SH', '', 'Foto Agung Bangun S, SH', '2016-02-13 02:45:27', '2016-02-13 02:45:27', 0),
(561, 'img_20160216_120122.jpg', 'image/jpeg', 'Foto Noor Khamimah', '', 'Foto Noor Khamimah', '2016-02-16 05:22:02', '2016-02-16 05:22:02', 0),
(562, 'img_20160218_102303.jpg', 'image/jpeg', '', '', '', '2016-02-18 03:03:23', '2016-02-18 03:03:23', 0),
(563, 'img_20160218_125910.jpg', 'image/jpeg', '', '', '', '2016-02-18 05:11:00', '2016-02-18 05:11:00', 0),
(564, 'img_20160218_224848.jpg', 'image/jpeg', '', '', '', '2016-02-18 15:48:48', '2016-02-18 15:48:48', 0),
(565, 'img_20160218_224950.jpg', 'image/jpeg', '', '', '', '2016-02-18 15:50:49', '2016-02-18 15:50:49', 0),
(566, 'img_20160219_012542.jpg', 'image/jpeg', '', '', '', '2016-02-18 18:42:25', '2016-02-18 18:42:25', 0),
(567, 'img_20160219_164001.png', 'image/png', 'Foto Mini Iskandar', '', 'Foto Mini Iskandar', '2016-02-19 09:01:41', '2016-02-19 09:01:41', 0),
(568, 'img_20160220_052158.jpg', 'image/jpeg', 'Foto BORIMIN', '', 'Foto BORIMIN', '2016-02-19 22:58:21', '2016-02-19 22:58:21', 0),
(569, 'img_20160222_074839.pdf', 'images/jpeg', 'Foto asdf', '', 'Foto asdf', '2016-02-22 00:39:48', '2016-02-22 00:39:48', 0),
(570, 'img_20160222_070140.pdf', 'images/jpeg', 'Foto asdf', '', 'Foto asdf', '2016-02-22 00:40:01', '2016-02-22 00:40:01', 0),
(571, 'img_20160222_070440.pdf', 'images/jpeg', 'Foto asdf', '', 'Foto asdf', '2016-02-22 00:40:04', '2016-02-22 00:40:04', 0),
(572, 'img_20160226_083349.JPG', 'image/jpeg', 'Foto Ghofar Ismail', '', 'Foto Ghofar Ismail', '2016-02-26 01:49:33', '2016-02-26 01:49:33', 0),
(573, 'img_20160226_113646.jpg', 'image/jpeg', 'Foto TEMUDIYONO', '', 'Foto TEMUDIYONO', '2016-02-26 04:46:36', '2016-02-26 04:46:36', 0),
(574, 'img_20160226_114458.jpg', 'image/jpeg', 'Foto TEMUDIYONO', '', 'Foto TEMUDIYONO', '2016-02-26 04:58:45', '2016-02-26 04:58:45', 0),
(575, 'img_20160226_125211.jpg', 'image/jpeg', 'Foto TEMUDIYONO', '', 'Foto TEMUDIYONO', '2016-02-26 05:11:53', '2016-02-26 05:11:53', 0),
(576, 'img_20160226_125519.jpg', 'image/jpeg', 'Foto TEMUDIYONO', '', 'Foto TEMUDIYONO', '2016-02-26 05:19:55', '2016-02-26 05:19:55', 0),
(577, 'img_20160229_002240.jpg', 'image/jpeg', 'Foto Krisnamurti Pramudito', '', 'Foto Krisnamurti Pramudito', '2016-02-28 17:40:22', '2016-02-28 17:40:22', 0),
(578, 'img_20160304_002830.jpg', 'image/jpeg', 'Foto Krisnamurti', '', 'Foto Krisnamurti', '2016-03-03 17:30:28', '2016-03-03 17:30:28', 0),
(579, 'img_20160309_124008.jpg', 'image/jpeg', 'Foto Noer Sony Subekti', '', 'Foto Noer Sony Subekti', '2016-03-09 05:08:40', '2016-03-09 05:08:40', 0),
(580, 'img_20160310_050233.jpg', 'image/jpeg', '', '', '', '2016-03-09 22:33:03', '2016-03-09 22:33:03', 0),
(581, 'img_20160312_023007.pdf', 'images/jpeg', 'Foto Lestari Ningsih', '', 'Foto Lestari Ningsih', '2016-03-11 19:07:30', '2016-03-11 19:07:30', 0),
(582, 'img_20160312_020708.pdf', 'images/jpeg', 'Foto Lestari Ningsih', '', 'Foto Lestari Ningsih', '2016-03-11 19:08:07', '2016-03-11 19:08:07', 0),
(583, 'img_20160312_045501.jpg', 'image/jpeg', 'Foto arsa100seed@gmail.com', '', 'Foto arsa100seed@gmail.com', '2016-03-11 21:01:56', '2016-03-11 21:01:56', 0),
(584, 'img_20160312_143951.pdf', 'images/jpeg', 'Foto Sayyidina Rajif Khasan Al Assegaf', '', 'Foto Sayyidina Rajif Khasan Al Assegaf', '2016-03-12 07:51:39', '2016-03-12 07:51:39', 0),
(585, 'img_20160312_153222.jpg', 'image/jpeg', 'Foto TEMUDIYONO', '', 'Foto TEMUDIYONO', '2016-03-12 08:22:33', '2016-03-12 08:22:33', 0),
(586, 'img_20160314_045006.jpg', 'image/jpeg', '', '', '', '2016-03-13 21:06:50', '2016-03-13 21:06:50', 0),
(587, 'img_20160315_074440.jpg', 'image/jpeg', 'Foto NUR HANDAYANI', '', 'Foto NUR HANDAYANI', '2016-03-15 00:40:44', '2016-03-15 00:40:44', 0),
(588, 'img_20160316_052155.jpg', 'image/jpeg', '', '', '', '2016-03-15 22:55:21', '2016-03-15 22:55:21', 0),
(589, 'img_20160316_064600.png', 'image/png', '', '', '', '2016-03-15 23:00:46', '2016-03-15 23:00:46', 0),
(590, 'img_20160318_173029.jpg', 'image/jpeg', '', '', '', '2016-03-18 10:29:30', '2016-03-18 10:29:30', 0),
(593, 'img_20160318_183602.jpg', 'image/jpeg', '', '', '', '2016-03-18 11:02:36', '2016-03-18 11:02:36', 0),
(594, 'img_20160318_185442.png', 'image/png', '', '', '', '2016-03-18 11:42:54', '2016-03-18 11:42:54', 0),
(595, 'img_20160318_182847.png', 'image/png', '', '', '', '2016-03-18 11:47:29', '2016-03-18 11:47:29', 0),
(596, 'img_20160318_195507.png', 'image/png', '', '', '', '2016-03-18 12:07:55', '2016-03-18 12:07:55', 0),
(597, 'img_20160318_190432.png', 'image/png', '', '', '', '2016-03-18 12:32:04', '2016-03-18 12:32:04', 0),
(598, 'img_20160318_192835.png', 'image/png', '', '', '', '2016-03-18 12:35:28', '2016-03-18 12:35:28', 0),
(599, 'img_20160318_192538.png', 'image/png', '', '', '', '2016-03-18 12:38:25', '2016-03-18 12:38:25', 0),
(600, 'img_20160319_005322.pdf', 'images/jpeg', 'Foto Alwi Attamimi', '', 'Foto Alwi Attamimi', '2016-03-18 17:22:53', '2016-03-18 17:22:53', 0),
(601, 'img_20160319_031123.pdf', 'images/jpeg', 'Foto Cahyadi', '', 'Foto Cahyadi', '2016-03-18 20:23:11', '2016-03-18 20:23:11', 0),
(602, 'img_20160319_151022.jpg', 'image/jpeg', 'Foto vieqi', '', 'Foto vieqi', '2016-03-19 08:22:10', '2016-03-19 08:22:10', 0),
(603, 'img_20160324_165354.png', 'image/png', '', '', '', '2016-03-24 09:54:53', '2016-03-24 09:54:53', 0),
(604, 'img_20160324_160857.png', 'image/png', '', '', '', '2016-03-24 09:57:09', '2016-03-24 09:57:09', 0),
(605, 'img_20160325_013935.jpg', 'image/jpeg', 'Foto MOH.ARIS KUSHARYANTO,S.STP', '', 'Foto MOH.ARIS KUSHARYANTO,S.STP', '2016-03-24 18:35:40', '2016-03-24 18:35:40', 0),
(606, 'img_20160325_024206.jpg', 'image/jpeg', 'Foto ENDAH SULISTYARINI,S.ST', '', 'Foto ENDAH SULISTYARINI,S.ST', '2016-03-24 19:06:42', '2016-03-24 19:06:42', 0),
(607, 'img_20160329_100824.pdf', 'images/jpeg', 'Foto MOH.ARIS KUSHARYANTO,S.STP', '', 'Foto MOH.ARIS KUSHARYANTO,S.STP', '2016-03-29 03:24:08', '2016-03-29 03:24:08', 0),
(608, 'img_20160405_220518.pdf', 'images/jpeg', 'Foto Yesser E', '', 'Foto Yesser E', '2016-04-05 15:18:05', '2016-04-05 15:18:05', 0),
(609, 'img_20160405_221818.pdf', 'images/jpeg', 'Foto Yesser E', '', 'Foto Yesser E', '2016-04-05 15:18:18', '2016-04-05 15:18:18', 0),
(610, 'img_20160406_060514.jpg', 'image/jpeg', 'Foto Ngabidin Nur Cahyo', '', 'Foto Ngabidin Nur Cahyo', '2016-04-05 23:14:06', '2016-04-05 23:14:06', 0),
(611, 'img_20160406_060959.jpg', 'image/jpeg', 'Foto Achmad Fauzi', '', 'Foto Achmad Fauzi', '2016-04-05 23:59:09', '2016-04-05 23:59:09', 0),
(612, 'img_20160415_162200.jpg', 'image/jpeg', 'Foto Dwi Arie Trisnawanto ', '', 'Foto Dwi Arie Trisnawanto ', '2016-04-15 09:00:23', '2016-04-15 09:00:23', 0),
(613, 'img_20160415_163611.jpg', 'image/jpeg', 'Foto Andri Hartono ', '', 'Foto Andri Hartono ', '2016-04-15 09:11:37', '2016-04-15 09:11:37', 0),
(614, 'img_20160512_191436.jpg', 'image/jpeg', '', '', '', '2016-05-12 12:36:14', '2016-05-12 12:36:14', 0),
(615, 'img_20160608_060537.pdf', 'images/jpeg', 'Foto Devi bin Idrus', '', 'Foto Devi bin Idrus', '2016-06-07 23:37:05', '2016-06-07 23:37:05', 0),
(616, 'img_20160608_063737.pdf', 'images/jpeg', 'Foto Devi bin Idrus', '', 'Foto Devi bin Idrus', '2016-06-07 23:37:37', '2016-06-07 23:37:37', 0),
(617, 'img_20160608_064837.pdf', 'images/jpeg', 'Foto Devi bin Idrus', '', 'Foto Devi bin Idrus', '2016-06-07 23:37:48', '2016-06-07 23:37:48', 0),
(618, 'img_20160608_060038.pdf', 'images/jpeg', 'Foto Devi bin Idrus', '', 'Foto Devi bin Idrus', '2016-06-07 23:38:00', '2016-06-07 23:38:00', 0),
(619, 'img_20160728_211940.pdf', 'images/jpeg', 'Foto Zaki', '', 'Foto Zaki', '2016-07-28 14:40:19', '2016-07-28 14:40:19', 0),
(620, 'img_20160728_214340.pdf', 'images/jpeg', 'Foto Zaki', '', 'Foto Zaki', '2016-07-28 14:40:43', '2016-07-28 14:40:43', 0),
(621, 'img_20160815_024459.pdf', 'images/jpeg', 'Foto Irawan Laksono', '', 'Foto Irawan Laksono', '2016-08-14 19:59:44', '2016-08-14 19:59:44', 0),
(622, 'img_20160820_152255.pdf', 'images/jpeg', 'Foto Amna Widiaksono', '', 'Foto Amna Widiaksono', '2016-08-20 08:55:22', '2016-08-20 08:55:22', 0),
(623, 'img_20160913_142929.jpg', 'image/jpeg', 'seminar14sep', '', 'seminar14sep', '2016-09-13 07:29:29', '2016-09-13 07:29:29', 0),
(624, 'img_20160923_134017.png', 'image/png', '', '', '', '2016-09-23 06:17:41', '2016-09-23 06:17:41', 0),
(625, 'img_20160928_195333.jpg', 'image/jpeg', 'Foto Agni Hotna Leliwati', '', 'Foto Agni Hotna Leliwati', '2016-09-28 12:33:53', '2016-09-28 12:33:53', 0),
(626, 'img_20160928_194154.jpg', 'image/jpeg', 'Foto Agni Hotna Leliwati', '', 'Foto Agni Hotna Leliwati', '2016-09-28 12:54:41', '2016-09-28 12:54:41', 0),
(627, 'img_20160928_203715.jpg', 'image/jpeg', 'Foto Agni Hotna Leliwati', '', 'Foto Agni Hotna Leliwati', '2016-09-28 13:15:37', '2016-09-28 13:15:37', 0),
(628, 'img_20160928_202833.jpg', 'image/jpeg', 'Foto Agni Hotna Leliwati', '', 'Foto Agni Hotna Leliwati', '2016-09-28 13:33:29', '2016-09-28 13:33:29', 0),
(629, 'img_20161018_023900.png', 'image/png', '', '', '', '2016-10-17 19:00:39', '2016-10-17 19:00:39', 0),
(630, 'img_20161018_020802.jpg', 'image/jpeg', '', '', '', '2016-10-17 19:02:08', '2016-10-17 19:02:08', 0),
(631, 'img_20161018_083631.png', 'image/png', '', '', '', '2016-10-18 01:31:36', '2016-10-18 01:31:36', 0),
(632, 'img_20161018_081959.jpg', 'image/jpeg', '', '', '', '2016-10-18 01:59:19', '2016-10-18 01:59:19', 0),
(633, 'img_20161018_094100.jpg', 'image/jpeg', '', '', '', '2016-10-18 02:00:41', '2016-10-18 02:00:41', 0),
(634, 'img_20161018_094801.jpg', 'image/jpeg', '', '', '', '2016-10-18 02:01:48', '2016-10-18 02:01:48', 0),
(635, 'img_20161018_090803.jpg', 'image/jpeg', '', '', '', '2016-10-18 02:03:08', '2016-10-18 02:03:08', 0),
(636, 'img_20161023_033558.jpg', 'image/jpeg', '', '', '', '2016-10-22 20:58:35', '2016-10-22 20:58:35', 0),
(637, 'img_20161023_035959.jpg', 'image/jpeg', '', '', '', '2016-10-22 20:59:59', '2016-10-22 20:59:59', 0),
(638, 'img_20161023_040811.jpg', 'image/jpeg', '', '', '', '2016-10-22 21:11:08', '2016-10-22 21:11:08', 0),
(639, 'img_20161027_080436.jpg', 'image/jpeg', '', '', '', '2016-10-27 01:36:04', '2016-10-27 01:36:04', 0),
(640, 'img_20161027_081545.jpg', 'image/jpeg', '', '', '', '2016-10-27 01:45:16', '2016-10-27 01:45:16', 0),
(641, 'img_20161027_083745.jpg', 'image/jpeg', '', '', '', '2016-10-27 01:45:37', '2016-10-27 01:45:37', 0),
(642, 'img_20161027_083146.jpg', 'image/jpeg', '', '', '', '2016-10-27 01:46:31', '2016-10-27 01:46:31', 0),
(643, 'img_20161027_091901.jpg', 'image/jpeg', '', '', '', '2016-10-27 02:01:19', '2016-10-27 02:01:19', 0),
(644, 'img_20161027_093201.jpg', 'image/jpeg', '', '', '', '2016-10-27 02:01:32', '2016-10-27 02:01:32', 0),
(645, 'img_20161027_090202.jpg', 'image/jpeg', '', '', '', '2016-10-27 02:02:02', '2016-10-27 02:02:02', 0),
(646, 'img_20161027_090902.jpg', 'image/jpeg', '', '', '', '2016-10-27 02:02:09', '2016-10-27 02:02:09', 0),
(647, 'img_20161027_101255.jpg', 'image/jpeg', 'mobil polisi', '', '', '2016-10-27 03:55:12', '2016-10-29 03:27:51', 0),
(648, 'img_20170306_054048.jpg', 'image/jpeg', '', '', '', '2017-03-05 22:48:40', '2017-03-05 22:48:40', 0),
(649, 'img_20170306_052449.jpg', 'image/jpeg', '', '', '', '2017-03-05 22:49:24', '2017-03-05 22:49:24', 0),
(650, 'img_20170306_092414.png', 'image/png', '', '', '', '2017-03-06 02:14:24', '2017-03-06 02:14:24', 0),
(651, 'img_20170309_042917.jpeg', 'image/jpeg', '', '', '', '2017-03-08 21:17:29', '2017-03-08 21:17:29', 0),
(652, 'img_20170309_045617.jpeg', 'image/jpeg', '', '', '', '2017-03-08 21:17:56', '2017-03-08 21:17:56', 0),
(653, 'img_20170309_042534.jpeg', 'image/jpeg', '', '', '', '2017-03-08 21:34:25', '2017-03-08 21:34:25', 0),
(654, 'img_20170309_045535.jpeg', 'image/jpeg', '', '', '', '2017-03-08 21:35:55', '2017-03-08 21:35:55', 0),
(655, 'img_20170309_043436.jpeg', 'image/jpeg', '', '', '', '2017-03-08 21:36:34', '2017-03-08 21:36:34', 0),
(656, 'img_20170309_040037.jpeg', 'image/jpeg', '', '', '', '2017-03-08 21:37:00', '2017-03-08 21:37:00', 0),
(657, 'img_20170309_062407.jpeg', 'image/jpeg', '', '', '', '2017-03-08 23:07:24', '2017-03-08 23:07:24', 0),
(658, 'img_20170309_150218.jpeg', 'image/jpeg', '', '', '', '2017-03-09 08:18:02', '2017-03-09 08:18:02', 0),
(659, 'img_20170310_025732.jpeg', 'image/jpeg', '', '', '', '2017-03-09 19:32:57', '2017-03-09 19:32:57', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ms_menu`
--

CREATE TABLE `ms_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_menu`
--

INSERT INTO `ms_menu` (`id`, `code`, `title`, `created_at`, `updated_at`) VALUES
(1, 'top_menu', 'Menu TOP', '2015-04-14 09:22:00', '2015-08-14 09:23:44'),
(2, 'user_menu_1', 'About Community', '2015-04-14 09:23:53', '2016-01-18 06:20:39'),
(3, 'user_menu_2', 'Product Enagic', '2015-04-14 09:28:07', '2016-02-06 21:16:59'),
(4, 'user_menu_3', 'Social Media', '2015-04-14 09:28:31', '2016-01-25 20:18:03'),
(5, 'user_menu_4', 'Investor Relations', '2015-04-14 09:28:46', '2015-04-18 15:15:30'),
(6, 'user_menu_5', 'Media', '2015-04-14 09:29:00', '2015-08-14 19:14:38'),
(7, 'num', 'Navigasi-utama', '2015-08-14 09:26:33', '2015-08-14 09:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `ms_menu_types`
--

CREATE TABLE `ms_menu_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_menu_types`
--

INSERT INTO `ms_menu_types` (`id`, `code`, `title`) VALUES
(1, 'category', 'Kategori'),
(2, 'post', 'Kiriman / Post'),
(3, 'portfolio', 'Portfolio / Galeri'),
(4, 'rss', 'RSS Feed'),
(5, 'link', 'Link / Url'),
(6, 'products', 'List of product'),
(7, 'nearbymitra', 'Gerai Terdekat'),
(8, 'mitracity', 'Mitra City'),
(9, 'ms_portfolio_category', 'Portfolio-Group'),
(10, 'ujian', 'Ujian/Tes'),
(11, 'cctv', 'CCTV'),
(12, 'locallink', 'Link Lokal');

-- --------------------------------------------------------

--
-- Table structure for table `ms_notification`
--

CREATE TABLE `ms_notification` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_notification`
--

INSERT INTO `ms_notification` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(6, 'judul 2', 'Gemar Sehati mempersembahkan:\nKanGen Water Seminar di Padang panjang\nSabtu, 23 april 2016\nsesi 1 pk 09.00 WIB - selesai | sesi 2 pk 13.00-selesai\nTempat: Aula A Hotel Pangeran - Jl. KH.A.DAHLAN No 7 Padang panjang', '2016-03-18 06:32:15', '0000-00-00 00:00:00'),
(5, 'judul', 'Ikuti Gemarsehati Bootcamp sessi-4 di Surabaya, Tanggal : 6-7 Agustus 2016', '2016-03-18 06:32:27', '0000-00-00 00:00:00'),
(7, 'Format Undangan SEMINAR STANDARD GEMARSEHATI: \r\nsilahkan di download di URL berikut :\r\nhttp://www.gemarsehati.com/upload/file/template_undangan_gemarsehati_2016.cdr\r\n', 'Format Undangan SEMINAR STANDARD GEMARSEHATI: \nsilahkan di download di URL berikut :\nhttp://www.gemarsehati.com/upload/file/template_undangan_gemarsehati_2016.cdr\n', '2016-04-12 07:19:21', '2016-03-24 05:50:44'),
(8, 'Reminder', 'Format Undangan SEMINAR STANDARD GEMARSEHATI: \r\nsilahkan di download di URL berikut :\r\nhttp://www.gemarsehati.com/upload/file/template_undangan_gemarsehati_2016.cdr', '2016-04-12 07:19:52', '2016-04-12 07:19:52'),
(9, 'Event Gemarsehati', 'Dear All the GREAT team,\r\nGemarsehati training di Surabaya:\r\nKYNU EAT and CHAT CAFE - Sabtu, 24 Mei 2016. Jam 13.00 - selesai\r\nPembicara : Bu Syofia dan Yussa Marulli , ST, CHt (6A3 Surabaya)', '2016-04-12 23:51:27', '2016-04-12 23:51:27'),
(10, 'Seminar 501 System Indonesia', 'Sabtu, 14 Mei 2016 - 13.00 WIB\r\nKYNU EAT and CHAT Cafe - Jl. Bratang Binangun I/23 - SBY\r\nEducator :\r\nIbu Dini & Ibu Sofia\r\nGuest Speaker :\r\nIr. Yussa Marulli (6A3 Enagic Indonesia)\r\nInfo :\r\n0853.1234.5959', '2016-05-13 07:51:25', '2016-05-13 07:51:25'),
(11, 'Seminar 501 System Indonesia (Reminder)', 'Seminar 501 System Indonesia. (Reminder)\r\nSabtu, 14 Mei 2016 - 13.00 WIB KYNU EAT and CHAT Cafe - Jl. Bratang Binangun I/23 - SBY Educator : Ibu Dini & Ibu Sofia Guest Speaker : Ir. Yussa Marulli (6A3 Enagic Indonesia) Info : 0853.1234.5959', '2016-05-13 23:51:14', '2016-05-13 23:51:14'),
(12, 'Opening Rumah Edukasi GEMARSEHATI', 'Mengharap kehadiran seluruh kawan kawan GemarSehati :\r\nHari/tgl : minggu, 12 Juni 2016 - 15.00 WIB - selesai\r\nDilanjut sholat terawih berjamaah di masjid depan Rumah Edukasi Gemarsehati\r\nLokasi : Jl.Medokan Asri Barat I Blok P2 Surabaya', '2016-06-09 10:14:53', '2016-06-09 10:14:53'),
(13, 'App Gemarsehati IOS Version', 'Untuk pengguna IOS, kini app gemarsehati sudah hadir di app store. Berikut URL nya : https://itunes.apple.com/us/app/kangen-gemarsehati/id1135881438?mt=8', '2016-07-22 05:59:45', '2016-07-22 05:59:45');

-- --------------------------------------------------------

--
-- Table structure for table `ms_portfolios`
--

CREATE TABLE `ms_portfolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `enabled` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ms_categories_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_portfolios`
--

INSERT INTO `ms_portfolios` (`id`, `title`, `description`, `parent_id`, `enabled`, `featured`, `created_at`, `updated_at`, `ms_categories_id`) VALUES
(1, 'Galeri KESS', '', 0, '1', '1', '2015-08-14 00:36:09', '2017-03-08 21:14:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_portfolio_category`
--

CREATE TABLE `ms_portfolio_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_portfolio_category`
--

INSERT INTO `ms_portfolio_category` (`id`, `name`) VALUES
(1, 'Photo'),
(2, 'Video');

-- --------------------------------------------------------

--
-- Table structure for table `ms_posts`
--

CREATE TABLE `ms_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `ms_media_id` int(11) NOT NULL,
  `ms_user_id` int(11) NOT NULL,
  `ms_portfolio_id` int(11) NOT NULL,
  `ms_status_posts_id` int(11) NOT NULL,
  `show_comment` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `seo` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_posts`
--

INSERT INTO `ms_posts` (`id`, `title`, `description`, `meta_description`, `ms_media_id`, `ms_user_id`, `ms_portfolio_id`, `ms_status_posts_id`, `show_comment`, `featured`, `seo`, `created_at`, `updated_at`) VALUES
(51, 'IOS', '<p>#ios</p>', '#ios', 0, 1, 0, 2, '', '', NULL, '2016-01-19 02:55:10', '2016-02-16 22:24:06'),
(52, 'Android', '<p>https://play.google.com/store/apps/details?id=kangen.gemarsehati.enagic</p>', '#android', 0, 1, 0, 2, '', '', NULL, '2016-01-19 02:55:29', '2016-03-10 01:43:41'),
(54, 'Contact', '<p>Contact</p>', 'Contact', 0, 1, 0, 2, '', '0', NULL, '2016-01-20 19:40:03', '2016-02-16 22:24:06'),
(55, 'Contact', '<p>Contact</p>', 'Contact', 0, 1, 0, 2, '', '', NULL, '2016-01-24 00:07:37', '2016-02-16 22:24:06'),
(56, 'Siapa itu Gemarsehati ?', '<p><strong>GEMARSEHATI </strong> adalah suatu komunitas anak bangsa yang peduli akan kesehatan masyarakat Indonesia dengan melakukan kampanye serta memberikan edukasi dan mendorong setiap anak bangsa untuk berprilaku hidup sehat dengan cara minum air yang sehat dan alami.</p>\r\n<p><strong>GEMARSEHATI </strong>didirikan dan berpusat di Surabaya oleh dua orang sahabat yang peduli akan kesehatan masyarakat Indonesia, mereka itu adalah PANGERAN CHANDRA SASMITA, SE dan Ir. YUSSA MARULLI.</p>\r\n<p><strong>GEMARSEHATI </strong>didirikan dan mulai aktifitas pertamanya pada tanggal 23 Maret 2014 di Surabaya dengan mengadakan Seminar Kesehatan pertama di Hotel Meritus Surabaya.</p>\r\n<p><strong>GEMARSEHATI </strong>dalam perkembangannya telah membuka beberapa cabang di Sidoarjo, Malang, Jember, Banyuwangi, Jakarta, Semarang, Jogja, Solo, Tegal, Balikpapan, Samarinda, Bontang, Palu dan akan terus berkembang ke seluruh wilayah Indonesia. Untuk itu kami memohon dukungan semua pihak akan niat baik kami dalam rangka kampanye dan memberikan edukasi pada masyarakat Indonesia atas bagaimana berprilaku hidup sehat dengan cara minum air yang sehat dan alami.</p>', 'GEMARSEHATI  adalah suatu komunitas anak bangsa yang peduli akan kesehatan masyarakat Indonesia dengan melakukan kampanye serta memberikan edukasi dan mendorong setiap anak bangsa untuk berprilaku hidup sehat dengan cara minum air yang sehat dan alami.\r\nGEMARSEHATI didirikan dan berpusat di Surabaya oleh dua orang sahabat yang peduli akan kesehatan masyarakat Indonesia, mereka itu adalah PANGERAN CHANDRA SASMITA, SE dan Ir. YUSSA MARULLI.\r\nGEMARSEHATI didirikan dan mulai aktifitas pertamanya pada tanggal 23 Maret 2014 di Surabaya dengan mengadakan Seminar Kesehatan pertama di Hotel Meritus Surabaya.\r\nGEMARSEHATI dalam perkembangannya telah membuka beberapa cabang di Sidoarjo, Malang, Jember, Banyuwangi, Jakarta, Semarang, Jogja, Solo, Tegal, Balikpapan, Samarinda, Bontang, Palu dan akan terus berkembang ke seluruh wilayah Indonesia. Untuk itu kami memohon dukungan semua pihak akan niat baik kami dalam rangka kampanye dan memberikan edukasi pada masyarakat Indonesia atas bagaimana berprilaku hidup sehat dengan cara minum air yang sehat dan alami.', 462, 1, 0, 2, '', '', NULL, '2016-01-27 03:10:27', '2016-02-16 22:24:06'),
(61, 'Bisnis Testimoni 1', '<p>Bisnis Testimoni 1</p>', 'Bisnis Testimoni 1', 488, 1, 0, 2, '', '', NULL, '2016-02-01 04:58:17', '2016-02-16 22:24:06'),
(65, 'Kesehatan Testimoni 1', '<p>Kesehatan Testimoni 1</p>', 'Kesehatan Testimoni 1', 488, 1, 0, 2, '', '', NULL, '2016-02-01 20:25:24', '2016-02-16 22:24:06'),
(77, 'Struktur', '<p><img src=\"http://korlantas.polri.go.id/wp-content/uploads/2015/08/Korlantas.jpg\" alt=\"None\" width=\"1024\" height=\"768\" /></p>', 'Struktur Korlantas Polri', 0, 1, 0, 2, '', '', NULL, '2016-10-17 08:57:34', '2016-10-17 08:57:34'),
(78, 'Tentang Ekonomi Syariah', '<p>Konsep ekonomi syariah mulai diperkenalkan kepada masyarakat pada tahun 1991 ketika Bank Muamalat Indonesia berdiri, yang kemudian diikuti oleh lembaga-lembaga keuangan lainnya. Pada waktu itu sosialisasi ekonomi syariah dilakukan masing-masing lembaga keuangan syariah. Setelah di evaluasi bersama, disadari bahwa sosialisasi sistem ekonomi syariah hanya dapat berhasil apabila dilakukan dengan cara yang terstruktur dan berkelanjutan.</p>\r\n<p>Menyadari hal tersebut, lembaga-lembaga keuangan syariah berkumpul dan mengajak seluruh kalangan yang berkepentingan untuk membentuk suatu organisasi, dengan usaha bersama akan melaksanakan program sosialisasi terstruktur dan berkesinambungan kepada masyarakat. Organisasi ini dinamakan &ldquo;Perkumpulan Masyarakat Ekonomi Syariah&rdquo; yang disingkat dengan MES, sebutan dalam bahasa Indonesia adalah Masyarakat Ekonomi Syariah, dalam bahasa Inggris adalah Islamic Economic Society atau dalam bahasa arabnya Mujtama&rsquo; al-Iqtishad al-Islamiy, didirikan pada hari Senin, tanggal 1 Muharram 1422 H, bertepatan pada tanggal 26 Maret 2001 M. Di deklarasikan pada hari Selasa, tanggal 2 Muharram 1422 H di Jakarta.</p>\r\n<p>Pendiri MES adalah Perorangan, lembaga keuangan, lembaga pendidikan, lembaga kajian dan badan usaha yang tertarik untuk mengembangkan ekonomi syariah. MES berasaskan Syariah Islam, serta tunduk pada peraturan perundang-undangan yang berlaku di Republik Indonesia, sehingga terbuka bagi setiap warga negara tanpa memandang keyakinan agamanya. Didirikan berdasarkan Akta No. 03 tanggal 22 Februari 2010 dan diperbaharui di dalam Akta No. 02 tanggal 16 April 2010 yang dibuat dihadapan Notaris Rini Martini Dahliani, SH, di Jakarta, akta mana telah memperoleh persetujuan Menteri Kehakiman dan Hak Asasi Manusia Republik Indonesia berdasarkan Surat Keputusan No. AHU-70.AH.01.06, tertanggal 25 Mei 2010 tentang Pengesahan Perkumpulan dan telah dimasukkan dalam tambahan berita negara No. 47 tanggal 14 April 2011.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>http://www.ekonomisyariah.org/tentang-mes/sejarah/</strong></p>', 'www.korlantas.polri.go.id merupakan bentuk pelayanan masyarakat online di bidang Lalu Lintas dan Angkutan Jalan (LLAJ)', 659, 1, 0, 2, '', '', NULL, '2016-10-18 01:31:46', '2017-03-06 02:22:07'),
(83, 'Profil', '<p><strong>Kedudukan, Tugas, dan Fungsi</strong><br />&nbsp;<br />I. Korlantas Polri merupakan unsur pelaksana tugas pokok yang berada di bawah Kapolri;<br />&nbsp;<br />II. Korlantas Polri bertugas:</p>\r\n<ol>\r\n<li>membina dan menyelenggarakan fungsi lalu lintas yang meliputi pendidikan masyarakat, penegakan hukum, pengkajian masalah lalu lintas, registrasi dan identifikasi pengemudi dan kendaraan bermotor serta patroli jalan raya;</li>\r\n<li>melaksanakan penertiban lalu lintas, manajemen operasional dan rekayasa lalu lintas (engineering );</li>\r\n<li>menyelenggarakan pusat Komunikasi, Koordinasi, Kendali dan Informasi (K3I) tentang lalu lintas;</li>\r\n<li>mengkoordinasikan pemangku kepentingan yang berkaitan dengan penyelenggaraan lalu lintas;</li>\r\n<li>memberikan rekomendasi dampak lalu lintas; dan</li>\r\n<li>melaksanakan koordinasi dan/atau pengawasan PPNS.</li>\r\n</ol>\r\n<p>&nbsp;<br />III. dalam melaksanakan tugas, Korlantas Polri menyelenggarakan fungsi:</p>\r\n<ol>\r\n<li>penyusunan kebijakan strategis yang berkaitan dengan peran dan fungsi polisi lalu lintas, perumusan dan atau pengembangan sistem dan metode termasuk petunjuk pelaksanaan fungsi lalu lintas, membangun kemitraan dan kerjasama baik dalam maupun luar negeri, serta menyelenggarakan koordinasi dengan pemangku kepentingan lainnya di bidang lalu lintas;</li>\r\n<li>pelaksanaan manajemen operasional lalu lintas yang meliputi kegiatan memelihara dan mewujudkan keamanan, keselamatan, ketertiban dan kelancaran lalu lintas di jalan raya, jalan tol, serta jalan-jalan luar kota sebagai penghubung (linking ping) antar kesatuan lalu lintas melalui kegiatan pengaturan, penjagaan, pengawalan, patroli, TPKP, Quick Respon Time, dan menjadi jejaring National Traffic Management Centre (NTMC).</li>\r\n<li>pengembangan sistem dan metode termasuk petunjuk pelaksanaan teknis penegakan hukum yang meliputi kegiatan penindakan terhadap pelanggaran aturan lalu lintas, penanganan kecelakaan lalu lintas, penyidikan kecelakaan lalu lintas, serta koordinasi dan pengawasan PPNS;</li>\r\n<li>pendidikan masyarakat dalam berlalu lintas, melalui kegiatan sosialisasi, penanaman nilai, membangun kesadaran, kepekaan, kepedulian akan tertib lalu lintas, serta pendidikan berlalu lintas secara formal dan informal;</li>\r\n<li>pembinaan teknis dan administrasi registrasi serta identifikasi pengemudi dan kendaraan bermotor yang meliputi kegiatan pengecekan administrasi dan fisik kendaraan serta pengujian kompetensi pengemudi untuk menjamin keabsahan dokumen kendaraan bermotor dan sarana kontrol dalam rangka penegakan hukum maupun untuk kepentingan forensik kepolisian;</li>\r\n<li>pengkajian bidang lalu lintas yang meliputi kegiatan keamanan dan keselamatan lalu lintas, pemetaan, inventarisasi, identifikasi wilayah, masalah maupun potensi-potensi yang berkaitan dengan lalu lintas dalam sistem Filling and Recording, baik untuk kepentingan internal maupun eksternal kepolisian, serta perumusan rekomendasi dampak lalu lintas; dan</li>\r\n<li>pelaksanaan operasional NTMC, yang meliputi kegiatan pengumpulan, pengolahan dan penyajian data lalu lintas, sebagai pusat kendali, koordinasi, komunikasi, dan informasi, pengembangan sistem dan teknologi informasi dan komunikasi lalu lintas, serta pelayanan informasi lalu lintas yang menyangkut pelanggaran dan kecelakaan lalu lintas dengan lingkup data jajaran Polri.</li>\r\n</ol>', 'Kedudukan, Tugas, dan Fungsi&nbsp;I. Korlantas Polri merupakan unsur pelaksana tugas pokok yang berada di bawah Kapolri;&nbsp;II. Korlantas Polri bertugas:\r\n\r\nmembina dan menyelenggarakan fungsi lalu lintas yang meliputi pendidikan masyarakat, penegakan hukum, pengkajian masalah lalu lintas, registrasi dan identifikasi pengemudi dan kendaraan bermotor serta patroli jalan raya;\r\nmelaksanakan penertiban lalu lintas, manajemen operasional dan rekayasa lalu lintas (engineering );\r\nmenyelenggarakan pusat Komunikasi, Koordinasi, Kendali dan Informasi (K3I) tentang lalu lintas;\r\nmengkoordinasikan pemangku kepentingan yang berkaitan dengan penyelenggaraan lalu lintas;\r\nmemberikan rekomendasi dampak lalu lintas; dan\r\nmelaksanakan koordinasi dan/atau pengawasan PPNS.\r\n\r\n&nbsp;III. dalam melaksanakan tugas, Korlantas Polri menyelenggarakan fungsi:\r\n\r\npenyusunan kebijakan strategis yang berkaitan dengan peran dan fungsi polisi lalu lintas, perumusan dan atau pengembangan sistem dan metode termasuk petunjuk pelaksanaan fungsi lalu lintas, membangun kemitraan dan kerjasama baik dalam maupun luar negeri, serta menyelenggarakan koordinasi dengan pemangku kepentingan lainnya di bidang lalu lintas;\r\npelaksanaan manajemen operasional lalu lintas yang meliputi kegiatan memelihara dan mewujudkan keamanan, keselamatan, ketertiban dan kelancaran lalu lintas di jalan raya, jalan tol, serta jalan-jalan luar kota sebagai penghubung (linking ping) antar kesatuan lalu lintas melalui kegiatan pengaturan, penjagaan, pengawalan, patroli, TPKP, Quick Respon Time, dan menjadi jejaring National Traffic Management Centre (NTMC).\r\npengembangan sistem dan metode termasuk petunjuk pelaksanaan teknis penegakan hukum yang meliputi kegiatan penindakan terhadap pelanggaran aturan lalu lintas, penanganan kecelakaan lalu lintas, penyidikan kecelakaan lalu lintas, serta koordinasi dan pengawasan PPNS;\r\npendidikan masyarakat dalam berlalu lintas, melalui kegiatan sosialisasi, penanaman nilai, membangun kesadaran, kepekaan, kepedulian akan tertib lalu lintas, serta pendidikan berlalu lintas secara formal dan informal;\r\npembinaan teknis dan administrasi registrasi serta identifikasi pengemudi dan kendaraan bermotor yang meliputi kegiatan pengecekan administrasi dan fisik kendaraan serta pengujian kompetensi pengemudi untuk menjamin keabsahan dokumen kendaraan bermotor dan sarana kontrol dalam rangka penegakan hukum maupun untuk kepentingan forensik kepolisian;\r\npengkajian bidang lalu lintas yang meliputi kegiatan keamanan dan keselamatan lalu lintas, pemetaan, inventarisasi, identifikasi wilayah, masalah maupun potensi-potensi yang berkaitan dengan lalu lintas dalam sistem Filling and Recording, baik untuk kepentingan internal maupun eksternal kepolisian, serta perumusan rekomendasi dampak lalu lintas; dan\r\npelaksanaan operasional NTMC, yang meliputi kegiatan pengumpulan, pengolahan dan penyajian data lalu lintas, sebagai pusat kendali, koordinasi, komunikasi, dan informasi, pengembangan sistem dan teknologi informasi dan komunikasi lalu lintas, serta pelayanan informasi lalu lintas yang menyangkut pelanggaran dan kecelakaan lalu lintas dengan lingkup data jajaran Polri.\r\n', 0, 1, 0, 2, '', '', NULL, '2016-10-22 20:57:21', '2016-10-22 20:57:21'),
(84, 'Sejarah', '<p><strong>Periode 1950-1959</strong></p>\r\n<p>Memasuki periode 1950-1959, Seksi Lalu Lintas lahir dalam wadah Polisi Negara RI. Sebenarnya usaha &ndash; usaha penyusunan kembali organisasi Polisi Indonesia itu sudah ada sejak diangkatnya Kepala Jawatan Kepolisian Negara, namun usaha itu terhenti pada saat pecah perang kemerdekaan kedua (Clash II). Setelah penyerahan kedaulatan Negara RI tanggal 29 Desember 1943, baru dapat dilanjutkan kembali.</p>\r\n<p>Pimpinan Polisi di daerah pendudukan yang dipegang oleh kader-kader Belanda diganti oleh kader-kader Polisi Indonesia. Hanya dalam mereorganisasi Kepolisian Indonesia dinamakan Jawatan Kepolisian dan pada masa terbentuknya Negara Kesatuan tanggal 17 Agustus 1950 berubah menjadi Jawatan Kepolisian Negara. Karena kemajuan dan perkembangan masyarakat yang mulai perlu diantisipasi, maka organisasi polisi memerlukan penyesuaian agar dapat mewadahi dan menangani pekerjaan dengan cepat.</p>\r\n<p>Untuk itu diperlukan spesialisasi pada 9 Januari 1952 dikeluarkan order KKN No.6/IV/Sek 52 yang menegaskan dimulainya pembentukan kesatuan-kesatuan khusus seperti Polisi Perairan dan Udara, serta Polisi Lalu Lintas yang dimasukkan dalam pengurusan bagian organisasi.</p>\r\n<p>Waktu itu, Polisi Lalu Lintas memiliki rumusan tugas, yaitu:</p>\r\n<p>I. Mengurus lalu lintas<br />II. Mengurus kecelakaan lalu lintas<br />III. Pendaftaran nomor bewijs<br />IV. Motor brigade keramaian<br />V. Komando pos radio dan bengkel</p>\r\n<p><strong>Masa Orde Lama</strong></p>\r\n<p>Dengan Dekrit Presiden 5 Juli 1959, adalah momen yang juga menjadi sejarah perjuangan Polantas dalam masa perubahan bentuk negara ini, Lalu apa yang fundamental bagi Polantas?<br />Masih dalam suasana paska-Dekrit, pada tanggal 23 Oktober 1959 dikeluarkan Peraturan Sementara Menteri /KKN No. 2PRA/MK/1959 tentang Susunan dan Tugas Markas Besar Polisi Negara. Ini memperluas status Seksi Lalu Lintas menjadi Dinas Lalu Lintas dan Polisi Negara Urusan Kereta Api (PNUK).</p>\r\n<p>Yang menjadi &ldquo;sutradara&rdquo; di balik pengabdian Dinas Lalu Lintas dan Polisi Negara Urusan Kereta Api(PNUK) adalah Kepala Dinas Lalu Lintas dan PNUK pertama, Ajun Komisaris Besar Polisi Untung Margono yang menggantikan Komisaris Besar Polisi HS Djajoesman. Ia mengawali masa-masa penting polisi. Termasuk lahirnya Undang-undang Pokok Kepolisian No. 13/1961 tanggal 19 Juni 1961. Aturan ini bukan sekedar undang-undang tertulis, namun menjadi sejarah Kepolisian RI yang sangat penting, sebagai realisasi cita-cita yang selalu menjiwai kehidupan Korps Kepolisian Negara seirama dengan gelora perjuangan rakyat.</p>\r\n<p>Dinas Lalu Lintas dan Polisi Negara Urusan Kereta Api (PNUK).</p>\r\n<p>Direktorat Lalu Lintas, nama ini menjadi simbol kuat. Pertama kali digunakan di tingkat pusat. Prosesnya jelas, tanggal 23 Nopember 1962 dikeluarkan peraturan 3M Menteri/KSK No.2PRT/KK/62. Itu membentuk kembali Dinas Lalu Lintas yang terpisah dari Polisi Tugas Umum, sedangkan PNUK tetap dimasukkan dalam jajaran Polisi Tugas Umum. Kemudian pada 14 februari 1964 dengan Surat Keputusan 3M Menpangab No Pol. 11/SK/MK/64, Dinas Lalu Lintas diperluas lagi statusnya menjadi Direktorat Lalu Lintas.</p>\r\n<p><strong>Masa Orde Baru</strong></p>\r\n<p>Karena pengalaman yang pahit dari peristiwa G30S/PKI yang mencerminkan tidak adanya integrasi antar unsur-unsur ABRI, maka untuk meningkatkan integrasi ABRI, tahun 1967 dengan SK Presiden No. 132/1967 tanggal 24 Agustus 1967 ditetapkan Pokok-Pokok Organisasi dan Prosedur Bidang Pertahanan dan Keamanan yang menyatakan ABRI merupakan bagian dari organisasi Departemen Hankam meliputi AD, AL, AU , dan AK yang masing-masing dipimpin oleh Panglima Angkatan dan bertanggung jawab atas pelaksanaan tugas dan kewajibannya kepada Menhankam/Pangab.</p>\r\n<p>Organisasi baru di tubuh Polri lahir atas hasil penjabaran dikeluarkannya Surat Keputusan Kapolri No. Pol 113/SK/1979 tanggal 17 September 1970 tentang Organisasi Staf Umum dan Staf Khusus dan Badan &ndash; Badan Pelaksana Polri Bidang lalu lintas, juga menyesuaikan. Dua tahun sebelum surat keputusan ini (tahun 1968), di tingkat pusat dibentuk Pusat Kesatuan Operasi Lalu Lintas (Pusatop Lantasi dengan komandan KBP Drs. UE Medelu. Dengan keluarnya SK tersebut berubah kembali menjadi Direktorat Lalu Lintas tahun 1970, yang merupakan salah satu unsur Komando Utama Samapta Polri, sehingga kemudian disebut Direktorat Lalu Lintas Komapta.</p>\r\n<p><strong>Sub-Dit.Lantas Polri Menjadi Dit.Lantas Polri</strong></p>\r\n<p>Pada tahun 1984, Dinas Lalu Lintas diperkecil menjadi Sub Direktorat Lalu Lintas Polri dibawah Dit Samapta. Namun, karena adanya kebutuhan yang tinggi maka dikembalikan lagi menjadi Dit Lalu Lintas Polri dan langsung dibawah Kapolri.</p>\r\n<p>Perkembangan terus terjadi pada tahun 1991 tepatnya tanggal 21 Nopember 1991, Sub direktorat Lalu Lintas dikembangkan kembali organisasinya menjadi Direktorat Lalu Lintas Polri, berkedudukan di bawah Kapolri, yang sehari-harinya dikoordinasikan oleh Deputi Operasi Kapolri.</p>\r\n<p>Di era reformasi, Polri terlepas dari organisasi ABRI/TNI. Dengan sendirinya Polri tidak lagi berada dibawah Menhankam/Pangab. Tetapi sudah sebagai institusi yang independent dengan diundangkannya UU No.2 Tahun 2002 tentang Kepolisian Negara RI, maka Kapolri berada dibawah serta bertanggung jawab langsung kepada Presiden RI. Begitu pula dengan Direktorat Lalu Lintas, berada didalam wadah Badan Pembinaan Keamanan Polri (Babinkam Polri).</p>\r\n<p><strong>Dari Dirlantas menjadi Korps Lalu Lintas</strong></p>\r\n<p>Saat ini reformasi birokrasi di lingkungan Polri terus bergulir, meliputi reformasi instrumental, struktural, dan kultural. Reformasi instrumental akan meliputi kendaraan dan teknologi pendukung tugas Polri di lapangan. Karena diharapkan tugas Polri menjadi lebih baik dibanding sbelumnya, sehingga harus memelihara peralatan yang dimiliki agar berfungsi dengan baik agar dapat membantu kinerja polisi di lapangan.</p>\r\n<p>Kemudian berdasarkan Peraturan Presiden No.52 tanggal 4 Agustus tahun 2010 Dit.Lantas Polri Menjadi Korps Lalu Lintas Polri (Korlantas Polri). Korlantas Polri berkedudukan langsung dibawah Kapolri, bertugas untuk membina dan menyelenggarakan fungsi Lalu Lintas meliputi pendidikan masyarakat, penegakkan hukum, pengkajian masalah lalu lintas, registrasi dan identifikasi pengemudi dan kendaraan bermotor serta patroli jalan raya.</p>', 'Sejarah Korlantas', 636, 1, 0, 2, '', '', NULL, '2016-10-22 20:58:48', '2016-10-22 20:58:48'),
(91, 'Profil', '<p><strong>Kedudukan, Tugas, dan Fungsi</strong><br />&nbsp;<br />I. Korlantas Polri merupakan unsur pelaksana tugas pokok yang berada di bawah Kapolri;<br />&nbsp;<br />II. Korlantas Polri bertugas:</p>\r\n<ol>\r\n<li>membina dan menyelenggarakan fungsi lalu lintas yang meliputi pendidikan masyarakat, penegakan hukum, pengkajian masalah lalu lintas, registrasi dan identifikasi pengemudi dan kendaraan bermotor serta patroli jalan raya;</li>\r\n<li>melaksanakan penertiban lalu lintas, manajemen operasional dan rekayasa lalu lintas (engineering );</li>\r\n<li>menyelenggarakan pusat Komunikasi, Koordinasi, Kendali dan Informasi (K3I) tentang lalu lintas;</li>\r\n<li>mengkoordinasikan pemangku kepentingan yang berkaitan dengan penyelenggaraan lalu lintas;</li>\r\n<li>memberikan rekomendasi dampak lalu lintas; dan</li>\r\n<li>melaksanakan koordinasi dan/atau pengawasan PPNS.</li>\r\n</ol>\r\n<p>&nbsp;<br />III. dalam melaksanakan tugas, Korlantas Polri menyelenggarakan fungsi:</p>\r\n<ol>\r\n<li>penyusunan kebijakan strategis yang berkaitan dengan peran dan fungsi polisi lalu lintas, perumusan dan atau pengembangan sistem dan metode termasuk petunjuk pelaksanaan fungsi lalu lintas, membangun kemitraan dan kerjasama baik dalam maupun luar negeri, serta menyelenggarakan koordinasi dengan pemangku kepentingan lainnya di bidang lalu lintas;</li>\r\n<li>pelaksanaan manajemen operasional lalu lintas yang meliputi kegiatan memelihara dan mewujudkan keamanan, keselamatan, ketertiban dan kelancaran lalu lintas di jalan raya, jalan tol, serta jalan-jalan luar kota sebagai penghubung (linking ping) antar kesatuan lalu lintas melalui kegiatan pengaturan, penjagaan, pengawalan, patroli, TPKP, Quick Respon Time, dan menjadi jejaring National Traffic Management Centre (NTMC).</li>\r\n<li>pengembangan sistem dan metode termasuk petunjuk pelaksanaan teknis penegakan hukum yang meliputi kegiatan penindakan terhadap pelanggaran aturan lalu lintas, penanganan kecelakaan lalu lintas, penyidikan kecelakaan lalu lintas, serta koordinasi dan pengawasan PPNS;</li>\r\n<li>pendidikan masyarakat dalam berlalu lintas, melalui kegiatan sosialisasi, penanaman nilai, membangun kesadaran, kepekaan, kepedulian akan tertib lalu lintas, serta pendidikan berlalu lintas secara formal dan informal;</li>\r\n<li>pembinaan teknis dan administrasi registrasi serta identifikasi pengemudi dan kendaraan bermotor yang meliputi kegiatan pengecekan administrasi dan fisik kendaraan serta pengujian kompetensi pengemudi untuk menjamin keabsahan dokumen kendaraan bermotor dan sarana kontrol dalam rangka penegakan hukum maupun untuk kepentingan forensik kepolisian;</li>\r\n<li>pengkajian bidang lalu lintas yang meliputi kegiatan keamanan dan keselamatan lalu lintas, pemetaan, inventarisasi, identifikasi wilayah, masalah maupun potensi-potensi yang berkaitan dengan lalu lintas dalam sistem Filling and Recording, baik untuk kepentingan internal maupun eksternal kepolisian, serta perumusan rekomendasi dampak lalu lintas; dan</li>\r\n<li>pelaksanaan operasional NTMC, yang meliputi kegiatan pengumpulan, pengolahan dan penyajian data lalu lintas, sebagai pusat kendali, koordinasi, komunikasi, dan informasi, pengembangan sistem dan teknologi informasi dan komunikasi lalu lintas, serta pelayanan informasi lalu lintas yang menyangkut pelanggaran dan kecelakaan lalu lintas dengan lingkup data jajaran Polri.</li>\r\n</ol>', 'Kedudukan, Tugas, dan Fungsi&nbsp;I. Korlantas Polri merupakan unsur pelaksana tugas pokok yang berada di bawah Kapolri;&nbsp;II. Korlantas Polri bertugas:\r\n\r\nmembina dan menyelenggarakan fungsi lalu lintas yang meliputi pendidikan masyarakat, penegakan hukum, pengkajian masalah lalu lintas, registrasi dan identifikasi pengemudi dan kendaraan bermotor serta patroli jalan raya;\r\nmelaksanakan penertiban lalu lintas, manajemen operasional dan rekayasa lalu lintas (engineering );\r\nmenyelenggarakan pusat Komunikasi, Koordinasi, Kendali dan Informasi (K3I) tentang lalu lintas;\r\nmengkoordinasikan pemangku kepentingan yang berkaitan dengan penyelenggaraan lalu lintas;\r\nmemberikan rekomendasi dampak lalu lintas; dan\r\nmelaksanakan koordinasi dan/atau pengawasan PPNS.\r\n\r\n&nbsp;III. dalam melaksanakan tugas, Korlantas Polri menyelenggarakan fungsi:\r\n\r\npenyusunan kebijakan strategis yang berkaitan dengan peran dan fungsi polisi lalu lintas, perumusan dan atau pengembangan sistem dan metode termasuk petunjuk pelaksanaan fungsi lalu lintas, membangun kemitraan dan kerjasama baik dalam maupun luar negeri, serta menyelenggarakan koordinasi dengan pemangku kepentingan lainnya di bidang lalu lintas;\r\npelaksanaan manajemen operasional lalu lintas yang meliputi kegiatan memelihara dan mewujudkan keamanan, keselamatan, ketertiban dan kelancaran lalu lintas di jalan raya, jalan tol, serta jalan-jalan luar kota sebagai penghubung (linking ping) antar kesatuan lalu lintas melalui kegiatan pengaturan, penjagaan, pengawalan, patroli, TPKP, Quick Respon Time, dan menjadi jejaring National Traffic Management Centre (NTMC).\r\npengembangan sistem dan metode termasuk petunjuk pelaksanaan teknis penegakan hukum yang meliputi kegiatan penindakan terhadap pelanggaran aturan lalu lintas, penanganan kecelakaan lalu lintas, penyidikan kecelakaan lalu lintas, serta koordinasi dan pengawasan PPNS;\r\npendidikan masyarakat dalam berlalu lintas, melalui kegiatan sosialisasi, penanaman nilai, membangun kesadaran, kepekaan, kepedulian akan tertib lalu lintas, serta pendidikan berlalu lintas secara formal dan informal;\r\npembinaan teknis dan administrasi registrasi serta identifikasi pengemudi dan kendaraan bermotor yang meliputi kegiatan pengecekan administrasi dan fisik kendaraan serta pengujian kompetensi pengemudi untuk menjamin keabsahan dokumen kendaraan bermotor dan sarana kontrol dalam rangka penegakan hukum maupun untuk kepentingan forensik kepolisian;\r\npengkajian bidang lalu lintas yang meliputi kegiatan keamanan dan keselamatan lalu lintas, pemetaan, inventarisasi, identifikasi wilayah, masalah maupun potensi-potensi yang berkaitan dengan lalu lintas dalam sistem Filling and Recording, baik untuk kepentingan internal maupun eksternal kepolisian, serta perumusan rekomendasi dampak lalu lintas; dan\r\npelaksanaan operasional NTMC, yang meliputi kegiatan pengumpulan, pengolahan dan penyajian data lalu lintas, sebagai pusat kendali, koordinasi, komunikasi, dan informasi, pengembangan sistem dan teknologi informasi dan komunikasi lalu lintas, serta pelayanan informasi lalu lintas yang menyangkut pelanggaran dan kecelakaan lalu lintas dengan lingkup data jajaran Polri.\r\n', 0, 1, 0, 2, '', '', '1477736656-Profil', '2016-10-29 03:24:16', '2016-10-29 03:24:16'),
(92, ' Bedanya Bank Syariah dan Bank Wakaf Ventura', '<p>Ikatan Cendekiawan Muslim Indonesia (ICMI) akan segera membentuk bank wakaf ventura. Lembaga ini pun nantinya akan dapat menampung dana wakaf untuk diberdayakan secara produktif. Namun meski memakai kata &lsquo;bank&rsquo;, lembaga ini sebenarnya berbentuk modal ventura.<br /><br />Kepala Departemen Pengawasan Industri Keuangan Non Bank II Otoritas Jasa Keuangan Achmad Buchori menuturkan, bentuk dari bank wakaf adalah modal ventura karena aturannya tidak ketat seperti bank. &ldquo;Modalnya tidak sebesar bank juga. Kalau ventura modal Rp 20 miliar sudah cukup,&rdquo; cetusnya.<br /><br />Pihaknya pun terus memberikan saran kepada tim pembentukan bank wakaf ventura. &ldquo;Katanya sudah siap tapi mereka belum mengajukan ijinnya, targetnya Maret diajukan. Kami berharap mereka bisa segera mengajukan sehingga kami bisa berikan ijinnya,&rdquo; tegas Buchori.<br /><br />Di sisi lain, Ketua Komite Bidang Integrasi, Inovasi dan Sinergi Industri Keuangan Syariah (Bank, Lembaga Pembiayaan, Pegadaian &amp; Lembaga Jasa Keuangan Lainnya) Masyarakat Ekonomi Syariah Imam T Saptono menjelaskan, berbeda dengan bank syariah yang hanya ditunjuk sebagai lembaga keuangan syariah penerima wakaf uang, bank wakaf ventura bisa bertindak sebagai nazhir wakaf. Sementara, bank syariah tidak bisa demikian.<br /><br />&ldquo;Bank syariah tidak pernah ditunjuk sebagai nazhir wakaf uang karena berdasar Undang-undang tidak boleh. Saya tidak tahu kenapa. Karena bank syariah tidak bisa, maka bank wakaf bentuknya modal ventura dan ini yang bertindak sebagai nazhir wakaf uang,&rdquo; paparnya.</p>\r\n<p>source : http://www.ekonomisyariah.org/6163/bedanya-bank-syariah-dan-bank-wakaf-ventura/</p>', 'Di sisi lain, Ketua Komite Bidang Integrasi, Inovasi dan Sinergi Industri Keuangan Syariah (Bank, Lembaga Pembiayaan, Pegadaian & Lembaga Jasa Keuangan Lainnya) Masyarakat Ekonomi Syariah Imam T Saptono menjelaskan, berbeda dengan bank syariah yang hanya ditunjuk sebagai lembaga keuangan syariah penerima wakaf uang, bank wakaf ventura bisa bertindak sebagai nazhir wakaf. Sementara, bank syariah tidak bisa demikian.', 657, 1, 0, 2, '', '', '1489039664--Bedanya-Bank-Syariah-dan-Bank-Wakaf-Ventura', '2017-03-08 23:07:44', '2017-03-08 23:07:44'),
(93, ' Arab Saudi Tertarik Berinvestasi Wisata Halal di Sumatra Barat', '<p>Raja Salman bin Abdulaziz Al Saud yang saat ini tengah berkunjung ke Indonesia dikabarkan tertarik dengan wisata halal di Sumatera Barat. Hal tersebut diungkapkan Menteri Pariwisata dan Ekonomi Kreatif &ndash; Arief Yahya, bahwa pihak Kerajaan Arab Saudi tertarik dengan pariwisata di Sumatera Barat sebagai destinasi wisata di Indonesia.<br /><br />&ldquo;Karena memang mereka sangat tertarik masuk (berinvestasi) pada sektor pariwisata di daerah itu (Sumbar),&rdquo; jelas Arief Yahya baru-baru ini di Jakarta.<br /><br />Menurut Arief, Sumatra Barat memiliki kekayaan wisata alam yang indah yang didukung dengan keunikan budayanya yang beragam. Salah satu tujuan wisata unggulan di provinsi ini misalnya Kawasan Mandeh yang disebut juga Raja Ampatnya Sumatera.<br /><br />Kunjungan kenegaraan Raja Salman dari Arab Saudi pada 1-9 Maret 2017 ini memang terkait dengan rencana investasi yang akan dilakukan pihak Kerajaan Arab Saudi di Indonesia. Kabarnya, rencana investasi dari Kerajaan Arab Saudi akan memakan biaya investasi sebesar 25 miliar USD. Nah, salah satu investasi Kerajaan Arab Saudi yang akan dikembangkan di Indonesia adalah di sektor pariwisata.<br /><br />Menurut Ketua Tim Percepatan Pengembangan Pariwisata Halal (TP3H) Kementerian Pariwisata &ndash; Riyanto Sofyan, jumlah investasi Kerajaan Arab Saudi pada sektor pariwisata di Indonesia diperkirakan akan mengkomposisi sampai 40% dari jumlah investasi keseluruhan. Hal tersebut dikarenakan investasi di sektor pariwisata akan lebih cepat hasilnya dibandingkan di sektor lainnya.<br /><br />Riyanto lalu menambahkan, bahwa provinsi Sumatra Barat memiliki prospek yang sangat bagus untuk pengembangan wisata halal berkerjasama dengan Kerajaan Arab Saudi tersebut, dengan potensi keindahan alam dan kekayaan budayanya.<br /><br />Belum lagi, Sumatra Barat juga sudah mempunyai nama di industri wisata halal dunia, dengan keberhasilan provinsi ini menyabet tiga penghargaan bergensi dalam ajang World Halal Tourism Award tahun 2016 lalu. Seperti kita telah ketahui, Sumatra Barat berhak atas gelar World&rsquo;s Best Halal Culinary Destination, World&rsquo;s Best Halal Destination dan World&rsquo;s Best Halal Tour Operator dalam World Halal Tourism Award 2016.<br /><br />Di ajang lokal pun, Sumatera Barat juga berhasil meraih empat penghargaan &ldquo;Kompetisi Pariwisata Halal Nasional&rdquo; (KPHN) oleh Kementerian Pariwisata Republik Indonesia, dengan memenangkan kategori destinasi wisata halal terbaik, kuliner halal terbaik, biro perjalanan wisata halal terbaik, dan restoran halal terbaik.</p>', 'Raja Salman bin Abdulaziz Al Saud yang saat ini tengah berkunjung ke Indonesia dikabarkan tertarik dengan wisata halal di Sumatera Barat. Hal tersebut diungkapkan Menteri Pariwisata dan Ekonomi Kreatif &ndash; Arief Yahya, bahwa pihak Kerajaan Arab Saudi tertarik dengan pariwisata di Sumatera Barat sebagai destinasi wisata di Indonesia.&ldquo;Karena memang mereka sangat tertarik masuk (berinvestasi) pada sektor pariwisata di daerah itu (Sumbar),&rdquo; jelas Arief Yahya baru-baru ini di Jakarta.Menurut Arief, Sumatra Barat memiliki kekayaan wisata alam yang indah yang didukung dengan keunikan budayanya yang beragam. Salah satu tujuan wisata unggulan di provinsi ini misalnya Kawasan Mandeh yang disebut juga Raja Ampatnya Sumatera.Kunjungan kenegaraan Raja Salman dari Arab Saudi pada 1-9 Maret 2017 ini memang terkait dengan rencana investasi yang akan dilakukan pihak Kerajaan Arab Saudi di Indonesia. Kabarnya, rencana investasi dari Kerajaan Arab Saudi akan memakan biaya investasi sebesar 25 miliar USD. Nah, salah satu investasi Kerajaan Arab Saudi yang akan dikembangkan di Indonesia adalah di sektor pariwisata.Menurut Ketua Tim Percepatan Pengembangan Pariwisata Halal (TP3H) Kementerian Pariwisata &ndash; Riyanto Sofyan, jumlah investasi Kerajaan Arab Saudi pada sektor pariwisata di Indonesia diperkirakan akan mengkomposisi sampai 40% dari jumlah investasi keseluruhan. Hal tersebut dikarenakan investasi di sektor pariwisata akan lebih cepat hasilnya dibandingkan di sektor lainnya.Riyanto lalu menambahkan, bahwa provinsi Sumatra Barat memiliki prospek yang sangat bagus untuk pengembangan wisata halal berkerjasama dengan Kerajaan Arab Saudi tersebut, dengan potensi keindahan alam dan kekayaan budayanya.Belum lagi, Sumatra Barat juga sudah mempunyai nama di industri wisata halal dunia, dengan keberhasilan provinsi ini menyabet tiga penghargaan bergensi dalam ajang World Halal Tourism Award tahun 2016 lalu. Seperti kita telah ketahui, Sumatra Barat berhak atas gelar World&rsquo;s Best Halal Culinary Destination, World&rsquo;s Best Halal Destination dan World&rsquo;s Best Halal Tour Operator dalam World Halal Tourism Award 2016.Di ajang lokal pun, Sumatera Barat juga berhasil meraih empat penghargaan &ldquo;Kompetisi Pariwisata Halal Nasional&rdquo; (KPHN) oleh Kementerian Pariwisata Republik Indonesia, dengan memenangkan kategori destinasi wisata halal terbaik, kuliner halal terbaik, biro perjalanan wisata halal terbaik, dan restoran halal terbaik.', 658, 1, 0, 2, '', '', '1489072695--Arab-Saudi-Tertarik-Berinvestasi-Wisata-Halal-di-Sumatra-Barat', '2017-03-09 08:18:15', '2017-03-09 08:18:15'),
(94, ' Zakat Sumbang 20 Persen Pengentasan Kemiskinan', '<p>Potensi zakat di seluruh dunia berjumlah ribuan triliun. Sekretaris Jenderal World Zakat Forum Ahmad Juwaini mengatakan, potensi zakat di seluruh dunia tercatat sebesar Rp 6000 triliun. &ldquo;Jumlahnya hampir tiga kali dari total APBN Indonesia, jadi potensi zakat sangat besar di seluruh dunia,&rdquo; tukasnya.<br /><br />Zakat pun telah menjadi salah satu instrumen pengentasan kemiskinan dan membantu mengatasi permasalahan sosial ekonomi. &ldquo;Dana zakat menyumbangkan kemampuan mengatasi kemiskinan sebesar 20 persen jadi kalau digunakan di berbagai negara ini akan luar biasa,&rdquo; cetus pria yang juga menjadi Ketua Komite Bidang Zakat, CSR dan Bisnis Sosial MES ini.<br /><br />Ia melanjutkan, banyak masyarakat miskin mayoritas berasal dari negara muslim. Di sisi lain, ada pula negara mayoritas muslim yang kaya. Oleh karena itu, ia pun mendorong hadirnya distribusi zakat antarnegara. &ldquo;Banyak negara muslim di Afrika yang masyarakatnya miskin, di sisi lain negara petrodolar juga kaya. Kalau ada proses distribusi zakat antaranegara ini akan sangat luar biasa,&rdquo; jelas Ahmad.<br /><br />Indonesia sendiri memiliki potensi zakat hingga Rp 217 triliun. Namun, pada akhir 2016 dana zakat yang terhimpun melalui lembaga amil zakat resmi baru sekitar Rp 5 triliun. Di tahun ini Badan Amil Zakat Nasional pun menargetkan penghimpunan zakat dapat naik sebesar 20 persen.<br /><br />Pada 2017 penghimpunan zakat secara nasional yang dilakukan oleh seluruh organisasi pengelola zakat resmi di Indonesia ditargetkan mencapai Rp 6 triliun. Jumlah tersebut diharapkan dapat mengentaskan 1 persen penduduk miskin di Indonesia, atau sekitar 280 ribu jiwa.</p>\r\n<p>source : http://www.ekonomisyariah.org/6164/zakat-sumbang-20-persen-pengentasan-kemiskinan/</p>', 'Pada 2017 penghimpunan zakat secara nasional yang dilakukan oleh seluruh organisasi pengelola zakat resmi di Indonesia ditargetkan mencapai Rp 6 triliun. Jumlah tersebut diharapkan dapat mengentaskan 1 persen penduduk miskin di Indonesia, atau sekitar 280 ribu jiwa.', 659, 1, 0, 2, '', '', '1489113190--Zakat-Sumbang-20-Persen-Pengentasan-Kemiskinan', '2017-03-09 19:33:10', '2017-03-09 19:33:10');

-- --------------------------------------------------------

--
-- Table structure for table `ms_privilige`
--

CREATE TABLE `ms_privilige` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_privilige`
--

INSERT INTO `ms_privilige` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'member');

-- --------------------------------------------------------

--
-- Table structure for table `ms_products`
--

CREATE TABLE `ms_products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ms_media_id` int(11) NOT NULL,
  `ms_categories_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_products`
--

INSERT INTO `ms_products` (`id`, `name`, `description`, `ms_media_id`, `ms_categories_id`) VALUES
(1, 'Leveluk SD 501', 'With the strongest electrolysis chamber available, fully-equipped with a built-in electrolysis chamber, and featuring a large LCD panel and clear voice prompts, the SD501 is the leader of the pack. 7-Platinum-plated Titanium plates comprise the electrolysis chamber.', 467, 2),
(2, 'Leveluk SUPER 501', 'This highly productive machine has 7 and 5 electrode plates, a twin hose system, industry leading cleaning system, a built-in tank for the electrolysis enhancer, and a water pressure regulating function making this machine a must for the large family!', 468, 2),
(3, 'Leveluk JRII', 'The JRII has three solid electrode plates which reduces the energy consumption. Due to the solid platinum-coated titanium plates, this unit can still produce the strong acid and strong Kangen waters. Due to the lower power consumption and fewer plates, this machine is only recommended for singles or couples. Families should consider one of the more robust models above due to the amount of output required.', 469, 2),
(4, 'Leveluk SD501 Platinum', 'Fully-equipped with a built-in electrolysis chamber, and featuring a large LCD panel and clear voice prompts, the SD501 PLATINUM is the leader of the pack. 7-Platinum-plated Titanium plates comprise the electrolysis chamber.', 470, 2),
(5, 'Leveluk R', 'Enagic prides itself as the GLOBAL LEADER in water ionization devices. In fact, Enagic single-handedly created the global market, and stands as the DOMINANT PLAYER. Now, YOU too can take advantage of Enagic\'s respect for ultimate quality, matched with a simple and effective entry model into the delights of water ionization. Deliver fresh, crisp, healthy Kangen Water® at the push of a button with our all-new-designed Leveluk R model.', 471, 2),
(6, 'Leveluk K8', 'The Kangen® 8 is now Enagic®\'s most powerful antioxidant machine - featuring 8 platinum-dipped titanium plates! This additional electrode plate increases the electrolysis surface area, improves water ionization, and heightens the antioxidant production potential. ', 526, 2),
(8, 'Anespa', 'Enjoy the soothing feeling of being in a Hot Spring Resort everyday!  Take pleasure in the relaxing effects of ANESPA\'s mineral-ion water. Ensures removal of virtually 100% of residual chlorine. The cartridge purification system effectively removes chlorine and bacteria, leaving you with a fresh, invigorating feeling.', 473, 2),
(9, 'Baju', 'Merchandise baju', 483, 1),
(10, 'Polo Shirt', 'Polo shirt gemar sehati', 483, 1),
(11, 'Mug', 'Mug Gemarsehati', 483, 1),
(12, 'Stiker', 'stiker gemar sehati', 483, 1),
(13, 'Botol', 'Botol Gemarsehati', 483, 1),
(14, 'Topi', 'topi gemarsehati', 483, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_product_categories`
--

CREATE TABLE `ms_product_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_product_categories`
--

INSERT INTO `ms_product_categories` (`id`, `name`, `description`) VALUES
(1, 'Merchandise', 'Merchandise desc'),
(2, 'Leveluk', 'Leveluk desc');

-- --------------------------------------------------------

--
-- Table structure for table `ms_provinces`
--

CREATE TABLE `ms_provinces` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_provinces`
--

INSERT INTO `ms_provinces` (`id`, `code`, `name`) VALUES
(1, NULL, 'Bali'),
(2, NULL, 'Bangka Belitung'),
(3, NULL, 'Banten'),
(4, NULL, 'Bengkulu'),
(5, NULL, 'DI Yogyakarta'),
(6, NULL, 'DKI Jakarta'),
(7, NULL, 'Gorontalo'),
(8, NULL, 'Jambi'),
(9, NULL, 'Jawa Barat'),
(10, NULL, 'Jawa Tengah'),
(11, NULL, 'Jawa Timur'),
(12, NULL, 'Kalimantan Barat'),
(13, NULL, 'Kalimantan Selatan'),
(14, NULL, 'Kalimantan Tengah'),
(15, NULL, 'Kalimantan Timur'),
(16, NULL, 'Kalimantan Utara'),
(17, NULL, 'Kepulauan Riau'),
(18, NULL, 'Lampung'),
(19, NULL, 'Maluku'),
(20, NULL, 'Maluku Utara'),
(21, NULL, 'Nanggroe Aceh Darussalam (NAD)'),
(22, NULL, 'Nusa Tenggara Barat (NTB)'),
(23, NULL, 'Nusa Tenggara Timur (NTT)'),
(24, NULL, 'Papua'),
(25, NULL, 'Papua Barat'),
(26, NULL, 'Riau'),
(27, NULL, 'Sulawesi Barat'),
(28, NULL, 'Sulawesi Selatan'),
(29, NULL, 'Sulawesi Tengah'),
(30, NULL, 'Sulawesi Tenggara'),
(31, NULL, 'Sulawesi Utara'),
(32, NULL, 'Sumatera Barat'),
(33, NULL, 'Sumatera Selatan'),
(34, NULL, 'Sumatera Utara');

-- --------------------------------------------------------

--
-- Table structure for table `ms_rss`
--

CREATE TABLE `ms_rss` (
  `id` int(10) UNSIGNED NOT NULL,
  `ms_user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_rss`
--

INSERT INTO `ms_rss` (`id`, `ms_user_id`, `title`, `link`, `featured`, `created_at`, `updated_at`) VALUES
(2, 0, 'Antisipasi Gangguan di Masa Pilkada, Brimob Tetapkan Status Siaga I', 'http://news.detik.com/berita/d-3332500/antisipasi-gangguan-di-masa-pilkada-brimob-tetapkan-status-siaga-i', '1', '2016-10-29 03:37:49', '2016-10-29 03:37:49');

-- --------------------------------------------------------

--
-- Table structure for table `ms_sliders`
--

CREATE TABLE `ms_sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ms_media_id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `h3` text COLLATE utf8_unicode_ci NOT NULL,
  `h2` text COLLATE utf8_unicode_ci NOT NULL,
  `h1` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_sliders`
--

INSERT INTO `ms_sliders` (`id`, `title`, `ms_media_id`, `link`, `h3`, `h2`, `h1`, `description`, `created_at`, `updated_at`) VALUES
(8, '', 649, '', 'demi membangun Negara Kesatuan Republik Indonesia', 'dan Tuntunan', 'Untu Kemajuan', '', '2016-10-27 02:07:01', '2017-03-05 22:50:54'),
(9, '', 648, '', '', 'Komunitas Ekonomi Syariah Surabaya', 'KESS', '', '2016-10-27 02:07:51', '2017-03-09 04:35:04');

-- --------------------------------------------------------

--
-- Table structure for table `ms_soal`
--

CREATE TABLE `ms_soal` (
  `id` int(11) NOT NULL,
  `ms_ujian_id` int(11) NOT NULL,
  `ms_media_id` int(11) DEFAULT NULL,
  `pertanyaan` text NOT NULL,
  `jawaban_a` text NOT NULL,
  `jawaban_b` text NOT NULL,
  `jawaban_c` text NOT NULL,
  `jawaban_d` text NOT NULL,
  `jawaban_benar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_soal`
--

INSERT INTO `ms_soal` (`id`, `ms_ujian_id`, `ms_media_id`, `pertanyaan`, `jawaban_a`, `jawaban_b`, `jawaban_c`, `jawaban_d`, `jawaban_benar`) VALUES
(3, 5, 0, 'Berapakah 1+1?', '1', '2', '3', '4', 'b'),
(4, 5, 0, 'Berapakah 1x1?', '1', '2', '3', '4', 'a'),
(5, 2, 647, 'Gambar mobil apa kah itu?', 'mobil-mobilan', 'mobil tentara', 'mobil ambulans', 'mobil polisi', 'd'),
(6, 2, 0, 'Aku adalah ...', 'kamu', 'dia', 'mereka', 'aku', 'd');

-- --------------------------------------------------------

--
-- Table structure for table `ms_socials`
--

CREATE TABLE `ms_socials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ms_status_posts`
--

CREATE TABLE `ms_status_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_status_posts`
--

INSERT INTO `ms_status_posts` (`id`, `title`) VALUES
(1, 'draft'),
(2, 'published');

-- --------------------------------------------------------

--
-- Table structure for table `ms_support_cities`
--

CREATE TABLE `ms_support_cities` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_support_cities`
--

INSERT INTO `ms_support_cities` (`id`, `city_id`, `name`) VALUES
(1, 14, 'Ambon'),
(2, 19, 'Balikpapan'),
(3, 20, 'Banda Aceh'),
(4, 21, 'Bandar Lampung'),
(5, 22, 'Bandung'),
(6, 23, 'Bandung'),
(7, 24, 'Bandung Barat'),
(8, 31, 'Bangkalan'),
(9, 33, 'Banjar'),
(10, 34, 'Banjar'),
(11, 35, 'Banjarbaru'),
(12, 36, 'Banjarmasin'),
(13, 37, 'Banjarnegara'),
(14, 39, 'Bantul'),
(15, 42, 'Banyuwangi'),
(16, 48, 'Batam'),
(17, 54, 'Bekasi'),
(18, 55, 'Bekasi'),
(19, 62, 'Bengkulu'),
(20, 63, 'Bengkulu Selatan'),
(21, 64, 'Bengkulu Tengah'),
(22, 65, 'Bengkulu Utara'),
(23, 67, 'Biak Numfor'),
(24, 70, 'Binjai'),
(25, 75, 'Blitar'),
(26, 76, 'Blora'),
(27, 78, 'Bogor'),
(28, 79, 'Bogor'),
(29, 80, 'Bojonegoro'),
(30, 86, 'Bondowoso'),
(31, 89, 'Bontang'),
(32, 91, 'Boyolali'),
(33, 92, 'Brebes'),
(34, 93, 'Bukittinggi'),
(35, 95, 'Bulukumba'),
(36, 103, 'Ciamis'),
(37, 104, 'Cianjur'),
(38, 105, 'Cilacap'),
(39, 106, 'Cilegon'),
(40, 107, 'Cimahi'),
(41, 108, 'Cirebon'),
(42, 109, 'Cirebon'),
(43, 114, 'Denpasar'),
(44, 115, 'Depok'),
(45, 120, 'Dumai'),
(46, 126, 'Garut'),
(47, 128, 'Gianyar'),
(48, 129, 'Gorontalo'),
(49, 130, 'Gorontalo'),
(50, 131, 'Gorontalo Utara'),
(51, 133, 'Gresik'),
(52, 149, 'Indramayu'),
(53, 151, 'Jakarta Barat'),
(54, 152, 'Jakarta Pusat'),
(55, 153, 'Jakarta Selatan'),
(56, 154, 'Jakarta Timur'),
(57, 155, 'Jakarta Utara'),
(58, 156, 'Jambi'),
(59, 157, 'Jayapura'),
(60, 160, 'Jember'),
(61, 163, 'Jepara'),
(62, 164, 'Jombang'),
(63, 169, 'Karanganyar'),
(64, 171, 'Karawang'),
(65, 177, 'Kebumen'),
(66, 178, 'Kediri'),
(67, 179, 'Kediri'),
(68, 182, 'Kendari'),
(69, 195, 'Ketapang'),
(70, 196, 'Klaten'),
(71, 203, 'Kotabaru'),
(72, 204, 'Kotamobagu'),
(73, 209, 'Kudus'),
(74, 212, 'Kupang'),
(75, 213, 'Kupang'),
(76, 220, 'Lahat'),
(77, 222, 'Lamongan'),
(78, 230, 'Langsa'),
(79, 235, 'Lhokseumawe'),
(80, 242, 'Lubuk Linggau'),
(81, 243, 'Lumajang'),
(82, 247, 'Madiun'),
(83, 248, 'Madiun'),
(84, 249, 'Magelang'),
(85, 250, 'Magelang'),
(86, 251, 'Magetan'),
(87, 252, 'Majalengka'),
(88, 255, 'Malang'),
(89, 256, 'Malang'),
(90, 265, 'Mamuju'),
(91, 266, 'Mamuju Utara'),
(92, 267, 'Manado'),
(93, 276, 'Mataram'),
(94, 278, 'Medan'),
(95, 283, 'Metro'),
(96, 289, 'Mojokerto'),
(97, 290, 'Mojokerto'),
(98, 292, 'Muara Enim'),
(99, 305, 'Nganjuk'),
(100, 306, 'Ngawi'),
(101, 318, 'Padang'),
(102, 321, 'Padang Panjang'),
(103, 326, 'Palangka Raya'),
(104, 327, 'Palembang'),
(105, 328, 'Palopo'),
(106, 329, 'Palu'),
(107, 330, 'Pamekasan'),
(108, 331, 'Pandeglang'),
(109, 334, 'Pangkal Pinang'),
(110, 336, 'Parepare'),
(111, 337, 'Pariaman'),
(112, 342, 'Pasuruan'),
(113, 343, 'Pasuruan'),
(114, 344, 'Pati'),
(115, 345, 'Payakumbuh'),
(116, 348, 'Pekalongan'),
(117, 349, 'Pekalongan'),
(118, 350, 'Pekanbaru'),
(119, 352, 'Pemalang'),
(120, 353, 'Pematang Siantar'),
(121, 363, 'Ponorogo'),
(122, 364, 'Pontianak'),
(123, 365, 'Pontianak'),
(124, 367, 'Prabumulih'),
(125, 369, 'Probolinggo'),
(126, 370, 'Probolinggo'),
(127, 375, 'Purbalingga'),
(128, 376, 'Purwakarta'),
(129, 377, 'Purworejo'),
(130, 386, 'Salatiga'),
(131, 387, 'Samarinda'),
(132, 391, 'Sanggau'),
(133, 398, 'Semarang'),
(134, 399, 'Semarang'),
(135, 402, 'Serang'),
(136, 403, 'Serang'),
(137, 409, 'Sidoarjo'),
(138, 415, 'Singkawang'),
(139, 417, 'Sintang'),
(140, 418, 'Situbondo'),
(141, 420, 'Solok'),
(142, 421, 'Solok'),
(143, 422, 'Solok Selatan'),
(144, 424, 'Sorong'),
(145, 425, 'Sorong'),
(146, 426, 'Sorong Selatan'),
(147, 427, 'Sragen'),
(148, 428, 'Subang'),
(149, 430, 'Sukabumi'),
(150, 431, 'Sukabumi'),
(151, 433, 'Sukoharjo'),
(152, 438, 'Sumbawa'),
(153, 439, 'Sumbawa Barat'),
(154, 440, 'Sumedang'),
(155, 441, 'Sumenep'),
(156, 442, 'Sungaipenuh'),
(157, 444, 'Surabaya'),
(158, 445, 'Surakarta (Solo)'),
(159, 447, 'Tabanan'),
(160, 455, 'Tangerang'),
(161, 456, 'Tangerang'),
(162, 457, 'Tangerang Selatan'),
(163, 462, 'Tanjung Pinang'),
(164, 467, 'Tarakan'),
(165, 468, 'Tasikmalaya'),
(166, 469, 'Tasikmalaya'),
(167, 470, 'Tebing Tinggi'),
(168, 472, 'Tegal'),
(169, 473, 'Tegal'),
(170, 476, 'Temanggung'),
(171, 489, 'Tuban'),
(172, 492, 'Tulungagung'),
(173, 497, 'Wonogiri'),
(174, 498, 'Wonosobo'),
(175, 501, 'Yogyakarta');

-- --------------------------------------------------------

--
-- Table structure for table `ms_tags`
--

CREATE TABLE `ms_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `ms_user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_tags`
--

INSERT INTO `ms_tags` (`id`, `title`, `description`, `ms_user_id`, `created_at`, `updated_at`) VALUES
(20, 'syariah', '', 0, '2016-10-17 08:16:09', '2016-10-17 08:16:09'),
(21, 'ekonomi', '', 0, '2016-10-17 08:57:14', '2016-10-17 08:57:14'),
(22, 'Sambutan', '', 0, '2016-10-18 01:31:11', '2016-10-18 01:31:11'),
(23, 'koperasi', '', 0, '2016-10-18 01:58:38', '2016-10-18 01:58:38'),
(24, 'surabaya', '', 0, '2016-10-18 02:00:12', '2016-10-18 02:00:12'),
(25, 'mandiri', '', 0, '2016-10-18 02:00:21', '2016-10-18 02:00:21'),
(26, 'islam', '', 0, '2016-10-18 02:01:40', '2016-10-18 02:01:40'),
(27, 'ekonomi islam', '', 0, '2016-10-18 02:02:39', '2016-10-18 02:02:39'),
(28, 'bebas riba', '', 0, '2016-10-18 02:02:46', '2016-10-18 02:02:46'),
(29, 'usaha umat', '', 0, '2016-10-22 20:57:15', '2016-10-22 20:57:15'),
(30, 'muslim mandiri', '', 0, '2016-10-22 21:06:23', '2016-10-22 21:06:23'),
(31, 'kess', '', 0, '2016-10-22 21:10:54', '2016-10-22 21:10:54'),
(32, 'komunitas', '', 0, '2016-10-22 21:14:55', '2016-10-22 21:14:55'),
(33, 'syar\'i', '', 0, '2016-10-22 21:16:10', '2016-10-22 21:16:10');

-- --------------------------------------------------------

--
-- Table structure for table `ms_themes`
--

CREATE TABLE `ms_themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ms_themes`
--

INSERT INTO `ms_themes` (`id`, `code`, `title`, `description`, `active`, `created_at`, `updated_at`) VALUES
(2, 'kess', 'kessv1', '', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ms_type_club`
--

CREATE TABLE `ms_type_club` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_type_club`
--

INSERT INTO `ms_type_club` (`id`, `name`, `code`) VALUES
(2, 'Core Leader', 'core-leader'),
(3, 'Double executive', 'double-executive'),
(4, 'Club Leader', 'club-leader'),
(5, 'Executive Leader', 'executive-leader'),
(6, 'Smartphone', 'phone'),
(7, 'notebook', 'laptop'),
(8, 'umroh', 'umroh'),
(9, 'trip', 'trip-bangkok'),
(10, 'Toyota-agya', 'agya');

-- --------------------------------------------------------

--
-- Table structure for table `ms_ujian`
--

CREATE TABLE `ms_ujian` (
  `id` int(11) NOT NULL,
  `ms_media_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `code` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_ujian`
--

INSERT INTO `ms_ujian` (`id`, `ms_media_id`, `title`, `description`, `code`, `created_at`, `updated_at`) VALUES
(2, 656, 'AL Qaulul Mufid ala Kitabit Tauhid', 'Untuk informasi kajian, silahkan kirim WhatsApp dengan format : MH, NAMA, IKHWAN/AKHWAT ke 081317889282', '2016-sim-a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 655, 'Massal Jahiliyah', 'Untuk informasi kajian, silahkan kirim WhatsApp dengan format : MH, NAMA, IKHWAN/AKHWAT ke 081317889282', '2016-sim-b1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 654, 'Pintu Setan', 'Untuk informasi kajian, silahkan kirim WhatsApp dengan format : MH, NAMA, IKHWAN/AKHWAT ke 081317889282', '2016-sim-b2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 653, 'Malaikatnya Org Beriman', 'Untuk informasi kajian, silahkan kirim WhatsApp dengan format : MH, NAMA, IKHWAN/AKHWAT ke 081317889282', '2016-sim-c', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('alvarisi@live.com', '86b06590850c38d3f305ecefa903eb76f2456b1e645566d086003c8f40a7df24', '2015-05-19 10:09:44');

-- --------------------------------------------------------

--
-- Table structure for table `tr_achievment`
--

CREATE TABLE `tr_achievment` (
  `id` int(11) NOT NULL,
  `ms_leaders_id` int(11) NOT NULL,
  `ms_achievement_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_category_posts`
--

CREATE TABLE `tr_category_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `ms_post_id` int(11) NOT NULL,
  `ms_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tr_category_posts`
--

INSERT INTO `tr_category_posts` (`id`, `ms_post_id`, `ms_category_id`) VALUES
(75, 34, 3),
(147, 32, 3),
(193, 51, 5),
(202, 55, 3),
(220, 40, 3),
(222, 56, 3),
(227, 58, 7),
(228, 59, 7),
(234, 62, 3),
(236, 64, 3),
(249, 54, 3),
(250, 50, 9),
(255, 63, 3),
(271, 52, 5),
(299, 77, 3),
(304, 83, 3),
(305, 84, 3),
(311, 91, 3),
(312, 78, 15),
(313, 92, 23),
(314, 93, 1),
(315, 93, 23),
(316, 94, 1),
(317, 94, 23);

-- --------------------------------------------------------

--
-- Table structure for table `tr_comments`
--

CREATE TABLE `tr_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `ms_post_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `enabled` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tr_detail_portfolios`
--

CREATE TABLE `tr_detail_portfolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ms_media_id` int(11) NOT NULL,
  `ms_portfolio_id` int(11) NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tr_detail_portfolios`
--

INSERT INTO `tr_detail_portfolios` (`id`, `type`, `ms_media_id`, `ms_portfolio_id`, `caption`, `description`, `link`) VALUES
(307, '1', 649, 1, 'Kegiatan KESS 1', '', ''),
(308, '1', 648, 1, 'Kegiatan KESS 2', '', ''),
(309, '1', 651, 1, 'Kegiatan KESS 3', '', ''),
(324, '1', 652, 1, 'Kegiatan KESS 4', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_logs`
--

CREATE TABLE `tr_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ms_user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `logable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logable_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tr_logs`
--

INSERT INTO `tr_logs` (`id`, `ms_user_id`, `name`, `action`, `date`, `logable_type`, `logable_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Detik Finance', 'insert', '2015-04-26 19:54:17', 'App\\Rss', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 'News', 'update', '2015-04-26 19:54:34', 'App\\MenuDetail', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 'News', 'update', '2015-04-26 19:56:46', 'App\\MenuDetail', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 'cek', 'insert', '2015-04-26 21:17:31', 'App\\Portfolio', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 'Small Medium Enterprise', 'update', '2015-04-26 21:17:46', 'App\\Portfolio', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 1, 'Small Medium Enterprise', 'update', '2015-04-26 21:18:24', 'App\\Portfolio', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1, 'baru', 'insert', '2015-04-26 21:20:59', 'App\\MenuDetail', 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 1, 'Small Medium Enterprise', 'update', '2015-04-26 21:21:26', 'App\\Portfolio', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 1, 'Agrobisnis', 'update', '2015-04-26 21:36:35', 'App\\Portfolio', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 1, 'cek', 'delete', '2015-04-26 21:37:49', 'App\\DetailPortfolio', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 1, 'cek', 'delete', '2015-04-26 21:42:17', 'App\\DetailPortfolio', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 1, 'cek', 'delete', '2015-04-26 21:48:37', 'App\\Portfolio', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 1, 'Portofolio Investment', 'insert', '2015-04-26 21:49:08', 'App\\Portfolio', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 1, 'Small Medium Enterprise', 'update', '2015-04-26 21:49:16', 'App\\Portfolio', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 1, 'Agrobisnis', 'update', '2015-04-26 21:49:22', 'App\\Portfolio', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 1, 'Financial', 'update', '2015-04-26 21:49:28', 'App\\Portfolio', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 1, 'Property', 'update', '2015-04-26 21:49:33', 'App\\Portfolio', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 1, 'Portofolio Investment', 'update', '2015-04-26 21:50:49', 'App\\MenuDetail', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 1, 'History', 'update', '2015-04-28 19:59:37', 'App\\Post', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 1, '', 'update', '2015-04-28 20:01:14', 'App\\Media', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 1, 'img_20150418_224834.JPG', 'delete', '2015-04-28 20:01:40', 'App\\Media', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 1, 'yosh', 'update', '2015-04-28 20:01:59', 'App\\Media', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 1, 'gant', 'update', '2015-04-28 20:02:12', 'App\\Media', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 1, '', 'update', '2015-04-28 20:02:30', 'App\\Media', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 1, '', 'update', '2015-04-28 20:02:51', 'App\\Media', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 1, '', 'update', '2015-04-28 20:03:04', 'App\\Media', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 1, '', 'update', '2015-04-28 20:03:16', 'App\\Media', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 1, 'img_20150418_224151.jpg - img_20150428_201404.jpg', 'update', '2015-04-28 20:04:14', 'App\\Media', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 1, '', 'update', '2015-04-28 20:04:34', 'App\\Media', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 1, '', 'update', '2015-04-28 20:04:59', 'App\\Media', 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 1, '', 'update', '2015-04-28 20:05:19', 'App\\Media', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 1, '', 'update', '2015-04-28 20:05:40', 'App\\Media', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 1, 'img_20150418_232318.jpg', 'delete', '2015-04-28 20:05:47', 'App\\Media', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 1, 'History', 'update', '2015-04-28 20:06:41', 'App\\Post', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 1, 'Management PT. Hasanah Mulia Investama', 'update', '2015-04-28 20:07:09', 'App\\Post', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 1, 'About Us', 'update', '2015-04-28 20:45:34', 'App\\Post', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 1, 'img_20150428_203646.jpg', 'insert', '2015-04-28 20:46:36', 'App\\Media', 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 1, 'img_20150428_205647.png', 'insert', '2015-04-28 20:47:56', 'App\\Media', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 1, 'Founder PT. Hasanah Mulia Investama', 'update', '2015-04-28 20:48:45', 'App\\Post', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 1, 'Founder PT. Hasanah Mulia Investama', 'update', '2015-04-28 20:56:22', 'App\\Post', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 1, 'About Us', 'update', '2015-04-28 20:56:41', 'App\\Post', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 1, 'Contact Us', 'update', '2015-04-28 20:56:54', 'App\\Post', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 1, 'Founder PT. Hasanah Mulia Investama', 'update', '2015-04-28 20:58:22', 'App\\Post', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 1, 'Founder PT. Hasanah Mulia Investama', 'update', '2015-04-28 21:00:42', 'App\\Post', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 1, 'Founder PT. Hasanah Mulia Investama', 'update', '2015-04-28 21:03:57', 'App\\Post', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 1, 'Legalitas Perusahaan', 'insert', '2015-04-29 14:46:59', 'App\\Post', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 1, 'Legalitas Perusahaan', 'update', '2015-04-29 14:49:55', 'App\\Post', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 1, 'Legalitas Perusahaan', 'update', '2015-04-29 14:50:13', 'App\\Post', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 1, 'Legalitas Perusahaan', 'insert', '2015-04-29 14:50:47', 'App\\Menu', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2305, 1, ' Zakat Sumbang 20 Persen Pengentasan Kemiskinan', 'insert', '2017-03-10 02:33:10', 'App\\Post', 94, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tr_media_posts`
--

CREATE TABLE `tr_media_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `ms_post_id` int(11) NOT NULL,
  `ms_media_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tr_media_posts`
--

INSERT INTO `tr_media_posts` (`id`, `ms_post_id`, `ms_media_id`) VALUES
(1, 0, 0),
(2, 82, 635),
(3, 81, 634),
(4, 80, 633),
(5, 79, 632);

-- --------------------------------------------------------

--
-- Table structure for table `tr_menu_details`
--

CREATE TABLE `tr_menu_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `ms_menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `ms_menu_type_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tr_menu_details`
--

INSERT INTO `tr_menu_details` (`id`, `ms_menu_id`, `parent_id`, `ms_menu_type_id`, `order`, `title`, `custom`, `code`) VALUES
(45, 6, 0, 5, 2, '4Jovem TV', '0', 'tivi'),
(46, 6, 0, 1, 1, '4Jovem Event', '2', 'event'),
(48, 4, 0, 5, 1, 'Facebook', 'http://facebook.com/gemarsehati', 'fanspage'),
(49, 4, 0, 5, 1, 'Twitter', 'http://twitter.com/Gemarsehatiteam', 'twitter'),
(70, 3, 0, 5, 5, 'Core Leader', '/member/core-leader', 'leader-core'),
(94, 4, 0, 5, 1, 'Instagram', 'http://instagram.com/gemarsehati', 'instagram'),
(106, 1, 0, 1, 1, 'Tentang Kami', '3', 'tentang'),
(110, 1, 106, 2, 1, 'Profil', '91', 'profil'),
(121, 1, 0, 1, 4, 'Media', '3', 'media'),
(122, 1, 121, 9, 2, 'Galeri', '1', 'galeri'),
(123, 1, 121, 5, 3, 'Statistik', 'http://dummy.link', 'statistik'),
(127, 1, 0, 5, 6, 'Tautan', 'https://www.polri.go.id/', 'tautan'),
(140, 1, 0, 1, 1, 'Berita', '1', 'news'),
(141, 1, 140, 1, 2, 'Berita', '1', 'berita'),
(143, 1, 0, 5, 1, 'Hubungi Kami', 'http://localhost/korlantas_mgm/contact', 'hubungi_kami'),
(145, 1, 0, 7, 1, 'Lokasi Koperasi', '', 'koperasi');

-- --------------------------------------------------------

--
-- Table structure for table `tr_post_tags`
--

CREATE TABLE `tr_post_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `ms_post_id` int(11) NOT NULL,
  `ms_tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tr_post_tags`
--

INSERT INTO `tr_post_tags` (`id`, `ms_post_id`, `ms_tag_id`) VALUES
(395, 77, 21),
(407, 83, 29),
(408, 84, 29),
(415, 78, 22);

-- --------------------------------------------------------

--
-- Table structure for table `tr_theme_menus`
--

CREATE TABLE `tr_theme_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `ms_theme_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tr_theme_menus`
--

INSERT INTO `tr_theme_menus` (`id`, `ms_theme_id`, `code`) VALUES
(1, 1, 'top_menu'),
(2, 1, 'user_menu_1'),
(3, 1, 'user_menu_2'),
(4, 1, 'user_menu_3'),
(5, 1, 'user_menu_4'),
(6, 1, 'user_menu_5'),
(7, 2, 'top_menu'),
(8, 2, 'user_menu_1'),
(9, 2, 'user_menu_2'),
(10, 2, 'user_menu_3');

-- --------------------------------------------------------

--
-- Table structure for table `tr_user_detail`
--

CREATE TABLE `tr_user_detail` (
  `id` int(11) NOT NULL,
  `referal` varchar(255) NOT NULL,
  `enagic_id` varchar(255) NOT NULL,
  `custom` varchar(255) NOT NULL,
  `jenis_mesin` varchar(255) NOT NULL,
  `sn_mesin` varchar(255) NOT NULL,
  `panggilan` varchar(255) NOT NULL,
  `notelp` varchar(255) NOT NULL,
  `nohp` varchar(255) NOT NULL,
  `pin_bb` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `kodepos` varchar(255) NOT NULL,
  `ms_gender_id` int(11) NOT NULL,
  `ms_city_id` int(11) NOT NULL,
  `ms_media_id` int(11) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_user_detail`
--

INSERT INTO `tr_user_detail` (`id`, `referal`, `enagic_id`, `custom`, `jenis_mesin`, `sn_mesin`, `panggilan`, `notelp`, `nohp`, `pin_bb`, `address`, `kodepos`, `ms_gender_id`, `ms_city_id`, `ms_media_id`, `facebook`, `twitter`, `latitude`, `longitude`) VALUES
(3, '', '1064209', 'Yussa', 'Leveluk SD 501', '', 'Yussa', '085234056590', '08113545456', '', 'Jl. Medokan Asri Barat X No 17 Rungkut', '60286', 1, 444, 562, 'facebook.com/gemarsehati', '', '-7.3294016', '112.7906533'),
(4, '', '1069547', 'Panca', 'Leveluk SD 501', '', 'Panca', '0315328736', '085312345959', '', 'Jalan Muria 6 Petemon Sawahan', '60252', 1, 444, 563, 'facebook.com/Pangeran Chandra Sasmita', '@pancasasmita', '-7.257484', '112.7232809');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ms_privilige_id` int(11) NOT NULL,
  `tr_user_detail_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `ms_privilige_id`, `tr_user_detail_id`, `created_at`, `updated_at`, `active`) VALUES
(1, 'adminkess', 'admin@kessby.com', '$2y$10$Uebz9brpnLoE.RVkwebRx.OXR3Hve4KXpkyX.xDrJ5ry9YoFfO4S.', 'O7GVGsVAFBnpX6uVfjj4ox1TngKm2pB8sTeYIH4AOKVUTTrRpx8hlSj3V8Dn', 1, 0, '2015-04-26 11:52:24', '2016-10-15 23:56:30', 1),
(2, 'author', 'author@kessby.com', '$2y$10$m9tYzjcfbgfw3UB5A54Z1uPbdaDOvFy9fDHhDh9OAv72Ak2EYtj3K', 'SPnx8UmGSPtqSLkb6gpt9nUxl4gcKjro13pr5AY60MdkBDNyZgX28EspUWpf', 1, 0, '0000-00-00 00:00:00', '2016-03-24 10:13:38', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ms_achievment`
--
ALTER TABLE `ms_achievment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ms_aduan`
--
ALTER TABLE `ms_aduan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_categories`
--
ALTER TABLE `ms_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_cctv`
--
ALTER TABLE `ms_cctv`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_cities`
--
ALTER TABLE `ms_cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ms_province_id` (`ms_province_id`);

--
-- Indexes for table `ms_clubs`
--
ALTER TABLE `ms_clubs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_contacts`
--
ALTER TABLE `ms_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_country`
--
ALTER TABLE `ms_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_download`
--
ALTER TABLE `ms_download`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_event`
--
ALTER TABLE `ms_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_files`
--
ALTER TABLE `ms_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_gender`
--
ALTER TABLE `ms_gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_general`
--
ALTER TABLE `ms_general`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_gerai`
--
ALTER TABLE `ms_gerai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_kategori_aduan`
--
ALTER TABLE `ms_kategori_aduan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_leaders`
--
ALTER TABLE `ms_leaders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ms_links`
--
ALTER TABLE `ms_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_medias`
--
ALTER TABLE `ms_medias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_menu`
--
ALTER TABLE `ms_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_menu_types`
--
ALTER TABLE `ms_menu_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_notification`
--
ALTER TABLE `ms_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_portfolios`
--
ALTER TABLE `ms_portfolios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_portfolio_category`
--
ALTER TABLE `ms_portfolio_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_posts`
--
ALTER TABLE `ms_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_privilige`
--
ALTER TABLE `ms_privilige`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_products`
--
ALTER TABLE `ms_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_product_categories`
--
ALTER TABLE `ms_product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_provinces`
--
ALTER TABLE `ms_provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_rss`
--
ALTER TABLE `ms_rss`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_sliders`
--
ALTER TABLE `ms_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_soal`
--
ALTER TABLE `ms_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_socials`
--
ALTER TABLE `ms_socials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_status_posts`
--
ALTER TABLE `ms_status_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_support_cities`
--
ALTER TABLE `ms_support_cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_tags`
--
ALTER TABLE `ms_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_themes`
--
ALTER TABLE `ms_themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_type_club`
--
ALTER TABLE `ms_type_club`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_ujian`
--
ALTER TABLE `ms_ujian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `tr_achievment`
--
ALTER TABLE `tr_achievment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tr_category_posts`
--
ALTER TABLE `tr_category_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_comments`
--
ALTER TABLE `tr_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_detail_portfolios`
--
ALTER TABLE `tr_detail_portfolios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_logs`
--
ALTER TABLE `tr_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_media_posts`
--
ALTER TABLE `tr_media_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_menu_details`
--
ALTER TABLE `tr_menu_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_post_tags`
--
ALTER TABLE `tr_post_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_theme_menus`
--
ALTER TABLE `tr_theme_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_user_detail`
--
ALTER TABLE `tr_user_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ms_achievment`
--
ALTER TABLE `ms_achievment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ms_aduan`
--
ALTER TABLE `ms_aduan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_categories`
--
ALTER TABLE `ms_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `ms_cctv`
--
ALTER TABLE `ms_cctv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ms_cities`
--
ALTER TABLE `ms_cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;
--
-- AUTO_INCREMENT for table `ms_clubs`
--
ALTER TABLE `ms_clubs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_contacts`
--
ALTER TABLE `ms_contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_country`
--
ALTER TABLE `ms_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;
--
-- AUTO_INCREMENT for table `ms_download`
--
ALTER TABLE `ms_download`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_event`
--
ALTER TABLE `ms_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `ms_files`
--
ALTER TABLE `ms_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `ms_gender`
--
ALTER TABLE `ms_gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_general`
--
ALTER TABLE `ms_general`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ms_gerai`
--
ALTER TABLE `ms_gerai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ms_kategori_aduan`
--
ALTER TABLE `ms_kategori_aduan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ms_leaders`
--
ALTER TABLE `ms_leaders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ms_links`
--
ALTER TABLE `ms_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_medias`
--
ALTER TABLE `ms_medias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=660;
--
-- AUTO_INCREMENT for table `ms_menu`
--
ALTER TABLE `ms_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ms_menu_types`
--
ALTER TABLE `ms_menu_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ms_notification`
--
ALTER TABLE `ms_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `ms_portfolios`
--
ALTER TABLE `ms_portfolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ms_portfolio_category`
--
ALTER TABLE `ms_portfolio_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_posts`
--
ALTER TABLE `ms_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `ms_privilige`
--
ALTER TABLE `ms_privilige`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_products`
--
ALTER TABLE `ms_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `ms_product_categories`
--
ALTER TABLE `ms_product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_provinces`
--
ALTER TABLE `ms_provinces`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `ms_rss`
--
ALTER TABLE `ms_rss`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_sliders`
--
ALTER TABLE `ms_sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ms_soal`
--
ALTER TABLE `ms_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ms_socials`
--
ALTER TABLE `ms_socials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_status_posts`
--
ALTER TABLE `ms_status_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_support_cities`
--
ALTER TABLE `ms_support_cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;
--
-- AUTO_INCREMENT for table `ms_tags`
--
ALTER TABLE `ms_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `ms_themes`
--
ALTER TABLE `ms_themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_type_club`
--
ALTER TABLE `ms_type_club`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `ms_ujian`
--
ALTER TABLE `ms_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tr_achievment`
--
ALTER TABLE `tr_achievment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_category_posts`
--
ALTER TABLE `tr_category_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=318;
--
-- AUTO_INCREMENT for table `tr_comments`
--
ALTER TABLE `tr_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_detail_portfolios`
--
ALTER TABLE `tr_detail_portfolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=325;
--
-- AUTO_INCREMENT for table `tr_logs`
--
ALTER TABLE `tr_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2306;
--
-- AUTO_INCREMENT for table `tr_media_posts`
--
ALTER TABLE `tr_media_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tr_menu_details`
--
ALTER TABLE `tr_menu_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;
--
-- AUTO_INCREMENT for table `tr_post_tags`
--
ALTER TABLE `tr_post_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=416;
--
-- AUTO_INCREMENT for table `tr_theme_menus`
--
ALTER TABLE `tr_theme_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tr_user_detail`
--
ALTER TABLE `tr_user_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;