                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{ asset('img/backend.png') }}" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>{!! Auth::user()->name !!}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i>{!! Auth::user()->email !!}</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="">
                            <a href="{!! url('vrs-admin') !!}">
                                <i class="fa fa-dashboard"></i>
                                <span>Dashboard</span>
                                <i class="fa fa-angle-right pull-right"></i>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="javascript:void(0)">
                                <i class="fa fa-thumb-tack"></i>
                                <span>Post</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ url('vrs-admin/post') }}"><i class="fa fa-angle-double-right"></i> Semua Post</a></li>
                                <li><a href="{{ url('vrs-admin/post/create') }}"><i class="fa fa-angle-double-right"></i> Tambah Baru</a></li>
                                <li><a href="{!! route('vrs-admin.category.index') !!}"><i class="fa fa-angle-double-right"></i> Kategori</a></li>
                                <li><a href="{!! route('vrs-admin.tag.index') !!}"><i class="fa fa-angle-double-right"></i> Tag</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="javascript:void(0)">
                                <i class="fa fa-thumb-tack"></i>
                                <span>Aduan</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ url('vrs-admin/aduan') }}"><i class="fa fa-angle-double-right"></i> Semua Aduan</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="javascript:void(0)">
                                <i class="fa fa-camera"></i>
                                <span>Media</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{!! route('vrs-admin.media.index') !!}"><i class="fa fa-angle-double-right"></i> Library</a></li>
                                <li><a href="{!! route('vrs-admin.portfolio.index') !!}"><i class="fa fa-angle-double-right"></i> Media List</a></li>
                                <li><a href="{!! route('vrs-admin.slider.index') !!}"><i class="fa fa-angle-double-right"></i> Slider</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="javascript:void(0)">
                                <i class="fa fa-camera"></i>
                                <span>CCTV</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{!! route('vrs-admin.cctv.index') !!}"><i class="fa fa-angle-double-right"></i> Semua CCTV</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="javascript:void(0)">
                                <i class="fa fa-camera"></i>
                                <span>File</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{!! route('vrs-admin.file.index') !!}"><i class="fa fa-angle-double-right"></i> Daftar File Pdf</a></li>
                                <li><a href="{!! route('vrs-admin.video.index') !!}"><i class="fa fa-angle-double-right"></i> Daftar File Video</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="javascript:void(0)">
                                <i class="fa fa-camera"></i>
                                <span>Event</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{!! route('vrs-admin.event.index') !!}"><i class="fa fa-angle-double-right"></i> Daftar Event</a></li>
                                <li><a href="{{ url('vrs-admin/event/create') }}"><i class="fa fa-angle-double-right"></i> Tambah Baru</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="javascript:void(0)">
                                <i class="fa fa-camera"></i>
                                <span>Soal Ujian</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{!! route('vrs-admin.soal.index') !!}"><i class="fa fa-angle-double-right"></i> Daftar Soal</a></li>
                                <li><a href="{!! route('vrs-admin.ujian.index') !!}"><i class="fa fa-angle-double-right"></i> Daftar Ujian</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="javascript:void(0)">
                                <i class="fa fa-font"></i>
                                <span>Tampilan</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <!-- <li><a href="{!! route('vrs-admin.theme.index') !!}"><i class="fa fa-angle-double-right"></i> Tema</a></li> -->
                                <li><a href="{!! route('vrs-admin.menu.index') !!}"><i class="fa fa-angle-double-right"></i> Menu</a></li>
                            </ul>
                        </li>
                         <!-- <li class="">
                            <a href="{!! route('vrs-admin.comment.index') !!}">
                                <i class="fa fa-comments"></i>
                                <span>Komentar</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                        </li> -->
                        <li class="treeview">
                            <a href="javascript:void(0)">
                                <i class="fa fa-users"></i>
                                <span>Gerai SIM</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{!! route('vrs-admin.gerai.index') !!}"><i class="fa fa-angle-double-right"></i>Daftar Gerai SIM</a></li>
                            </ul>                            
                        </li>
                        <li class="treeview">
                            <a href="javascript:void(0)">
                                <i class="fa fa-users"></i>
                                <span>Testimoni</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{!! route('vrs-admin.testi.index') !!}"><i class="fa fa-angle-double-right"></i>Daftar Testimoni</a></li>
                                <li><a href="{!! route('vrs-admin.testi.create') !!}"><i class="fa fa-angle-double-right"></i>Tambah Baru</a></li>
                                <!-- <li><a href="{!! route('vrs-admin.achievement.index') !!}"><i class="fa fa-angle-double-right"></i>Daftar Achievement</a></li> -->
                            </ul>                            
                        </li>
                        <li class="treeview">
                            <a href="javascript:void(0)">
                                <i class="fa fa-gear"></i>
                                <span>Pengaturan</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{!! route('vrs-admin.general.index') !!}"><i class="fa fa-angle-double-right"></i> Umum</a></li>
                                <li><a href="{!! route('vrs-admin.contact.edit',[2]) !!}"><i class="fa fa-angle-double-right"></i> Kontak</a></li>
                                <li><a href="{!! route('vrs-admin.rss.index') !!}"><i class="fa fa-angle-double-right"></i> RSS</a></li>
                                <!-- <li><a href="{!! route('vrs-admin.social.index') !!}"><i class="fa fa-angle-double-right"></i> Social Media</a></li> -->
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->