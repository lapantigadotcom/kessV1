        <!-- bootstrap 3.0.2 -->
        
        <link rel="stylesheet" type="text/css" href="{{ asset('css/adminlte/bootstrap.min.css') }}">
        <!-- font Awesome -->

        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

        <!-- Ionicons -->

        <!-- bootstrap wysihtml5 - text editor -->
        
        <link rel="stylesheet" type="text/css" href="{{ asset('css/adminlte/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
        <!-- Theme style -->
        
        <link rel="stylesheet" type="text/css" href="{{ asset('css/adminlte/AdminLTE.css') }}">

        
        <link rel="stylesheet" type="text/css" href="{{ asset('css/adminlte/skins/_all-skins.min.css') }}">
        
        <link rel="stylesheet" type="text/css" href="{{ asset('css/adminlte/style.css') }}">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        @yield('custom-head')