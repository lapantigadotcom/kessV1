<!-- jQuery 2.0.2 
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
        <script type="text/javascript" src="{{ asset('js/adminlte/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        <script type="text/javascript" src="{{ asset('js/adminlte/bootstrap.min.js') }}"></script>
        
        <!-- AdminLTE App -->
        <script type="text/javascript" src="{{ asset('js/adminlte/app.min.js') }}"></script>

        @yield('custom-footer')