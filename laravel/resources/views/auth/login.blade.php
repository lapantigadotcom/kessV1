@extends('auth.app')

@section('content')
<div class="login-box">
  <div class="login-logo">
  <!-- <a href="{!! URL::to('/') !!}"><b>Administrator</b><i class="fa fa-fw fa-lock"></i>Panel</a> -->
  <a href="{!! URL::to('/') !!}"><b>LOGIN</b><i class="fa fa-fw fa-lock"></i>Panel</a>
</div><!-- /.login-logo -->
<div class="login-box-body">
    <!-- <p class="login-box-msg">Masuk ke sistem</p> -->
    @if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
    <form role="form" method="POST" action="{{ url('/auth/login') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group has-feedback">
        {!! Form::text('email',old('email'), array('class' => 'form-control', 'placeholder' => 'email')) !!}
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        {!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) !!}
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="row">
        <div class="col-xs-8">    
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" value="1" name="remember_me" > <span style="color:black;">Ingat Saya</span>
          </label>
      </div>                        
  </div><!-- /.col -->
  <div class="col-xs-4">
      <button type="submit" class="btn btn-success">Sign In</button>
  </div><!-- /.col -->
</div>
</form>

</div><!-- /.login-box-body -->
                <span>       </p>  <center>Gemarsehati. 2016</center>
                 
</span>
                 

            
</div><!-- /.login-box -->
@endsection
