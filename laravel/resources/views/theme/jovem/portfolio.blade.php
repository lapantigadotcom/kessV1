    @extends('theme.hasanahmulia.layout.index')

@section('content')
    <div class="row">
        <div class="large-12 columns">
            <nav class="breadcrumbs"> 
                <a href="{!! URL::to('/') !!}">Home</a> 
                <a href="#" class="current">{{ $data['content']->title }}</a>
            </nav>           
        </div>
    </div>

    <!-- start section project -->
    <div class="row">
        <div class="large-12 columns">
                <div class="large-4 columns" id="left-portfolio">
                <h1>{{ $data['content']->title }} </h1>        
                    @if($data['content']->detailPortfolio()->count() > 0)
                    <h4><a href="{!! route('home.portfolio',[ $data['content']->id ]) !!}">{{ $data['content']->title }}</a></h4>
                    @endif
                    @foreach($data['content']->subportfolio as $row)
                    @if($row->detailPortfolio()->count() > 0)
                    <h4><a href="{!! route('home.portfolio',[ $row->parent_id, $row->id ]) !!}">{{ $row->title }}</a></h4>
                    @endif
                    @endforeach
                </div>
                <div class="large-8 columns">
                @if(empty($data['custom']))
                @if($data['content']->detailPortfolio()->count() > 0)
                    <h2>{{ $data['content']->title }}</h2>
                    <hr>
                    @foreach($data['content']->detailPortfolio as $row)
                    <div class="row portfolio">
                        <div class="columns portfolio" id="right-portfolio">
                            <img src="{{ asset('upload/media/'.$row->media->file) }}" class="img-responsive">
                            <div class="desc">
                                <div class="title-desc green">
                                    <h5><a href="javascript:void(0)">{{ $row->caption }}</a></h5>
                                </div>
                                <div class="content-desc">
                                    {{ $row->description }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                    @else
                    <?php
                    $idC = $data['custom'];
                    $data_t = $data['content']->subportfolio->first();
                     ?>
                    <h2>{{ $data_t->title }}</h2>
                    <hr>
                    @foreach($data_t->detailPortfolio as $row)
                    <div class="row portfolio">
                        <div class="columns portfolio" id="right-portfolio">
                            <img src="{{ asset('upload/media/'.$row->media->file) }}" class="img-responsive">
                            <div class="desc">
                                <div class="title-desc green">
                                    <h5><a href="{!! route('home.portfolio',[ $data_t->id]) !!}">{{ $row->caption }}</a></h5>
                                </div>
                                <div class="content-desc">
                                    {{ $row->description }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                    @endif
                @else
                    <?php
                    $idC = $data['custom'];
                    $data_t = $data['content']->subportfolio->filter(function($v) use($idC){
                        if($v->id==$idC){
                          return true;
                        }
                      });
                     ?>
                    @foreach($data_t as $v)
                    <h2>{{ $v->title }}</h2>
                    <hr>
                    @if($v->detailPortfolio()->count() > 0)
                    @foreach($v->detailPortfolio as $row)
                    <div class="row portfolio">
                        <div class="columns portfolio" id="right-portfolio">
                            <img src="{{ asset('upload/media/'.$row->media->file) }}" class="img-responsive">
                            <div class="desc">
                                <div class="title-desc green">
                                    <h5><a href="{!! route('home.portfolio',[ $v->id]) !!}">{{ $row->caption }}</a></h5>
                                </div>
                                <div class="content-desc">
                                    {{ $row->description }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                    @endif
                    @endforeach

                @endif
                </div>
            
            

        </div>
    </div>
</div>
<!-- end section grey -->
@endsection