@extends('theme.jovem.layout.index')

@section('content')


<title>Redaksi 4Jovem - Cek Resi</title>

<div class="row">
    <div class="col-md-12">
        <!-- TARIF ONGKIR -->
        <h3 class="text-center">
            Lacak resi ekspedisi dengan cepat disini.
        </h3>
        <center>
            <img src="{{ asset('theme/jovem/img/lacakresi.jpg') }}"  alt="cek resi" class="img-responsive">
        </center>
        <h6>Lacak resi hanya tersedia untuk kurir <b>JNE</b></h6>
        {!! Form::open(array('url' => 'tracking','method' => 'post')) !!}
        <div class="form-group">
            {!! Form::label('resi','No Resi : ') !!}
            {!! Form::text('resi',null, ['class' => 'form-control', 'id' => 'resi']) !!}
        </div>
        <div class="form-group" >
           {!! Form::submit('Lacak',['class' => 'btn btn-info' ]) !!}
       </div>
       {!! Form::close() !!}
       <!-- /TARIF ONGKIR -->
       <br>&nbsp;  

       
        @if(isset($data['result']))
        @if($data['result']['status_code'] == 200)
        <h3 class="text-center">Ekspedisi JNE</h3>
        <h5 class="text-center">I. Informasi Pengiriman</h5>
        <table class="table">
            <tr>
                <td>
                    No Resi
                </td>
                <td>
                    {!! $data['result']['waybill_number'] !!}
                </td>
            </tr>
            <tr>
                <td>
                    Status
                </td>
                <td>
                    {!! $data['result']['status'] !!}
                </td>
            </tr>
            <tr>
                <td>
                    Service
                </td>
                <td>
                    {!! $data['result']['service_code'] !!}
                </td>
            </tr>
            <tr>
                <td>
                    Dikirim tanggal
                </td>
                <td>
                    {!! $data['result']['waybill_time'] !!}
                </td>
            </tr>
            <tr>
                <td>
                    Dikirim oleh
                </td>
                <td>
                    {!! $data['result']['shipper_name'] !!} <br>
                    {!! $data['result']['shipper_address'] !!}
                </td>
            </tr>
            <tr>
                <td>
                    Dikirim ke
                </td>
                <td>
                    {!! $data['result']['receiver_name'] !!} <br>
                    {!! $data['result']['receiver_address'] !!}
                </td>
            </tr>
        </table>
        <h5 class="text-center">II. Status Pengiriman</h5>
        <table class="table">
            <tr>
                <th>
                    Tanggal
                </th>
                <th>
                    Lokasi
                </th>
                <th>
                    Keterangan
                </th>
            </tr>
            @if(count($data['result']['manifest']) > 0)
            @foreach($data['result']['manifest'] as $row)
            <tr>
                <td>
                    {!! $row['time'] !!}
                </td>
                <td>
                    {!! $row['city'] !!}
                </td>
                <td>
                    {!! $row['description'] !!}
                </td>
            </tr>
            @endforeach
            @endif
        </table>
        @else
        <h4>Data tidak ditemukan </h4>
        @endif
        @endif
    </div>
    <div class="col-md-12">&nbsp;&nbsp;
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <div class="addthis_sharing_toolbox"></div>
        <hr>
        <br>&nbsp;
        <br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;


        @endsection

        @section('custom-footer')

        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55cd97ec6f575082" async="async"></script>

        @endsection