<title>Korlantas Polri | {{ $data['title'] }}</title>
@extends('theme.jovem.layout.index')
@section('content')
<section id="blog" class="">
  <!-- Main Container Starts -->
  <div class="container pt-60 pb-60">

    <div class="main-container">
      <!-- Nested Row Starts -->
      <div class="row">
        <!-- Mainarea Starts -->
        <div class="col-md-9 col-xs-12">
          <!-- News Post Single Starts -->
          <div class="news-post-single">
            <!-- News Post Starts -->
            <article class="news-post">
              <div class="inner">
                <h2>Simulasi Ujian {{ $data['title'] }}</h2>
                <style type="text/css">
                .ujian-placeholder{
                  background-color: #efefef; 
                  padding: 10%;
                }
                .ujian-soal{
                  display: none;
                }
                li.soal-item{
                  display: none;
                }
                </style>
                <div class="row ujian-deskripsi">
                  <div class="col-md-12 ujian-placeholder">
                    <center>
                      @if($data['totalQuestion'] != 0)
                      <h3>Nilai Simulasi Ujian {{ $data['title']}} Anda</h3>
                      <h1>
                        {{ $data['totalTrue']/$data['totalQuestion']*100}}
                      </h1>
                      <p><b>{{$data['totalTrue']}}</b> Jawaban Benar dari <b>{{ $data['totalQuestion']}}</b> Pertanyaan</p>
                      @else
                        <h3>Terjadi Kesalahan</h3>
                      @endif
                    </center>
                  </div>
                </div>
              </div>
            </article>
          <!-- News Post Ends -->
          </div>
        </div>
        <style type="text/css">
        .img.dsq-widget-avatar{height: 50px}
        .dsq-widget-meta{font-size: 12px; color: red}
        .dsq-widget-comment{font-size: 12px}
        .dsq-widget-list{padding: 0px;margin: 0px}
        .dsq-widget-meta a{font-size: 12px;color: red}

        </style>
        <!-- Sidearea Starts -->
        <div class="col-md-3 col-xs-12">
          <!-- Categories Starts --><!-- Komentar artikel -->
          <ul class="list-unstyled">
            <h4 class="side-heading1 top">LATEST COMMENTS</h4>
            <script type="text/javascript" src="http://gemarsehati.disqus.com/recent_comments_widget.js?num_items=3&hide_avatars=1&avatar_size=70&excerpt_length=90"></script>
          </ul> <br>
        <!-- /Komentar artikel -->
          <h4 class="side-heading1 top">LATEST ARTICLES</h4>
          <ul class="list-unstyled list-style-1">
            @foreach($data['latest_posts'] as $row)
            <li><a href="#">          
              <a href="{!! route('home.post',[ $row->id,'post' ]) !!}"><h5>  <b> {!! substr(strip_tags($row->title),0, 20).'...' !!} </b> </h5></a>
              <p style="font-size:13px">
                {!! substr(strip_tags($row->description),0, 140).'...' !!}
              </p>
              <small>
                <i class="fa fa-calendar"></i> </i> {{ date("d M Y" ,strtotime($row->created_at)) }} &nbsp;&nbsp;&nbsp;
                <i class="fa fa-user"></i> </i> {{ $row->user->name }} &nbsp;&nbsp;&nbsp;
              </small>
              <hr>
            </li>
            @endforeach
          </ul>
        <!-- Categories Ends -->
        </div>
        <!-- Sidearea Ends -->
      </div>
    </div>
  </div>
</section>
 
@endsection
@section('custom-footer')
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56a630c285f8f2c7" async="async"></script>
<script>
$('#register-modal').Carousel({

});
</script>
@endsection