<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="www.korlantas.polri.go.id merupakan bentuk pelayanan masyarakat online di bidang Lalu Lintas dan Angkutan Jalan (LLAJ), dimana salah satu elemen penting dalam mewujudkan penyelenggaraan negara yang terbuka adalah hak publik untuk memperoleh pelayanan dan informasi publik sesuai tatanan didalam berbangsa dan bernegara" />
<meta name="keywords" content="korlantas,polri,lantas,web korlantas,polisi,lalu lintas" />
<meta name="author" content="lapantiga.com" />

<!-- Page Title -->
<title>
  {{ $general->site_title }} 
</title>

@include('theme.jovem.partials.head')
</head>
<body class="boxed-layout pt-40 pb-40 pt-sm-0" data-bg-img="{{ asset('images/pattern/dot-lapantiga.png')}}">
    <div id="wrapper" class="clearfix">
      <!-- preloader -->
      <div id="preloader">
        <div id="spinner">
              <img src="{{ asset('images/badged_lantas.png')}}" width="240px" alt="">
              <img src="{{ asset('images/preloaders/4.gif')}}" alt="">
        </div>
      </div>
      <!-- Header -->
      <header id="header" class="header">
        <div class="header-top bg-theme-color-2 sm-text-center p-0">
          <div class="container">
            <div class="row">
              <div class="col-md-4">
                <div class="widget no-border m-0">
                  <ul class="list-inline font-13 sm-text-center mt-5">
                    <li>
                      <a class="text-white" href="#">BAHASA</a>
                    </li>
                    <li class="text-white">|</li>
                    <li>
                      <a class="text-white" href="#">ENGLISH</a>
                    </li>
                    <li class="text-white">|</li>
                     
                  </ul>
                </div>
              </div>
              <div class="col-md-8">
                <div class="widget no-border m-0 mr-15 pull-right flip sm-pull-none sm-text-center">
                  <ul class="styled-icons icon-circled icon-sm pull-right flip sm-pull-none sm-text-center mt-sm-15">
                    <li><a href="{{$data['contact']->facebook}}"><i class="fa fa-facebook text-white"></i></a></li>
                    <li><a href="{{$data['contact']->twitter}}"><i class="fa fa-twitter text-white"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="header-middle p-0 bg-lightest xs-text-center">
          <div class="container pt-0 pb-0">
            <div class="row">
              <div class="col-xs-12 col-sm-4 col-md-5">
                <div class="widget no-border m-0">
                  <a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="javascript:void(0)"><img src="{{ asset('images/logo_korlantas.png')}}" alt=""></a>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
                  <ul class="list-inline">
                    <li><i class="fa fa-phone-square text-theme-colored font-36 mt-5 sm-display-block"></i></li>
                    <li>
                      <a href="#" class="font-12 text-gray text-uppercase">Call Center</a>
                      <h5 class="font-14 m-0">{{$data['contact']->telephone}}</h5>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
                  <ul class="list-inline">
                    <li><i class="fa fa-mobile text-theme-colored font-36 mt-5 sm-display-block"></i></li>
                    <li>
                      <a href="#" class="font-12 text-gray text-uppercase">SMS Center</a>
                      <h5 class="font-13 text-blue m-0"> {{$data['contact']->mobile}}</h5>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="header-nav">
          <div class="header-nav-wrapper navbar-scrolltofixed bg-theme-colored border-bottom-theme-color-2-1px">
            <div class="container">
              <nav id="menuzord" class="menuzord bg-theme-colored pull-left flip menuzord-responsive">
                <ul class="menuzord-menu">
                  <li ><a href="#home"><i class="fa fa-home"></i></a></li>
                  <?php
                    $top_menu = $menu->filter(function($v){
                      if($v->code=='top_menu'){
                        return true;
                      }
                    });
                  ?>
                  @foreach($top_menu as $row)
                    @foreach($row->menuDetail as $val)
                    <?php 
                    if($val->parent_id != 0)
                      continue;
                    $url = '';
                    switch ($val->menuType->id) {
                      case '1':
                        $url = route('home.category',[ $val->custom ]);
                        break;
                      case '2':
                        $url = route('home.post',[ $val->custom ]);
                        break;
                      case '3':
                        $url = route('home.portfolio',[ $val->custom ]);
                        break;

                      case '4':
                        $url = route('home.rss',[ $val->custom ]);
                        break;
                      case '5':
                        $url = $val->custom;
                        break; 
                      case '6':
                        $url = route('home.products');
                        break;
                      case '7':
                        $url = route('home.mitra');
                        break;
                      case '8':
                        $url = route('home.mitraCity');
                        break;
                      case '9':
                        $url = route('home.type',[ $val->custom ]);
                        break;
                      case '10':
                        $url = route('home.soal_soal');
                        break;
                      default:
                        # code...
                        break;
                    }
                    ?>
                    <li><a
                    @if($val->submenu()->count() > 0 )
                      href="#"
                    @else
                      href="{{ $url }}" 
                    @endif
                    >
                    {{ $val->title }}
                    </a>
                    @if($val->submenu()->count() > 0 )
                      <ul class="dropdown">
                      @foreach($val->submenu as $v)
                        <?php 
                          $url = '';
                          switch ($v->menuType->id) {
                            case '1':
                              $url = route('home.category',[ $v->custom ]);
                              break;
                            case '2':
                              $url = route('home.post',[ $v->custom ]);
                              break;
                            case '3':
                              $url = route('home.portfolio',[ $v->custom ]);
                              break;
                            case '4':
                              $url = route('home.rss',[ $v->custom ]);
                              break;
                            case '5':
                                $url = $v->custom;
                                break; 
                            case '6':
                              $url = route('home.products');
                              break;
                            case '7':
                              $url = route('home.mitra');
                              break;
                            case '8':
                              $url = route('home.mitraCity');
                              break;
                            case '9':
                              $url = route('home.type',[ $v->custom ]);
                              break;
                            case '10':
                              $url = route('home.soal_soal');
                              break;
                            default:
                              # code...
                              break;
                        }
                        ?>
                        <li>
                          <a href="{!! url($url) !!}">{!! $v->title !!}</a>
                          @if($v->submenu()->count() > 0 )
                          <ul class="dropdown">
                          @foreach($v->submenu as $w)
                            <?php 
                                  $url = '';
                                  switch ($w->menuType->id) {
                                    case '1':
                                      $url = route('home.category',[ $w->custom ]);
                                      break;
                                    case '2':
                                      $url = route('home.post',[ $w->custom ]);
                                      break;
                                    case '3':
                                      $url = route('home.portfolio',[ $w->custom ]);
                                      break;
                                    case '4':
                                      $url = route('home.rss',[ $w->custom ]);
                                      break;
                                    case '5':
                                        $url = $w->custom;
                                        break; 
                                    case '6':
                                      $url = route('home.products');
                                      break;
                                    case '7':
                                      $url = route('home.mitra');
                                      break;
                                    case '8':
                                      $url = route('home.mitraCity');
                                      break;
                                    case '9':
                                      $url = route('home.type',[ $w->custom ]);
                                      break;
                                    case '10':
                                      $url = route('home.soal_soal');
                                      break;
                                    default:
                                      # code...
                                      break;
                                  }
                                  ?>
                                <li class="">
                                  <a href="{!! url($url) !!}" >{!! $w->title !!}</a>
                                </li>
                              @endforeach
                            </ul>
                          @endif
                        </li>
                        @endforeach
                      </ul>
                    @endif
                    </li>
                    @endforeach
                  @endforeach
                </ul>
                <ul class="pull-right flip hidden-sm hidden-xs">
                  <li>
                    <!-- Modal: donate now Starts -->
                    <a class="btn btn-colored btn-flat bg-theme-color-2 text-white font-14 font-14 bs-modal-ajax-load mt-0 p-25 pr-15 pl-15" data-toggle="modal" data-target="#BSParentModal" href="ajax-load/download-category.html">Buku Panduan</a>
                    <!-- Modal: donate now End -->
                  </li>
                </ul>
                <div id="top-search-bar" class="collapse">
                  <div class="container">
                    <form role="search" action="#" class="search_form_top" method="get">
                      <input type="text" placeholder="Type text and press Enter..." name="s" class="form-control" autocomplete="off">
                      <span class="search-close"><i class="fa fa-search"></i></span>
                    </form>
                  </div>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </header>

      <!-- Start main-content -->
      <div class="main-content shadow">
        @if(!isset($data['index']))
          <section class="bg-theme-color-2" style="margin-top:25px">
            <div class="container pt-0 pb-0">
              <div class="row">
                <div class="call-to-action pt-30 pb-20">
                  <div class="col-md-6">
                    <h3 class="mt-5 mb-5 text-white vertical-align-middle"><i class="fa fa-search mr-10 font-48 vertical-align-middle"></i> Pencarian Artikel / Berita</h3>
                  </div>
                  <div class="col-md-6">
                    <!-- Mailchimp Subscription Form Starts Here -->
                    {!! Form::open(array('url' => '/cari','method' => 'post'), ['class' => 'newsletter-form mt-10']) !!}
                      <div class="input-group">
                        <?php
                        $value = null;
                        if (isset($data['keyword']))
                          $value = $data['keyword'];
                        ?>
                        {!! Form::text('keyword',$value, ['class' => 'form-control', 'placeholder' => 'Cari Berita', 'class' => 'form-control input-lg font-16', 'data-height' => '45px']) !!}
                        <span class="input-group-btn">
                          {!! Form::submit('Temukan',['class' => 'btn bg-theme-colored text-white btn-xs m-0 font-14', 'data-height' => '45px']) !!}
                        </span>
                      </div>
                    {!! Form::close()!!}
                    <br>
                  </div>
                </div>
              </div>
            </div>
          </section>
        @endif
        @yield('content')
      <!-- end main-content -->
      </div>
      @include('theme.jovem.partials.footer')
      @include('theme.jovem.partials.script')
    </div>
    
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73060896-1', 'auto');
  ga('send', 'pageview');
</script>

    <!-- Use CSS to replace link text with flag icons -->
<ul class="translation-links">
  <li><a href="#" class="spanish" data-lang="Spanish">Spanish</a></li>
  <li><a href="#" class="german" data-lang="German">German</a></li>
</ul>

  <!-- Code provided by Google -->
  <div id="google_translate_element"></div>
  <script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
    }
  </script>
  <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>

  <!-- Flag click handler -->
  <script type="text/javascript">
      $('.translation-links a').click(function() {
        var lang = $(this).data('lang');
        var $frame = $('.goog-te-menu-frame:first');
        if (!$frame.size()) {
          alert("Error: Could not find Google translate frame.");
          return false;
        }
        $frame.contents().find('.goog-te-menu2-item span.text:contains('+lang+')').get(0).click();
        return false;
      });
  </script>
</body>
</html>