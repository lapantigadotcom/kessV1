<!DOCTYPE html>
<html>
<head>
  	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<title>
    {{ $general->site_title }} 
	</title>
	
<script type="text/javascript"></script>
  @include('theme.jovem.partials.head')
</head>
<body>
  <div id="wrapper" class="container">
    @include('theme.jovem.partials.menu')

    @yield('content')  

  @include('theme.jovem.partials.footer') 
  @include('theme.jovem.partials.script')
  </div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73060896-1', 'auto');
  ga('send', 'pageview');

</script>



</body>
</html>