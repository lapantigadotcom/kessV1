@extends('theme.jovem.layout.index')
@section('content')
<style type="text/css">
.news-post{
  background-color: #fefefe;
  -webkit-transition: background-color 0.5s;
  border-top: solid 0.5px #ccc; 
  border-bottom: solid 0.5px #ccc; 
  margin: 10px; 
  padding: 10px
}
.news-post:hover{
  background-color: #efefef;
}

.post-title h4{
  color: #000;
  -webkit-transition: color 0.5s;
}

.post-title h4:hover{
  color: #16174F;
}

</style>
<div style='margin: 25px'>
  
  <!-- Main Banner Ends -->
  <!-- Main Container Starts -->
  <div class="main-container">
    <!-- Nested Row Starts -->
    <div class="row">
      <div class="col-md-9 col-xs-12">
        <h2>Daftar Artikel</h2>
        @foreach($data['content']->sortByDesc('created_at') as $v)
        <div class="news-post-list">
          @if($v->ms_media_id != '0')
          <article class="news-post" style="">
            <a href="{!! route('home.post',[ $v->id,'post' ]) !!}" class="post-title"><h4>{{ $v->title }}</h4></a>
            <ul class="list-unstyled list-inline post-meta">
                <li>
                  <i class="fa fa-calendar"></i> 
                  {{ date("d M Y" ,strtotime($v->created_at)) }}
                </li>
                <li>
                  <i class="fa fa-user"></i> 
                  By {{ $v->user->name }} 
                </li>
                <li>
                  <i class="fa fa-eye"></i>
                    <?php
                    /*$found = 0;
                    foreach($data['stat_pages'] as $info)
                    {
                      if($info["url"] == "/artikel/gemarsehati=".$v->id."?post=")
                      {
                        echo $info["pageViews"];
                        $found = 1;
                      }
                    }
                    if($found == 0)
                      echo "0";*/
                    ?> 
                </li>
                <li>
                  <i class="fa fa-tag"></i>
                    <?php
                    $tagCount = count($v->tag);
                    $currentTag = 0;
                    ?>
                    @foreach($v->tag as $row)
                      <span style="font-size:10px"> {!! $row->title !!} 
                      </span>
                    @endforeach
                </li>
              </ul>
            <div class="row" style="margin-top:10px">
              <div class="col-md-4">
                <img src="{{ asset('upload/media/'.$v->mediaPost->file) }}"  class="img-responsive img-center-sm img-center-xs" width="850">
              </div>
              <div class="col-md-8">
                <div class="inner">
                  <p><?php $desc = explode("<!-- pagebreak -->", $v->description); ?>
                    <?php
                    $desc = explode("</p>", $desc[0]);
                    ?>
                    @if(strlen($desc[0]) > 500)
                    {!! substr($desc[0], 0, 500) !!}...
                    @else
                    {!! $desc[0] !!}&nbsp;
                    @endif
                  </p>
                </div>
                <a href="{!! route('home.post',[ $v->id,'post' ]) !!}" class="btn btn-secondary pull-right">
                  Read More 
                  <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
          </article>
          @else
          <article class="news-post" style="">
            <a href="{!! route('home.post',[ $v->id,'post' ]) !!}" class="post-title"><h4>{{ $v->title }}</h4></a>
            <ul class="list-unstyled list-inline post-meta">
                <li>
                  <i class="fa fa-calendar"></i> 
                  {{ date("d M Y" ,strtotime($v->created_at)) }}
                </li>
                <li>
                  <i class="fa fa-user"></i> 
                  By {{ $v->user->name }} 
                </li>
                <li>
                  <i class="fa fa-eye"></i>
                    <?php
                    /*$found = 0;
                    foreach($data['stat_pages'] as $info)
                    {
                      if($info["url"] == "/artikel/gemarsehati=".$v->id."?post=")
                      {
                        echo $info["pageViews"];
                        $found = 1;
                      }
                    }
                    if($found == 0)
                      echo "0";*/
                    ?> 
                </li>
                <li>
                  <i class="fa fa-tag"></i>
                    <?php
                    $tagCount = count($v->tag);
                    $currentTag = 0;
                    ?>
                    @foreach($v->tag as $row)
                      <span style="font-size:10px"> {!! $row->title !!} 
                      </span>
                    @endforeach
                </li>
              </ul>
            <div class="row" style="margin-top:10px">
              
              <div class="col-md-12">
                <div class="inner">
                  <p><?php $desc = explode("<!-- pagebreak -->", $v->description); ?>
                    <?php
                    $desc = explode("</p>", $desc[0]);
                    ?>
                    @if(strlen($desc[0]) > 500)
                    {!! substr($desc[0], 0, 500) !!}...
                    @else
                    {!! $desc[0] !!}&nbsp;
                    @endif
                  </p>
                </div>
                <a href="{!! route('home.post',[ $v->id,'post' ]) !!}" class="btn btn-secondary pull-right">
                  Read More 
                  <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
          </article>
          @endif
        </div>
        @endforeach
        <div class="row" id="paginator">
          <center>
            {!! $data['content']->render() !!}
          </center>
        </div>
      </div>
       
  <style type="text/css">
  .img.dsq-widget-avatar{height: 50px}
  .dsq-widget-meta{font-size: 12px; color: red}
  .dsq-widget-comment{font-size: 12px}
  .dsq-widget-list{padding: 0px;margin: 0px}
  .dsq-widget-meta a{font-size: 12px;color: red}

  </style>


       <!-- Sidearea Starts -->
            <div class="col-md-3 col-xs-12">
            <!-- Komentar artikel -->
                       <ul class="list-unstyled">
              <h4 class="side-heading1 top">LATEST COMMENTS</h4>
   
  <script type="text/javascript" src="http://gemarsehati.disqus.com/recent_comments_widget.js?num_items=2&hide_avatars=1&avatar_size=70&excerpt_length=90"></script>
            
  </ul> <br>
            <!-- /Komentar artikel -->

             <ul class="list-unstyled list-style-1">

              <h4 class="side-heading1 top">LATEST TWIT</h4>

                        <a class="twitter-timeline"  href="https://twitter.com/Gemarsehatiteam" data-widget-id="692292481396928512">Tweets by @Gemarsehatiteam</a>
              <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </ul>
                        <br>
              <h4 class="side-heading1 top">LATEST ARTICLES</h4>
                      @foreach($data['latest_posts'] as $row)

              <ul class="list-unstyled list-style-1">
                <li> <a href="{!! route('home.post',[ $row->id,'post' ]) !!}"><h5>  <b> {!! substr(strip_tags($row->title),0, 20).'...' !!} </b> </h5></a>
                   <p style="font-size:13px">
                      {!! substr(strip_tags($row->description),0, 140).'...' !!}<a style="font-size:13px;color:red" href="{!! route('home.post',[ $v->id,'post' ]) !!}">[Read More]</a>
                    </p>
                <small>
                       <i class="fa fa-calendar"></i> </i> {{ date("d M Y" ,strtotime($row->created_at)) }} &nbsp;&nbsp;&nbsp;
                       <i class="fa fa-user"></i> </i> {{ $row->user->name }} &nbsp; 

                    </small>
                    <hr></li>
                         @endforeach

              </ul>
            <!-- Categories Ends -->
             

            </div>
          <!-- Sidearea Ends -->
    </div>     
  </div>
  <!-- section FB -->
</div>                        
@endsection

      
