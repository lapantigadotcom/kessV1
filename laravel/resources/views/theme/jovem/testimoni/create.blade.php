@extends('theme.jovem.layout.index')

@section('content')
<div class="row" style="min-height:70%;">
  <div class="col-md-12">
    <div class="main-banner six">
      <div class="container">
        <h2><span>Mitra Dashboard</span></h2>
      </div>
    </div>
    <div class="breadcrumb">
      <div class="container">
        <ul class="list-unstyled list-inline">
          <li><a href="{!! URL::to('/') !!}">Home</a></li>
          <li><a href="@if(Auth::check()) {{ route('home.profile', Auth::user()->id) }} @else # @endif">My Profile</a></li>
          <li><a href="javascript:void(0);">Testimoni</a></li>
        </ul>
      </div>
    </div>
    <div class="main-container">    
      <div class="contact-content">
        <div class="tab-pane fade in active" id="tab-1">
          <div class="row">
            <div class="col-sm-8 col-xs-12">
              <h3>Beri Testimoni</h3>
              @include('page.partials.notification')
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                <strong>Whoops!</strong> Form yang anda isi belum lengkap.<br><br>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif
              <div class="box-body">
              {!! Form::open(['route'=>'home.doTestimoni', 'method' => 'POST','files' => true]) !!}              
                @include('theme.jovem.testimoni.form',['buttonSubmit' => 'SUBMIT'])
              {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/jovem/plugins/pretty-photo/css/prettyPhoto.css') }}">

@endsection

@section('custom-footer')
  @include('page.scripts.media')
  <script type="text/javascript">
          $(document).ready(function() {
            // var reference = (function tipeportfolio(){
            //  var type = $('#type').val();
            //  switch(type){
            //    case '1':
            //      $('#mediaformcontainer').show();
            //    break;
            //    case '2':
            //      $('#mediaformcontainer').hide();
            //    break;
            //  }
            //     return tipeportfolio;
            // }());
            // $('#type').change(function(){
            //  reference();
            //  console.log("ganti");
            // });
          });
          </script>
<script type="text/javascript" src="{{ asset('theme/jovem/plugins/pretty-photo/js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
   $(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square', autoplay_slideshow: false});

 });
</script>
@endsection