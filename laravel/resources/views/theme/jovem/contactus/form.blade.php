		<div class="form-group">
			<div class="form-group col-md-12">
				{!! Form::label('name','Nama * : ') !!}
				{!! Form::text('name',null, ['class' => 'form-control']) !!}
			</div>
		</div>		
		<div class="form-group">
			<div class="form-group col-md-6">
				{!! Form::label('email','Email * : ') !!}
				{!! Form::text('email',null, ['class' => 'form-control']) !!}
			</div>
			<div class="form-group col-md-6">
				{!! Form::label('nohp','No Hp : ') !!}
				{!! Form::text('nohp',null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group col-md-12">
			{!! Form::label('title','Judul * : ') !!}
			{!! Form::text('title',null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group col-md-12">
			{!! Form::label('message','Pesan * : ') !!}
			{!! Form::textarea('message',null, ['class' => 'form-control','id' => 'textEditor']) !!}
		</div>
			<div class="form-group col-md-12">
			{!! Form::submit($buttonSubmit,['class' => 'btn btn-info']) !!}
		</div>