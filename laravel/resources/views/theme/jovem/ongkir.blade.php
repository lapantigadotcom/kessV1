@extends('theme.jovem.layout.index')

@section('content')


<title>Redaksi 4Jovem - Cek Biaya Pengiriman</title>
 
<div class="row">
    <div class="col-md-12">
        <!-- TARIF ONGKIR -->


                                    <h3 class="text-center">
                                        Periksa ongkos kirim dengan cepat disini.
                                    </h3>
                            <center>
                                    <img src="{{ asset('theme/jovem/img/icon-kurir.jpg') }}" class="img-responsive" alt="cek tarif">
                            </center>
                                    {!! Form::open(array('url' => 'ongkir','method' => 'post')) !!}



                             <div class="form-group">
                                {!! Form::label('courier','Kurir : ') !!}
                                <select name="courier" class="form-control" id="courier">
                                    <option value="pos"  @if(isset($data['isian_courier']) && $data['isian_courier'] == 'pos') selected @endif >
                                         POS Indonesia
                                    </option>

                                    <option value="jne"  @if(isset($data['isian_courier']) && $data['isian_courier'] == 'jne') selected @endif>
                                         Jalur Nugraha Ekakurir (JNE)
                                    </option>
                                    
                                    <option value="tiki"  @if(isset($data['isian_courier']) && $data['isian_courier'] == 'tiki') selected @endif>
                                        Titipan Kilat (Tiki)
                                    </option>
                                    <option value="rpx"  @if(isset($data['isian_courier']) && $data['isian_courier'] == 'rpx') selected @endif>
                                        RPX
                                    </option>
                                    <option value="pcp"  @if(isset($data['isian_courier']) && $data['isian_courier'] == 'pcp') selected @endif>
                                        PCP (PRIORITY, CARGO & PACKAGE)
                                    </option>
                                     <option value="esl"  @if(isset($data['isian_courier']) && $data['isian_courier'] == 'esl') selected @endif>
                                        EKA SARI LORENA (ESL)
                                    </option>

                                </select>
                            </div>


                                <div class="form-group">
                                    {!! Form::label('source','Asal : ') !!}
                                    <select name="source" class="form-control" id="source">
                                        @foreach($data['city'] as $row)
                                        <option value="{!! $row->id !!}" @if(isset($data['isian_source']) && $data['isian_source']== $row->id) selected @endif>
                                            {!! $row->name !!}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="form-group">
                                    {!! Form::label('destination','Tujuan : ') !!}
                                    <select name="destination" class="form-control" id="destination">
                                        @foreach($data['city'] as $row)
                                        <option value="{!! $row->id !!}" @if(isset($data['isian_destination']) && $data['isian_destination']== $row->id) selected @endif>
                                            {!! $row->name !!}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                               


                    <div class="form-group">
                        {!! Form::label('weight','Berat(gram) : ') !!}
                        {!! Form::text('weight',isset($data['isian_weight'])?$data['isian_weight']:null, ['class' => 'form-control','maxlength' => '4', 'id' => 'weight']) !!}
                    </div>
             
                    <div class="form-group" >
                             {!! Form::submit('Hitung',['class' => 'btn btn-info' ]) !!}
                    </div>
                    {!! Form::close() !!}
                


        <!-- /TARIF ONGKIR -->
         <br>&nbsp;  

                     <h6>Apabila Tarif tidak keluar di tabel, berarti tidak mempunyai rute pengiriman ke daerah yang Anda Maksud. 
                        Silahkan Menggunakan Kurir lain (Ex: Pos Indonesia)</h6>


                            @if(isset($data['results']))
                            <h4>{!! $data['context'] !!}</h4>
                            <table class="table ongkir-tabel">
                                <thead>
                                         <th style="background:#5D524E;color:#fff">Jenis Layanan</th>
                                       <th style="background:#5D524E;color:#fff">Tarif</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($data['results'] as $row)
                                    <tr>  
                                        <td style="color:#5D524E"><b>{!! $row[0] !!}</b> - {!! $row[3] !!}</td>
                                        <td>Rp. {!! $row[1] !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                        <div class="col-md-12">&nbsp;&nbsp;
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_sharing_toolbox"></div>
                            <hr>
                     <br>&nbsp;
                     <br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;


                    @endsection

                    @section('custom-footer')

                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55cd97ec6f575082" async="async"></script>

                            @endsection