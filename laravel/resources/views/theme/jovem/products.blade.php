@extends('theme.jovem.layout.index')

@section('content')
 
<div class="row" style="min-height:70%;">
  <div class="col-md-12">
    <div class="main-banner six" style="background:transparent url( {{ asset('theme/enagic/images/header-produk.jpg') }} ) repeat scroll center top">
        <div class="container">
          <h2>&nbsp;</h2>
        </div>
      </div>
    <div class="breadcrumb">
      <div class="container">
        <ul class="list-unstyled list-inline">
          <li><a href="{!! URL::to('/') !!}">Home</a></li>
          <li><a href="javascript:void(0);">Product Enagic</a></li>
        </ul>
      </div>
    </div>
    <div class="tabs-wrap">    
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab-1">
          <div class="row">
            <div class="col-md-12 gallery" style="float:left;">
            <?php 
            $i = 1;
            ?>
             @foreach($data['products'] as $row)
             <?php               
              echo "<div class='col-md-4 col-sm-6 col-xs-12'>";
             ?>
             <div class="box1 text-center-xs">
              @if(file_exists('./upload/'.$row->file))
              <center><img width="200px" height="200px" src="{{ asset('upload/media/'.$row->media->file) }}" class="img-responsive img-center-sm img-center-xs" alt="{{ $row->caption}}"></center>
              @else
              <center><img src="{!! asset('theme/jovem/img/user.png') !!}" class="img-responsive img-center-sm img-center-xs" alt="{{ $row->caption}}"></center>
                @endif
                <h4>{{ $row->name}}</h4>
                <p>{!! substr(strip_tags($row->description), 0, 120).'...' !!}</p>
                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#myModal{{ $i }}">Details</button>
              </div>
              <!-- Modal -->
              <div id="myModal{{ $i }}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">{{ $row->name }}</h4>
                    </div>
                    <div class="modal-body">
                      <p>{{ $row->description }}</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <a href="{{ route('home.subscribe') }}" class="btn btn-secondary">Subscribe!</a>
                    </div>
                  </div>
                </div>
              </div>
              <?php 
                echo "</div>";
                $i++;
               ?>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12 text-right" style="margin-left:auto; margin-right:0;">
    {!! $data['products']->render() !!}
  </div>
</div>




    @endsection

    @section('custom-head')
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/jovem/plugins/pretty-photo/css/prettyPhoto.css') }}">
    
    @endsection

    @section('custom-footer')
    <script type="text/javascript" src="{{ asset('theme/jovem/plugins/pretty-photo/js/jquery.prettyPhoto.js') }}"></script>
    <script type="text/javascript">
      $(document).ready(function(){
       $(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square', autoplay_slideshow: false});

     });
    </script>
    @endsection