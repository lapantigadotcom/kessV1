<title>Korlantas Polri | {{ $data['ujian']->title }}</title>
@extends('theme.jovem.layout.index')
@section('content')
<section id="blog" class="">
  <!-- Main Container Starts -->
  <div class="container pt-60 pb-60">

    <div class="main-container">
      <!-- Nested Row Starts -->
      <div class="row">
        <!-- Mainarea Starts -->
        <div class="col-md-9 col-xs-12">
          <!-- News Post Single Starts -->
          <div class="news-post-single">
            <!-- News Post Starts -->
            <article class="news-post">
              <div class="inner">
                <h2>Simulasi Ujian {{ $data['ujian']->title }}</h2>
                <style type="text/css">
                .ujian-placeholder{
                  background-color: #efefef; 
                  padding: 10%;
                }
                .ujian-soal{
                  display: none;
                }
                li.soal-item{
                  display: none;
                }
                </style>
                <div class="row ujian-deskripsi">
                  <div class="col-md-12 ujian-placeholder">
                    <p>{{$data['ujian']->description}}</p>
                    <center>
                      <button class="btn btn-info" onclick="MulaiUjian()">
                        Mulai ujian
                      </button>
                    </center>
                  </div>
                </div>
                <div class="row ujian-soal">
                  {!! Form::open(array('route' => 'home.ujian.hasil', 'method' => 'post')) !!}
                    {!! Form::hidden('id_ujian', $data['ujian']->id)!!}
                    <div class="col-md-12 ujian-placeholder">
                      <ul class="soal-container">
                        <?php
                        $nomor = 0;
                        $total = count($data['soal']);
                        ?>
                        @foreach($data['soal'] as $soal)
                        <li class="soal-item" id="soal[{{$soal->id}}]" nomor="{{$soal->id}}">
                          <div class="row" style="border-bottom: solid 0.5px #ccc; margin-bottom: 10px">
                            <h4>
                            Pertanyaan <b>{{++$nomor}}</b>/{{$total}}
                            </h4>
                          </div>
                          <div class="row" style="border-bottom: solid 0.5px #ccc; padding-bottom: 10px; margin-top: 10px; margin-bottom: 10px">
                            @if(isset($soal->ms_media_id->file))
                              <center>
                                <img src="{{asset('upload/media/'.$soal->ms_media_id->file)}}">
                              </center>
                            @endif
                            {{$soal->pertanyaan}}
                          </div>
                          <div class="row">
                            <h4>Jawaban: </h4>
                            <div class="radio">
                              <label><input type="radio" name="soal[{{$soal->id}}]" value="a">{{$soal->jawaban_a}}</label>
                            </div>
                            <div class="radio">
                              <label><input type="radio" name="soal[{{$soal->id}}]" value="b">{{$soal->jawaban_b}}</label>
                            </div>
                            <div class="radio">
                              <label><input type="radio" name="soal[{{$soal->id}}]" value="c">{{$soal->jawaban_c}}</label>
                            </div>
                            <div class="radio">
                              <label><input type="radio" name="soal[{{$soal->id}}]" value="d">{{$soal->jawaban_d}}</label>
                            </div>
                          </div>
                        </li>
                        @endforeach
                      </ul>
                      <button class="btn btn-info pull-right" onclick="SoalSelanjutnya()" id="button_next" type="button">Selanjutnya</button>
                      <button class="btn btn-info" onclick="SoalSebelumnya()" id="button_prev" type="button">Sebelumnya</button>
                      <button class="btn btn-success pull-right" onclick="SoalSebelumnya()" id="button_fin" type="submit">Selesaikan Tes</button>
                    </div>
                  {!!Form::close() !!}
                </div>
              </div>
            </article>
          <!-- News Post Ends -->
          </div>
        </div>
        <style type="text/css">
        .img.dsq-widget-avatar{height: 50px}
        .dsq-widget-meta{font-size: 12px; color: red}
        .dsq-widget-comment{font-size: 12px}
        .dsq-widget-list{padding: 0px;margin: 0px}
        .dsq-widget-meta a{font-size: 12px;color: red}

        </style>
        <!-- Sidearea Starts -->
        <div class="col-md-3 col-xs-12">
          <!-- Categories Starts --><!-- Komentar artikel -->
          <ul class="list-unstyled">
            <h4 class="side-heading1 top">LATEST COMMENTS</h4>
            <script type="text/javascript" src="http://gemarsehati.disqus.com/recent_comments_widget.js?num_items=3&hide_avatars=1&avatar_size=70&excerpt_length=90"></script>
          </ul> <br>
        <!-- /Komentar artikel -->
          <h4 class="side-heading1 top">LATEST ARTICLES</h4>
          <ul class="list-unstyled list-style-1">
            @foreach($data['latest_posts'] as $row)
            <li><a href="#">          
              <a href="{!! route('home.post',[ $row->id,'post' ]) !!}"><h5>  <b> {!! substr(strip_tags($row->title),0, 20).'...' !!} </b> </h5></a>
              <p style="font-size:13px">
                {!! substr(strip_tags($row->description),0, 140).'...' !!}
              </p>
              <small>
                <i class="fa fa-calendar"></i> </i> {{ date("d M Y" ,strtotime($row->created_at)) }} &nbsp;&nbsp;&nbsp;
                <i class="fa fa-user"></i> </i> {{ $row->user->name }} &nbsp;&nbsp;&nbsp;
              </small>
              <hr>
            </li>
            @endforeach
          </ul>
        <!-- Categories Ends -->
        </div>
        <!-- Sidearea Ends -->
      </div>
    </div>
  </div>
</section>
 
@endsection
@section('custom-footer')
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56a630c285f8f2c7" async="async"></script>
<script>
var position = 0;
var daftarSoal = $(".soal-item");
var totalSoal = {{count($data['soal'])}};
ButtonHider();
function MulaiUjian(){
  var soalAktif = $(daftarSoal[position]);
  $(".ujian-soal").show();
  $(".ujian-deskripsi").hide();
  soalAktif.show();
}
function SoalSelanjutnya(){
  if (position+1 < totalSoal){
    $(".soal-item").hide();
    $(daftarSoal[++position]).show();
  }
  ButtonHider();
}
function SoalSebelumnya(){
  if (position-1 > -1){
    $(".soal-item").hide();
    $(daftarSoal[--position]).show();
  }
  ButtonHider();
}

function ButtonHider(){
  $("#button_next").show();
  $("#button_prev").show();
  $("#button_fin").hide();
  if (position == 0){
    $("#button_prev").hide();
  } 
  if (position+1 == totalSoal){
    $("#button_next").hide();
    $("#button_fin").show();
  } 
}
$('#register-modal').Carousel({

});
</script>
@endsection