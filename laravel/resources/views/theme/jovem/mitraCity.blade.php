@extends('theme.jovem.layout.index')

@section('content')
<div class="row" style="min-height:70%;">
  <div class="col-md-12">
    <div class="main-banner six" style="background:transparent url( {{ asset('theme/enagic/images/header-mitra.jpg') }} ) repeat scroll center top">
      <div class="container">
      </div>
    </div>
    <div class="breadcrumb">
      <div class="container">
        <ul class="list-unstyled list-inline">
          <li><a href="{!! URL::to('/') !!}">Home</a></li>
          <li><a href="javascript:void(0);">Find Mitra Gemarsehati</a></li>
        </ul>
      </div>
    </div>
    <div class="main-container">    
      <!-- MAP START -->
      <script type="text/javascript">
      var mitraJson ='<?php echo json_encode($data['mitraDetail'])?>';
      var currentLocation = '';
      var nearbyDistance = 20;

      function showMitraByCity(id) {
        //$("#accordion-faqs").append(id);
        $("#accordion-faqs").empty();
        var count = 0;
        var result = jQuery.parseJSON(mitraJson);
        for(var k in result) {
          if(k == 0 || result[k].kota_id != id)
            continue;
          $("#accordion-faqs").append(getAppendedMitra(result[k], k));
          count++;
        }
        if(count == 0)
          $("#accordion-faqs").append("Not found");
      }  

      function getAppendedMitra(mitra, k) {
        var appendStr = "";

        //<!-- Panel Heading Starts -->
        var str1 = "<div class='panel'>";
        str1 += "<div class='panel-heading'>";
        str1 += "<h5 class='panel-title'>";
        str1 += "<a class='collapsed' aria-expanded='false' data-toggle='collapse' data-parent='#accordion-faqs' href='#collapse" + k + "'>";
        str1 += mitra.nama;
        str1 += "<span class='fa pull-right fa-plus'></span></a>";
        str1 += "</h5>";
        str1 += "</div>";
        //<!-- Panel Heading Ends -->
        //<!-- Panel Body Starts -->
        str1 += "<div style='height: 0px;' aria-expanded='false' id='collapse" + k + "' class='panel-collapse collapse'>";
        str1 += "<div class='panel-body'>";
        str1 += "<p>";
        str1 += "<div class='panel-body'>";
        str1 += "<ul class='list-unstyled'>";
        str1 += "<li class='row'>";
        str1 += "<span class='col-sm-4 col-xs-12'>"; 
        str1 += "<img src=" + mitra.foto + " alt='Profile Image' class='img-responsive img-center-xs' width='150'>";
        str1 += "</span>";
        str1 += "<span class='col-sm-4 col-xs-12'><strong>Email</strong><br>";
        str1 += "<strong>No. HP</strong><br>";
        str1 += "<strong>Enagic ID</strong><br>";
        str1 += "<strong>Alamat</strong><br>";
        str1 += "<strong>No. Tlp</strong><br>";
        str1 += "<strong>Facebook</strong>";
        str1 += "</span>";
        str1 += "<span class='col-sm-4 col-xs-12'>";
        str1 += mitra.email + "<br>";
        str1 += mitra.nohp + "<br>";
        str1 += mitra.enagic_id + "<br>";
        str1 += mitra.alamat + "<br>";
        str1 += mitra.notelp + "<br>";
        str1 += "<a href='//" + mitra.facebook + "'>" + mitra.facebook + "</a>";
        str1 += "</span>";
        str1 += "</li>";
        str1 += "</ul>";
        str1 += "</div>";
        str1 += "</p>";
        str1 += "</div>";
        str1 += "</div>";
        //<!-- Panel Body Ends -->
        appendStr = str1;

        return appendStr;
      }
      </script>
      <!-- MAP END -->
      <h3 class="main-heading2 nomargin-top">Gemarsehati Mitra</h3>
      <div class="box-body">
        <?php
          $arrCity = array();
          foreach($data['city'] as $row)
          {
            $arrCity[$row->id] = $row->name;
          }
        ?>
        <div class="form-group">
          {!! Form::label('ms_city_id','Pilih Kota : ') !!}
          <div class="col-md-12">
            <div class="col-md-6">
              {!! Form::select('ms_city_id',$arrCity,null, ['class' => 'form-control']) !!}
            </div>
            <button onclick="showMitraByCity(document.getElementById('ms_city_id').value)" class="btn btn-info">Search</button> 
          </div>
        </div>
      </div>
      <div class="spacer-block"></div>
      <div class="panel-group" id="accordion-faqs">
      </div>
    </div>
  </div>
</div>

@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/jovem/plugins/pretty-photo/css/prettyPhoto.css') }}">

@endsection

@section('custom-footer')
<script type="text/javascript" src="{{ asset('theme/jovem/plugins/pretty-photo/js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
   $(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square', autoplay_slideshow: false});

  });
</script>
@endsection