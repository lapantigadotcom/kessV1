@extends('theme.jovem.layout.index')

@section('content')
<div class="row" style="min-height:70%;">
  <div class="col-md-12">
    <div class="main-banner six">
      <div class="container">
        <h2><span>Mitra Dashboard</span></h2>
      </div>
    </div>
    <div class="breadcrumb">
      <div class="container">
        <ul class="list-unstyled list-inline">
          <li><a href="{!! URL::to('/') !!}">Home</a></li>
          <li><a href="javascript:void(0);">My Profile</a></li>
        </ul>
      </div>
    </div>
    <div class="main-container">    
      <!-- Doctor Profile Starts -->
        <div class="row">
          <div class="col-sm-5 col-xs-12">
            <div class="profile-block">
              <div class="panel panel-profile">
                <div class="panel-heading">
                  <img src="@if($data['mitra']->detail->media != null) {{ asset('upload/media/'.$data['mitra']->detail->media->file) }} @endif" width="470" alt="Profile Image" class="img-responsive img-center-xs">
                  <h3 class="panel-title">{{ $data['mitra']->name }}</h3>
                  <p class="caption">{{ $data['mitra']->rank }}</p>
                </div>
                <div class="panel-body">
                  <ul class="list-unstyled">
                    <li class="row">
                      <span class="col-sm-4 col-xs-12"><strong>Email</strong></span>
                      <span class="col-sm-8 col-xs-12">{{ $data['mitra']->email }}</span>
                    </li>
                    <li class="row">
                      <span class="col-sm-4 col-xs-12"><strong>No. HP</strong></span>
                      <span class="col-sm-8 col-xs-12">{{ $data['mitra']->detail->nohp }}</span>
                    </li>
                    <li class="row">
                      <span class="col-sm-4 col-xs-12"><strong>Enagic ID</strong></span>
                      <span class="col-sm-8 col-xs-12">{{ $data['mitra']->detail->enagic_id }}</span>
                    </li>
                    <li class="row">
                      <span class="col-sm-4 col-xs-12"><strong>Alamat</strong></span>
                      <span class="col-sm-8 col-xs-12">{{ $data['mitra']->detail->address }}</span>
                    </li>
                  </ul>
                </div>
                <div class="panel-footer text-center-md text-center-sm text-center-xs">
                  <div class="row">
                    <div class="col-lg-4 col-xs-12">
                      <ul class="list-unstyled list-inline sm-links">
                        <li><a href="//{{ $data['mitra']->detail->twitter }}"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="//{{ $data['mitra']->detail->facebook }}"><i class="fa fa-facebook"></i></a></li>                         
                      </ul>
                    </div>
                    @if(Auth::check())
                    @if($data['mitra']->id == Auth::user()->id)
                    <div class="col-lg-4 col-xs-12">
                      <a href="{{ route('home.profile.edit', Auth::user()->id) }}" class="btn btn-secondary text-uppercase">Edit Profile</a>
                    </div>
                    <div class="col-lg-4 col-xs-12">
                      <a href="{{ route('home.testimoni') }}" class="btn btn-secondary text-uppercase">Beri Testimoni</a>
                    </div>
                    @endif
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
          @if(Auth::check())
          <div class="col-sm-7 col-xs-12">
            <div class="profile-details">
              <h3 class="main-heading2">Gemarsehati </h3>
              <h4>Video Edukasi / Presentasi</h4>
              <!-- Latest News Carousel Starts -->
              <div id="video-carousel" class="news-carousel carousel slide" data-ride="carousel">
              <!-- Wrapper for Slides Starts -->
                <div class="carousel-inner">
                  @for($i = 0; $i < $data['video']->count(); $i++)
                  <!-- Doctor Bio #1 Starts -->
                  <div class="item @if($i == 0) active @endif">
                    <!-- News Post Starts -->
                    <?php $counter = 3; ?>
                    @while($counter--)                  
                      <?php 
                      if($i + 1 > $data['video']->count())
                        break;
                      $content = $data['video'][$i];
                      ?>         
                      <div class="inner">
                        <h5>{{ $content->title }}</h5>
                        <p>
                          {{ $content->description }}
                        </p>
                        <p>
                          <a href="{{ asset('upload/file/'.$content->file) }}" class="btn btn-secondary text-uppercase">download video</a>
                        </p>
                      </div>
                      <?php $i++; ?>
                    @endwhile
                    <?php $i--; ?>
                    <!-- News Post Ends -->   
                  </div>
                  @endfor
                </div>
                <!-- Controls Starts -->
                <a class="left carousel-control" href="#video-carousel" role="button" data-slide="prev">
                  <span class="fa fa-angle-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#video-carousel" role="button" data-slide="next">
                  <span class="fa fa-angle-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
                <!-- Controls Ends -->
              </div>
            </div>
          </div>
          @endif
        </div>
      <!-- Profile Ends -->
      <!-- Spacer Block Starts -->
        <div class="spacer-block"></div>
      <!-- Spacer Block Ends --><!-- Related Best Doctors Starts --> 
        @if(Auth::check())
        <h2 class="main-heading2 nomargin-top">Artikel Khusus Mitra</h2>
        <!-- Latest News Carousel Starts -->
        <div id="news-carousel" class="news-carousel carousel slide" data-ride="carousel">
        <!-- Wrapper for Slides Starts -->
          <div class="carousel-inner">
            @for($i = 0; $i < $data['pdf']->count(); $i++)
            <!-- Doctor Bio #1 Starts -->
              <div class="item @if($i == 0) active @endif">
                <div class="row">
                  <!-- News Post Starts -->
                  <?php $counter = 3; ?>
                  @while($counter--)                  
                  <?php 
                  if($i + 1 > $data['pdf']->count())
                    break;
                  $content = $data['pdf'][$i];
                  ?>
                  <div class="col-sm-4 col-xs-12">
                    <div class="news-post-box">
                      <img src="@if($content->media != null) {{ asset('upload/media/'.$content->media->file) }} @endif" alt="enagic2" class="img-responsive img-center-sm img-center-xs">
                      <div class="inner">
                        <h5>
                          <a href="#">{{ $content->title }}</a>
                        </h5>
                        <ul class="list-unstyled list-inline post-meta">
                          <li>
                            <i class="fa fa-calendar"></i> {{ $content->created_at }}
                          </li>                 
                        </ul>
                        <p>
                          {{ $content->description }}
                        </p>
                        <a href="{{ asset('upload/file/'.$content->file) }}" class="btn btn-secondary">                
                          <i class="fa fa-arrow-circle-down"></i> download
                        </a>
                      </div>    
                    </div> 
                  </div>
                  <?php $i++; ?>
                  @endwhile
                  <?php $i--; ?>
                  <!-- News Post Ends -->     
                </div>
              </div>
            <!-- Doctor Bio #1 Ends -->
            @endfor
            </div>
            <!-- Controls Starts -->
            <a class="left carousel-control" href="#news-carousel" role="button" data-slide="prev">
              <span class="fa fa-angle-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#news-carousel" role="button" data-slide="next">
              <span class="fa fa-angle-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
            <!-- Controls Ends -->
        </div>
        @endif
      <!-- Related Best Doctors Ends -->
      </div>
    </div>
  </div>
</div>

@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/jovem/plugins/pretty-photo/css/prettyPhoto.css') }}">

@endsection

@section('custom-footer')
<script type="text/javascript" src="{{ asset('theme/jovem/plugins/pretty-photo/js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
   $(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square', autoplay_slideshow: false});

 });
</script>
@endsection