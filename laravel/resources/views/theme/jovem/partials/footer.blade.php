<!-- Footer -->
<footer id="footer" class="footer divider layer-overlay overlay-dark-9" data-bg-img="{{ asset('/images/bg_footer_korlantas.jpg')}}">
	<div class="container">
	  <div class="row border-bottom">
	    <div class="col-sm-6 col-md-3">
	      <div class="widget dark">
	        <h4 class="widget-title">Korlantas Polri</h4>
	        <p>{{ $data['contact']->address}}</p>
	        <ul class="list-inline mt-5">
	          <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-color-2 mr-5"></i> <a class="text-gray" href="#">{{$data['contact']->telephone}}</a> </li>
	          <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-color-2 mr-5"></i> <a class="text-gray" href="#">{{$data['contact']->email}}</a> </li>
	          <li class="m-0 pl-10 pr-10"> <i class="fa fa-globe text-theme-color-2 mr-5"></i> <a class="text-gray" href="#">{{$data['contact']->website}}</a> </li>
	        </ul>
	      </div>
	    </div>
	    <div class="col-sm-6 col-md-3">
	      <div class="widget dark">
	        <h4 class="widget-title">Tautan Lainnya</h4>
	        <ul class="list angle-double-right list-border">
	          <li><a href="#">Polri</a></li>
	          <li><a href="#">Itwasum</a></li>
	          <li><a href="#">Lantas</a></li>
	          <li><a href="#">Propam</a></li>
	         <li><a href="#">NTMC</a></li>              
	        </ul>
	      </div>
	    </div>
	    <div class="col-sm-6 col-md-3">
	      <div class="widget dark">
	        <h4 class="widget-title">Twitter Korlantas</h4>
	       <!--  <div class="twitter-feed list-border clearfix" data-username="ntmclantaspolri" data-count="2"></div> -->
	      <div class="twitter-feed list-border clearfix" data-username="" data-count="2"></div>

	      </div>
	    </div>
	    <div class="col-sm-6 col-md-3">
	      <div class="widget dark">
	        <h4 class="widget-title line-bottom-theme-colored-2">Jam Operasional Korlantas</h4>
	        <div class="opening-hourse">
	          <ul class="list-border">
	            <li class="clearfix"> <span> Senin - Jumat :  </span>
	              <div class="value pull-right"> 08.00 - 15.00 </div>
	            </li>
	            
	            <li class="clearfix"> <span> Sabtu : </span>
	              <div class="value pull-right"> 08.00 - 13.00 </div>
	            </li>
	            <li class="clearfix"> <span></span>
	              <div class="value pull-right"> <img src="{{ asset('/images/badged_lantas.png')}}"> </div>
	            </li>

	          </ul>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="row mt-30">
	    <div class="col-md-2">
	      <div class="widget dark">
	        <h5 class="widget-title mb-10">SMS Center Korlantas</h5>
	        <div class="text-gray">
	          {{$data['contact']->mobile}}<br>
	         </div>
	      </div>
	    </div>
	    <div class="col-md-3">
	      <div class="widget dark">
	        <h5 class="widget-title mb-10">Korlantas Sosmed</h5>
	        <ul class="styled-icons icon-bordered icon-sm">
	          <li><a href="{{$data['contact']->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
	          <li><a href="{{$data['contact']->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
	          
	        </ul>
	      </div>
	    </div>
	     
	  </div>
	</div>
	<div class="footer-bottom bg-black-333">
	  <div class="container pt-20 pb-20">
	    <div class="row">
	      <div class="col-md-6">
	        <p class="font-11 text-black-777 m-0">Copyright &copy;2016 Korlantas Polri. All Rights Reserved. Best view on 1280 x 720 px</p>
	      </div>
	      <div class="col-md-6 text-right">
	        <div class="widget no-border m-0">
	          <ul class="list-inline sm-text-center mt-5 font-12">
	            <li>
	              <a href="#">Korlantas</a>
	            </li>
	            <li>|</li>
	            <li>
	              <a href="#">Panduan</a>
	            </li>
	            <li>|</li>
	            <li>
	              <a href="#">Lokasi Pendaftaran</a>
	            </li>
	          </ul>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</footer>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>

<!-- Footer Scripts -->
<!-- external javascripts -->
<script src="{{ asset('/js/jquery-2.2.0.min.js')}}"></script>
<script src="{{ asset('/js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('/js/bootstrap.min.js')}}"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="{{ asset('js/jquery-plugin-collection.js')}}"></script>
<!-- JS | Custom script for all pages -->
<script src="{{ asset('js/custom.js')}}"></script>
<script src="{{ asset('js/extra.js')}}"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="{{ asset('js/revolution-slider/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{ asset('js/revolution-slider/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{ asset('js/extra-rev-slider.js')}}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.video.min.js')}}"></script>
@yield('custom-footer')
