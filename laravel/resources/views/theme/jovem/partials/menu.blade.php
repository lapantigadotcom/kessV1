<div class="header__container">
  <div class="container">
    <header class="header">
      <div class="header__logo">
        <a href="">
          <img class="img-responsive" srcset="/assets/tema/lapantiga/images/logo-lapantiga.png" alt="logo lapantiga" src="/assets/tema/lapantiga/images/logo-lapantiga.png">
        </a>
        <button data-target="#cargopress-navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
          <span class="navbar-toggle__text">MENU</span>
          <span class="navbar-toggle__icon-bar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </span>
        </button>
      </div>
      <div class="header__navigation">
        <nav class="collapse navbar-collapse" id="cargopress-navbar-collapse">
          <ul class="main-navigation js-main-nav js-dropdown">
            <li class="current-menu-item">
              <a href="">Home</a>
            </li>
            <li><a style="cursor: pointer;" target="_self" id="aboutjs">About</a></li>
            <li><a style="cursor: pointer;" target="_self" id="workjs">Workarea</a></li>
            <li><a href="portfolio.html">Portfolio</a></li>

            <li class="menu-item-has-children">
              <a href="https://www.lapantiga.com/#blogthumb">Blogs</a>
              <ul role="menu" class="sub-menu">
                <li><a href="tutorial/">Tutorial</a></li>
                <li><a href="news/">News</a></li>
                <li><a href="technology/">Technology</a></li>
              </ul>
            </li>
            <li><a href="contact-us.html">Contact</a></li>
          </ul>
        </nav>
      </div>
      <div class="header__widgets">
        <div class="widget-icon-box">
          <div class="icon-box">  
            <i class="fa fa-headphones"></i>
            <h4 class="icon-box__title">Call Us Now</h4>
            <span class="icon-box__subtitle">+62.31.50.43.83.2</span>
          </div>  
        </div>    
        <div class="widget-icon-box">     
          <div class="icon-box">  
            <i class="fa fa-twitter"></i>
            <h4 class="icon-box__title">Follow Us</h4>
            <span class="icon-box__subtitle">@lapantigadotcom</span>
          </div>
        </div>  
        <div class="widget-icon-box">
          <div class="icon-box">
            <i class="fa fa-envelope-o"></i>
            <h4 class="icon-box__title">Email Us</h4>
            <span class="icon-box__subtitle">info@lapantiga.com</span>
          </div>
        </div>
        <a target="_self" id="button_requestQuote" class="btn btn-info">REQUEST A QUOTE</a>
      </div>  
      <div class="header__navigation-widgets">
        <a target="_blank" href="https://plus.google.com/+lapantigadotcom" class="social-icons__link"><i class="fa fa-google-plus"></i></a>
        <a target="_blank" href="https://twitter.com/lapantigadotcom" class="social-icons__link"><i class="fa fa-twitter"></i></a>
      </div>
    </header>
  </div>
</div>