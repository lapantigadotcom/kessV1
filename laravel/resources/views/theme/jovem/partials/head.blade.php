<?php
	$meta = $general->meta_description;
	$keyword = $general->meta_keywords;
?>
@if(isset($data['content']->meta_description))
	<?php 
	$meta.=$data['content']->meta_description;
	 ?>
@endif
<meta name="description" content="{{ $meta }}">
<meta name="keywords" content="{{ $keyword }}">
<meta name="author" content="lapantiga.com">

<link href="{{ asset('/images/favicon32.png')}}" rel="shortcut icon" type="image/png">
<link href="{{ asset('/images/apple-touch-icon.png')}}" rel="apple-touch-icon">
<link href="{{ asset('/images/apple-touch-icon-72x72.png')}}" rel="apple-touch-icon" sizes="72x72">
<link href="{{ asset('/images/apple-touch-icon-114x114.png')}}" rel="apple-touch-icon" sizes="114x114">
<link href="{{ asset('/images/apple-touch-icon-144x144.png')}}" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="{{ asset('/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/animate.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/css-plugin-collections.css')}}" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="{{ asset('/css/menuzord-skins/menuzord-rounded-boxed.css')}}" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="{{ asset('/css/style-main.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="{{ asset('/css/preloader.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="{{ asset('/css/custom-bootstrap-margin-padding.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="{{ asset('/css/responsive.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<link href="{{ asset('/css/style.css')}}" rel="stylesheet" type="text/css">

<!-- Revolution Slider 5.x CSS settings -->
<link  href="{{ asset('/js/revolution-slider/css/settings.css')}}" rel="stylesheet" type="text/css"/>
<link  href="{{ asset('/js/revolution-slider/css/layers.css')}}" rel="stylesheet" type="text/css"/>
<link  href="{{ asset('/js/revolution-slider/css/navigation.css')}}" rel="stylesheet" type="text/css"/>

<!-- CSS | Theme Color -->
<link href="{{ asset('/css/colors/theme-skin-color-set-5.css')}}" rel="stylesheet" type="text/css">


<!-- ================================================================================================================= -->
<?php
	// require_once('F:\xampp\htdocs\enagic\vendor\twitter-api-php\TwitterAPIExchange.php');

?>
@yield('custom-head')