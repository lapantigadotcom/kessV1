@extends('theme.jovem.layout.index')

@section('content')
 
<div class="row" style="min-height:70%;">
  <div class="col-md-12">
    <div class="main-banner six" style="background:transparent url( {{ asset('theme/enagic/images/header-insert2.jpg') }} ) repeat scroll center top">
      <div class="container">
        <h2><span>Mitra Register</span></h2>
      </div>
    </div>
    <div class="breadcrumb">
      <div class="container">
        <ul class="list-unstyled list-inline">
          <li><a href="{!! URL::to('/') !!}">Home</a></li>
          <li><a href="javascript:void(0);">Register</a></li>
        </ul>
      </div>
    </div>
    <div class="main-container">    
      <div class="contact-content">
        <div class="tab-pane fade in active" id="tab-1">
          <div class="row">
            <div class="col-sm-8 col-xs-12">
              <h3>Gemarsehati Registrasi Mitra   </h3>
              <h7>Please Field Form Below (*) required Field</h7>
              @include('page.partials.notification')
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                <strong>Maaf!</strong> Form yang anda isi belum lengkap.<br><br>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif
                <div class="box-body">
                {!! Form::open(['route'=>'home.doRegister', 'method' => 'POST','files' => true]) !!}
                  <?php
                    $arrGender = array();
                    $arrCity = array();
                    foreach($data['gender'] as $row)
                    {
                      $arrGender[$row->id] = $row->name;
                    }
                    foreach($data['city'] as $row)
                    {
                      $arrCity[$row->id] = $row->name;
                    }
                  ?>
                  @include('theme.jovem.register.form',['buttonSubmit' => 'SUBMIT'])
                {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
            <!-- Box #1 Starts -->
              <div class="cblock-1">
                <span class="icon-wrap"><i class="fa fa-map-marker"></i></span>
                <h4>Our Contact</h4>
                <ul class="list-unstyled">
                  <li> Untuk Mendapatkan informasi lebih lanjut silahkan kontak mitra terdekat kami di kota Anda. 
                    <a href="{{ route('home.mitraCity') }}" target="_blank"><br>[Temukan Mitra]</a>

                    <!--{{ $data['contact']->address }} --></li>
                 <!-- <li> {{ $data['contact']->email }}</li>
                  <li> {{ $data['contact']->telephone }} </li> -->
                </ul>
              </div>
            <!-- Box #1 Ends -->             
            </div>
            <div class="col-sm-4 col-xs-12">
 
              <div class="cblock-1">
                <span class="icon-wrap red"><i class="fa fa-exclamation"></i></span>
                <h4>Note</h4>
                <ul class="list-unstyled">
                  <li>Setelah Pendaftaran Berhasil, Kami akan segera memverifikasi Anda. Terima Kasih </li>
                  <li>Call Support:</li>
                  <li>
                  <strong> Silahkan Kontak Referal Anda.<!--{{ $data['contact']->telephone }}  --></strong> </li>
                </ul>
              </div>   

              <div class="cblock-1">
                <span class="icon-wrap"><i class="fa fa-map-o"></i></span>
                <h4>Panduan Koordinat</h4>
                <ul class="list-unstyled">
                  <li>Untuk Mendapatkan Koordinat lokasi Anda, Silahkan ikuti panduan berikut </li>
                  <li><a href="https://support.google.com/maps/answer/18539?source=gsearch&hl=en" target="_blank">[ Lihat Panduan ]</a></li>
                </ul>
              </div>


            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

    @endsection

    @section('custom-head')
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/jovem/plugins/pretty-photo/css/prettyPhoto.css') }}">
    
    @endsection

    @section('custom-footer')
    <script src="{{ asset('plugins/bootstrapFileStyle/bootstrap-filestyle.min.js') }}"></script>
    <script type="text/javascript">
      $('input[type=file]').filestyle();
    </script>
    <script type="text/javascript" src="{{ asset('theme/jovem/plugins/pretty-photo/js/jquery.prettyPhoto.js') }}"></script>
    <script type="text/javascript">
      $(document).ready(function(){
       $(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square', autoplay_slideshow: false});

     });
    </script>
    @endsection