@extends('theme.jovem.layout.index')

@section('content')
 
<div class="row" style="min-height:70%;">
  <div class="col-md-12">
    <div class="main-banner six" style="background:transparent url( {{ asset('theme/enagic/images/header-insert2.jpg') }} ) repeat scroll center top">
      <div class="container">
        <h2><span>Leaders</span></h2>
      </div>
    </div>
    <div class="breadcrumb">
      <div class="container">
        <ul class="list-unstyled list-inline">
          <li><a href="{!! URL::to('/') !!}">Home</a></li>
          <li><a href="javascript:void(0);">Leaders</a></li>
        </ul>
      </div>
    </div>
    <div class="tabs-wrap">    
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab-1">
          <div class="row">
            <div class="col-md-12 gallery" style="float:left;">
            <?php 
            $i = 1;
            ?>
             @foreach($data['leaders'] as $row)
             <?php               
              echo "<div class='col-md-3 col-sm-6 col-xs-12'>";
             ?>
              <div class="box1 text-center">
                <div class="profile-img">
                  <img src="@if($row->media != null) {{ asset('upload/media/'.$row->media->file) }} @endif" alt="{{ $row->name!=''?$row->name:'Profile'.$i }}" class="img-responsive img-center-sm img-center-xs" style="max-height:220px">
                  <div class="overlay">
                    <ul class="list-unstyled list-inline sm-links">
                      <li><a href="//{{ $row->twitter!=''?$row->twitter:'' }}"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="//{{ $row->facebook!=''?$row->facebook:'' }}"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="//{{ $row->skype!=''?$row->skype:'' }}"><i class="fa fa-skype"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="inner">
                  <h5>{{ $row->name!=''?$row->name:'Profile' }}</h5>
                  <p class="designation">{{ $row->rank_name!=''?$row->rank_name:'Rank' }}</p>
                  <p class="divider"><i class="fa fa-plus-square"></i></p>
                  @foreach($row->achievements as $achievement)
                  <p>
                    {{ $achievement->name!=''?$achievement->name:'Achievement' }}
                  </p>
                  @endforeach
                </div>              
                <a href="//{{ $row->video_profile!=''?$row->video_profile:'#' }}" class="btn btn-transparent inverse text-uppercase">Video Profile</a>
              </div>
              <?php 
                echo "</div>";
                $i++;
               ?>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12 text-right" style="margin-left:auto; margin-right:0;">
    {!! $data['leaders']->render() !!}
  </div>
</div>
<div class="spacer-block"></div>




    @endsection

    @section('custom-head')
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/jovem/plugins/pretty-photo/css/prettyPhoto.css') }}">
    
    @endsection

    @section('custom-footer')
    <script type="text/javascript" src="{{ asset('theme/jovem/plugins/pretty-photo/js/jquery.prettyPhoto.js') }}"></script>
    <script type="text/javascript">
      $(document).ready(function(){
       $(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square', autoplay_slideshow: false});

     });
    </script>
    @endsection