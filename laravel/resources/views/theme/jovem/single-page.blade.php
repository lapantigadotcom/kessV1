<title>Korlantas Polri | {{ $data['content']->title }}</title>
@extends('theme.jovem.layout.index')
@section('content')
<section id="blog" class="">
  <!-- Main Container Starts -->
  <div class="container pt-60 pb-60">

    <div class="main-container">
      <!-- Nested Row Starts -->
      <div class="row">
        <!-- Mainarea Starts -->
        <div class="col-md-9 col-xs-12">
          <!-- News Post Single Starts -->
          <div class="news-post-single">
            <!-- News Post Starts -->
            <article class="news-post">
              <div class="inner">
                <h2>{{ $data['content']->title }}</h2>
                <style type="text/css">
                ul.post-meta {
                  border-bottom: 0.5px solid #ccc; 
                  margin-bottom: 12px;
                }
                ul.post-meta li{
                  padding: 6px
                }
                .news-post-content{
                  margin-top: 1em;
                  margin-bottom: 1em;
                  padding: 1em;
                  padding-left: 0px;
                  border-top: 0.5px solid #ccc;
                  border-bottom: 0.5px solid #ccc;
                }
                </style>
                <ul class="list-unstyled list-inline post-meta">
                  <li>
                    <i class="fa fa-calendar"></i> 
                    Diposting tgl : {{ date("d M Y" ,strtotime($data['content']->created_at)) }}
                  </li>
                  <li>
                    <i class="fa fa-user"></i> 
                    By <a href="#">{{ $data['content']->user->name }} </a>
                  </li>

                  <li>  <i class="fa fa-tag"></i>
                    @foreach($data['content']->tag as $row)
                    <a href="#">{!! $row->title !!}</a>,
                    @endforeach
                  </li>
                </ul>
                @if($data['content']->mediaPost != null) 
                <center>
                  <img src="{{ asset('upload/media/'.$data['content']->mediaPost->file) }}" alt="#" class="img-responsive img-center-sm img-center-xs">
                </center>
                @endif
                <div class="news-post-content">
                  {!! $data['content']->description !!}
                  <br>
                  <div class="addthis_native_toolbox">
                  </div>
                </div>
              </div>
            </article>
          <!-- News Post Ends -->
          </div>
        <!-- News Post Single Ends -->
          <div class="blog-author-bio">
            <h4 class="side-heading1">Comments</h4>
            <div class="row">
              <div class="col-xs-9 text-justify">
                <div id="disqus_thread"></div>
                <script>
                  (function() {  // DON'T EDIT BELOW THIS LINE
                      var d = document, s = d.createElement('script');
                      
                      s.src = '//gemarsehati.disqus.com/embed.js';
                      
                      s.setAttribute('data-timestamp', +new Date());
                      (d.head || d.body).appendChild(s);
                  })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>    
              </div>
            </div>
          </div>
        </div>
        <style type="text/css">
        .img.dsq-widget-avatar{height: 50px}
        .dsq-widget-meta{font-size: 12px; color: red}
        .dsq-widget-comment{font-size: 12px}
        .dsq-widget-list{padding: 0px;margin: 0px}
        .dsq-widget-meta a{font-size: 12px;color: red}

        </style>
        <!-- Sidearea Starts -->
        <div class="col-md-3 col-xs-12">
          <!-- Categories Starts --><!-- Komentar artikel -->
          <br>
        <!-- /Komentar artikel -->
          <h4 class="side-heading1 top">LATEST ARTICLES</h4>
          <ul class="list-unstyled list-style-1">
            @foreach($data['latest_posts'] as $row)
            <li><a href="#">          
              <a href="{!! route('home.post',[ $row->id,'post' ]) !!}"><h5>  <b> {!! substr(strip_tags($row->title),0, 20).'...' !!} </b> </h5></a>
              <p style="font-size:13px">
                {!! substr(strip_tags($row->description),0, 140).'...' !!}
              </p>
              <small>
                <i class="fa fa-calendar"></i> </i> {{ date("d M Y" ,strtotime($row->created_at)) }} &nbsp;&nbsp;&nbsp;
                <i class="fa fa-user"></i> </i> {{ $row->user->name }} &nbsp;&nbsp;&nbsp;
              </small>
              <hr>
            </li>
            @endforeach
          </ul>
        <!-- Categories Ends -->
        </div>
        <!-- Sidearea Ends -->
      </div>
    </div>
  </div>
</section>
 
@endsection
@section('custom-footer')
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56a630c285f8f2c7" async="async"></script>
<script>
$('#register-modal').Carousel({

});
</script>
@endsection