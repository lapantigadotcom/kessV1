@extends('theme.jovem.layout.index')

@section('content')                       

<!-- Section: home -->
<section id="home">
  <div class="container-fluid p-0">

    <!-- Slider Revolution Start -->
    <div class="rev_slider_wrapper">
      <div class="rev_slider rev_slider_default" data-version="5.0">
        <ul>
          <?php
          $rs = 0;
          ?>
          @foreach($data['slider'] as $slider)
          <!-- SLIDE 2 -->
          <li data-index="rs-{{++$rs}}" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{asset('upload/media/'.$slider->file)}}" data-rotate="0" data-saveperformance="off" data-title="{{$slider->title}}" data-description="">
            <!-- MAIN IMAGE -->
            <img src="{{asset('upload/media/'.$slider->file)}}"  alt=""  data-bgposition="center 40%" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
            <!-- LAYERS -->

            <!-- LAYER NR. 1 -->
            <div class="tp-caption tp-resizeme text-uppercase  bg-dark-transparent-5 text-white font-raleway border-left-theme-color-2-6px border-right-theme-color-2-6px pl-30 pr-30"
              id="rs-2-layer-1"
              data-x="['center']"
              data-hoffset="['0']"
              data-y="['middle']"
              data-voffset="['-90']" 
              data-fontsize="['28']"
              data-lineheight="['54']"
              data-width="none"
              data-height="none"
              data-whitespace="nowrap"
              data-transform_idle="o:1;s:500"
              data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
              data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
              data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
              data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
              data-start="1000" 
              data-splitin="none" 
              data-splitout="none" 
              data-responsive_offset="on"
              style="z-index: 7; white-space: nowrap; font-weight:400; border-radius: 30px;">{{$slider->h1}}
            </div>

            <!-- LAYER NR. 2 -->
            <div class="tp-caption tp-resizeme text-uppercase bg-theme-colored-transparent text-white font-raleway pl-30 pr-30"
              id="rs-2-layer-2"
              data-x="['center']"
              data-hoffset="['0']"
              data-y="['middle']"
              data-voffset="['-20']"
              data-fontsize="['48']"
              data-lineheight="['70']"
              data-width="none"
              data-height="none"
              data-whitespace="nowrap"
              data-transform_idle="o:1;s:500"
              data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
              data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
              data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
              data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
              data-start="1000" 
              data-splitin="none" 
              data-splitout="none" 
              data-responsive_offset="on"
              style="z-index: 7; white-space: nowrap; font-weight:700; border-radius: 30px;">{{$slider->h2}}
            </div>

            <!-- LAYER NR. 3 -->
            <div class="tp-caption tp-resizeme text-white text-center" 
              id="rs-2-layer-3"
              data-x="['center']"
              data-hoffset="['0']"
              data-y="['middle']"
              data-voffset="['50']"
              data-fontsize="['16']"
              data-lineheight="['28']"
              data-width="none"
              data-height="none"
              data-whitespace="nowrap"
              data-transform_idle="o:1;s:500"
              data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
              data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
              data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
              data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
              data-start="1400" 
              data-splitin="none" 
              data-splitout="none" 
              data-responsive_offset="on"
              style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">
              {{$slider->h3}}
              
            </div>
            <!-- LAYER NR. 4 -->  
          </li>
          @endforeach
        </ul>
      </div>
    <!-- end .rev_slider -->
    </div>
    <!-- Slider Revolution Ends -->
  </div>
</section>

<!-- Section: blog -->
<section id="blog" class="">
  <div class="container pt-60 pb-60">
    <div class="section-title mb-0">
      <div class="row">
        <div class="col-md-8">
          <h2 class="mt-0 text-uppercase font-28 line-bottom">Korlantas <span class="text-theme-color-2 font-weight-400">News</span></h2>
          <h4 class="pb-20">Informasi dan berita Korlantas Polri</h4>
       </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="owl-carousel-3col" data-dots="true">
          @foreach($data['latest_news'] as $ln)
            <div class="item">
              <article class="post clearfix maxwidth600 mb-sm-30">
                <div class="entry-header">
                  <div class="post-thumb thumb"> <img src="{{asset('upload/media/'.$ln->file)}}" alt="" class="img-responsive img-fullwidth"> </div>
                  <div class="entry-meta meta-absolute text-center pl-15 pr-15">
                  <div class="display-table">
                    <div class="display-table-cell">
                      <ul>
                        <li><a class="text-white" href="#"><i class="fa fa-eye"></i> 385 <br> View</a></li>
                      </ul>
                    </div>
                  </div>
                  </div>
                </div>
                <div class="entry-content border-1px p-20">
                  <h5 class="entry-title mt-0 pt-0">
                    <a href="{{URL::to('/artikel/gemarsehati='.$ln->id)}}">
                      @if(strlen($ln->title) > 20) 
                        <h4>
                          {{substr($ln->title,0,30)}}...
                        </h4>
                      @else 
                        <h4>
                          {{$ln->title}}
                        </h4>
                      @endif
                    </a>
                  </h5>
                  <p class="text-left mb-20 mt-15 font-13">
                    @if(strlen($ln->description) > 185)
                      {!! substr($ln->description, 0, 185) !!}... 
                    @else
                      {!! $ln->description !!}
                    @endif
                  </p>
                  <a class="btn btn-flat btn-dark btn-theme-colored btn-sm pull-left" href="{{URL::to('/artikel/gemarsehati='.$ln->id)}}">Selengkapnya</a>
                  <ul class="list-inline entry-date pull-right font-12 mt-5">
                    <li><a class="text-theme-colored" href="#">{{$ln->name}} |</a></li>
                    <li><span class="text-theme-colored">{{$ln->created_at}}</span></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
              </article>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Divider: search artikel -->
<section class="bg-theme-color-2">
  <div class="container pt-0 pb-0">
    <div class="row">
      <div class="call-to-action pt-30 pb-20">
        <div class="col-md-6">
          <h3 class="mt-5 mb-5 text-white vertical-align-middle"><i class="fa fa-search mr-10 font-48 vertical-align-middle"></i> Pencarian Artikel / Berita</h3>
        </div>
        <div class="col-md-6">
          <!-- Mailchimp Subscription Form Starts Here -->
          {!! Form::open(array('url' => '/cari','method' => 'post'), ['class' => 'newsletter-form mt-10']) !!}
            <div class="input-group">
              <?php
              $value = null;
              if (isset($data['keyword']))
                $value = $data['keyword'];
              ?>
              {!! Form::text('keyword',$value, ['class' => 'form-control', 'placeholder' => 'Cari Berita', 'class' => 'form-control input-lg font-16', 'data-height' => '45px']) !!}
              <span class="input-group-btn">
                {!! Form::submit('Temukan',['class' => 'btn bg-theme-colored text-white btn-xs m-0 font-14', 'data-height' => '45px']) !!}
              </span>
            </div>
          {!! Form::close()!!}
          <br>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Section:about-->
<section>
  <div class="container pb-60">
    <div class="section-content">
      <div class="row">
        <div class="col-md-8">
          <div class="meet-doctors">
            <h2 class="text-uppercase mt-0 line-height-1"><i class="fa fa-paper-plane"></i> <span class="text-theme-colored">Selamat datang  </span></h2>
            <h6 class="text-uppercase letter-space-5 line-bottom title font-playfair text-uppercase">di website interaktif www.korlantas.polri.go.id</h6>
            <p>Untuk menjalankan fungsi organisasi guna mencapai visi dan misinya, Korlantas Polri dilengkapi dengan program kerja, yang salah satunya adalah <b>program Quick Wins </b> pada fungsi lalu lintas Polri yang terdiri dari 8 program yaitu :</p>
          </div>
          <div class="row mb-sm-30">
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0 mb-20">
               <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-desktop text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Transparan</h5>
                <p class="text-gray">Transparansi pelayanan SSB.</p>
               </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0 mb-20">
               <a class="icon bg-theme-color-2 icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-link text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Online</h5>
                <p class="text-gray">Mewujudkan akses transparansi online</p>
               </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0 mb-20">
               <a class="icon bg-theme-color-2 icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-cogs text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Iptek</h5>
                <p class="text-gray">Mewujudkan iptek kepolisian online</p>
               </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0 mb-20">
               <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-external-link text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Perintek</h5>
                <p class="text-gray">Perangkat informasi teknologi kepolisian .</p>
               </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0">
               <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-balance-scale text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Hukum</h5>
                <p class="text-gray">Mewujudkan hukum kepolisian online.</p>
               </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0">
               <a class="icon bg-theme-color-2 icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-users text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Komunitas</h5>
                <p class="text-gray">Implementasi strategi polmas terhadap masyarakat/komunitas.</p>
               </div>
              </div>
            </div>
             <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0">
               <a class="icon bg-theme-color-2 icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-bolt text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Quick Response</h5>
                <p class="text-gray">Ketanggapsegera-an  </p>
               </div>
              </div>
            </div>
             <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0">
               <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip" >
                <i class="fa fa-road text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Penanganan</h5>
                <p class="text-gray">Penanganan kecelakaan dan pelanggaran lalu lintas</p>
               </div>
              </div>
            </div>

          </div>
        </div>
        <div class="col-md-4">
         <div class="p-30 mt-0 bg-theme-colored">
          <h3 class="title-pattern mt-0"><span class="text-white">Respon <span class="text-theme-color-2">Cepat</span></span></h3>
          <!-- Appilication Form Start-->
          {!! Form::open(array('route' => 'home.adukan', 'method' => 'post', 'class'=>'reservation-form mt-20', 'id' =>'reservation_form')) !!}
          <!-- <form id="reservation_form" name="reservation_form" class="reservation-form mt-20" method="post" action="{{URL::to('adukan')}}"> -->
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group mb-20">
                  <input placeholder="Nama Lengkap" type="text" id="reservation_name" name="nama_lengkap" required="" class="form-control">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input placeholder="Email" type="text" id="reservation_email" name="email" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input placeholder="Handphone" type="text" id="reservation_phone" name="hp" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <div class="styled-select">
                    <?php
                    $kategoriList = [];
                    foreach ($data['kategori_aduan'] as $key => $value) {
                      $kategoriList[$value->id] = $value->title;
                    }
                    ?>
                    {!! Form::select('kategori', $kategoriList, null, ['class' => 'form-control', 'id' => 'kategori', 'required']) !!}
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input placeholder="Alamat Lengkap" type="text" id="alamat" name="alamat" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-12 ">
                <div class="form-group">
                  <textarea placeholder="Pesan.." rows="3" class="form-control required" name="isi" id="form_message" aria-required="true"></textarea>
                </div>
              </div>
              <div class="col-sm-12 text-center">
                <div class="form-group mb-20">
                  <img src="./images/captcha.jpg">
                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group mb-0 mt-10">
                  <input name="form_botcheck" class="form-control" type="hidden" value="">
                  <button type="submit" class="btn btn-colored btn-theme-color-2 text-white btn-lg btn-block" data-loading-text="Please wait...">Kirim ></button>
                </div>
              </div>
            </div>
          <!-- </form> -->
          {!! Form::close() !!}
          <!-- Application Form End-->           
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Divider: statistik -->
<section class="divider parallax layer-overlay overlay-theme-colored-9" data-bg-img="./images/slider/bg_middle_section_sim_keliling.jpg" data-parallax-ratio="0.7">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
        <div class="funfact text-center">
          <i class="fa fa-area-chart mt-5 text-theme-color-2"></i>
          <h2 data-animation-duration="2000" data-value="15698" class="animate-number text-white mt-0 font-38 font-weight-500">0</h2>
          <h5 class="text-white text-uppercase mb-0">Perhari</h5>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
        <div class="funfact text-center">
          <i class="fa fa-car mt-5 text-theme-color-2"></i>
          <h2 data-animation-duration="2000" data-value="5652" class="animate-number text-white mt-0 font-38 font-weight-500">0</h2>
          <h5 class="text-white text-uppercase mb-0">SIM A</h5>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
        <div class="funfact text-center">
          <i class="fa fa-truck mt-5 text-theme-color-2"></i>
          <h2 data-animation-duration="2000" data-value="2412" class="animate-number text-white mt-0 font-38 font-weight-500">0</h2>
          <h5 class="text-white text-uppercase mb-0">SIM B 1</h5>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 mb-md-0">
        <div class="funfact text-center">
          <i class="fa fa-motorcycle mt-5 text-theme-color-2"></i>
          <h2 data-animation-duration="2000" data-value="7085" class="animate-number text-white mt-0 font-38 font-weight-500">0</h2>
          <h5 class="text-white text-uppercase mb-0">SIM C</h5>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Section: Soal-soal -->
<section class="bg-lighter">
  <div class="container pb-40">
    <div class="section-title mb-0">
    <div class="row">
      <div class="col-md-8">
        <h2 class="text-uppercase font-28 line-bottom mt-0 line-height-1">Soal <span class="text-theme-color-2 font-weight-400">Latihan</span></h2>
        <h4 class="pb-20">Persiapan Ujian Surat Ijin Mengemudi</h4>
     </div>
    </div>
    </div>
    <div class="section-content">
     <div class="row">
        @foreach($data['soal_index'] as $s)
          <div class="col-sm-6 col-md-3">
            <div class="service-block bg-white">
              <div class="thumb"> <img alt="featured project" src="{{URL::to('upload/media/'.$s->file)}}" class="img-fullwidth">
               <h4 class="text-white mt-0 mb-0"><span class="price">{{$s->title}}</span></h4>
              </div>
              <div class="content text-left flip p-25 pt-0"><br>
                 <p style="height:100px; overflow: hidden">{{$s->description}}</p>
                 <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="{{URL::to('ujian/korlantas/'.$s->code)}}" style="position-bottom: 10px">Selengkapnya</a>
              </div>
              <div class="row">
              </div>
            </div>
          </div>
        @endforeach
<!--         <div class="col-sm-6 col-md-3">
          <div class="service-block bg-white">
            <div class="thumb"> <img alt="featured project" src="./images/sim_a.jpg" class="img-fullwidth">
             <h4 class="text-white mt-0 mb-0"><span class="price"> SIM A</span></h4>
            </div>
            <div class="content text-left flip p-25 pt-0"><br>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam ipsum quis ipsum facilisis sit amet.</p>
             <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="#">Selengkapnya</a>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="service-block bg-white">
            <div class="thumb"> <img alt="featured project" src="./images/sim_b1.jpg" class="img-fullwidth">
              <h4 class="text-white mt-0 mb-0"><span class="price"> SIM B 1</span></h4>
             </div>
            <div class="content text-left flip p-25 pt-0"><br>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam ipsum quis ipsum facilisis sit amet.</p>
             <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="#">Selengkapnya</a>
           </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="service-block bg-white">
            <div class="thumb"> <img alt="featured project" src="./images/sim_b2.jpg" class="img-fullwidth">
              <h4 class="text-white mt-0 mb-0"><span class="price"> SIM B 2</span></h4>
             </div>
            <div class="content text-left flip p-25 pt-0">
               <br>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam ipsum quis ipsum facilisis sit amet.</p>
             <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="#">Selengkapnya</a>
           </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="service-block bg-white">
            <div class="thumb"> <img alt="featured project" src="./images/sim_c.jpg" class="img-fullwidth">
              <h4 class="text-white mt-0 mb-0"><span class="price"> SIM C</span></h4>
            </div>
            <div class="content text-left flip p-25 pt-0"><br>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam ipsum quis ipsum facilisis sit amet.</p>
             <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="#">Selengkapnya</a>
            </div>
          </div>
        </div> -->
     </div>
    </div>
  </div>
</section>

<!-- Section: Why Choose Us -->
<section id="event">
  <div class="container">
    <div class="section-content">
     <div class="row">
        <div class="col-md-5"> 
         <img src="./images/kpl_korlantas.png" class="img-fullwidth" alt=" Drs. Agung Budi Maryoto MSi">
        </div>
        <div class="col-md-7 pb-sm-20">
          <!-- <h3 class="title line-bottom mb-20 font-28 mt-0 line-height-1">Kata <span class="text-theme-color-2 font-weight-400">Sambutan</span></h3> -->
          <h3 class="title line-bottom mb-20 font-28 mt-0 line-height-1">{{$data['sambutan']->title}}</span></h3>
          <!-- <h6 class="text-uppercase letter-space-5  title font-playfair text-uppercase">Kepala Korps Lalu Lintas Polri<br>Drs. Agung Budi Maryoto, MSi.</h6> -->
           <p class="mb-20">
            @if(strlen($data['sambutan']->description) > 320)
              {!!substr($data['sambutan']->description, 0, 319)!!}...
              <b>Selengkapnya></b>
            @else
              {!!$data['sambutan']->description!!}
            @endif
          </p>
          <div class="col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
            <div class="icon-box text-center pl-0 pr-0 mb-0">
              <a href="#" class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-md">
                <i class="fa fa-building text-white"></i>
              </a>
              <h5 class="icon-box-title mt-15 mb-10 letter-space-4 text-uppercase"><strong>Satpas</strong></h5>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
            <div class="icon-box text-center pl-0 pr-0 mb-0">
              <a href="#" class="icon bg-theme-color-2 icon-circled icon-border-effect effect-circle icon-md">
                <i class="fa fa-truck text-white"></i>
              </a>
              <h5 class="icon-box-title mt-15 mb-10 letter-space-4 text-uppercase"><strong>SIM Keliling</strong></h5>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
            <div class="icon-box text-center pl-0 pr-0 mb-0">
              <a href="#" class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-md">
                <i class="fa fa-home text-white"></i>
              </a>
              <h5 class="icon-box-title mt-15 mb-0 letter-space-4 text-uppercase"><strong>Gerai SIM</strong></h5>
            </div>
          </div>
         </div>
       </div>
     </div>
   </div>
</section>



<!-- Section: Client Say -->
<section class="divider parallax layer-overlay overlay-theme-colored-9" data-background-ratio="0.5" data-bg-img="./images/slider/bg_section_testimoni.jpg">
  <div class="container pt-60 pb-60">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h2 class="line-bottom-center text-gray-lightgray text-center mt-0 mb-30">Testimoni</h2>
        <div class="owl-carousel-1col" data-dots="true">
          @foreach($data['testi'] as $t)
            <div class="item">
              <div class="testimonial-wrapper text-center">
                <div class="thumb"><img class="img-circle" alt="" src="{{asset('upload/media/'.$t->file)}}"></div>
                <div class="content pt-10">
                  <p class="font-15 text-white"><em>{{$t->rank_name}}</em></p>
                  <i class="fa fa-quote-right font-36 mt-10 text-gray-lightgray"></i>
                  <h4 class="author text-theme-color-2 mb-0">{{$t->name}}</h4>
                 </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>


<!-- Section: Gallery -->
<section class="bg-lighter">
  <div class="container pt-60">
    <div class="section-title mb-0">
      <div class="row">
        <div class="col-md-8">
          <h2 class="mt-0 text-uppercase font-28 line-bottom">GALERI <span class="text-theme-color-2 font-weight-400">Korlantas</span></h2>
          <h4 class="pb-20">Dokumentasi kegiatan / aktivitas Korlantas Polri.</h4>
       </div>
      </div>
    </div>
    <div class="section-content">
      <div class="row">
        <div class="col-md-12">
          
          <!-- Portfolio Gallery Grid -->
          <div id="grid" class="gallery-isotope grid-4 gutter clearfix">
            <?php
            $galeryCount = 0;
            //dd($data['portfolio2']);
            ?>
            <!-- Portfolio Item Start --> 
            @foreach($data['portfolio2'] as $g)
              <div class="gallery-item select{{ ++$galeryCount}}" style="overflow: hidden;">
                <div class="thumb" style="max-height: 200px">
                  <img class="img-fullwidth" src="{{asset('upload/media/'.$g->file)}}" alt="project">
                  <div class="overlay-shade"></div>
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                        <a data-lightbox="image" href="{{asset('upload/media/'.$g->file)}}"><i class="fa fa-plus"></i></a>
                       </div>
                    </div>
                  </div>
                  <a class="hover-link" data-lightbox="image" href="{{asset('upload/media/'.$g->file)}}">View more</a>
                </div>
              </div>
            @endforeach
            <!-- Portfolio Item End -->
          </div>
          <!-- End Portfolio Gallery Grid -->
        </div>
      </div>
    </div>
  </div>
</section>  
@endsection