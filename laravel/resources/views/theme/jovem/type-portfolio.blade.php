@extends('theme.jovem.layout.index')

@section('content')
 
<!-- Main Banner Starts -->
      <div class="main-banner six" style="background:transparent url( {{ asset('theme/enagic/images/header-media2.jpg') }} ) repeat scroll center top">
        <div class="container">
          <h2><span>Gemarsehati | {{ $data['category']->name }}</span></h2>
        </div>
      </div>
    <!-- Main Banner Ends -->
<!-- Breadcrumb Starts -->
      <div class="breadcrumb">
        <div class="container">
          <ul class="list-unstyled list-inline">
             <li><a href="#"><b> <a href="{!! URL::to('/') !!}">Home</a></b> </li>
  <li><a href="active"> <a href="javascript:void(0);" class="current">{{ $data['category']->name }}</a></i>
</a></li>
          </ul>
        </div>
      </div>    
    <!-- Breadcrumb Ends -->

    <div class="row" style="min-height:70%;">
      <div class="col-md-12">


        <div class="col-md-offset-1 col-md-10">
         
 <div style="visibility: visible; animation-name: fadeInRight;" class="title-line wow fadeInRight animated">
                  </div>
          <br><br>
          <div class="row">
            <div class="col-md-12 gallery" style="float:left;">
            <?php 
            $i = 1;
            ?>
             @foreach($data['group-portfolio'] as $row)
             <?php
             
             if(($i-1)%4 == 0)
              echo "<div class='row'>";
             ?>
             <div class="col-md-3">
                <a href="{{ route('home.portfolio', $row->id) }}" title="{{ $row->title}}">
                  <img src="{{ asset('theme/enagic/images/media-category.jpg') }}" class="img-responsive" alt="{{ $row->title}}">
                </a><br>
                <center><h5>{{ $row->title}}</h5>
                <p>{!! $row->description !!}</p></center>
                <br><br>
              </div>
              <?php 
              if($i%4 == 0 || $i == $data['group-portfolio']->count())
              {
                echo "</div>";
              }
                $i++;
               ?>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center" style="margin-left:auto; margin-right:auto;">
        {!! $data['group-portfolio']->render() !!}
      </div>
    </div>
    
 


        @endsection

        @section('custom-head')
        <link rel="stylesheet" type="text/css" href="{{ asset('theme/jovem/plugins/pretty-photo/css/prettyPhoto.css') }}">
        
        @endsection

        @section('custom-footer')
        <script type="text/javascript" src="{{ asset('theme/jovem/plugins/pretty-photo/js/jquery.prettyPhoto.js') }}"></script>
        <script type="text/javascript">
          $(document).ready(function(){
           $(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square', autoplay_slideshow: false});

         });
        </script>
        @endsection