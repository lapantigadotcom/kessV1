<?php
	$meta = $general->meta_description;
	$keyword = $general->meta_keywords;
?>
@if(isset($data['content']->meta_description))
	<?php 
	$meta.=$data['content']->meta_description;
	 ?>
@endif
<meta name="description" content="{{ $meta }}">
<meta name="keywords" content="{{ $keyword }}">
<meta name="author" content="lapantiga.com">
  
<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/tema/kessv1/images/favicon/apple-icon-57x57.png')}}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/tema/kessv1/images/favicon/apple-icon-60x60.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/tema/kessv1/images/favicon/apple-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/tema/kessv1/images/favicon/apple-icon-76x76.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/tema/kessv1/images/favicon/apple-icon-114x114.png')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/tema/kessv1/images/favicon/apple-icon-120x120.png')}}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/tema/kessv1/images/favicon/apple-icon-144x144.png')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/tema/kessv1/images/favicon/apple-icon-152x152.png')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/tema/kessv1/images/favicon/apple-icon-180x180.png')}}">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="assets/tema/kessv1/images/favicon/apple-icon-144x144.png.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<!-- Stylesheet -->
<link href="{{ asset('/css/lapantiga.css')}}" rel="stylesheet" type="text/css">

<link href="{{ asset('/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/animate.css')}}" rel="stylesheet" type="text/css">
<!-- <link href="{{ asset('/css/additional.css')}}" rel="stylesheet" type="text/css"> -->

<link href="{{ asset('/css/css-plugin-collections.css')}}" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="{{ asset('/css/menuzord-skins/menuzord-rounded-boxed.css')}}" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="{{ asset('/css/style-main.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="{{ asset('/css/preloader.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="{{ asset('/css/custom-bootstrap-margin-padding.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="{{ asset('/css/responsive.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<link href="{{ asset('/css/style.css')}}" rel="stylesheet" type="text/css">

<!-- Revolution Slider 5.x CSS settings -->
<link  href="{{ asset('/js/revolution-slider/css/settings.css')}}" rel="stylesheet" type="text/css"/>
<link  href="{{ asset('/js/revolution-slider/css/layers.css')}}" rel="stylesheet" type="text/css"/>
<link  href="{{ asset('/js/revolution-slider/css/navigation.css')}}" rel="stylesheet" type="text/css"/>

<!-- CSS | Theme Color -->
<link href="{{ asset('/css/colors/theme-skin-color-set-5.css')}}" rel="stylesheet" type="text/css">
<?php
	// require_once('F:\xampp\htdocs\enagic\vendor\twitter-api-php\TwitterAPIExchange.php');

?>
 

@yield('custom-head')