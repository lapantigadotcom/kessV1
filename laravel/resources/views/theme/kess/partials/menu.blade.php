      <header id="header" class="header">
        <div class="header-top bg-theme-color-2 sm-text-center p-0">
          <div class="container">
            <div class="row">
              <div class="col-md-4">
                <div class="widget no-border m-0">
                  <ul class="list-inline font-13 sm-text-center mt-5">
                     <li>
                      <a class="text-white" href="#">Komunitas Ekonomi Syariah SURABAYA</a>
                    </li>
                      
                      
                  </ul>
                </div>
              </div>
              <div class="col-md-8">
                <div class="widget m-0 pull-right sm-pull-none sm-text-center">
                  <ul class="list-inline pull-right">
                     
                    <li class="mb-0 pb-0">
                      <div class="top-dropdown-outer pt-5 pb-10">
                        <a class="top-search-box has-dropdown text-white text-hover-theme-colored"><i class="fa fa-search font-13"></i> &nbsp;</a>
                        <ul class="dropdown">
                          <li>
                            <div class="search-form-wrapper">
                              <form method="get" class="mt-10">
                                <input type="text" onfocus="if(this.value =='Enter your search') { this.value = ''; }" onblur="if(this.value == '') { this.value ='Enter your search'; }" value="Enter your search" id="searchinput" name="s" class="">
                                <label><input type="submit" name="submit" value=""></label>
                              </form>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="widget no-border m-0 mr-15 pull-right flip sm-pull-none sm-text-center">
                  <ul class="styled-icons icon-circled icon-sm pull-right flip sm-pull-none sm-text-center mt-sm-15">
                    <li><a href="{{$data['contact']->facebook}}"><i class="fa fa-facebook text-white"></i></a></li>
                    <li><a href="{{$data['contact']->twitter}}"><i class="fa fa-twitter text-white"></i></a></li>
                     
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="header-middle p-0 bg-lightest xs-text-center">
          <div class="container pt-0 pb-0">
            <div class="row">
              <div class="col-xs-12 col-sm-4 col-md-5">
                <div class="widget no-border m-0">
                  <a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="{!! URL::to('/') !!}"><img src="{{ asset('assets/tema/kessv1/images/logo_kess.png')}}" style="max-width:250px" alt=""></a>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
                  <ul class="list-inline">
                    <li><i class="fa fa-phone-square text-theme-colored font-36 mt-5 sm-display-block"></i></li>
                    <li>
                      <a href="#" class="font-12 text-gray text-uppercase">Call Center</a>
                      <h5 class="font-14 m-0">{{$data['contact']->telephone}}</h5>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
                  <ul class="list-inline">
                    <li><i class="fa fa-mobile text-theme-colored font-36 mt-5 sm-display-block"></i></li>
                    <li>
                      <a href="#" class="font-12 text-gray text-uppercase">SMS Center</a>
                      <h5 class="font-13 text-blue m-0"> {{$data['contact']->mobile}}</h5>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="header-nav">
          <div class="header-nav-wrapper navbar-scrolltofixed bg-theme-colored border-bottom-theme-color-2-1px">
            <div class="container">
              <nav id="menuzord" class="menuzord bg-theme-colored pull-left flip menuzord-responsive">
                <ul class="menuzord-menu">
                  <li ><a href="{!! URL::to('/') !!}"><i class="fa fa-home"></i></a></li>
                  <?php
                    $top_menu = $menu->filter(function($v){
                      if($v->code=='top_menu'){
                        return true;
                      }
                    });
                  ?>
                  @foreach($top_menu as $row)
                    @foreach($row->menuDetail as $val)
                    <?php 
                    if($val->parent_id != 0)
                      continue;
                    $url = '';
                    switch ($val->menuType->id) {
                      case '1':
                        $url = route('home.category',[ $val->custom ]);
                        break;
                      case '2':
                        $url = route('home.post',[ $val->custom ]);
                        break;
                      case '3':
                        $url = route('home.portfolio',[ $val->custom ]);
                        break;

                      case '4':
                        $url = route('home.rss',[ $val->custom ]);
                        break;
                      case '5':
                        $url = $val->custom;
                        break; 
                      case '6':
                        $url = route('home.products');
                        break;
                      case '7':
                        $url = route('home.mitra');
                        break;
                      case '8':
                        $url = route('home.mitraCity');
                        break;
                      case '9':
                        $url = route('home.type',[ $val->custom ]);
                        break;
                      case '10':
                        $url = route('home.soal_soal');
                        break;
                      case '11':
                        $url = route('home.cctv');
                        break;
                      default:
                        # code...
                        break;
                    }
                    ?>
                    <li><a
                    @if($val->submenu()->count() > 0 )
                      href="#"
                    @else
                      href="{{ $url }}" 
                    @endif
                    >
                    {{ $val->title }}
                    </a>
                    @if($val->submenu()->count() > 0 )
                      <ul class="dropdown">
                      @foreach($val->submenu as $v)
                        <?php 
                          $url = '';
                          switch ($v->menuType->id) {
                            case '1':
                              $url = route('home.category',[ $v->custom ]);
                              break;
                            case '2':
                              $url = route('home.post',[ $v->custom ]);
                              break;
                            case '3':
                              $url = route('home.portfolio',[ $v->custom ]);
                              break;
                            case '4':
                              $url = route('home.rss',[ $v->custom ]);
                              break;
                            case '5':
                                $url = $v->custom;
                                break; 
                            case '6':
                              $url = route('home.products');
                              break;
                            case '7':
                              $url = route('home.mitra');
                              break;
                            case '8':
                              $url = route('home.mitraCity');
                              break;
                            case '9':
                              $url = route('home.type',[ $v->custom ]);
                              break;
                            case '10':
                              $url = route('home.soal_soal');
                              break;
                            case '11':
                              $url = route('home.cctv');
                              break;
                            default:
                              # code...
                              break;
                        }
                        ?>
                        <li>
                          <a href="{!! url($url) !!}">{!! $v->title !!}</a>
                          @if($v->submenu()->count() > 0 )
                          <ul class="dropdown">
                          @foreach($v->submenu as $w)
                            <?php 
                                  $url = '';
                                  switch ($w->menuType->id) {
                                    case '1':
                                      $url = route('home.category',[ $w->custom ]);
                                      break;
                                    case '2':
                                      $url = route('home.post',[ $w->custom ]);
                                      break;
                                    case '3':
                                      $url = route('home.portfolio',[ $w->custom ]);
                                      break;
                                    case '4':
                                      $url = route('home.rss',[ $w->custom ]);
                                      break;
                                    case '5':
                                        $url = $w->custom;
                                        break; 
                                    case '6':
                                      $url = route('home.products');
                                      break;
                                    case '7':
                                      $url = route('home.mitra');
                                      break;
                                    case '8':
                                      $url = route('home.mitraCity');
                                      break;
                                    case '9':
                                      $url = route('home.type',[ $w->custom ]);
                                      break;
                                    case '10':
                                      $url = route('home.soal_soal');
                                      break;
                                    case '11':
                                      $url = route('home.cctv');
                                      break;
                                    default:
                                      # code...
                                      break;
                                  }
                                  ?>
                                <li class="">
                                  <a href="{!! url($url) !!}" >{!! $w->title !!}</a>
                                </li>
                              @endforeach
                            </ul>
                          @endif
                        </li>
                        @endforeach
                      </ul>
                    @endif
                    </li>
                    @endforeach
                  @endforeach
                </ul>
                <ul class="pull-right flip hidden-sm hidden-xs">
                  <li>
                    <!-- Modal: donate now Starts -->
                    <a class="btn btn-colored btn-flat bg-theme-color-2 text-white font-14 font-14 bs-modal-ajax-load mt-0 p-25 pr-15 pl-15"  target="_blanks" href="{!! URL::to('/daftar') !!}">Pendaftaran</a>
                    <!-- Modal: donate now End -->
                  </li>
                </ul>
                <div id="top-search-bar" class="collapse">
                  <div class="container">
                    <form role="search" action="#" class="search_form_top" method="get">
                      <input type="text" placeholder="Type text and press Enter..." name="s" class="form-control" autocomplete="off">
                      <span class="search-close"><i class="fa fa-search"></i></span>
                    </form>
                  </div>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </header>
