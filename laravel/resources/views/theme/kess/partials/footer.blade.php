<!-- Footer -->
<footer id="footer" class="footer divider " style="background:#444" >
	<div class="container">
	  <div class="row">
	    <div class="col-sm-6 col-md-3">
	      <div class="widget dark">
	        <h4 class="widget-title">KESS</h4>
	        <p>{{ $data['contact']->address}}</p>
	        <ul class="list-inline mt-5">
	          <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-color-2 mr-5"></i> <a class="text-gray" href="#">{{$data['contact']->telephone}}</a> </li>
	          <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-color-2 mr-5"></i> <a class="text-gray" href="#">{{$data['contact']->email}}</a> </li>
	          <li class="m-0 pl-10 pr-10"> <i class="fa fa-globe text-theme-color-2 mr-5"></i> <a class="text-gray" href="#">kessurabaya.com<!-- {{$data['contact']->website}} --></a> </li>
	        </ul>
	      </div>
	    </div>
	    <div class="col-sm-6 col-md-3">
	      <div class="widget dark">
	        <h4 class="widget-title">External Link</h4>
	        <ul class="list angle-double-right list-border">
	          <li><a href="#">KESS Jakarta</a></li>
	          <li><a href="#">KESS Bandung</a></li>
	          <li><a href="#">KESS Medan</a></li>
	          <li><a href="#">KESS Makassar</a></li>
 	        </ul>
	      </div>
	    </div>
	    <div class="col-sm-6 col-md-3">
	      <div class="widget dark">
	        <h4 class="widget-title">Twitter KESS</h4>
	       <!--  <div class="twitter-feed list-border clearfix" data-username="ntmclantaspolri" data-count="2"></div> -->
	      <div class="twitter-feed list-border clearfix" data-username="" data-count="2"></div>

	      </div>
	    </div>
	    <div class="col-sm-6 col-md-3">
	      <div class="widget dark">
	        <h4 class="widget-title line-bottom-theme-colored-2">Jam Operasional KESS</h4>
	        <div class="opening-hourse">
	          <ul class="list-border">
	            <li class="clearfix"> <span> Senin - Jumat :  </span>
	              <div class="value pull-right"> 08.00 - 15.00 </div>
	            </li>
	            
	            <li class="clearfix"> <span> Sabtu : </span>
	              <div class="value pull-right"> 08.00 - 13.00 </div>
	            </li>
	            <!-- <li class="clearfix"> <span></span>
	              <div class="value pull-right"> <img src="{{ asset('/images/badged_lantas.png')}}"> </div>
	            </li> -->

	          </ul>
	        </div>
	      </div>
	    </div>
	  </div>
	  
	</div>
	<div class="footer-bottom bg-black-333">
	  <div class="container pt-20 pb-20">
	    <div class="row">
	      <div class="col-md-6">
	        <p class="font-11 text-black-777 m-0" style="color:#fff" >Copyright &copy;2017 Komunitas Ekonomi Syariah Surabaya. All Rights Reserved. Best view on 1280 x 720 px</p>
	      </div>
	      <div class="col-md-6 text-right">
	        <div class="widget no-border m-0">
	          <ul class="list-inline sm-text-center mt-5 font-12" >
	            <li >
	              <a href="#">KESS</a>
	            </li>
	            <li>|</li>
	            <li>
	              <a href="#">Panduan</a>
	            </li>
	            <li>|</li>
	            <li>
	              <a href="#">Lokasi Koperasi</a>
	            </li>
	          </ul>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</footer>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>

 
<script src="{{ asset('/js/jquery-2.2.0.min.js')}}"></script>
<script src="{{ asset('/js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('/js/bootstrap.min.js')}}"></script>
 <script src="{{ asset('js/jquery-plugin-collection.js')}}"></script>
 <script src="{{ asset('js/custom.js')}}"></script>
<script src="{{ asset('js/extra.js')}}"></script>

 <script src="{{ asset('js/revolution-slider/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{ asset('js/revolution-slider/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{ asset('js/extra-rev-slider.js')}}"></script> 
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/extensions/revolution.extension.video.min.js')}}"></script>
@yield('custom-footer')
