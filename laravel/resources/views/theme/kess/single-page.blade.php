@extends('theme.kess.layout.index')
@section('content')
<title>KESS | {{ $data['content']->title }}</title>

<section id="blog" class="">
  <!-- Main Container Starts -->
  <div class="container pt-60 pb-60">

    <div class="main-container">
      <!-- Nested Row Starts -->
      <div class="row">
        <!-- Mainarea Starts -->
        <div class="col-md-9 col-xs-12">
          <!-- News Post Single Starts -->
          <div class="news-post-single">
            <!-- News Post Starts -->
            <article class="news-post">
              <div class="inner">
                <h2>{{ $data['content']->title }}</h2>
                
                <ul class="list-unstyled list-inline post-meta">
                  <li>
                    <i class="fa fa-calendar"></i> 
                    Diposting tgl : {{ date("d M Y" ,strtotime($data['content']->created_at)) }}
                  </li>
                  <li>
                    <i class="fa fa-user"></i> 
                    By <a href="#">{{ $data['content']->user->name }} </a>
                  </li>

                  <li>  <i class="fa fa-tag"></i>
                    @foreach($data['content']->tag as $row)
                    <a href="#">{!! $row->title !!}</a>,
                    @endforeach
                  </li>
                </ul>
                @if($data['content']->mediaPost != null) 
                <center>
                  <img src="{{ asset('upload/media/'.$data['content']->mediaPost->file) }}" alt="#" class="img-responsive img-center-sm img-center-xs">
                </center>
                @endif
                <div class="news-post-content">
                  {!! $data['content']->description !!}
                   <div class="addthis_native_toolbox">
                  </div>
                </div>
              </div>
            </article>
          <!-- News Post Ends -->
          </div>
           <div class="blog-author-bio">
             <div class="row">
              <div class="col-xs-9 text-justify">
                  
               </div>
            </div>
          </div>
        </div>
         
        @include('theme.kess.layout.sidebar_artikel')
      </div>
    </div>
  </div>
</section>@include('theme.kess.layout.search')
 <style type="text/css">
                ul.post-meta {
                  border-bottom: 0.5px solid #ccc; 
                  margin-bottom: 12px;
                }
                ul.post-meta li{
                  padding: 6px
                }
                .news-post-content{
                  margin-top: 1em;
                  margin-bottom: 1em;
                  padding: 1em;
                  padding-left: 0px;
                   border-bottom: 0.5px solid #ccc;
                }
                </style>
@endsection
@section('custom-footer')
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56a630c285f8f2c7" async="async"></script>
 
@endsection