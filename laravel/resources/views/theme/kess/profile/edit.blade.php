@extends('theme.korlantas.layout.index')

@section('content')
<div class="row" style="min-height:70%;">
  <div class="col-md-12">
    <div class="main-banner six" style="background:transparent url( {{ asset('theme/enagic/images/header-dashboard.jpg') }} ) repeat scroll center top">
        <div class="container">
          <h2>&nbsp;</h2>
        </div>
      </div>
    <div class="breadcrumb">
      <div class="container">
        <ul class="list-unstyled list-inline">
          <li><a href="{!! URL::to('/') !!}">Home</a></li>
          <li><a href="javascript:void(0);">My Profile</a></li>
        </ul>
      </div>
    </div>
    <div class="main-container">    
      <!-- Doctor Profile Starts -->
        <div class="row">
          <div class="col-sm-5 col-xs-12">
            <div class="profile-block">
              <div class="panel panel-profile">
                <div class="panel-heading">
                  <img src="@if($data['mitra']->detail->media != null) {{ asset('upload/media/'.$data['mitra']->detail->media->file) }} @endif" width="470" alt="Profile Image" class="img-responsive img-center-xs">
                  <h3 class="panel-title">{{ $data['mitra']->name }}</h3>
                  <p class="caption">{{ $data['mitra']->rank }}</p>
                </div>
                <div class="panel-body">
                  <ul class="list-unstyled">
                    <li class="row">
                      <span class="col-sm-4 col-xs-12"><strong>Email</strong></span>
                      <span class="col-sm-8 col-xs-12">{{ $data['mitra']->email }}</span>
                    </li>
                    <li class="row">
                      <span class="col-sm-4 col-xs-12"><strong>No. HP</strong></span>
                      <span class="col-sm-8 col-xs-12">{{ $data['mitra']->detail->nohp }}</span>
                    </li>
                    <li class="row">
                      <span class="col-sm-4 col-xs-12"><strong>Enagic ID</strong></span>
                      <span class="col-sm-8 col-xs-12">{{ $data['mitra']->detail->enagic_id }}</span>
                    </li>
                    <li class="row">
                      <span class="col-sm-4 col-xs-12"><strong>Alamat</strong></span>
                      <span class="col-sm-8 col-xs-12">{{ $data['mitra']->detail->address }}</span>
                    </li>
                  </ul>
                </div>
                <div class="panel-footer text-center-md text-center-sm text-center-xs">
                  <div class="row">
                    <div class="col-lg-6 col-xs-12">
                      <ul class="list-unstyled list-inline sm-links">
                        <li><a href="//{{ $data['mitra']->detail->twitter }}"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="//{{ $data['mitra']->detail->facebook }}"><i class="fa fa-facebook"></i></a></li>                         
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>          
          <div class="col-sm-7 col-xs-12">
            <h3 class="main-heading2">Edit Data Mitra </h3>
            <h7>* Required Field</h7>
            @include('page.partials.notification')
            @if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>Whoops!</strong> Form yang anda isi belum lengkap.<br><br>
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
            <div class="box-body">
              {!! Form::model($data['content_detail'],array('action' => ['HomeController@profileUpdate',$data['mitra']->id],'method' => 'PATCH', 'files' => true)) !!}
              <div class="col-md-12">
                @include('page.errors.list')
                  <?php
                    $arrGender = array();
                    $arrCity = array();
                    foreach($data['gender'] as $row)
                    {
                      $arrGender[$row->id] = $row->name;
                    }
                    foreach($data['city'] as $row)
                    {
                      $arrCity[$row->id] = $row->name;
                    }
                  ?>
                @include('theme.korlantas.profile.form',['submitText' => 'Perbarui'])
              </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      <!-- Profile Ends -->
      <!-- Spacer Block Starts -->
        <div class="spacer-block"></div>
      <!-- Spacer Block Ends --><!-- Related Best Doctors Starts --> 
        <h2 class="main-heading2 nomargin-top">Artikel Khusus Mitra</h2>
        <!-- Latest News Carousel Starts -->
        <div id="news-carousel" class="news-carousel carousel slide" data-ride="carousel">
        <!-- Wrapper for Slides Starts -->
          <div class="carousel-inner">
            @for($i = 0; $i < $data['pdf']->count(); $i++)
            <!-- Doctor Bio #1 Starts -->
              <div class="item @if($i == 0) active @endif">
                <div class="row">
                  <!-- News Post Starts -->
                  <?php $counter = 3; ?>
                  @while($counter--)                  
                  <?php 
                  if($i + 1 > $data['pdf']->count())
                    break;
                  $content = $data['pdf'][$i];
                  ?>
                  <div class="col-sm-4 col-xs-12">
                    <div class="news-post-box">
                      <img src="@if($content->media != null) {{ asset('upload/media/'.$content->media->file) }} @endif" alt="enagic2" class="img-responsive img-center-sm img-center-xs">
                      <div class="inner">
                        <h5>
                          <a href="#">{{ $content->title }}</a>
                        </h5>
                        <ul class="list-unstyled list-inline post-meta">
                          <li>
                            <i class="fa fa-calendar"></i> {{ $content->created_at }}
                          </li>                 
                        </ul>
                        <p>
                          {{ $content->description }}
                        </p>
                        <a href="{{ asset('upload/file/'.$content->file) }}" class="btn btn-secondary">                
                          <i class="fa fa-arrow-circle-down"></i> download
                        </a>
                      </div>    
                    </div> 
                  </div>
                  <?php $i++; ?>
                  @endwhile
                  <?php $i--; ?>
                  <!-- News Post Ends -->     
                </div>
              </div>
            <!-- Doctor Bio #1 Ends -->
            @endfor
            </div>
            <!-- Controls Starts -->
            <a class="left carousel-control" href="#news-carousel" role="button" data-slide="prev">
              <span class="fa fa-angle-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#news-carousel" role="button" data-slide="next">
              <span class="fa fa-angle-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
            <!-- Controls Ends -->
        </div>
      <!-- Related Best Doctors Ends -->
      </div>
    </div>
  </div>
 

@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/enagic/plugins/pretty-photo/css/prettyPhoto.css') }}">

@endsection

@section('custom-footer')
  @if($data['mitra']->detail->media != null)
    @include('page.scripts.media',['featuredImage' => $data['mitra']->detail->media->file ])
  @else
    @include('page.scripts.media')
  @endif
  <script type="text/javascript">
          $(document).ready(function() {
            // var reference = (function tipeportfolio(){
            //  var type = $('#type').val();
            //  switch(type){
            //    case '1':
            //      $('#mediaformcontainer').show();
            //    break;
            //    case '2':
            //      $('#mediaformcontainer').hide();
            //    break;
            //  }
            //     return tipeportfolio;
            // }());
            // $('#type').change(function(){
            //  reference();
            //  console.log("ganti");
            // });
          });
          </script>
<script type="text/javascript" src="{{ asset('theme/enagic/plugins/pretty-photo/js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
   $(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square', autoplay_slideshow: false});

 });
</script>
@endsection