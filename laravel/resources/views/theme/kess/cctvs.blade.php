@extends('theme.korlantas.layout.index')
@section('content')
<style type="text/css">
.news-post{
  background-color: #fefefe;
  -webkit-transition: background-color 0.5s;
  border-top: solid 0.5px #ccc; 
  border-bottom: solid 0.5px #ccc; 
  margin: 10px; 
  padding: 10px
}
.news-post:hover{
  background-color: #efefef;
}

.post-title h4{
  color: #000;
  -webkit-transition: color 0.5s;
}

.post-title h4:hover{
  color: #16174F;
}

</style>
<div style='margin: 25px'>
  
  <!-- Main Banner Ends -->
  <!-- Main Container Starts -->
  <div class="main-container">
    <!-- Nested Row Starts -->
    <div class="row">
      <div class="col-md-9 col-xs-12">
        <h2>Kamera CCTV</h2>
        @if (count($data['content']) < 1)
          <hr>
          <h4>Data tidak tersedia</h4>
        @else
        @foreach($data['content'] as $v)
        <div class="news-post-list">
          @if($v->ms_media_id != '0')
          <article class="news-post" style="">
            <a href="{!! route('home.cctv',[ $v->id ]) !!}" class="post-title"><h4>{{ $v->title }}</h4></a>
            <ul class="list-unstyled list-inline post-meta">
                <li>
                  <i class="fa fa-calendar"></i> 
                  {{ date("d M Y" ,strtotime($v->created_at)) }}
                </li>
                <li>
                  <i class="fa fa-eye"></i>
                    <?php
                    /*$found = 0;
                    foreach($data['stat_pages'] as $info)
                    {
                      if($info["url"] == "/artikel/gemarsehati=".$v->id."?post=")
                      {
                        echo $info["pageViews"];
                        $found = 1;
                      }
                    }
                    if($found == 0)
                      echo "0";*/
                    ?> 
                </li>
            </ul>
            <div class="row" style="margin-top:10px">
              <div class="col-md-4">
                <img src="{{ asset('upload/media/'.$v->file) }}"  class="img-responsive img-center-sm img-center-xs" width="850">
              </div>
              <div class="col-md-8">
                <div class="inner">
                  <p><?php $desc = explode("<!-- pagebreak -->", $v->description); ?>
                    <?php
                    $desc = explode("</p>", $desc[0]);
                    ?>
                    @if(strlen($desc[0]) > 500)
                    {!! substr($desc[0], 0, 500) !!}...
                    @else
                    {!! $desc[0] !!}&nbsp;
                    @endif
                  </p>
                </div>
                <a href="{!! route('home.cctv.view',[ $v->id ]) !!}" class="btn btn-secondary pull-right">
                  Lihat cctv
                  <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
          </article>
          @else
          <article class="news-post" style="">
            <a href="{!! route('home.cctv',[ $v->id,'cctv' ]) !!}" class="post-title"><h4>{{ $v->title }}</h4></a>
            <ul class="list-unstyled list-inline post-meta">
                <li>
                  <i class="fa fa-calendar"></i> 
                  {{ date("d M Y" ,strtotime($v->created_at)) }}
                </li>
                <li>
                  <i class="fa fa-eye"></i>
                    <?php
                    /*$found = 0;
                    foreach($data['stat_pages'] as $info)
                    {
                      if($info["url"] == "/artikel/gemarsehati=".$v->id."?post=")
                      {
                        echo $info["pageViews"];
                        $found = 1;
                      }
                    }
                    if($found == 0)
                      echo "0";*/
                    ?> 
                </li>
              </ul>
            <div class="row" style="margin-top:10px">
              <div class="col-md-12">
                <div class="inner">
                  <p><?php $desc = explode("<!-- pagebreak -->", $v->description); ?>
                    <?php
                    $desc = explode("</p>", $desc[0]);
                    ?>
                    @if(strlen($desc[0]) > 500)
                    {!! substr($desc[0], 0, 500) !!}...
                    @else
                    {!! $desc[0] !!}&nbsp;
                    @endif
                  </p>
                </div>
                <a href="{!! route('home.cctv.view',[ $v->id,'cctv' ]) !!}" class="btn btn-secondary pull-right">
                  Lihat cctv
                  <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
          </article>
          @endif
        </div>
        @endforeach
        @endif
        <div class="row" id="paginator">
          <center>
            {!! $data['content']->render() !!}
          </center>
        </div>
      </div>
       
  <style type="text/css">
  .img.dsq-widget-avatar{height: 50px}
  .dsq-widget-meta{font-size: 12px; color: red}
  .dsq-widget-comment{font-size: 12px}
  .dsq-widget-list{padding: 0px;margin: 0px}
  .dsq-widget-meta a{font-size: 12px;color: red}

  </style>


      @include('theme.korlantas.layout.sidebar_artikel')
    </div>     
  </div>
  <!-- section FB -->
</div>         
@include('theme.korlantas.layout.search')               
@endsection