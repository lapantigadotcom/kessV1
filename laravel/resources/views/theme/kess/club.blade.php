@extends('theme.korlantas.layout.index')

@section('content')


<div class="row">
  
  </div> 
  <div class="row">
    <div class="col-md-12"  >
        
   
   <center>
      <img src="{!! asset('theme/korlantas/img/seal-club.png') !!}" width="340" class="img-responsive"></center>
    </div>
  </div>   



  <div class="row" style="min-height:70%;">
    <div class="col-md-12">
      <div class="col-md-offset-1 col-md-10">
<div class="text-center">
                         <h2 style="visibility: visible; animation-name: fadeInLeft;" class="wow fadeInLeft animated">
                        4korlantas <span><i class="fa fa-trophy"></i> </span>Clubs </h2>
                  <div style="visibility: visible; animation-name: fadeInRight;" class="title-line wow fadeInRight animated">
                  </div>

                  </div>
         <div class="row">
          
          @if($data['content']->count() > 0)
          <div class="col-md-12">
            
          
          <?php 
            $i = 1;
            ?>
          @foreach($data['content'] as $row)
          <?php 
          if(($i-1)%4 == 0)
              echo "<div class='row'>";
             ?>
          <div class="col-md-3"> 
          	@if(file_exists('./upload/member/'.$row->photo))
                    <img style="visibility: visible; animation-name: fadeInRight "
                  src="{!! asset('upload/member/'.$row->photo) !!}" class="img-responsive img-thumbnail circles wow fadeInRight animated">
            @else
            <img src="{!! asset('theme/korlantas/img/user.png') !!}" class="img-responsive img-thumbnail circles">
            @endif
            <center><h5><b>{!! $row->name !!}</b></h5>
            <p><i class="fa fa-user"></i> | {!! ucfirst($row->typeClub->name) !!}<br>
            <i class="fa fa-trophy"></i> | {!! $row->city !!}<br>
           <span style="font-size:12px; font-style: italic"> {!! $row->reward !!}</span></p></center>
          </div>
          <?php 
              if($i%4 == 0 || $i == $data['content']->count())
              {
                echo "</div>";
              }
                $i++;
               ?>
          @endforeach
          </div>
          @else
          <div class="col-md-12">
            <h4 class="text-center">- Data member club tidak tersedia -</h4>
          </div>
          @endif

        </div>

      </div>
    </div>
  </div>
  	<div class="row">
      <div class="col-md-12 text-center">
        {!! $data['content']->render() !!}
      </div>
        
    </div>

    
  <br>  <br>
  <br>


  <div class="row">
  <div class="col-md-12">
    <marquee scrolldelay="90" style="font-size:12px;padding:5px">
    @if($data['latest']->count() > 0)
    <em><b>Redaksi4korlantas - Artikel Terbaru:</b> </em>
    @foreach($data['latest'] as $row)
    <b>{!! $row->title !!}</b> - 
    {!! substr(strip_tags($row->description),0,150) !!} 
    @endforeach
    @endif
    </marquee>
  </div>
  </div>

@include('theme.korlantas.layout.search')
    @endsection

    @section('custom-footer')


    @endsection