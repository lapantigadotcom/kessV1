@extends('theme.korlantas.layout.index')

@section('content')
<title>Redaksi 4korlantas - Cek Biaya Pengiriman</title>
<div class="row">
	<div class="col-md-12">
		<!-- TARIF ONGKIR -->
		<h3 class="text-center">
			Periksa ongkos kirim International dengan cepat disini.
		</h3>
		<center>
			<img src="{{ asset('theme/korlantas/img/ongkir-internasional.jpg') }}"  alt="cek tarif internasional" class="img-responsive">
		</center>
		{!! Form::open(array('url' => 'ongkir-international','method' => 'post')) !!}
		<div class="form-group">
			{!! Form::label('source','Asal : ') !!}
			<select name="source" class="form-control" id="source">
				@foreach($data['city'] as $row)
				<option value="{!! $row->city_id !!}" @if(isset($data['isian_source']) && $data['isian_source']== $row->city_id) selected @endif>
					{!! $row->name !!}
				</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			{!! Form::label('destination','Tujuan : ') !!}
			<select name="destination" class="form-control" id="destination">
				@foreach($data['country'] as $row)
				<option value="{!! $row->country_id !!}" @if(isset($data['isian_destination']) && $data['isian_destination']== $row->country_id) selected @endif>
					{!! $row->name !!}
				</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			{!! Form::label('weight','Berat(gram) : ') !!}
			{!! Form::text('weight',isset($data['isian_weight'])?$data['isian_weight']:null, ['class' => 'form-control','maxlength' => '4', 'id' => 'weight']) !!}
		</div>
		
		<div class="form-group" >
			{!! Form::submit('Hitung',['class' => 'btn btn-info' ]) !!}
		</div>
		{!! Form::close() !!}
		


		<!-- /TARIF ONGKIR -->
		<br>&nbsp;  

		<h6>
			Hasil ongkos kirim menggunakan perhitungan dari kurir <b>POS Indonesia</b>
		</h6>


		@if(isset($data['results']))
		@if(empty($data['results']))
		<h4>
			Data tidak ditemukan.
		</h4>
		@else
		<h4>{!! $data['context'] !!}</h4>
		<table class="table ongkir-tabel">
			<thead>
				<th style="background:#5D524E;color:#fff">Jenis Layanan</th>
				<th style="background:#5D524E;color:#fff">Tarif</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data['results']['cost'] as $row)
			<tr>  
				<td style="color:#5D524E"><b>{!! $row[0] !!}</b> </td>
				<td>Rp. {!! number_format($data['results']['currency']*$row[1], 2) !!}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@endif
	@endif
</div>
<div class="col-md-12">&nbsp;&nbsp;
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<div class="addthis_sharing_toolbox"></div>
	<hr>
	<br>&nbsp;
	<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;


	@endsection

	@section('custom-footer')

	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55cd97ec6f575082" async="async"></script>

	@endsection