@extends('theme.kess.layout.index')

@section('content')
 
<div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="upload/media/img_20170306_052449.jpg">
      <div class="container pt-90 pb-50">
        <!-- Section Content -->
        <div class="section-content pt-100">
          <div class="row"> 
             
          </div>
        </div>
      </div>
    </section>


</div>
 
          <div class="row">

            <div class="col-sm-8 col-xs-12">
               
              @include('page.partials.notification')
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                <strong>Maaf!</strong> Form yang anda isi belum lengkap.<br><br>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif
                <div class="box-body">
                {!! Form::open(['route'=>'home.doRegister', 'method' => 'POST','files' => true]) !!}
                  <?php
                    $arrGender = array();
                    $arrCity = array();
                    foreach($data['gender'] as $row)
                    {
                      $arrGender[$row->id] = $row->name;
                    }
                    foreach($data['city'] as $row)
                    {
                      $arrCity[$row->id] = $row->name;
                    }
                  ?>
                  @include('theme.kess.register.form',['buttonSubmit' => 'SUBMIT'])
                {!! Form::close() !!}
                </div>
                
            </div>
            
    @endsection

    @section('custom-head')
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/korlantas/plugins/pretty-photo/css/prettyPhoto.css') }}">
    
    @endsection

    @section('custom-footer')
    <script src="{{ asset('plugins/bootstrapFileStyle/bootstrap-filestyle.min.js') }}"></script>
    <script type="text/javascript">
      $('input[type=file]').filestyle();
    </script>
    <script type="text/javascript" src="{{ asset('theme/korlantas/plugins/pretty-photo/js/jquery.prettyPhoto.js') }}"></script>
    <script type="text/javascript">
      $(document).ready(function(){
       $(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square', autoplay_slideshow: false});

     });
    </script>
    @endsection