<section class="bg-lighter">
  <div class="container pt-60">
    <div class="section-title mb-0">
      <div class="row">
        <div class="col-md-8">
          <h2 class="mt-0 text-uppercase font-28 line-bottom">GALERI <span class="text-theme-color-2 font-weight-400">KESS</span></h2>
          <h4 class="pb-20">Kegiatan Komunitas Ekonomi Syariah Surabaya.</h4>
       </div>
      </div>
    </div>
    <div class="section-content">
      <div class="row">
        <div class="col-md-12">
          
          <!-- Portfolio Gallery Grid -->
          <div id="grid" class="gallery-isotope grid-4 gutter clearfix">
            <?php
            $galeryCount = 0;
            //dd($data['portfolio2']);
            ?>
            <!-- Portfolio Item Start --> 
            @foreach($data['portfolio2'] as $g)
              <div class="gallery-item select{{ ++$galeryCount}}" style="overflow: hidden;">
                <div  style="max-height: 160px;">
                  <img class="img-fullwidth" src="{{asset('upload/media/'.$g->file)}}" alt="project">
                  <div class="overlay-shade"></div>
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                        <a data-lightbox="image" href="{{asset('upload/media/'.$g->file)}}"><i class="fa fa-plus"></i></a>
                       </div>
                    </div>
                  </div>
                  <a class="hover-link" data-lightbox="image" href="{{asset('upload/media/'.$g->file)}}">View more</a>
                </div>
              </div>
            @endforeach
            <!-- Portfolio Item End -->
          </div>
          <!-- End Portfolio Gallery Grid -->
        </div>
      </div>
    </div>
  </div>
</section>  