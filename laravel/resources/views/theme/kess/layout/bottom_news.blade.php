         <div class="row">
 
            <div class="col-xs-10 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="owl-carousel-3col" data-dots="false">
                      @foreach($data['kegiatan']->sortByDesc('created_at') as $v)
                        @if($v->ms_media_id != '0')
                        <div class="item">
                          <article class="post clearfix maxwidth600 mb-sm-30">
                            <div class="entry-header">
                              <div class="post-thumb thumb" style="height: 100px; width: 100%; overflow: hidden;"> <img src="{{ asset('upload/media/'.$v->mediaPost->file) }}" style="max-height:160px" alt="{{ $v->title }}" class="img-responsive img-fullwidth">  </div>
                              <div class="entry-meta  text-center pl-15 pr-15">
                                <div class="entry-date media-left text-center flip bg-theme-colored border-top-theme-color-2-3px pt-5 pr-15 pb-5 pl-15">
                                <ul>
                                  <li class="font-16 text-white font-weight-600">{{ date("d" ,strtotime($v->created_at)) }}</li>
                                  <li class="font-12 text-white text-uppercase">{{ date("M" ,strtotime($v->created_at)) }}</li>
                                </ul>
                              </div>
                               </div>
                            </div>
                            <div class="entry-content border-1px p-20" style="height: 200px">
                              <h5 class="entry-title mt-0 pt-0">
                                <a href="#">
                                    <h4>
                                      {{ strlen($v->title) > 25?substr($v->title,0,25).'...':$row->title }}
                                    </h4>
                                </a>
                              </h5>
                              <div class="text-left mb-20 mt-15 font-13">
                                <?php 
                                $desc = explode("<!-- pagebreak -->", $v->description);
                                $desc = explode("</p>", $desc[0]);
                                ?>
                                @if(strlen($desc[0]) > 100)
                                  {!! substr($desc[0], 0, 100) !!}...&nbsp;
                                @else
                                  {!! $desc[0] !!}&nbsp;
                                @endif
                              </div>
                            </div>
                            <div class="entry-content border-1px p-20">
                              <div class="text-left mt-20 mt-15 font-13">
                                <a class="btn btn-flat btn-dark btn-theme-colored btn-sm pull-left" href="{!! route('home.post',[ $v->id,'post' ]) !!}">Selengkapnya</a>
                                 
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </article>
                        </div>
                        @else

                        <div class="item">
                          <article class="post clearfix maxwidth600 mb-sm-30">
                            <div class="entry-header">
                              <div class="post-thumb thumb"> <img src="{{ asset('upload/media/'.$v->mediaPost->file) }}" style="max-height:160px" alt="{{ $v->title }}" class="img-responsive img-fullwidth">  </div>
                              <div class="entry-meta text-center pl-15 pr-15">
                                <div class="entry-date media-left text-center flip bg-theme-colored border-top-theme-color-2-3px pt-5 pr-15 pb-5 pl-15">
                                <ul>
                                  <li class="font-16 text-white font-weight-600">{{ date("d" ,strtotime($v->created_at)) }}</li>
                                  <li class="font-12 text-white text-uppercase">{{ date("M" ,strtotime($v->created_at)) }}</li>
                                </ul>
                              </div>
                               </div>
                            </div>
                            <div class="entry-content border-1px p-20">
                              <h5 class="entry-title mt-0 pt-0">
                                <a href="#">
                                  
                                    <h4>
                                      {{ strlen($v->title) > 25?substr($v->title,0,25).'...':$row->title }}
                                    </h4>
                                  
                                </a>
                              </h5>
                              <p class="text-left mb-20 mt-15 font-13">
                                 <?php $desc = explode("<!-- pagebreak -->", $v->description); ?>
                                <?php
                                $desc = explode("</p>", $desc[0]);
                                ?>
                                @if(strlen($desc[0]) > 100)
                                {!! substr($desc[0], 0, 100) !!}...
                                @else
                                {!! $desc[0] !!}&nbsp;
                                @endif  
                                 <ul class="list-inline entry-date pull-right font-12 mt-5">
                                <li><a class="text-theme-colored" href="#">{{ $v->user->name }} </a></li>
                                 
                              </ul>
                              </p>
                              <a class="btn btn-flat btn-dark btn-theme-colored btn-sm pull-left" href="{!! route('home.post',[ $v->id,'post' ]) !!}">Selengkapnya</a>
                             
                              <div class="clearfix"></div>
                            </div>
                          </article>
                        </div>

                        @endif
                      @endforeach
                    </div>
               
           </div>