  <!-- Section: blog -->
    <section id="blog" class="bg-lighter">
      <div class="container">
        <div class="section-title mb-10">
          <div class="row">
            <div class="col-md-8">
<h2 class="mt-0 text-uppercase font-28 line-bottom" style="color:#005E26">KESS <span class="text-theme-color-2 font-weight-400">Update</span></h2>
          <h4 class="pb-20">Informasi dan berita KESS</h4>           </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
                      @foreach($data['kegiatan']->sortByDesc('created_at') as $v)
            <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
              <article class="post clearfix mb-sm-30">
                <div class="entry-header">
                <div class="post-thumb thumb" style="height: 150px; width: 100%; overflow: hidden;"> <img src="{{ asset('upload/media/'.$v->mediaPost->file) }}" style="max-height:160px" alt="{{ $v->title }}" class="img-responsive img-fullwidth">  </div>

                </div>
                <div class="entry-content p-20 pr-10 bg-white">
                  <div class="entry-meta media mt-0 no-bg no-border">
                    <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                      <ul>
                        <li class="font-16 text-white font-weight-600 border-bottom">{{ date("d" ,strtotime($v->created_at)) }}</li>
                        <li class="font-12 text-white text-uppercase">{{ date("M" ,strtotime($v->created_at)) }}</li>
                      </ul>
                    </div>
                    <div class="media-body pl-15">
                      <div class="event-content pull-left flip">
                        <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="#">
                                                              {{ strlen($v->title) > 25?substr($v->title,0,25).'...':$row->title }}


                        </a></h4>
                        <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-user mr-5 text-theme-colored"></i>diposting oleh: <b style="color:green">{{ $v->user->name }} </b></span>                       
                       </div>
                    </div>
                  </div>
                  <p class="mt-10">  <?php 
                                $desc = explode("<!-- pagebreak -->", $v->description);
                                $desc = explode("</p>", $desc[0]);
                                ?>
                                @if(strlen($desc[0]) > 100)
                                  {!! substr($desc[0], 0, 100) !!}...&nbsp;
                                @else
                                  {!! $desc[0] !!}&nbsp;
                                @endif</p>
                  <a href="{!! route('home.post',[ $v->id,'post' ]) !!}" class="btn-read-more">selengkapnya</a>
                  <div class="clearfix"></div>
                </div>
              </article>
            </div>
                      @endforeach
          </div>
        </div>
      </div>
    </section>


 
