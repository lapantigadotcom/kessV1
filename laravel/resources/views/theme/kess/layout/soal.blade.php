<section class="bg-lighter">
  <div class="container pb-40">
    <div class="section-title mb-0">
    <div class="row">
      <div class="col-md-8">
        <h2 class="text-uppercase font-28 line-bottom mt-0 line-height-1">Info <span class="text-theme-color-2 font-weight-400">Kajian</span></h2>
        <h4 class="pb-20">Informasi Kajian Komunitas Ekonomi Syariah Surabaya</h4>
     </div>
    </div>
    </div>
    <div class="section-content">
     <div class="row">
        @foreach($data['soal_index'] as $s)
          <div class="col-sm-6 col-md-3 text-center">
            <div class="service-block bg-white">
              <div class="thumb"> <img alt="featured project" src="{{URL::to('upload/media/'.$s->file)}}" class="img-fullwidth">
               <h4 class="text-white mt-0 mb-0"><span class="price">{{$s->title}}</span></h4>
              </div>
              <div class="content text-left flip p-25 pt-0"><br>
                 <p style="height:100px; overflow: hidden">{{$s->description}}</p>
                 <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="{{URL::to('upload/media/'.$s->file)}}"><i class="fa fa-download fa-2x"></i></a>
              </div>
              <div class="row">
              </div>
            </div>
          </div>
        @endforeach 
     </div>
    </div>
  </div>
</section>