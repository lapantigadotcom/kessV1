<section class="divider parallax layer-overlay overlay-theme-colored-9" data-background-ratio="0.5" data-bg-img="{{ asset('/upload/media/img_20170306_054048.jpg')}}">
  <div class="container pt-60 pb-60">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h2 class="line-bottom-center text-gray-lightgray text-center mt-0 mb-30">Mutiara Hadist</h2>
        <div class="owl-carousel-1col" data-dots="true">
          @foreach($data['testi'] as $t)
            <div class="item">
              <div class="testimonial-wrapper text-center">
                <div class="thumb">
<!--                   <img class="img-circle" alt="" style="max-height:60px" src="{{asset('upload/media/'.$t->file)}}"> -->
</div>
                <div class="content pt-10">
                  <p class="font-15 text-white"><em>{{$t->rank_name}}</em></p>
                  <i class="fa fa-quote-right font-36 mt-10 text-gray-lightgray"></i>
                  <h4 class="author text-theme-color-2 mb-0">{{$t->name}}</h4>
                 </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>