<!-- Divider: Contact -->
    <section class="divider layer-overlay overlay-white-9" >
      <div class="container">
        <div class="row pt-30">
          <div class="col-md-8">
            <h3 class="line-bottom mt-0 mb-20">Registrasi Anggota KESS</h3> 

            <!-- Contact Form -->
           <div class="form-group">
						<div class="form-group col-md-6">
							{!! Form::label('name','Nama Lengkap* : ') !!}
							{!! Form::text('name',null, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group col-md-6">
							{!! Form::label('panggilan','Panggilan* : ') !!}
							{!! Form::text('panggilan',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							{!! Form::label('custom','Custom Link (hanya angka/huruf tanpa spasi, contoh : namasaya) : ') !!}
							{!! Form::text('custom',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							{!! Form::label('notelp','No. Telpon : ') !!}
							{!! Form::text('notelp',null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('nohp','No. HP* : ') !!}
							{!! Form::text('nohp',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							{!! Form::label('ms_gender_id','Jenis Kelamin* : ') !!}
							{!! Form::select('ms_gender_id',$arrGender,null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('address','Alamat Lengkap : ') !!}
							{!! Form::text('address',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							{!! Form::label('ms_city_id','Kota* : ') !!}
							{!! Form::select('ms_city_id',$arrCity,null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-3">
							{!! Form::label('kodepos','Kode Pos : ') !!}
							{!! Form::text('kodepos',null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-3">
							{!! Form::label('pin_bb','Pin BB : ') !!}
							{!! Form::text('pin_bb',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							{!! Form::label('coord','Koordinat alamat : ') !!}
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								{!! Form::label('latitude','Latitude : ') !!}
								{!! Form::text('latitude',null, ['class' => 'form-control']) !!}
							</div>
							<div class="col-md-6">
								{!! Form::label('longitude','Longitude : ') !!}
								{!! Form::text('longitude',null, ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							{!! Form::label('email','Email* : ') !!}
							{!! Form::text('email',null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('password','Password* (min 6 karakter): ') !!}
							{!! Form::password('password', array('class' => 'form-control')) !!}
						</div>
					</div>
					<div class="form-group">
						 
					</div>
					<div class="form-group">
						<div class="col-md-12">
							{!! Form::label('coord','Masukkan photo usaha Anda : ') !!}
						</div>
						<div class="col-md-6">
 							<!-- @if(isset($data['content']->file))
								{!! HTML::image(asset('upload/media/'.$data['content']->file),'',['class' => 'col-md-5 img-thumbnail']) !!}
							@endif -->
							{!! Form::file('file', ['class' => 'form-control']) !!}
						</div>	

									
					</div> 
					<div class="form-group">
						 
					<div class="col-md-6">
						{!! Form::submit($buttonSubmit,['class' => 'btn btn-info']) !!}
					</div>	
					</div>	
          </div>
          <div class="col-md-4">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="icon-box left media bg-black-333 p-25 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-map-2 text-theme-color-2"></i></a>
                  <div class="media-body"> <strong class="text-white">OUR OFFICE LOCATION</strong>
                    <p class="text-white">#405, Lan Streen, Los Vegas, USA</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-12 text-white">
                <div class="icon-box left media bg-black-333 p-25 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-call text-theme-color-2"></i></a>
                  <div class="media-body"> <strong class="text-white">OUR CONTACT NUMBER</strong>
                    <p>+325 12345 65478</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-12 text-white">
                <div class="icon-box left media bg-black-333 p-25 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-mail text-theme-color-2"></i></a>
                  <div class="media-body"> <strong class="text-white">OUR CONTACT E-MAIL</strong>
                    <p>supporte@yourdomin.com</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-12 text-white">
                <div class="icon-box left media bg-black-333 p-25 mb-20"> <a class="media-left pull-left" href="#"> <i class="fa fa-skype text-theme-color-2"></i></a>
                  <div class="media-body"> <strong class="text-white">Make a Video Call</strong>
                    <p>ThemeMascotSkype</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>




					