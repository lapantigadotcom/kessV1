@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Tema
					</h3>                                    
				</div>
				<div class="box-body  table-responsive">
				<table class="table" id="dataTables">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>Judul</th>
							<th>Kode</th>
							<th>Deskripsi</th>
							<th>Menu</th>
							<th>Aktif</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data['content'] as $row)
						<tr>
							<td>&nbsp;</td>
							<td>
								{{ $row->title }}
								
							</td>
							<td>{{ $row->code }}</td>
							<td>
								{{ $row->description }}
							</td>
							<td>
								<?php
								$arrMenu = array();
								foreach ($row->themeMenu as $val) {
									array_push($arrMenu, $val->code);
								}
								?>
								{!! implode(', ',$arrMenu) !!}
							</td>
							<td>
								{{ $row->active=='1'?'active':'' }}
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</section>
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/dataTables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/dataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
</script>
@stop