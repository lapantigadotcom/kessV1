					<div class="form-group">
						{!! Form::label('ms_media_id','Gambar : ') !!}
						<a href="javascript:void(0)" class="btn btn-default" onclick="setFeaturedImage()">Pilih Media</a>
						<div class="col-md-12" id="featuredImage">
						</div>
						{!! Form::hidden('ms_media_id',null, ['class' => 'form-control','id' => 'ms_media_id']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('pertanyaan','Pertanyaan : ') !!}
						{!! Form::textarea('pertanyaan',null, ['class' => 'form-control','rows'=>'3']) !!}
					</div>
					<hr>
					<div class="form-group">
						{!! Form::label('jawaban_a','Pilihan Jawaban A : ') !!}
						{!! Form::text('jawaban_a',null, ['class' => 'form-control', 'required']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('jawaban_b','Pilihan Jawaban  B: ') !!}
						{!! Form::text('jawaban_b',null, ['class' => 'form-control', 'required']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('jawaban_c','Pilihan Jawaban  C: ') !!}
						{!! Form::text('jawaban_c',null, ['class' => 'form-control', 'required']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('jawaban_d','Pilihan Jawaban  D: ') !!}
						{!! Form::text('jawaban_d',null, ['class' => 'form-control', 'required']) !!}
					</div>
					<hr>
					<div class="form-group">
						{!! Form::label('jawaban_benar','Jawaban Benar: ') !!}
						{!! Form::select('jawaban_benar', [
							'a' => 'Jawaban A',
							'b' => 'Jawaban B',
							'c' => 'Jawaban C',
							'd' => 'Jawaban D',
						], null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						<?php
						$ujianList = [];
						foreach ($data['ujian'] as $key => $value) {
							$ujianList[$value->id] = $value->title;
						}
						?>
						{!! Form::label('ujian','Untuk Ujian : ') !!}
						{!! Form::select('ujian', $ujianList, null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::submit($buttonSubmit,['class' => 'btn btn-info']) !!}
					</div>