@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Tambah Member
					</h3>                                    
				</div>
				<div class="box-body">
					{!! Form::open(['route'=>'vrs-admin.club.store', 'method' => 'POST', 'files' => 'true']) !!}
					@include('page.club.form',['buttonSubmit' => 'Tambah'])
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
@endsection