					@include('page.errors.list')
					<div class="form-group col-md-12">
						{!! Form::label('name','Nama : ') !!}
						{!! Form::text('name',null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group col-md-12">
						{!! Form::label('ms_type_club_id','Tipe Reward : ') !!}
						{!! Form::select('ms_type_club_id', $data['typeclub'], null, array('class' => 'form-control')
						) !!}
					</div>
					<div class="form-group">
						{!! Form::label('photo','Photo: ') !!}
						<div class="col-md-12" id="featuredImage">
						@if(isset($data['content']->photo))
							{!! HTML::image(asset('upload/member/'.$data['content']->photo),'',['class' => 'col-md-5 img-thumbnail']) !!}
						@endif
						</div>
						{!! Form::file('photo', ['class' => 'form-control']) !!}
						
					</div>
					<div class="form-group col-md-12">
						{!! Form::label('city','Kota : ') !!}
						{!! Form::text('city',null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group col-md-12">
						{!! Form::label('reward','Reward : ') !!}
						{!! Form::textarea('reward',null, ['class' => 'form-control','rows'=>'3']) !!}
					</div>
					
					<div class="form-group col-md-12">
						{!! Form::submit($buttonSubmit,['class' => 'btn btn-info']) !!}
					</div>