@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						EDIT MITRA
					</h3>                                    
				</div>
				<div class="box-body">
					{!! Form::model($data['content_detail'],array('action' => ['UserController@update',$data['content']->id],'method' => 'PATCH')) !!}
					<div class="col-md-9">
              			@include('page.partials.notification')
						@include('page.errors.list')
	                  	<?php
		                    $arrGender = array();
		                    $arrCity = array();
		                    foreach($data['gender'] as $row)
		                    {
		                      $arrGender[$row->id] = $row->name;
		                    }
		                    foreach($data['city'] as $row)
		                    {
		                      $arrCity[$row->id] = $row->name;
		                    }
	                  	?>
						@include('page.user.form',['submitText' => 'Perbarui'])
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.partials.media')
@endsection

@section('custom-head')

@stop
@section('custom-footer')
	@if($data['content_detail']->media != null)
		@include('page.scripts.media',['featuredImage' => $data['content_detail']->media->file ])
	@else
		@include('page.scripts.media')
	@endif
	<script type="text/javascript">
					$(document).ready(function() {
						// var reference = (function tipeportfolio(){
						// 	var type = $('#type').val();
						// 	switch(type){
						// 		case '1':
						// 			$('#mediaformcontainer').show();
						// 		break;
						// 		case '2':
						// 			$('#mediaformcontainer').hide();
						// 		break;
						// 	}
						//     return tipeportfolio;
						// }());
						// $('#type').change(function(){
						// 	reference();
						// 	console.log("ganti");
						// });
					});
					</script>
{!! HTML::script('plugins/jqueryValidation/jquery.validate.min.js') !!}
{!! HTML::script('plugins/tinymce/js/tinymce/tinymce.min.js') !!}
@stop
