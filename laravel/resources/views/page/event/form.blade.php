		<div class="form-group">
			{!! Form::label('name','Name : ') !!}
			{!! Form::text('name',null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('description','Deskripsi : ') !!}
			{!! Form::textarea('description',null, ['class' => 'form-control','id' => 'textEditor']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('date','Tanggal : ') !!}
			<?php $assignData = ""; ?>
			@if(isset($data['content']->date))
				<?php $assignData = date('d/m/Y', strtotime($data['content']->date)); ?>
			@endif
			{!! Form::text('date', $assignData, ['id' => 'datepicker']) !!}
		</div>
		<div class="form-group">
			{!! Form::submit($submitText,['class' => 'btn btn-info']) !!}
		</div>