@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Edit CCTV
					</h3>                                    
				</div>
				<div class="box-body">
					{!! Form::model($data['content'],array('action' => ['CCTVController@update',$data['content']->id],'method' => 'PATCH','files' => true)) !!}
					@include('page.cctv.form',['submitText' => 'Perbarui'])
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.partials.media')
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/chartjs/Chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
</script>
@include('page.scripts.media')
@stop