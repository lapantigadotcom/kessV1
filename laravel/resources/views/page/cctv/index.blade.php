@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						CCTV
					</h3>                                    
				</div>
				<div class="box-body table-responsive">
					<div class="row">
					@include('page.partials.notification')
					</div>
					<div class="row">
						<div class="col-md-3">
							{!! Form::open(array('route' => 'vrs-admin.cctv.store', 'method' => 'post','files' => true)) !!}
							@include('page.errors.list')
							@include('page.cctv.form',['submitText' => 'Tambah'])
							{!! Form::close() !!}
						</div>
						<div class="col-md-9">
							<table class="table" id="dataTables">
								<thead>
									<tr>
										<th class="col-md-1">&nbsp;</th>
										<th class="col-md-9">Nama CCTV</th>
										<th class="col-md-9">Deskripsi</th>
										<th class="col-md-9">Embed Script</th>
										
									</tr>
								</thead>
								<tbody>
									@foreach($data['content'] as $row)
									<tr>
										<td>&nbsp;</td>
										<td>
											{{ $row->title }}
											<div class="action-post-hover">
												<a href="{{route('vrs-admin.cctv.edit',[ $row->id ])}}">edit</a>
												<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('vrs-admin.cctv.delete',[ $row->id ]) !!}" style="color:red;">trash</a>
											</div>
										</td>
										<td>
											{{$row->description}}
										</td>
										<td>{{$row->embed}}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.partials.media')
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/chartjs/Chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
</script>
@include('page.scripts.media')
@stop