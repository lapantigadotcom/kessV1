@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						MENU <em><strong> {{  $data['content']->title }} </strong></em>
					</h3>                                    
				</div>
				<div class="box-body table-responsive">
					@include('page.partials.notification')
					<div class="col-md-5">
						<h5>Tambah Detail Menu Baru</h5>
						{!! Form::open(['route'=>'vrs-admin.menu.detail.store', 'method' => 'POST']) !!}
						<?php
						$menuId = $data['content']->id;
						$arrMenuDetail = array();
						$arrMenuDetail[0] = 'Tidak Ada';
						foreach($data['content']->menuDetail as $row)
						{
							$arrMenuDetail[$row->id] = $row->title;
						}
						$arrMenuType = array();
						foreach($data['menu_type'] as $row)
						{
							$arrMenuType[$row->id] = $row->title;
						}
						?>
						@include('page.menu.form-detail',['buttonSubmit' => 'Tambahkan','arrMenuDetail' => $arrMenuDetail])
						{!! Form::close() !!}
					</div>
					<div class="col-md-7">
						<div class="row">
							<p><b>Daftar Detail Menu</b></p>
						</div>
						<table class="table" id="dataTables">
							<thead>
								<tr>
									<th>&nbsp;</th>
									<th>Judul</th>
									<th>Kode</th>
								</tr>
							</thead>
							<tbody>
								@foreach($data['content']->menuDetail as $row)
								<tr>
									<td>&nbsp;</td>
									<td>
										{{ $row->title }}
										<div class="action-post-hover">
											<a href="{{route('vrs-admin.menu.detail.edit',[ $row->id ])}}">edit</a>
											<a href="{{route('vrs-admin.menu.detail.delete',[ $row->id ])}}" style="color:red">trash</a>
										</div>
									</td>
									<td>
										{{ $row->code }}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>		

@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#category-container').hide();

	$(function () {
		   // your code goes here
		   var tipe = $('#ms_menu_type_id').val();
		   categoryType(tipe);
		});
	$('#ms_menu_type_id').change(function(){
		var tipe = $('#ms_menu_type_id').val();
		categoryType(tipe);
	});
	function categoryType(tipe){
		switch(tipe)
		{
			case '1':
			$.ajax(
			{
				url:"{{ route('vrs-admin.category.api.getall') }}",
				type: "GET",
				data:{
					title: tipe
				}
			}).done(function(data,status){
				if(status=='success')
				{	
					console.log(data);
					var content = '';
					$.each(data, function(key, value){
						content += "<div class='col-md-6'><label><input type='radio' name='custom' value='" + data[key].id +"' /> "+ data[key].title +"</label></div>";
					});
					var html = "<div class='form-group'>"+ content +"</div>";
					$('#menu_type_container').empty();
					$('#menu_type_container').html(html);
				}
			});
			break;
			case '2':
			$.ajax(
			{
				url:"{{ route('vrs-admin.post.api.getall') }}",
				type: "GET",
				data:{
					title: tipe
				}
			}).done(function(data,status){
				if(status=='success')
				{	
					console.log(data);
					var content = '';
					$.each(data, function(key, value){
						var str = data[key].title;
						content += "<div class='col-md-6'><label><input type='radio' name='custom' value='" + data[key].id +"' /> "+ str.substring(0,15) + "..." +"</label></div>";
					});
					var html = "<div class='form-group'>"+ content +"</div>";
					$('#menu_type_container').empty();
					$('#menu_type_container').html(html);
				}
			});
			break;
			case '3':
			$.ajax(
			{
				url:"{{ route('vrs-admin.portfolio.api.getall') }}",
				type: "GET",
				data:{
					title: tipe
				}
			}).done(function(data,status){
				console.log(status);
				if(status=='success')
				{	
					console.log(data);
					var content = '';
					$.each(data, function(key, value){
						var str = data[key].title;
						content += "<div class='col-md-6'><label><input type='radio' name='custom' value='" + data[key].id +"' /> "+ str.substring(0,15) + "..." +"</label></div>";
					});
					var html = "<div class='form-group'>"+ content +"</div>";
					$('#menu_type_container').empty();
					$('#menu_type_container').html(html);
				}
			});
			break;
			case '4':
			$.ajax(
			{
				url:"{{ route('vrs-admin.rss.api.getall') }}",
				type: "GET",
				data:{
					title: tipe
				}
			}).done(function(data,status){
				console.log(status);
				if(status=='success')
				{	
					console.log(data);
					var content = '';
					$.each(data, function(key, value){
						var str = data[key].title;
						content += "<div class='col-md-6'><label><input type='radio' name='custom' value='" + data[key].id +"' /> "+ str.substring(0,15) + "..." +"</label></div>";
					});
					var html = "<div class='form-group'>"+ content +"</div>";
					$('#menu_type_container').empty();
					$('#menu_type_container').html(html);
				}
			});
			break;
			case '5' :
			$('#menu_type_container').empty();
			var html = "<div class='col-md-12'><label for='custom'>Link : </label>";
			html += "<input id='custom' type='text' class='form-control' name='custom' placeholder='ketikan link. (Misal: http://innvaderstudio.com)' /><br></div>";
			$('#menu_type_container').html(html);
			break;
			case '6' :
			$('#menu_type_container').empty();
			$('#menu_type_container').html(html);
			break;
			case '7' :
			$('#menu_type_container').empty();
			$('#menu_type_container').html(html);
			break;
			case '8' :
			$('#menu_type_container').empty();
			$('#menu_type_container').html(html);
			break;
			case '9' :
			$.ajax(
			{
				url:"{{ route('vrs-admin.portfoliocategory.api.getall') }}",
				type: "GET",
				data:{
					title: tipe
				}
			}).done(function(data,status){
				console.log(status);
				if(status=='success')
				{	
					console.log(data);
					var content = '';
					$.each(data, function(key, value){
						var str = data[key].name;
						content += "<div class='col-md-6'><label><input type='radio' name='custom' value='" + data[key].id +"' /> "+ str.substring(0,15) + "..." +"</label></div>";
					});
					var html = "<div class='form-group'>"+ content +"</div>";
					$('#menu_type_container').empty();
					$('#menu_type_container').html(html);
				}
			});
			break;
		}
	}
</script>
@stop