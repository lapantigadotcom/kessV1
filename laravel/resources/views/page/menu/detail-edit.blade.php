@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Edit Detail Menu
					</h3>                                    
				</div>
				<div class="box-body">
			{!! Form::model($data['content'],['action'=>['MenuDetailController@update',$data['content']->id], 'method' => 'PATCH']) !!}
				<?php
				$menuId = $data['content']->ms_menu_id;
				$arrMenuDetail = array();
				$arrMenuDetail[0] = 'Tidak Ada';
				foreach($data['content']->menu->menuDetail as $row)
				{
				 	$arrMenuDetail[$row->id] = $row->title;
				}
				$arrMenuType = array();
				foreach($data['menu_type'] as $row)
				{
				 	$arrMenuType[$row->id] = $row->title;
				}
				?>
				@include('page.menu.form-detail',['buttonSubmit' => 'Perbarui','arrMenuDetail' => $arrMenuDetail])
			{!! Form::close() !!}
		</div>
</div>
</div>
</div>
</section>
@endsection

@section('custom-footer')
	<script type="text/javascript">
		
		$('#ms_menu_type_id').change(function(){
			var tipe = $('#ms_menu_type_id').val();
		   	categoryType(tipe);
		});
		function categoryType(tipe){
			console.log(tipe);
			var defaultP = "{{ $data['content']->custom }}";
			var defaultT = "{{ $data['content']->ms_menu_type_id }}";
			switch(tipe)
		   {
		   	case '1':
		   		$.ajax(
		   			{
		   				url:"{{ route('vrs-admin.category.api.getall') }}",
		   				type: "GET",
		   				data:{
				          
				        }
				    }).done(function(data,status){
				    	if(status=='success')
			            {	
			            	console.log(data);
			            	var content = '';
			            	$.each(data, function(key, value){
			            		if({{ $data['content']->ms_menu_type_id }} == '1' && data[key].id==defaultP)
			            		{	
			            			content += "<div class='col-md-6'><label><input type='radio' checked name='custom' value='" + data[key].id +"' /> "+ data[key].title +"</label></div>";
			            		}else{
			            			content += "<div class='col-md-6'><label><input type='radio' name='custom' value='" + data[key].id +"' /> "+ data[key].title +"</label></div>";
			            		}
			            		
			            	});
			            	var html = "<div class='form-group'>"+ content +"</div>";
			            	$('#menu_type_container').empty();
			            	$('#menu_type_container').html(html);
			            }
				    });
		   		break;
		   	case '2':
		   		$.ajax(
		   			{
		   				url:"{{ route('vrs-admin.post.api.getall') }}",
		   				type: "GET",
		   				data:{
				          
				        }
				    }).done(function(data,status){
				    	if(status=='success')
			            {	
			            	console.log(data);
			            	var content = '';
			            	$.each(data, function(key, value){
			            		var str = data[key].title;
			            		if({{ $data['content']->ms_menu_type_id }} == '2' && data[key].id==defaultP)
			            		{	
			            			content += "<div class='col-md-6'><label><input type='radio' checked name='custom' value='" + data[key].id +"' /> "+ str.substring(0,15) + "..." +"</label></div>";
			            		}else{
			            			content += "<div class='col-md-6'><label><input type='radio' name='custom' value='" + data[key].id +"' /> "+ str.substring(0,15) + "..." +"</label></div>";
			            		}
			            		
			            	});
			            	var html = "<div class='form-group'>"+ content +"</div>";
			            	$('#menu_type_container').empty();
			            	$('#menu_type_container').html(html);
			            }
				    });
		   		break;
		   	case '3':
		   		$.ajax(
		   			{
		   				url:"{{ route('vrs-admin.portfolio.api.getall') }}",
		   				type: "GET",
		   				data:{
				          
				        }
				    }).done(function(data,status){
				    	console.log(status);
				    	if(status=='success')
			            {	
			            	console.log(data);
			            	var content = '';
			            	$.each(data, function(key, value){
			            		var str = data[key].title;
			            		if({{ $data['content']->ms_menu_type_id }} == '3' && data[key].id==defaultP)
			            		{	
			            			content += "<div class='col-md-6'><label><input type='radio' checked name='custom' value='" + data[key].id +"' /> "+ str.substring(0,15) + "..." +"</label></div>";
			            		}else{
			            			content += "<div class='col-md-6'><label><input type='radio' name='custom' value='" + data[key].id +"' /> "+ str.substring(0,15) + "..." +"</label></div>";
			            		}
			            	});
			            	var html = "<div class='form-group'>"+ content +"</div>";
			            	$('#menu_type_container').empty();
			            	$('#menu_type_container').html(html);
			            }
				    });
		   		break;
		   	case '4':
		   		$.ajax(
		   			{
		   				url:"{{ route('vrs-admin.rss.api.getall') }}",
		   				type: "GET",
		   				data:{
				          
				        }
				    }).done(function(data,status){
				    	console.log(status);
				    	if(status=='success')
			            {	
			            	console.log(data);
			            	var content = '';
			            	$.each(data, function(key, value){
			            		var str = data[key].title;
			            		if({{ $data['content']->ms_menu_type_id }} == '3' && data[key].id==defaultP)
			            		{	
			            			content += "<div class='col-md-6'><label><input type='radio' checked name='custom' value='" + data[key].id +"' /> "+ str.substring(0,15) + "..." +"</label></div>";
			            		}else{
			            			content += "<div class='col-md-6'><label><input type='radio' name='custom' value='" + data[key].id +"' /> "+ str.substring(0,15) + "..." +"</label></div>";
			            		}
			            	});
			            	var html = "<div class='form-group'>"+ content +"</div>";
			            	$('#menu_type_container').empty();
			            	$('#menu_type_container').html(html);
			            }
				    });
		   		break;
		   	case '5':
		   		$('#menu_type_container').empty();
		   		var html = "<div class='col-md-12'><label for='custom'>Link : </label>";
		   		if(defaultT==tipe)
		   			html += "<input id='custom' type='text' class='form-control' name='custom' placeholder='ketikan link. (Misal: http://innvaderstudio.com)' value='" + defaultP + "' /><br></div>";
		   		else
		   			html += "<input id='custom' type='text' class='form-control' name='custom' placeholder='ketikan link. (Misal: http://innvaderstudio.com)' value='' /><br></div>";
		   		$('#menu_type_container').html(html);
		   		break;
			case '6' :
			$('#menu_type_container').empty();
			$('#menu_type_container').html(html);
			break;
			case '7' :
			$('#menu_type_container').empty();
			$('#menu_type_container').html(html);
			break;
			case '8' :
			$('#menu_type_container').empty();
			$('#menu_type_container').html(html);
			break;
			case '9' :
			$.ajax(
			{
				url:"{{ route('vrs-admin.portfoliocategory.api.getall') }}",
				type: "GET",
				data:{
					title: tipe
				}
			}).done(function(data,status){
				console.log(status);
				if(status=='success')
				{	
					console.log(data);
					var content = '';
					$.each(data, function(key, value){
						var str = data[key].name;
						content += "<div class='col-md-6'><label><input type='radio' name='custom' value='" + data[key].id +"' /> "+ str.substring(0,15) + "..." +"</label></div>";
					});
					var html = "<div class='form-group'>"+ content +"</div>";
					$('#menu_type_container').empty();
					$('#menu_type_container').html(html);
				}
			});
			break;
			case '10' :
				$('#menu_type_container').empty();
				break;
		   }
		}
	</script>
@stop