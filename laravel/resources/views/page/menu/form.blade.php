					<div class="form-group">
						{!! Form::label('title','Nama : ') !!}
						{!! Form::text('title',null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('code','Kode : ') !!}
						{!! Form::text('code',null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::submit($buttonSubmit,['class' => 'btn btn-info']) !!}
					</div>