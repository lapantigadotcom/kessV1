					<div class="form-group">
						{!! Form::label('type','Tipe : ') !!}
						<select class="form-control" name="type" id="type">
							<option value="1" @if($buttonSubmit == 'Perbarui') @if($data['content']->type=='1') selected="" @endif @endif>Foto</option>
							<option value="2" @if($buttonSubmit == 'Perbarui') @if($data['content']->type=='2') selected="" @endif @endif>Video (Url Youtube)</option>
						</select>
					</div>
					<div class="form-group" id="mediaformcontainer">
						{!! Form::label('ms_media_id','Gambar : ') !!}
						<div class="col-md-12" id="featuredImage">
						</div>
						<a href="javascript:void(0)" class="btn btn-default" onclick="setFeaturedImage()">Pilih Media</a>
						{!! Form::hidden('ms_media_id',null, ['class' => 'form-control','id' => 'ms_media_id']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('caption','Judul : ') !!}
						{!! Form::text('caption',null, ['class' => 'form-control']) !!}
						{!! Form::hidden('ms_portfolio_id',$ms_portfolio_id, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('link','Link : ') !!}
						{!! Form::text('link',null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('description','Deskripsi : ') !!}
						{!! Form::textarea('description',null, ['class' => 'form-control','rows'=>'3']) !!}
					</div>
					<div class="form-group">
						{!! Form::submit($buttonSubmit,['class' => 'btn btn-info']) !!}
					</div>


					