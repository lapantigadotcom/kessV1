					<div class="form-group">
						<div class="form-group col-md-12">
							{!! Form::label('title','Nama Gerai : ') !!}
							{!! Form::text('title',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="form-group col-md-12">
							{!! Form::label('description','Deskripsi Gerai : ') !!}
							{!! Form::textarea('description',null, ['class' => 'form-control', 'rows' => 3]) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="form-group col-md-12">
							{!! Form::label('address','Alamat Gerai : ') !!}
							{!! Form::textarea('address',null, ['class' => 'form-control', 'rows' => 3]) !!}
						</div>
					</div>
					<div class="form-group col-md-12">
						{!! Form::submit($submitText,['class' => 'btn btn-info']) !!}
					</div>