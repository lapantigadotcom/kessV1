          			@include('page.partials.notification')
					@include('page.errors.list')
					<div class="form-group col-md-12">
						{!! Form::label('title','Judul : ') !!}
						{!! Form::text('title',null, ['class' => 'form-control','rows'=>'3']) !!}
					</div>
					<div class="form-group col-md-12">
						{!! Form::label('message','Pesan : ') !!}
						{!! Form::textarea('message',null, ['class' => 'form-control','rows'=>'3']) !!}
					</div>
					<div class="form-group col-md-12">
						{!! Form::submit($submitText,['class' => 'btn btn-info']) !!}
					</div>