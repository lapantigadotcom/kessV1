			<div class="panel panel-default">
		  		<div class="panel-heading">Achievement</div>
				<div class="panel-body">
					<div id="listAchievement" class="row">
					<?php $arrAchievement = array(); ?>
					@if(isset($data['content']))						
						@foreach($data['content']->achievements as $row)
						<?php array_push($arrAchievement, $row->id); ?>
						@endforeach
						
					@endif
			    	@foreach($data['achievement'] as $row)
			    		<div class="col-md-12">
			    			<label>
		    					<input type="checkbox" name="achievements[]" {{ in_array($row->id, $arrAchievement)?'checked':'' }} value="{!! $row->id !!}"> {!! $row->name !!}
		    				</label>
			    		</div>
			    	@endforeach
			    	</div>
			  	</div>
			</div>