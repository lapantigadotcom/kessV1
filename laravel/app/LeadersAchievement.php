<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadersAchievement extends Model {

	protected $table = 'tr_achievment';
    protected $fillable = array('ms_achievement_id', 'ms_leaders_id');
	public $timestamps = false;

	// public function achievement()
 //    {
 //        return $this->belongsTo('\App\LeadersAchievement','ms_leaders_id','id');
 //    }
}
