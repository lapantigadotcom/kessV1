<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model {

	protected $table = 'tr_user_detail';
	protected $fillable = array('name', 'panggilan', 'notelp', 'nohp', 'address', 'ms_gender_id', 'ms_city_id', 'facebook', 'enagic_id', 'kodepos', 'ms_media_id', 'latitude', 'longitude', 'twitter', 'jenis_mesin', 'sn_mesin', 'pin_bb', 'custom');
	public $timestamps = false;
    public function media()
    {
        return $this->hasOne('\App\Media', 'id', 'ms_media_id');
    }
    public function city()
    {
        return $this->hasOne('\App\City', 'id', 'ms_city_id');
    }
}
