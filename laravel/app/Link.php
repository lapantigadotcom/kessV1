<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model {

	protected $table = 'ms_links';
	protected $fillable = array('title','link','active');
	public $timestamps = true;
}