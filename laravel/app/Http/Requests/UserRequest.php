<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Input;

class UserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rule = [
			'name' => 'required',
			'panggilan' => 'required',
			'nohp' => 'required',
			'ms_gender_id' => 'required',
			'ms_city_id' => 'required',
			'email' => 'required',
			'password' => 'required|min:6',
			'file' => 'required|image'
		];
		// if(Input::get('password') != Input::get('password_conf'))
		// {
		// 	$rule['password'] = 'required';
		// 	$rule['password_conf'] = 'required';
		// }
		return $rule;
	}

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
	    return [
	        'name.required' => 'Nama lengkap harus diisi',
	        'panggilan.required'  => 'Nama panggilan harus diisi',
	        'nohp.required'  => 'No.HP harus diisi',
	        'email.required'  => 'Email harus diisi',
	        'password.required'  => 'Password harus diisi',
	        'password.min'  => 'Password minimal 6 karakter',
	        'file.required'  => 'Harus mengunggah foto'
	    ];
	}

}
