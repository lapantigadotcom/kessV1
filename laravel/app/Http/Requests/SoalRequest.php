<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SoalRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'pertanyaan' => 'required',
			'jawaban_a' => 'required',
			'jawaban_b' => 'required',
			'jawaban_c' => 'required',
			'jawaban_d' => 'required',
			'jawaban_benar' => 'required'
		];
	}

}
