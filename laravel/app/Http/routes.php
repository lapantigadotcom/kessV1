<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('sendmail', 'HomeController@sendMail');
Route::get('notfound', ['as' => 'notfound', 'uses' => 'HomeController@notfound']);
// Route::get('/korlantas/portfolio={id}{page?}',array('as' => 'home.category', 'uses' => 'HomeController@category'));
Route::get('/kess/kategori={id}{page?}',array('as' => 'home.category', 'uses' => 'HomeController@category'));
Route::get('/user/login',array('as' => 'home.login', 'uses' => 'HomeController@showLoginUser'));
Route::get('/cctv/', ['as' => 'home.cctv', 'uses' => 'HomeController@cctvs']);
Route::get('/cctv/{id}', ['as' => 'home.cctv.view', 'uses' => 'HomeController@cctv']);
//Route::post('/user/doLogin',array('as' => 'home.doLogin', 'uses' => 'HomeController@showLoginUser'));
//Route::post('/user/doLogin', '/auth/login');
//Route::get('/user/register', '/register');
Route::post('/adukan', ['as' => 'home.adukan', 'uses' => 'HomeController@adukan']);
Route::get('/ujian/',array('as' => 'home.soal_soal', 'uses' => 'HomeController@soal_soal'));
Route::get('/ujian/kess/{code}',array('as' => 'home.ujian', 'uses' => 'HomeController@ujian'));
Route::post('/ujian/hasil/{code}',array('as' => 'home.ujian.hasil', 'uses' => 'HomeController@hasil'));

Route::get('/artikel/kess={id}',array('as' => 'home.post', 'uses' => 'HomeController@post'));
Route::get('/products',array('as' => 'home.products', 'uses' => 'HomeController@products'));
Route::get('/products/{page?}',array('as' => 'home.products', 'uses' => 'HomeController@products'));
Route::get('/register', array('as' => 'home.register', 'uses' => 'HomeController@register'));
Route::post('/doRegister', array('as' => 'home.doRegister', 'uses' => 'HomeController@doRegister'));
Route::get('/mitra',array('as' => 'home.mitra', 'uses' => 'HomeController@mitra'));
Route::get('/mitraCity',array('as' => 'home.mitraCity', 'uses' => 'HomeController@mitraCity'));
Route::get('/profile/{id}',array('as' => 'home.profile', 'uses' => 'HomeController@profile'));
Route::get('/mitra/{custom}',array('as' => 'home.profileCustom', 'uses' => 'HomeController@profileCustom'));
Route::get('/profile/edit/{id}',array('as' => 'home.profile.edit', 'uses' => 'HomeController@profileEdit'));
Route::patch('/profile/update/{id}', array('as' => 'home.profile.update', 'uses' => 'HomeController@profileUpdate'));
Route::get('/testimoni', array('as' => 'home.testimoni', 'uses' => 'HomeController@testimoni'));
Route::post('/doTestimoni', array('as' => 'home.doTestimoni', 'uses' => 'HomeController@doTestimoni'));
Route::get('/contact', array('as' => 'home.contact', 'uses' => 'HomeController@contact'));
Route::post('/doContact', array('as' => 'home.doContact', 'uses' => 'HomeController@doContact'));
Route::get('/calendar', array('as' => 'home.calendar', 'uses' => 'HomeController@calendar'));
Route::get('/resetPass', array('as' => 'home.resetPass', 'uses' => 'HomeController@resetPass'));
Route::post('/doResetPass', array('as' => 'home.doResetPass', 'uses' => 'HomeController@doResetPass'));
Route::get('/subscribe', array('as' => 'home.subscribe', 'uses' => 'HomeController@subscribe'));
Route::post('/doSubscribe', array('as' => 'home.doSubscribe', 'uses' => 'HomeController@doSubscribe'));
Route::get('/testi', array('as' => 'home.testi', 'uses' => 'HomeController@leaders'));

// mobile api
Route::get('/api/getvideo',array('as' => 'api.getvideo', 'uses' => 'MobileServiceController@getVideo'));
Route::get('/api/getphoto',array('as' => 'api.getphoto', 'uses' => 'MobileServiceController@getPhoto'));
Route::get('/api/getpdfuser',array('as' => 'api.getpdfuser', 'uses' => 'MobileServiceController@getPdfUser'));
Route::get('/api/getpdfumum',array('as' => 'api.getpdfumum', 'uses' => 'MobileServiceController@getPdfUmum'));
Route::get('/api/getpdfall',array('as' => 'api.getpdfall', 'uses' => 'MobileServiceController@getPdfAll'));
Route::get('/api/getevent',array('as' => 'api.getevent', 'uses' => 'MobileServiceController@getEvent'));
Route::get('/api/getmitra',array('as' => 'api.getmitra', 'uses' => 'MobileServiceController@getMitra'));
Route::get('/api/getmitraid/{id}',array('as' => 'api.getmitraid', 'uses' => 'MobileServiceController@getMitraId'));
//Route::get('/api/getmitracity/{id}',array('as' => 'api.getmitracity', 'uses' => 'MobileServiceController@getMitraCity'));
Route::get('/api/gettestimoni',array('as' => 'api.gettestimoni', 'uses' => 'MobileServiceController@getTestimoni'));
Route::get('/api/getarticles',array('as' => 'api.getarticles', 'uses' => 'MobileServiceController@getArticles'));
Route::get('/api/gettechnology',array('as' => 'api.gettechnolgoy', 'uses' => 'MobileServiceController@getTechnology'));
Route::post('/api/login',array('as' => 'api.login', 'uses' => 'MobileServiceController@login'));
Route::post('/api/register',array('as' => 'api.register', 'uses' => 'MobileServiceController@register'));
Route::post('/api/contactus',array('as' => 'api.contactus', 'uses' => 'MobileServiceController@contactUs'));
Route::post('/api/resetpass',array('as' => 'api.resetpass', 'uses' => 'MobileServiceController@resetPass'));
Route::get('/api/getcity',array('as' => 'api.getcity', 'uses' => 'MobileServiceController@getCity'));
Route::get('/api/getcity/{id}',array('as' => 'api.getcity', 'uses' => 'MobileServiceController@getMitraCity'));
Route::get('/api/getgender',array('as' => 'api.getgender', 'uses' => 'MobileServiceController@getGender'));
Route::get('/api/pushnot',array('as' => 'api.pushnot', 'uses' => 'MobileServiceController@pushNot'));
Route::get('/api/aboutus',array('as' => 'api.aboutUs', 'uses' => 'MobileServiceController@aboutUs'));
Route::get('/api/getleveluk',array('as' => 'api.getleveluk','uses'=> 'MobileServiceController@getLevelUk'));
Route::get('/api/gettestimonikesehatan',array('as' => 'api.gettestimonikesehatan', 'uses' => 'MobileServiceController@getTestimoniKesehatan'));
Route::get('/api/gettestimonibisnis',array('as' => 'api.gettestimonibisnis', 'uses' => 'MobileServiceController@getTestimoniBisnis'));
Route::get('/api/getcredit',array('as' => 'api.getcredit','uses'=> 'MobileServiceController@getCredit'));
Route::get('/api/getphotoalbum',array('as' => 'api.getphotoalbum','uses'=> 'MobileServiceController@getPhotoAlbum'));
Route::get('/api/getnotification',array('as' => 'api.getnotification','uses'=> 'MobileServiceController@getNotification'));
// Route::get('/page/portfolio/{id}/{idchild?}',array('as' => 'home.portfolio', 'uses' => 'HomeController@portfolio')); 
// Route::get('/page/post/{id}/{page?}',array('as' => 'home.post', 'uses' => 'HomeController@post'));
// Route::get('/page/category/{id}/{page?}',array('as' => 'home.category', 'uses' => 'HomeController@category'));


Route::get('/media/korlantas={id}/{idchild?}',array('as' => 'home.portfolio', 'uses' => 'HomeController@portfolio'));
Route::get('/media/type={id}/{idchild?}',array('as' => 'home.type', 'uses' => 'HomeController@portfolioType'));
Route::get('/page/rss/{id}/{page?}',array('as' => 'home.rss', 'uses' => 'HomeController@rss'));
Route::post('/cari',array('as' => 'home.search', 'uses' => 'HomeController@search'));
// Route::post('/search',array('as' => 'home.search', 'uses' => 'HomeController@search'));
// Route::post('/comment', array('as' => 'home.comment', 'uses' => 'HomeController@comment'));
// Route::get('/ongkir', array('as' => 'home.ongkir', 'uses' => 'HomeController@getOngkir'));
// Route::post('/ongkir', array('as' => 'home.ongkir.store', 'uses' => 'HomeController@postOngkir'));
//Route::get('/member/{kode}', array('as' => 'home.member', 'uses' => 'HomeController@getMember'));
// Route::get('/tracking', array('as' => 'home.tracking', 'uses' => 'HomeController@getFack'));
// Route::post('/tracking', array('as' => 'home.tracking.store', 'uses' => 'HomeController@postCinta'));
// Route::get('/ongkir-international', array('as' => 'home.ongkirint', 'uses' => 'HomeController@getOngkirInt'));
// Route::post('/ongkir-international', array('as' => 'home.ongkirint.store', 'uses' => 'HomeController@postOngkirInt'));
// Route::get('/custom', 'HomeController@maudy');


//Route::get('protected', ['middleware' => ['auth', 'admin'], function() 
Route::group(['middleware' => ['auth', 'admin']], function()
{

	Route::get('vrs-admin', 'AdminController@index');
	Route::get('vrs-admin/createnotification', 'AdminController@createNotification');
	Route::post('vrs-admin/password', array('as' => 'vrs-admin.admin.password', 'uses' => 'AdminController@Changepassword'));
	Route::get('vrs-admin/password', array('as' => 'vrs-admin.admin.password', 'uses' => 'AdminController@password'));

	Route::get('vrs-admin/post/getAllAjax',array('as' => 'vrs-admin.post.api.getall','uses' => 'PostController@getAllAjax'));
	Route::get('vrs-admin/post/{post}/delete',array('as'=> 'vrs-admin.post.delete','uses' => 'PostController@destroy'));
	Route::get('vrs-admin/post/featured/{id}', array('as' => 'vrs-admin.post.featured','uses' => 'PostController@featured'));
	Route::resource('vrs-admin/post', 'PostController');

	Route::resource('vrs-admin/aduan', 'AduanController');
	Route::get('vrs-admin/replyForm/{id}', ['as' => 'vrs-admin.aduan.replyForm', 'uses' => 'AduanController@replyForm']);
	Route::post('vrs-admin/reply/{id}', ['as' => 'vrs-admin.aduan.reply', 'uses' => 'AduanController@reply']);

	Route::get('vrs-admin/product/getAllAjax',array('as' => 'vrs-admin.product.api.getall','uses' => 'ProductController@getAllAjax'));
	Route::get('vrs-admin/product/{product}/delete',array('as'=> 'vrs-admin.product.delete','uses' => 'ProductController@destroy'));
	Route::resource('vrs-admin/product', 'ProductController');

	Route::get('vrs-admin/event/getAllAjax',array('as' => 'vrs-admin.event.api.getall','uses' => 'EventController@getAllAjax'));
	Route::get('vrs-admin/event/{event}/delete',array('as'=> 'vrs-admin.event.delete','uses' => 'EventController@destroy'));
	Route::resource('vrs-admin/event', 'EventController');

	Route::get('vrs-admin/gerai/getAllAjax',array('as' => 'vrs-admin.gerai.api.getall','uses' => 'GeraiController@getAllAjax'));
	Route::get('vrs-admin/gerai/{gerai}/delete',array('as'=> 'vrs-admin.gerai.delete','uses' => 'GeraiController@destroy'));
	Route::get('vrs-admin/gerai/featured/{id}', array('as' => 'vrs-admin.gerai.featured','uses' => 'GeraiController@featured'));
	Route::resource('vrs-admin/gerai', 'GeraiController');

	Route::get('vrs-admin/testi/getAllAjax',array('as' => 'vrs-admin.testi.api.getall','uses' => 'LeadersController@getAllAjax'));
	Route::get('vrs-admin/testi/{leader}/delete',array('as'=> 'vrs-admin.testi.delete','uses' => 'LeadersController@destroy'));
	Route::resource('vrs-admin/testi', 'LeadersController');

	Route::get('vrs-admin/category/storeAjax','CategoryController@storeAjax');
	Route::get('vrs-admin/category/{post}/delete',array('as'=> 'vrs-admin.category.delete','uses' => 'CategoryController@destroy'));
	Route::get('vrs-admin/category/getAllAjax',array('as' => 'vrs-admin.category.api.getall','uses' => 'CategoryController@getAllAjax'));
	Route::resource('vrs-admin/category', 'CategoryController');

	Route::get('vrs-admin/product-category/storeAjax','ProductCategoriesController@storeAjax');
	Route::get('vrs-admin/product-category/{product}/delete',array('as'=> 'vrs-admin.product-category.delete','uses' => 'ProductCategoriesController@destroy'));
	Route::get('vrs-admin/product-category/getAllAjax',array('as' => 'vrs-admin.product-category.api.getall','uses' => 'ProductCategoriesController@getAllAjax'));
	Route::resource('vrs-admin/product-category', 'ProductCategoriesController');

	Route::get('vrs-admin/achievement/storeAjax','AchievementController@storeAjax');
	Route::get('vrs-admin/achievement/{leader}/delete',array('as'=> 'vrs-admin.achievement.delete','uses' => 'AchievementController@destroy'));
	Route::get('vrs-admin/achievement/getAllAjax',array('as' => 'vrs-admin.achievement.api.getall','uses' => 'AchievementController@getAllAjax'));
	Route::resource('vrs-admin/achievement', 'AchievementController');

	Route::get('vrs-admin/tag/storeAjax','TagController@storeAjax');
	Route::get('vrs-admin/tag/{tag}/delete',array('as'=> 'vrs-admin.tag.delete','uses' => 'TagController@destroy'));
	Route::resource('vrs-admin/tag', 'TagController');

	Route::post('vrs-admin/media/storeAjax','MediaController@storeAjax');
	Route::get('vrs-admin/media/getAllData','MediaController@getAllData');
	Route::get('vrs-admin/media/getMedia/{name}','MediaController@getMedia');
	Route::get('vrs-admin/media/{media}/delete',array('as'=> 'vrs-admin.media.delete','uses' => 'MediaController@destroy'));
	
	Route::resource('vrs-admin/media', 'MediaController');
	Route::get('vrs-admin/slider/{slider}/delete',array('as'=> 'vrs-admin.slider.delete','uses' => 'SliderController@destroy'));
	Route::resource('vrs-admin/slider', 'SliderController');

	Route::resource('vrs-admin/cctv', 'CCTVController');
	Route::get('vrs-admin/cctv/{cctv}/delete',array('as'=> 'vrs-admin.cctv.delete','uses' => 'CCTVController@destroy'));

	Route::get('vrs-admin/file/getMedia/{name}','FileController@getMedia');
	//Route::post('vrs-admin/file/create',array('as'=> 'vrs-admin.file.create','uses' => 'FileController@create'));
	Route::get('vrs-admin/file/{file}/delete',array('as'=> 'vrs-admin.file.delete','uses' => 'FileController@destroy'));
	Route::get('vrs-admin/file/featured/{id}', array('as' => 'vrs-admin.file.featured','uses' => 'FileController@featured'));
	Route::resource('vrs-admin/file', 'FileController');

	Route::get('vrs-admin/video/getMedia/{name}','VideoController@getMedia');
	Route::get('vrs-admin/video/{video}/delete',array('as'=> 'vrs-admin.video.delete','uses' => 'VideoController@destroy'));
	Route::get('vrs-admin/video/featured/{id}', array('as' => 'vrs-admin.video.featured','uses' => 'VideoController@featured'));
	Route::resource('vrs-admin/video', 'VideoController');

	Route::resource('vrs-admin/theme', 'ThemeController');
	Route::get('vrs-admin/club/{club}/delete',array('as'=> 'vrs-admin.club.delete','uses' => 'ClubController@destroy'));
	Route::resource('vrs-admin/club', 'ClubController');

	Route::get('vrs-admin/portfolio/enabled/{id}/{menu}',array('as'=> 'vrs-admin.portfolio.enabled','uses' => 'PortfolioController@enabled'));
	Route::get('vrs-admin/portfolio/featured/{id}', array('as' => 'vrs-admin.portfolio.featured','uses' => 'PortfolioController@featured'));
	Route::get('vrs-admin/portfolio/{menu}/delete',array('as'=> 'vrs-admin.portfolio.delete','uses' => 'PortfolioController@destroy'));


	Route::get('vrs-admin/detailportfolio/{menu}/delete',array('as'=> 'vrs-admin.detailportfolio.delete','uses' => 'DetailPortfolioController@destroy'));
	Route::resource('vrs-admin/detailportfolio', 'DetailPortfolioController');

	Route::get('vrs-admin/portfolio/getAllData',array('as' => 'vrs-admin.portfolio.api.getall','uses' => 'PortfolioController@getAllData'));
	Route::resource('vrs-admin/portfolio', 'PortfolioController');

	Route::get('vrs-admin/menu/{menu}/delete',array('as'=> 'vrs-admin.menu.delete','uses' => 'MenuController@destroy'));
	Route::resource('vrs-admin/menu', 'MenuController');
	Route::get('vrs-admin/menu/detail/{menu}/delete',array('as'=> 'vrs-admin.menu.detail.delete','uses' => 'MenuDetailController@destroy'));
	Route::get('vrs-admin/menu/detail/getjson',array('as'=> 'vrs-admin.menu.detail.getjson','uses' => 'MenuDetailController@getjson'));
	Route::resource('vrs-admin/menu/detail', 'MenuDetailController');

	Route::resource('vrs-admin/ujian', 'UjianController');
	Route::get('vrs-admin/ujian/{ujian}/delete',array('as'=> 'vrs-admin.ujian.delete','uses' => 'UjianController@destroy'));

	Route::resource('vrs-admin/soal', 'SoalController');
	Route::get('vrs-admin/soal/{soal}/delete',array('as'=> 'vrs-admin.soal.delete','uses' => 'SoalController@destroy'));

	Route::post('vrs-admin/general/upload',array('as' => 'vrs-admin.general.upload', 'uses' => 'GeneralController@upload'));
	Route::resource('vrs-admin/general', 'GeneralController');

	Route::get('vrs-admin/contact/enabled/{id}',array('as' => 'vrs-admin.contact.enabled', 'uses' => 'ContactController@enabled'));
	Route::get('vrs-admin/contact/{contact}/delete',array('as'=> 'vrs-admin.contact.delete','uses' => 'ContactController@destroy'));
	Route::resource('vrs-admin/contact', 'ContactController');

	Route::any('/imglist', ['as'=>'imglist', 'uses'=>'FileBrowserController@imageList']);
	//Image upload

	Route::any('/upload', [ 'uses' =>'FileBrowserController@index']);

	Route::get('vrs-admin/rss/getAllAjax',array('as' => 'vrs-admin.rss.api.getall','uses' => 'RssController@getAllAjax'));
	Route::get('vrs-admin/rss/{rss}/delete',array('as'=> 'vrs-admin.rss.delete','uses' => 'RssController@destroy'));
	Route::resource('vrs-admin/rss', 'RssController');

	Route::get('vrs-admin/social/{social}/delete',array('as'=> 'vrs-admin.social.delete','uses' => 'SocialController@destroy'));
	Route::resource('vrs-admin/social', 'SocialController');

	Route::get('vrs-admin/comment/{comment}/delete',array('as'=> 'vrs-admin.comment.delete','uses' => 'CommentController@destroy'));
	Route::get('vrs-admin/comment/{comment}/approve/{on}',array('as'=> 'vrs-admin.comment.approve','uses' => 'CommentController@approve'));
	Route::resource('vrs-admin/comment','CommentController');

	Route::get('vrs-admin/typeclub/{id}/delete',array('as'=> 'vrs-admin.typeclub.delete','uses' => 'TypeClubController@destroy'));
	Route::resource('vrs-admin/typeclub','TypeClubController');

	Route::get('vrs-admin/portfoliocategory/getAllAjax',array('as' => 'vrs-admin.portfoliocategory.api.getall','uses' => 'PortfolioCategoryController@getAllAjax'));

	//Route::get('vrs-admin/notification/getAllAjax',array('as' => 'vrs-admin.post.api.getall','uses' => 'PostController@getAllAjax'));
	Route::get('vrs-admin/notification/create',array('as'=> 'vrs-admin.notification.create','uses' => 'NotificationController@create'));
	Route::resource('vrs-admin/notification', 'NotificationController');

//}]);
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'user' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController'
]);

Route::get('analitics',function(){
	$analyticsData = LaravelAnalytics::getTopBrowsers(7);
	dd($analyticsData);
	exit();
});
