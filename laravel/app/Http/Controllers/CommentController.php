<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Comment;
use App\Http\CommentRequests;
use App\Http\CommentRequests\CommentCommentRequest;
use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use Illuminate\Http\CommentRequest;
use Session;

class CommentController extends BasicController {

	public function __construct() {
		$this->modelName = 'App\Comment';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$data['content'] = Comment::with('post')->orderBy('created_at','desc')->get();
		return view('page.comment.index')->with('data',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function approve($id, $app)
	{
		$data=Comment::find($id);
		$data->enabled=$app;
		$data->save();
		if($app=='1')
			Session::flash('success','Komentar berhasil di approve');
		else
			Session::flash('success','Komentar berhasil di unapprove');
		return redirect()->back();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CommentRequest $request)
	{
		$data = Comment::create($request->all());
		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $request->input('title');
		$data->logs()->save($this->log($log));
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.comment.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content']=Comment::find($id);
		$data['comment'] = Comment::all();

		return view('page.comment.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CommentRequest $request,$id)
	{

		$data = Comment::find($id);
		$data->update($request->all());

		$log = array();
		$log['action'] = 'update';
		$log['name'] = $request->input('title');
		$data->logs()->save($this->log($log));


		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('vrs-admin.comment.index');
	}
	public function destroy($id)
	{
		$data = Comment::find($id);

		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->back();
	}

}
