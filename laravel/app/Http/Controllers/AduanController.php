<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Session;
use DB;
use Input;
class AduanController extends BasicController {
	public function index(){
		$data['content'] = DB::table('ms_aduan')->join('ms_kategori_aduan', 'ms_aduan.ms_kategori_aduan_id', '=', 'ms_kategori_aduan.id')->get();
		return view('page.aduan.index',compact('data'));
	}

	public function replyForm($id){
		$data['content'] = DB::table('ms_aduan')->where('id', $id)->first();
		return view('page.aduan.reply',compact('data'));
	}

	public function reply(){
		dd(Input::all());
	}
}