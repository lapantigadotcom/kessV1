<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 
*/

use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\VideoRequest;
use App\Media;
use App\FilePdf;
use File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Storage;
use Session;
use Image;
use Input;

class VideoController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	protected $path;
	function __construct() {
		$this->path = 'upload/file/';
	}
	public function index()
	{
		$data['content'] = FilePdf::where('mime','video')->orderBy('created_at','desc')->get();
		return view('page.video.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('page.video.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(VideoRequest $request)
	{
		$data = New FilePdf;
		$fileName = '';
		if ($request->hasFile('file'))
		{
		    $file = $request->file('file');
		    $oriName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
			$extension = $file->getClientOriginalExtension();
			$fileName = $oriName.'_'.date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path,$fileName);
			$data->mime = 'video';//$extension;//$file->getClientMimeType();

			// $img = File::make($this->path.$fileName);
			// $img->save($this->path.$fileName, 60);
		}
		
		$data->file = $fileName;
		$data->title = $request->input('title');
		$data->description = $request->input('description');
		$data->isumum = 1;
		//$data->ms_media_id = $request->input('ms_media_id');
		$data->save();

		// $log = array();
		// $log['action'] = 'insert';
		// $log['name'] = $fileName;
		// $data->logs()->save($this->log($log));
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.video.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content']=FilePdf::find($id);
		return view('page.video.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$data = FilePdf::find($id);
		$fileName = $data->file;

		if ($request->hasFile('file'))
		{
		    $file = $request->file('file');
		    $oriName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
			$extension = $file->getClientOriginalExtension();
			$fileName = $oriName.'_'.date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path,$fileName);
			$data->mime = $extension;
			$data->file = $fileName;

			// $log = array();
			// $log['action'] = 'update';
			// $log['name'] = $data->file.' - '.$fileName;
			// $data->logs()->save($this->log($log));


			// $file->move($this->path,$fileName);
			// $img = Image::make($this->path.$fileName);
			// $img->save($this->path.$fileName, 60);
			// if(File::exists('upload/file/'.$data->file))
			// {
			// 	File::delete('upload/file/'.$data->file);
			// }

			// $data->mime = $file->getClientMimeType();
		}else{
			// $log = array();
			// $log['action'] = 'update';
			// $log['name'] = $data->caption;
			// $data->logs()->save($this->log($log));
		}
		
		$data->title = $request->input('title');
		$data->description = $request->input('description');
		//$data->ms_media_id = $request->input('ms_media_id');
		$data->save();

		echo $request->input('title')."<br>";
		echo $request->input('description')."<br>";
		echo $request->input('ms_media_id')."<br>";

		return redirect()->route('vrs-admin.video.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = FilePdf::find($id);
		// $log = array();
		// $log['action'] = 'delete';
		// $log['name'] = $data->file;

		File::delete($this->path.$data->file);	

		// $data->logs()->save($this->log($log));
		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.video.index');
	}

	public function featured($id)
	{
		$data = FilePdf::find($id);
		if(empty($data))
		{
			return;
		}
		if($data->isumum == '1')
		{
			$data->isumum = '0';
		}else{
			$data->isumum = '1';
		}
		$data->save();
		return;
	}
}
