<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Category;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\MitraRequest;
use App\Http\Requests\PostRequest;
use App\Http\Requests\ContactUsRequest;
use App\Http\Requests\TestimoniRequest;
use App\Portfolio;
use App\PortfolioCategory;
use App\Post;
use App\Leaders;
use App\Rss;
use App\Slider;
use App\Theme;
use App\General;
use App\Download;
use App\Products;
use App\Contact;
use App\Gender;
use App\City;
use App\User;
use App\UserDetail;
use App\Media;
use App\Event;
use App\FilePdf;
//use FeedReader;
use Illuminate\Http\Request;
use Session;
use App\ThirdParty\Ongkir;
use Image;
use Auth;
use Redirect;
use Mail;
use Input;
use DateTime;
//use LaravelAnalytics;
use DB;
class HomeController extends Controller {

	protected $theme;
	protected $data;
	protected $api_shipping;

	public function __construct($foo = null) {
		$this->path = 'upload/media/';
		$theme_t = Theme::where('active','1')->first();
		$this->api_shipping = 'a6d63f4a881977714ca6ffef85e955e9';
		$this->theme = $theme_t->code;
		$this->data['contact'] = Contact::where('featured','1')->first();
		$this->data['download'] = Download::all();
		$this->data['products'] = Products::with('media')->where('ms_categories_id', 1)->get()->take(8);
		$this->data['leaders'] = Leaders::with('achievements')->orderBy('rank','asc')->get();
		$this->data['about_sekilas'] = Post::with('category')->whereHas('category', function($q)
		{
		    $q->where('ms_category_id', '9'); // category 9 = sekilas
		})->get()->take(1);
		$this->data['beritadepan'] = Post::with('category')->whereHas('category', function($q)
		{	  
		    $q->where('ms_category_id', '23'); // category 9 = sekilas
		})->get()->take(6);


		$this->data['latest'] = Post::with('category')->where('featured','1')->whereHas('category', function($q)
		{
			$q->where('ms_category_id', '8'); // category 8 = kelebihan
		})->orderBy('created_at','desc')->get()->take(4);
		$this->data['latest_posts'] = Post::with('category')->whereHas('category', function($q)
		{
		    $q->where('ms_category_id', '1'); // category 2 = event
		})->orderBy('created_at','desc')->get()->take(4);
		$this->data['slider'] = Slider::with('media')->join('ms_medias', 'ms_sliders.ms_media_id', '=', 'ms_medias.id')->orderBy('ms_sliders.created_at','desc')->get();
		$this->data['slider_posts'] = Post::with('category')->whereHas('category', function($q)
		{
			$q->where('ms_category_id', '1'); // category 1 = berita
		})->orderBy('created_at','desc')->get()->take(3);
		$this->data['portfolio2'] = Portfolio::where('featured', 1)
									->join('tr_detail_portfolios', 'ms_portfolios.id', '=', 'tr_detail_portfolios.ms_portfolio_id')
									->join('ms_medias', 'tr_detail_portfolios.ms_media_id', '=', 'ms_medias.id')
									->where('tr_detail_portfolios.type', 1)
									->orderBy('tr_detail_portfolios.id', 'desc')
									->limit(8)
									->get();
		$this->data['portfolio'] = Portfolio::with('detailPortfolio','detailPortfolio.media')->where('featured','1')->whereHas('detailPortfolio', function($q)
		{
		    $q->where('type', '1');
		})->get();
		$this->data['kangen_posts'] = Post::with('category')->whereHas('category', function($q)
		{
		    $q->where('ms_category_id', '14'); // category 14 = kangen edukasi
		})->get()->take(6);
		
		$this->data['kategori_aduan'] = DB::table('ms_kategori_aduan')->get();
		$this->data['event'] = Event::orderBy('date','asc')->get();
		$this->data['soal_index'] = DB::table('ms_ujian')
									->join('ms_medias', 'ms_ujian.ms_media_id', '=', 'ms_medias.id')
									->select('ms_medias.file', 'ms_ujian.id', 'ms_ujian.title', 'ms_ujian.description', 'ms_ujian.code')
									->orderBy('ms_ujian.id', 'desc')->limit(4)->get();
		//$this->data['stat_pages'] = LaravelAnalytics::getMostVisitedPages(30, 100000);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = $this->data;
		$data_t = Rss::where('featured','1')->first();
		$data['RSStitle'] = 'RSS '.ucfirst($data_t->title);
		//$rss = FeedReader::read($data_t->link);
		//$data['RSS'] = $rss->get_items(0,3);
		$data['galeri'] = Media::orderBy('id', 'desc')->limit(8)->get();
		$frontName = 'Sambutan';
		$data['sambutan'] = Post::join('tr_category_posts', 'ms_posts.id', '=', 'tr_category_posts.ms_post_id')
								->join('ms_categories', 'tr_category_posts.ms_category_id', '=', 'ms_categories.id')
								->where('ms_categories.title', $frontName)
								->select('ms_posts.title', 'ms_posts.description')
								->orderBy('ms_posts.id', 'desc')
								->first();
		$jenis = 'Berita';

			$data['kegiatan'] = Post::with('mediaPost','user')->whereHas('category',function($q) {
			$q->where('ms_category_id', '23'); // category 25 = berita kegiatan
		})->orderBy('created_at','desc')
		// ->get()->take(3);
		->get()->take(4);
		$data['latest_news'] = Post::join('tr_category_posts', 'ms_posts.id', '=', 'tr_category_posts.ms_post_id')
								->join('ms_categories', 'tr_category_posts.ms_category_id', '=', 'ms_categories.id')
								->join('tr_media_posts', 'ms_posts.id', '=', 'tr_media_posts.ms_post_id')
								->join('ms_medias', 'tr_media_posts.ms_media_id', '=', 'ms_medias.id')
								->join('users', 'ms_posts.ms_user_id', '=', 'users.id')
								->where('ms_categories.title', $jenis)
								->select('ms_posts.title', 'ms_posts.description', 'users.name', 'ms_posts.created_at', 'ms_medias.file', 'ms_posts.id')
								->orderBy('created_at', 'desc')
								->limit(4)
								->get();

		$data['testi'] = Leaders::join('ms_medias', 'ms_leaders.ms_media_id', '=', 'ms_medias.id')
								->select('ms_leaders.name', 'ms_leaders.rank_name', 'ms_medias.file')
								->orderBy('ms_leaders.id')->limit(3)
								->get();
		$data['index'] = true;
		return view('theme.'.$this->theme.'.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
	}

	public function leaders()
	{
		$data = $this->data;
		$page = 'leaders';
		$data['content'] = Leaders::orderBy('rank', 'asc');

		$data['leaders'] = $data['content']->paginate(12);
		$data['leaders']->setPath("");

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function resetPass()
	{
		$data = $this->data;
		$page = 'resetpass/create';
		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function doResetPass(Request $request)
	{
		$user = User::where('email', $request->email)->get()->first();
		if($user != null)
		{
			//echo $user->name;
			$newPass = $this->generateRandomString(6);
			$content = "Password baru anda adalah ".$newPass;
			$email = $request->email;
			echo $content;
			//$email = "pentol231094@gmail.com";
			Mail::raw($content, function($message) use ($email)
			{
			    $message->to($email);
			    //$message->to('anton.ferryanto@gmail.com');
			    //$message->to('support@gemarsehati.com');
			    //$message->to('pentol231094@gmail.com');
			});
			$user->password = \Hash::make($newPass);
			$user->save();
			Session::flash('success','Password baru sudah dikirim ke email anda');
		}
		else			
			Session::flash('error','Email tidak terdaftar');

		$data = $this->data;
		$page = 'resetpass/create';
		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	function generateRandomString($length = 10) 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function calendar()
	{
		$data = $this->data;
		$page = 'calendar';
		$this->data['event'] = Event::orderBy('date','asc')->get();

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function showLoginUser()
	{
		$data = $this->data;
		$page = 'login/login';
		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function profile($id)
	{
		$data = $this->data;
		$page = 'profile/profile';
		$data['mitra'] = User::find($id);
		$data['pdf'] = FilePdf::where('mime','pdf')->get();
		$data['video'] = FilePdf::where('mime','video')->get();

		//return view('theme.'.$this->theme.".".$page,compact('data'));
		//route('home.profileCustom', $data['mitra']->detail->custom);
	 	return redirect('/mitra/'.$data['mitra']->detail->custom);
	}

	public function profileCustom($custom)
	{
		//echo $custom;
		if($custom == null || $custom == "")
			$custom = Session::get('custom');
		$data = $this->data;
		$page = 'profile/profile';
		$data['mitra'] = User::with('detail')->whereHas('detail', function($q) use($custom)
		{
		    $q->where('custom', $custom); 
		})->get()->first();
		$data['pdf'] = FilePdf::where('mime','pdf')->get();
		$data['video'] = FilePdf::where('mime','video')->get();

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function profileEdit($id)
	{
		$data = $this->data;
		$page = 'profile/edit';
		$data['mitra'] = User::find($id);
		$data['pdf'] = FilePdf::all();

		//$data['content'] = User::all()->find($id);
		$data['content_detail'] = $data['mitra']->detail()->first();
		$data['content_detail']['name'] = $data['mitra']->name;
		$data['content_detail']['email'] = $data['mitra']->email;
		$data['content_detail']['password'] = $data['mitra']->password;
			
		$data['gender'] = Gender::all();
		$data['city'] = City::all();

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function profileUpdate(MitraRequest $request, $id)
	{
		$page = 'profile/profile';
		if(Auth::check() && Auth::user()->id == $id)
		{									
			if(Auth::user()->email == $request->email || User::where('email', $request->email)->get()->first() == null)
			{
				$data = User::find($id);
				$userdetail = UserDetail::find($data->tr_user_detail_id);
				$data->update(array(
					'email' => $request->email,
					'name' => $request->name
					));
				$userdetail->update(array(
					'panggilan' => $request->panggilan,
					'custom' => $request->custom,
					'notelp' => $request->notelp,
					'nohp' => $request->nohp,
					'address' => $request->address,
					'kodepos' => $request->kodepos,
					'latitude' => $request->latitude,
					'longitude' => $request->longitude,
					'facebook' => $request->facebook,
					'twitter' => $request->twitter,
					'pin_bb' => $request->pin_bb,
					'ms_city_id' => $request->ms_city_id,
					'ms_gender_id' => $request->ms_gender_id
					));
				if($request->password != "")
					$data->update(array(
						'password' => \Hash::make($request->password)
						));

				$foto = New Media;
				$fileName = '';
				//echo $request->file;
				if ($request->hasFile('file'))
				{
				    $file = $request->file('file');
					$extension = $file->getClientOriginalExtension();
					$fileName = 'img_'.date('Ymd_Hsi').'.'.$extension;
					$file->move($this->path,$fileName);
					$foto->mime = $file->getClientMimeType();
					$img = Image::make($this->path.$fileName);
					$img->save($this->path.$fileName, 60);
					$foto->file = $fileName;
					$foto->caption = 'Foto '.$request->name;
					$foto->description = 'Foto '.$request->name;
					$foto->save();
					$userdetail->update(array(
						'ms_media_id' => $foto->id
						));
					//echo " ada";
				}

				Session::flash('success','Data berhasil diperbarui');
			}
			else
			{
				Session::flash('success','Email sudah digunakan');
			}
			//redirect
			$data = $this->data;
			$data['mitra'] = User::find($id);
			$data['pdf'] = FilePdf::all();
			$data['video'] = FilePdf::where('mime','video')->get();

			//$data['content'] = User::all()->find($id);
			$data['content_detail'] = $data['mitra']->detail()->first();
			$data['content_detail']['name'] = $data['mitra']->name;
			$data['content_detail']['email'] = $data['mitra']->email;
			$data['content_detail']['password'] = $data['mitra']->password;
				
			$data['gender'] = Gender::all();
			$data['city'] = City::all();
			return view('theme.'.$this->theme.".".$page,compact('data'));
		}
		$data = $this->data;
		$page = 'profile/profile';
		$data['mitra'] = User::find($id);
		$data['pdf'] = FilePdf::where('mime','pdf')->get();
		$data['video'] = FilePdf::where('mime','video')->get();
		//Session::flash('success','Registrasi gagal');

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}


	public function mitra()
	{
		$data = $this->data;
		$page = 'gerai';
		$data['gerai'] = DB::table('ms_gerai')->orderBy('id', 'desc')->get();
		$data['mitra'] = User::where('ms_privilige_id', '2')->where('active', '1')->get();
		$arr[] = array();
		foreach ($data['gerai'] as $row) 
		{
			$arr[] = array(
				'id' => $row->id,
				'title' => $row->title,
				'description' => $row->description,
				'address' => $row->address,
				'koordinat' => $row->latitude.",".$row->longitude
				);
		}
		$data['mitraDetail'] = $arr;

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function mitraCity()
	{
		$data = $this->data;
		$page = 'mitraCity';
		$data['mitra'] = User::where('ms_privilige_id', '2')->where('active', '1')->get();
		$arr[] = array();
		foreach ($data['mitra'] as $row) 
		{
			$arr[] = array(
				'id' => $row->id,
				'kota_id' => $row->detail->ms_city_id,
				'enagic_id' => $row->detail->enagic_id,
				'nama' => preg_replace('/[^A-Za-z0-9\-]/', ' ', $row->name),
				'url' => '#',
				'nohp' => $row->detail->nohp,
				'notelp' => $row->detail->notelp,
				'alamat' =>	preg_replace('/[^A-Za-z0-9\-]/', ' ', $row->detail->address),
				'kota' => $row->detail->city->name,
				'facebook' =>	$row->detail->facebook,
				'email' =>	$row->email,
				'foto' => asset('upload/media/'.$row->detail->media->file),
				'koordinat' => $row->detail->latitude.",".$row->detail->longitude
				);
		}
		$data['mitraDetail'] = $arr;
		$data['city'] = City::all();

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}
	

	function getCoordinatesFromMitra($address, $city)
	{
		$coord = $this->getCoordinates($address);
		if($coord == 'null')
		{
			$coord = $this->getCoordinates($address.", ".$city);
			if($coord == 'null')
			{
				$coord = $this->getCoordinates($city);
				if($coord == 'null')
					$coord = 'not found';
			}
		}
		return $coord;
	}

	function getCoordinates($address) 
	{ 
		$key1 = "AIzaSyDenG9f8NSuPaEkFoxpMpHZrs3DyMHsBSk";
		$key2 = "AIzaSyAbcKfXVS-7CROSC5Jafzv5yjEzbqtuIno";
		$key3 = "AIzaSyCpcC4_Xhs6v5MxQoTAHkDCqcIXcrdQWDo";
		$key = $key3;
		//echo $address."<br>";
		$address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern		 
		$url = "https://maps.google.com/maps/api/geocode/json?key=$key&sensor=false&address=$address";		 
		$response = file_get_contents($url);		 
		$json = json_decode($response,TRUE); //generate array object from the response from the web	
		if(count ($json['results'])	> 0)
			return ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);	 		
		return 'null';
	}

	public function register()
	{
		$data = $this->data;
		$page = 'member-register';
		$data['gender'] = Gender::all();
		$data['city'] = City::all();

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function doRegister(UserRequest $request)
	{
		if(User::where('email', $request->email)->get()->first() == null)
		{
			$data = User::create($request->all());
			$data->ms_privilige_id = 2;
			$data->password = \Hash::make($data->password);

			$foto = New Media;
			$fileName = '';
			//echo $request->file;
			if ($request->hasFile('file'))
			{
			    $file = $request->file('file');
				$extension = $file->getClientOriginalExtension();
				$fileName = 'img_'.date('Ymd_Hsi').'.'.$extension;
				$file->move($this->path,$fileName);
				$foto->mime = $file->getClientMimeType();
				$img = Image::make($this->path.$fileName);
				$img->save($this->path.$fileName, 60);
				//echo "has";
			}
				//echo "nope";

			$foto->file = $fileName;
			$foto->caption = 'Foto '.$request->name;
			$foto->description = 'Foto '.$request->name;
			$foto->save();

			if($request->latitude == "" || $request->longitude == "")
			{
				$cityName = City::where('id', $request->ms_city_id)->first()->name;
				$tmpCoord = $this->getCoordinatesFromMitra($request->address, $cityName);
				if($tmpCoord == 'null')
				{
					$tmpCoord = $this->getCoordinates($request->address.", ".$cityName);
					if($tmpCoord == 'null')
					{
						$tmpCoord = $this->getCoordinates($cityName);
						if($tmpCoord == 'null')
							$tmpCoord = "0,0";
					}
				}
	    		$tmpCoord = explode(',', $tmpCoord);
	    		$request->latitude = $tmpCoord[0];
	    		$request->longitude = $tmpCoord[1];
			}

			$userdetail = UserDetail::create(array(
				'panggilan' => $request->panggilan,
				'custom' => $request->custom,
				'notelp' => $request->notelp,
				'nohp' => $request->nohp,
				'address' => $request->address,
				'kodepos' => $request->kodepos,
				'facebook' => $request->facebook,
				'twitter' => $request->twitter,
				'latitude' => $request->latitude,
				'longitude' => $request->longitude,
				'pin_bb' => $request->pin_bb,
				'ms_media_id' => $foto->id,
				'ms_gender_id' => $request->ms_gender_id,
				'ms_city_id' => $request->ms_city_id
				));
			$data->tr_user_detail_id = $userdetail->id;
			$userdetail->ms_media_id = $foto->id;

			$userdetail->save();
			$data->save();
			 
			Session::flash('success','Registrasi berhasil');
		}
		else
		{
			Session::flash('error','Email sudah digunakan');
			return redirect()->route('home.register');
		}
	}

	public function testimoni()
	{
		$data = $this->data;
		$page = 'testimoni/create';

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function doTestimoni(TestimoniRequest $request)
	{
		if(Auth::check())
		{
			$data=Post::create($request->all());
			$data->ms_user_id = Auth::id();
			$data->ms_status_posts_id = 2;
			$data->title = "Testimoni by ".Auth::user()->name;
			$data->category()->attach(7);

			$foto = New Media;
			$fileName = '';
			if ($request->hasFile('file'))
			{
			    $file = $request->file('file');
				$extension = $file->getClientOriginalExtension();
				$fileName = 'img_'.date('Ymd_Hsi').'.'.$extension;
				$file->move($this->path,$fileName);
				$foto->mime = $file->getClientMimeType();
				$img = Image::make($this->path.$fileName);
				$img->save($this->path.$fileName, 60);

				$foto->file = $fileName;
				$foto->caption = 'Foto '.$request->name;
				$foto->description = 'Foto '.$request->name;
				$foto->save();
				$data->ms_media_id = $foto->id;
			}

			$data->save();
		}

		//redirect
		$data = $this->data;
		$page = 'testimoni/create';
		Session::flash('success','Testimoni terkirim');

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function contact()
	{
		$data = $this->data;
		$page = 'contactus/create';

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function doContact(ContactUsRequest $request)
	{
		$content = "From : ".$request->name."\n";
		$content .= "Email : ".$request->email."\n";
		$content .= "No Hp : ".$request->nohp."\n";
		$content .= "Subject : ".$request->title."\n";
		$content .= "Message : \n\n".$request->message."";

		$contact = Contact::all()->first();
		$email = $contact->email;		
 		Mail::raw($content, function($message) use ($email)
		{
		    $message->to($email);
		    
		});

		//redirect
		$data = $this->data;
		$page = 'contactus/create';
		Session::flash('success','Pesan terkirim');
		//echo $content;

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function subscribe()
	{
		$data = $this->data;
		$page = 'subscribe/create';

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function doSubscribe(ContactUsRequest $request)
	{
		$content = "From : ".$request->name."\n";
		$content .= "Email : ".$request->email."\n";
		$content .= "No Hp : ".$request->nohp."\n";
		$content .= "Subject : <Subscribe> ".$request->title."\n";
		$content .= "Message : \n\n".$request->message."";

		$contact = Contact::all()->first();
		$email = $contact->email;		
 		Mail::raw($content, function($message) use ($email)
		{
		    $message->to($email);
		   
		});

		//redirect
		$data = $this->data;
		$page = 'subscribe/create';
		Session::flash('success','Subscribe terkirim');
		//echo $content;

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function products($id=null,$idC=null)
	{
		$data = $this->data;
		$page = 'products';
		$data['custom'] = $idC;
		$data['content'] = Products::where('ms_categories_id', 2);

		$data['products'] = $data['content']->paginate(6);
		$data['products']->setPath("");

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function soal_soal($page = 'soal-soal'){
		$data = $this->data;
		$data['content'] = DB::table('ms_ujian')
		->orderBy('id', 'desc')
		->paginate(7);
		//$data['content']->setPath("kategori=".$id);

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function category($id=null,$page = 'category')
	{
		$data = $this->data;
		$category = Category::with('subcategory','subcategory.subcategory','subcategory.subcategory')->find($id);
		$data['context'] = 'Kategori '.ucfirst($category->title);
		$data['tags'] = \App\Tag::all();
		$data['featured_portfolio'] = \App\Portfolio::where('featured','1')->whereHas('detailPortfolio', function($q)
		{
		    $q->where('type', '1');

		})->first();
		// dd($data['latest_posts']);
		$arr_Category = array();
		array_push($arr_Category, $id);
		if($category->subcategory()->count() > 0)
		{
			foreach ($category->subcategory as $row) {
				array_push($arr_Category, $row->id);
				if($row->subcategory()->count() > 0){
					foreach ($row->subcategory as $val) {
						array_push($arr_Category, $val->id);
					}
				}
			}
		}
		// $data['content'] = Category::with('post','post.mediaPost')->whereIn('id',$arr_Category)->paginate(6);
		$data['content'] = Post::with('mediaPost','user')->whereHas('category',function($q) use ($arr_Category){
			$q->whereIn('ms_categories.id',$arr_Category);
		})->orderBy('created_at','desc')
		->paginate(7);
		$data['content']->setPath("kategori=".$id);
		return view('theme.'.$this->theme.".".$page,compact('data'));

	}
	public function rss($id=null,$page='products')
	{
		if(empty($id))
			exit();
		$data = $this->data;

		$data_t = Rss::find($id);
		$data['context'] = 'RSS '.ucfirst($data_t->title);
		$rss = FeedReader::read($data_t->link);
		$data['content'] = $rss->get_items(0,10);
	  
	    return view('theme.'.$this->theme.".".$page,compact('data'));
	    // echo $row->data["child"][""]["description"][0]["data"];
	}
	public function post($id=null,$page = 'single-page')
	{
		$data = $this->data;
		$data['content'] = Post::with('comment','mediaPost','portfolio','portfolio.detailPortfolio','portfolio.detailPortfolio.media')->find($id);
		if (!empty($data['content'])){
			if($data['content']->ms_portfolio_id!='0' || $data['content']->ms_portfolio_id!='' )

				$data['portfolio'] = Portfolio::with('detailPortfolio','detailPortfolio.media')->where('id','<>',$id)->whereHas('detailPortfolio', function($q)
				{
				    $q->where('type', '1');

				})->get();
			else
				$data['portfolio'] = Portfolio::with('detailPortfolio','detailPortfolio.media')->whereHas('detailPortfolio', function($q)
				{
				    $q->where('type', '1');

				})->get();
			return view('theme.'.$this->theme.".".$page,compact('data'));
		} 
		return redirect()->route('notfound');
	}
	public function portfolio($id=null,$idC=null)
	{
		$data = $this->data;
		$page = '';
		$data['custom'] = $idC;
		$data['content'] = Portfolio::with('subportfolio','detailPortfolio','detailPortfolio.media')->find($id);

		// dd($data['content']->subportfolio()->count());
		if($data['content']->subportfolio->count() > 0)
		{
			$page = 'single-portfolio';
		}else{
			$page = 'single-portfolio';
		}
		$data['portfolio'] = $data['content']->detailPortfolio()->paginate(12);
		$data['portfolio']->setPath("gemarsehati=".$id);

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function portfolioType($id=null,$idC=null)
	{
		$data = $this->data;
		$page = 'type-portfolio';
		$data['custom'] = $idC;
		$data['category'] = PortfolioCategory::where('id', $id)->first();
		$data['content'] = Portfolio::where('ms_categories_id', $id)->with('subportfolio','detailPortfolio','detailPortfolio.media');
		 
		$data['group-portfolio'] = $data['content']->paginate(12);
		$data['group-portfolio']->setPath("type=".$id);
	 

		return view('theme.'.$this->theme.".".$page,compact('data'));
	}

	public function search(Request $request,$page='cari')
	{
		$data = $this->data;

		$keyword = $request->input('keyword');
		$data['context'] = 'Hasil Pencarian \''.$keyword.'\'';
		$data['keyword'] = $keyword;
		$data['content'] = \App\Post::where(function($q) use($keyword){
			$q->where('title','like','%'.$keyword.'%')->where('ms_status_posts_id',2);
		})->orWhere(function($q) use($keyword){
			$q->where('description','like','%'.$keyword.'%')->where('ms_status_posts_id',2);
		})->paginate(10);
		return view('theme.'.$this->theme.".".$page,compact('data'));
	}
	public function comment(CommentRequest $request ){
		$data = Comment::create($request->all());
		$data->enabled = '0';
		$data->save();
		Session::flash('success','Comment sent');
		return redirect()->back();

	}
	
	public function getMember($kode)
	{
		$data = $this->data;
		$data['content'] = \App\Club::with('typeClub')->whereHas('typeClub', function($q) use ($kode){
				$q->where('code', $kode);
		})->orderBy('created_at','desc')->paginate(15);
		$data['content']->setPath($kode);
		return view('theme.'.$this->theme.".club",compact('data'));
	}
	
	public function getOngkir()
	{
		$data['city'] = \App\City::all();
		$data['province'] = \App\Province::all();
		$data['featured_portfolio'] = \App\Portfolio::where('featured','1')->whereHas('detailPortfolio', function($q)
		{
		    $q->where('type', '1');

		})->first();
		return view('theme.'.$this->theme.".ongkir",compact('data'));
	}
	public function postOngkir(Request $request)
	{
		$source = $request->input('source');
		$destination = $request->input('destination');
		$weight = intval($request->input('weight'));
		$courier = $request->input('courier');
		$source_obj = \App\City::find($source);
		$destination_obj = \App\City::find($destination);

		$data['results'] = $this->calculateShipping($source, $destination, $weight, $courier);
		$data['context'] = "Hasil pengecekan dari <b>".$source_obj->name."</b> ke <b>".$destination_obj->name." </b> @<b>".$weight."</b> gram";
		$data['isian_source'] = $source;
		$data['isian_destination'] = $destination;
		$data['isian_courier'] = $courier;
		$data['isian_weight'] = $weight;

		$data['city'] = \App\City::all();
		$data['province'] = \App\Province::all();
		$data['featured_portfolio'] = \App\Portfolio::where('featured','1')->whereHas('detailPortfolio', function($q)
		{
		    $q->where('type', '1');

		})->first();
		return view('theme.'.$this->theme.".ongkir",compact('data'));
	}
	function calculateShipping($source, $destination, $weight , $courier_id=null, $service_courier = null){
		$ongkir = new Ongkir($this->api_shipping);
		 
		if(empty($courier_id))
		{
 			$cost = $ongkir->getCost($source,$destination,intval($weight),'pos');
			$min_obj = array();
			if(empty($cost))
			{
 			}
			
			foreach ($cost->{'rajaongkir'}->{'results'} as $row) {
				foreach ($row->{'costs'} as $val) {
					if(empty($val->cost))
							continue;
					$temp = array();

						array_push($temp, $val->service);
						array_push($temp, intval($val->cost[0]->value));
						array_push($temp, intval($val->cost[0]->etd));
						array_push($temp, $val->description);
					array_push($min_obj, $temp);
				}
			}
	 
			return $min_obj;
		}
		else if(!empty($courier_id))
		{ 
			$cost = $ongkir->getCost($source,$destination,intval($weight),$courier_id);
			$min_obj = array();
			$tmp_t = array();
			if(!empty($cost)){
			
				foreach ($cost->{'rajaongkir'}->{'results'} as $row) {
					foreach ($row->{'costs'} as $val) {
						if(empty($val->cost))
							continue;
						
						$temp = array();

							array_push($temp, $val->service);
							array_push($temp, intval($val->cost[0]->value));
							array_push($temp, intval($val->cost[0]->etd));
							array_push($temp, $val->description);
						array_push($min_obj, $temp);
					}
				}
		 
				return $min_obj;
			}else{
				 
				$cost = $ongkir->getCost($source,$destination,$weight,$courier_id);
				$min_obj = array();
				if(!empty($cost)){
					foreach ($cost->{'rajaongkir'}->{'results'} as $row) {
						foreach ($row->{'costs'} as $val) {
							if(empty($val->cost))
							continue;
							$temp = array();

								array_push($temp, $val->service);
								array_push($temp, intval($val->cost[0]->value));
								array_push($temp, intval($val->cost[0]->etd));
								array_push($temp, $val->description);
							array_push($min_obj, $temp);
						}
					}
					// Hasil =
					// array:3 [▼
					//   0 => "Pos Kilat Khusus"
					//   1 => 41000
					//   2 => 4
					// ]
					// $order->service_courier = $min_obj[0];
					// $order->shipping_price = $min_obj[1];
					// $order->ms_courier_id = $courier->id;
					// $order->save();
					return $min_obj;
				}

			}
 			return null;
		}
	}

	public function track(){
		$data = $this->data;
		return view('theme.'.$this->theme.".tracking",compact('data'));
	}
	public function postTrack(Request $request){
		$panah = $request->input('resi');
		$data = $this->data;
		$data['result'] = $this->ApiOngkir($panah);
		return view('theme.'.$this->theme.".tracking",compact('data'));
	}

	public function ApiOngkir($td){
		$small = new Ongkir($this->api_shipping);
		$luffy = $small->mainmain($td);
		$melina = array();
		if(!empty($luffy)){
			$melina['status_code'] = $luffy->{'rajaongkir'}->{'status'}->{'code'};
			if ($melina['status_code'] == 200) {
				$melina['delivered'] = $luffy->{'rajaongkir'}->{'result'}->{'delivered'};
				$melina['courier_name'] = $luffy->{'rajaongkir'}->{'result'}->{'summary'}->{'courier_name'};
				$melina['service_code'] = $luffy->{'rajaongkir'}->{'result'}->{'summary'}->{'service_code'};
				$melina['waybill_number'] = $luffy->{'rajaongkir'}->{'result'}->{'summary'}->{'waybill_number'};
				$melina['status'] = $luffy->{'rajaongkir'}->{'result'}->{'summary'}->{'status'};

				$melina['waybill_time'] = $luffy->{'rajaongkir'}->{'result'}->{'details'}->{'waybill_date'}." ".$luffy->{'rajaongkir'}->{'result'}->{'details'}->{'waybill_time'};
				$melina['shipper_name'] = $luffy->{'rajaongkir'}->{'result'}->{'summary'}->{'shipper_name'};
				$melina['shipper_address'] = $luffy->{'rajaongkir'}->{'result'}->{'details'}->{'shipper_address1'}." ".$luffy->{'rajaongkir'}->{'result'}->{'details'}->{'shipper_address2'}." ".$luffy->{'rajaongkir'}->{'result'}->{'details'}->{'shipper_address3'};

				$melina['receiver_name'] = $luffy->{'rajaongkir'}->{'result'}->{'details'}->{'receiver_name'};
				$melina['receiver_address'] = $luffy->{'rajaongkir'}->{'result'}->{'details'}->{'receiver_city'}." ".$luffy->{'rajaongkir'}->{'result'}->{'details'}->{'receiver_address2'}." ".$luffy->{'rajaongkir'}->{'result'}->{'details'}->{'receiver_address3'};
				$tmp = array();
				foreach ($luffy->{'rajaongkir'}->{'result'}->{'manifest'} as $row) {
					
					$temp=array(	"description" 		=> $row->manifest_description, 
											"time" 	=> $row->manifest_date." ".$row->manifest_time,
											"city" 	=> $row->city_name
											);
					array_push($tmp, $temp);
				}
				$melina['manifest'] = $tmp;
			}
		}
		return $melina;
	}

	function internasional(){
		 
		$arrayName = array();
		array_push($arrayName, "internationalDestination");
		array_push($arrayName, "");
		$small = new Ongkir($this->api_shipping);
		$luffy = $small->luarnegeri($arrayName);
		
		foreach($luffy->{'rajaongkir'}->{'results'} as $row){
			$data = new \App\Country;
			$data->country_id = $row->country_id;
			$data->name = $row->country_name;
			$data->save();
		}
		dd($luffy);
		
	}

	public function getOngkirInt()
	{
		$data['city'] = \App\CitySupport::all();
		$data['province'] = \App\Province::all();
		$data['country'] = \App\Country::all();
		$data['featured_portfolio'] = \App\Portfolio::where('featured','1')->whereHas('detailPortfolio', function($q)
		{
		    $q->where('type', '1');

		})->first();
		return view('theme.'.$this->theme.".ongkir-international",compact('data'));
	}
	public function postOngkirInt(Request $request)
	{
		$source = $request->input('source');
		$destination = $request->input('destination');
		$weight = intval($request->input('weight'));
		$courier = 'pos';
		$source_obj = \App\CitySupport::where('city_id',$source)->first();
		$destination_obj = \App\Country::where('country_id',$destination)->first();


		$temp = $this->calculateShippingInt($source, $destination, $weight, $courier);
		if(!empty($temp)){
			$data['results']['cost'] = $temp[0];
			$data['results']['currency'] = $temp[1];
		}else{
			$data['results'] = null;
		}
		
		$data['context'] = "Hasil pengecekan dari <b>".$source_obj->name."</b> ke <b>".$destination_obj->name." </b> @<b>".$weight."</b> gram<br><br> <b>POS INDONESIA</b>";
		$data['isian_source'] = $source;
		$data['isian_destination'] = $destination;
		$data['isian_courier'] = $courier;
		$data['isian_weight'] = $weight;

		$data['city'] = \App\CitySupport::all();
		$data['country'] = \App\Country::all();
		$data['featured_portfolio'] = \App\Portfolio::where('featured','1')->whereHas('detailPortfolio', function($q)
		{
		    $q->where('type', '1');

		})->first();
		return view('theme.'.$this->theme.".ongkir-international",compact('data'));
	}
	function calculateShippingInt($source, $destination, $weight , $courier_id='pos'){
		$ongkir = new Ongkir($this->api_shipping);
		if(!empty($courier_id))
		{
			$cost = $ongkir->getCostInt($source,$destination,intval($weight),$courier_id);
			$data = array();
			$min_obj = array();
			$tmp_t = array();
			if(!empty($cost)){
			
				foreach ($cost->{'rajaongkir'}->{'results'} as $row) {
					foreach ($row->{'costs'} as $val) {
						if(empty($val->cost))
							continue;
						
						$temp = array();

							array_push($temp, $val->service);
							array_push($temp, $val->cost);
						array_push($min_obj, $temp);
					}
				}

				$temp = $cost->{'rajaongkir'}->{'currency'}->{'value'};
				// Hasil =
				// array:3 [▼
				//   0 => "Pos Kilat Khusus"
				//   1 => 41000
				//   2 => 4
				// ]
				array_push($data, $min_obj);
				array_push($data, $temp);
				return $data;
			}
			return null;
		}
	}

	public function ujian($code){
		$data = $this->data;
		$data['ujian'] = DB::table('ms_ujian')->where('code', $code)->first();

		if (count($data['ujian']) > 0){
			$data['soal'] = DB::table('ms_soal')->where('ms_ujian_id', $data['ujian']->id)->get();
			foreach ($data['soal'] as $soal) {
				if ($soal->ms_media_id != null || $soal->ms_media_id != 0)
					$soal->ms_media_id = Media::find($soal->ms_media_id);
			}
		}
		else
			$data['soal'] = null;
		$data['latest_posts'] = $this->data['latest_posts'];
		return view('theme.'.$this->theme.'.ujian',compact('data'));
	}

	public function hasil(){
		$data = $this->data;
		$soal = DB::table('ms_soal')->where('ms_ujian_id', Input::get('id_ujian'))->get();
		$totalTrue = 0;
		$soalUser = Input::get('soal');
		foreach ($soal as $key) {
			if (isset($soalUser[$key->id])){
				if ($soalUser[$key->id] == $key->jawaban_benar)
					$totalTrue++;
			}
			/*if ($soal[$key->id] == $key->jawaban_benar){
				$totalTrue++;
			}*/
		}
		$data['totalTrue'] = $totalTrue;
		$data['totalQuestion'] = count($soal);
		$ujian = DB::table('ms_ujian')->where('id', Input::get('id_ujian'))->first();
		$data['title'] = $ujian->title;
		$data['latest_posts'] = $this->data['latest_posts'];
		//dd(Input::all());
		return view('theme.'.$this->theme.'.hasil',compact('data'));
	}

	public function adukan(){
		
		DB::table('ms_aduan')->insert([
				'nama_lengkap' => Input::get('nama_lengkap'),
				'email' => Input::get('email'),
				'hp' => Input::get('hp'),
				'ms_kategori_aduan_id' => Input::get('kategori'),
				'alamat' => Input::get('alamat'),
				'isi' => Input::get('isi'),
				'created_at' => new DateTime(),
				'updated_at' => new DateTime()
			]);
		return redirect('/');
	}

	public function notfound(){
		echo "<h1>404</h1><p>Data not found</p>";
	}

	public function statistic(){
		return view('theme.'.$this->theme.'.statistic',compact('data'));
	}

	public function cctvs(){
		$data = $this->data;
		$data['content'] = DB::table('ms_cctv')
							->join('ms_medias', 'ms_cctv.ms_media_id', '=', 'ms_medias.id')
							->orderBy('ms_cctv.id', 'desc')
							->select('ms_cctv.id', 'ms_medias.file', 'ms_cctv.description', 'ms_cctv.ms_media_id', 'ms_cctv.title', 'ms_cctv.created_at')
							->paginate(10);
		return view('theme.'.$this->theme.'.cctvs',compact('data'));
	}

	public function cctv($id){
		$data = $this->data;
		$data['content'] = DB::table('ms_cctv')->where('id', $id)->first();
		return view('theme.'.$this->theme.'.cctv',compact('data'));
	}

	public function sendMail(){
		Mail::send('theme.'.$this->theme.'.mail', $this->data, function ($message) {
		    $message->from('john@johndoe.com', 'John Doe');
		    $message->sender('john@johndoe1.com', 'John Doe 1');
		
		    $message->to('dmazter.mgm@gmail.com', 'dmazter');
		
		    $message->subject('Subject');
		
		    $message->priority(3);
		});
	}
}
