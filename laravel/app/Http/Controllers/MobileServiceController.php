<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 
*/

use App\Category;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\UserRequest;
use App\Portfolio;
use App\Post;
use App\Leaders;
use App\Rss;
use App\Slider;
use App\Theme;
use App\General;
use App\Download;
use App\Products;
use App\Contact;
use App\Gender;
use App\City;
use App\User;
use App\UserDetail;
use App\Media;
use App\Event;
use App\FilePdf;
use App\Notification;
use FeedReader;
use Illuminate\Http\Request;
use Session;
use App\ThirdParty\Ongkir;
use Image;
use Hash;
use Response;
use Mail;
use Auth;
use Input;
use Request as Req;
//use DeviceToken;

class MobileServiceController extends BasicController {

	function __construct() {
		$this->path = 'upload/media/';
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE"); 
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(SliderRequest $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(MitraRequest $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getVideo()
	{
		$portfolio = Portfolio::all();

		$arr = array();
		foreach ($portfolio as $row) 
		{
			foreach ($row->detailPortfolio as $row2)
			{				
				if ($row2->type != '2')
					continue;
				$arr[] = array(
					'judul' => "$row2->caption", 
					'date' => "$row->updated_at",
					'link' => $this->urlToEmbedYoutube($row2->link)
					);
			}
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getVideoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	private function urlToEmbedYoutube($url)
	{
		//preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
	    	
		$re = "/(?:\?v=|\?list=|be\/)(\w*)/";
		//$re="/be\/(.*?)\?list|v=(.*?)\&|v=(.*?)/";
		preg_match($re,$url,$matches);
		$id=$matches[1];
		//echo $id;
				echo " ";
		return "https://www.youtube.com/embed/".$id;
	}

	public function getPhoto()
	{
		$portfolio = Portfolio::all();

		$arr = array();
		foreach ($portfolio as $row) 
		{
			foreach ($row->detailPortfolio as $row2)
			{
				if ($row2->type != '1')
					continue;
				$arr[] = array(
					'id_album' => $row->id,
					'album' => $row->title,
					'link' => asset('upload/media/'.$row2->media->file)
					);

			}
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	public function getPdfUser()
	{
		$pdf = FilePdf::where('mime','pdf')->where('isumum','0')->get();

		$arr = array();
		foreach ($pdf as $row) 
		{
			$arr[] = array(
				'nama_pdf' => $row->file,
				'info_pdf' => $row->description,
				'link_pdf' => asset('upload/file/'.$row->file)
				);
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	public function getPdfUmum()
	{
		$pdf = FilePdf::where('mime','pdf')->where('isumum','1')->get();

		$arr = array();
		foreach ($pdf as $row) 
		{
			$arr[] = array(
				'nama_pdf' => $row->file,
				'info_pdf' => $row->description,
				'link_pdf' => asset('upload/file/'.$row->file)
				);
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	public function getPdfAll()
	{
		$pdf = FilePdf::all();

		$arr = array();
		foreach ($pdf as $row) 
		{
			$arr[] = array(
				'nama_pdf' => $row->file,
				'info_pdf' => $row->description,
				'link_pdf' => asset('upload/file/'.$row->file)
				);
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	public function getEvent()
	{
		$event = Event::orderBy('date', 'asc')->get();

		$arr = array();
		foreach ($event as $row) 
		{
			$arr[] = array(
				'id' => $row->id,
				'judul' => $row->name,
				'date' => date('Y-m-d', strtotime($row->date)),
				'deskripsi' => $row->description
				);
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	public function getMitra()
	{
		$mitra = User::where('ms_privilige_id', '2')->where('active', '1')->get();
		$arr = array();
		foreach($mitra as $row)
		{
			$arr[] = array(
				'id' => $row->id,
				'enagic_id' => $row->detail->enagic_id,
				'nama' => $row->name,
				'url' => '#',
				'nohp' => $row->detail->nohp,
				'notelp' => $row->detail->notelp,
				'alamat' =>	preg_replace('/[^A-Za-z0-9\-]/', ' ', $row->detail->address),
				'kota' =>	$row->detail->city->name,
				'facebook' =>	$row->detail->facebook,
				'email' =>	$row->email,
				'pin_bb' => $row->detail->pin_bb,
				'sn_mesin' => $row->detail->sn_mesin,
				'jenis_mesin' => $row->detail->jenis_mesin,
				'foto' => asset('upload/media/'.$row->detail->media->file),
				'koordinat' => $row->detail->latitude.",".$row->detail->longitude
				);
		}
		echo json_encode($arr);
	}

	public function getMitraId($id)
	{
		$row = User::where('ms_privilige_id', '2')->where('active', '1')->where('id', $id)->get()->first();
		//echo $row;
		$arr = array();

		$arr = array(
			'id' => $row->id,
			'enagic_id' => $row->detail->enagic_id,
			'nama' => $row->name,
			'url' => '#',
			'nohp' => $row->detail->nohp,
			'notelp' => $row->detail->notelp,
			'alamat' =>	preg_replace('/[^A-Za-z0-9\-]/', ' ', $row->detail->address),
			'kota' => $row->detail->city->name,
			'facebook' =>	$row->detail->facebook,
			'email' =>	$row->email,
			'pin_bb' => $row->detail->pin_bb,
			'sn_mesin' => $row->detail->sn_mesin,
			'jenis_mesin' => $row->detail->jenis_mesin,
			'foto' => asset('upload/media/'.$row->detail->media->file),
			'koordinat' => $row->detail->latitude.",".$row->detail->longitude
			);

		echo json_encode($arr);
	}

	public function getTestimoni()
	{
		$post = Post::with('mediaPost')->whereHas('category', function($q)
		{
		    $q->where('ms_category_id', '7'); // category 7 = testimoni
		})->get();

		$arr = array();
		foreach ($post as $row) 
		{
			if($row->mediaPost == null || $row->user == null)
				continue;
			$arr[] = array(
				'id' => $row->id,
				'id_user' => $row->user->id,
				'nama_user' => $row->user->name,
				'lokasi' => $row->user->detail->city->name,
				'date' => date('Y-m-d', strtotime($row->created_at)),
				'content' => strip_tags($row->description),
				'gambar' => asset('upload/media/'.$row->mediaPost->file)
				);
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	public function getarticles()
	{
		$post = Post::with('category')->whereHas('category', function($q)
		{
		    $q->where('ms_category_id', '2'); // category 2 = event
		})->get();

		$arr = array();
		foreach ($post as $row) 
		{
			$arr[] = array(
				'id' => $row->id,
				'user' => $row->user->name,
				'judul' => $row->title,
				'date' => date('Y-m-d', strtotime($row->created_at)),
				'foto' => asset('upload/media/'.$row->mediaPost->file),
				'content' => strip_tags($row->description)
				);
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	public function aboutUs()
	{
		$post = Post::with('category')->whereHas('category', function($q)
		{
		    $q->where('ms_category_id', '3'); // category 3 = company
		})->get();

		$arr = array();
		foreach ($post as $row) 
		{
			if($row->id != 56 && $row->name != "Siapa itu Gemarsehati ?")
				continue;
			$arr [] = array(
				'id' => $row->id,
				'user' => $row->user->name,
				'judul' => $row->title,
				'date' => date('Y-m-d', strtotime($row->created_at)),
				'foto' => asset('upload/media/'.$row->mediaPost->file),
				'content' => strip_tags($row->description)
				);
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	//convert base64 string to image file
	function convertImage($s, $name){
		$defPath = $this->path;
	    //$data = explode(',', $s);
	    $decoded = base64_decode($s);
	    file_put_contents($defPath.$name, $decoded);
	    //return $defPath.$name;
	}

	public function register(Request $request)
	{
		$rstr = $this->get_string_between($request, '{', '}');
		echo "{".$rstr."}"; // input
		$request = json_decode("{".$rstr."}");
		echo $request->email; // input

		// insert start
		if(User::where('email', $request->email)->get()->first() == null)
		{
			//handle upload foto
			$foto = New Media;
			$fileName = '';
			$file = $request->file;
			
		    //$file = $request->file('file');
			$extension = "pdf";//$file->getClientOriginalExtension();
			$fileName = 'img_'.date('Ymd_Hsi').'.'.$extension;
			//$file->move($this->path,$fileName);
			$foto->mime = "images/jpeg";//$file->getClientMimeType();
			//$img = Image::make($this->path.$fileName);
			//$img->save($this->path.$fileName, 60);
			$this->convertImage($file, $fileName);
			$foto->file = $fileName;
			$foto->caption = 'Foto '.$request->name;
			$foto->description = 'Foto '.$request->name;
			$foto->save();
			//end handle upload foto

			$data = User::create($request->all());
			$data->ms_privilige_id = 2;
			$data->password = \Hash::make($data->password);
			$userdetail = UserDetail::create(array(
				'panggilan' => $request->panggilan,
				'notelp' => $request->notelp,
				'nohp' => $request->nohp,
				'address' => $request->address,
				'kodepos' => $request->kodepos,
				'facebook' => $request->facebook,
				'twitter' => $request->twitter,
				'latitude' => $request->latitude,
				'longitude' => $request->longitude,
				'pin_bb' => $request->pin_bb,
				'ms_media_id' => $foto->id,
				'ms_gender_id' => $request->ms_gender_id,
				'ms_city_id' => $request->ms_city_id
				));
			$data->tr_user_detail_id = $userdetail->id;
			$userdetail->ms_media_id = $foto->id;

			$userdetail->save();
			$data->save();
			$json = array(
				'status' => '200',
				'message' => 'registerSuccess',
				'data' => 'Register Berhasil!'
				);
			echo json_encode($json);
		}
		else
		{
			$json = array(
				'status' => '200',
				'message' => 'registerFailed',
				'data' => 'Email sudah dipakai!'
				);
			echo json_encode($json);
		}
		// insert ends
	}

	public function login(Request $request)
	{
		$rstr = $this->get_string_between($request, '{', '}');
		$request = json_decode("{".$rstr."}");

		$email = $request->email;
		$password = $request->password;

		$user = User::where('email', $email)->get()->first();
		if($user != null)
		{
			$hashedPassword = $user->password;
			if (Hash::check($password, $hashedPassword))
			{
				$json = array(
					'status' => '200',
					'message' => 'loginSuccess',
					'data' => 'Anda berhasil login!',
					'id' => $user->id
					);
				echo json_encode($json);
			}
			else
			{
				$json = array(
					'status' => '200',
					'message' => 'loginFailed',
					'data' => 'Email atau password anda salah!'
					);
				echo json_encode($json);
			}
		}
		else
		{
			$json = array(
				'status' => '200',
				'message' => 'loginFailed',
				'data' => 'Email atau password anda salah!'
				);
			echo json_encode($json);
		}
	}

	public function getCity()
	{
		$city = City::all();

		$arr = array();
		foreach ($city as $row) 
		{
			$arr[] = array(
				'id' => $row->id,
				'name' => $row->name
				);
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	public function getGender()
	{
		$gender = Gender::all();

		$arr = array();
		foreach ($gender as $row) 
		{
			$arr[] = array(
				'id' => $row->id,
				'name' => $row->name
				);
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	public function contactUs(Request $request)
	{		
		// echo $request;
		$rstr = $this->get_string_between($request, '{', '}');
		//echo "{".$rstr."}";
		$request = json_decode("{".$rstr."}");
		// echo $request->email;
		// echo "yang diterima servis adalah = ".$request->email;//."<br>";
		$content = "From : ".$request->name."\n";
		$content .= "Email : ".$request->email."\n";
		$content .= "No Hp : ".$request->nohp."\n";
		$content .= "Subject : ".$request->title."\n";
		$content .= "Message : \n\n".$request->message."";
		$contact = Contact::all()->first();
		$email = $contact->email;		
		//echo $email;
		Mail::raw($content, function($message) use ($email)
		{
		    $message->to($email);
		    //$message->to('pentol231094@gmail.com');
		    //$message->to('anton.ferryanto@gmail.com');
		    //$message->to('support@gemarsehati.com');
		});


		$json = array(
			'status' => '200',
			'message' => 'contactUsSuccess',
			'data' => 'Pesan anda sudah terkirim!'
			);
		echo json_encode($json);
	}

	public function pushNot()
	{
	    $deviceToken = 'AIzaSyAyRmR8sCC5j3V-MZA8y866HTkyxTB92S4';//$token->device_token;
	    \PushNotification::app('gemarsehati-android')
	        ->to($deviceToken)
	        ->send('Hello World, i`m a push message');
	}

	public function resetPass(Request $request)
	{
		// echo $request;
		$rstr = $this->get_string_between($request, '{', '}');
		// echo "{".$rstr."}";
		$request = json_decode("{".$rstr."}");
		// echo $request->email;
		// echo "yang diterima servis adalah = ".$request->foo;//."<br>";
		$user = User::where('email', $request->email)->get()->first();
		if($user != null)
		{
			//echo $user->name;
			$newPass = $this->generateRandomString(6);
			$content = "Password baru anda adalah ".$newPass;
			$email = $request->email;
			Mail::raw($content, function($message) use ($email)
			{
			    $message->to($email);
			});
			$user->password = \Hash::make($newPass);
			$user->save();

			$json = array(
				'status' => '200',
				'message' => 'resetPassSuccess',
				'data' => 'Password baru telah dikirim ke email anda'
				);
			echo json_encode($json);
		}
		else
		{
			$json = array(
				'status' => '200',
				'message' => 'resetPassFailed',
				'data' => 'Email tidak terdaftar'
				);
			echo json_encode($json);
		}
	}

	function generateRandomString($length = 10) 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function get_string_between($string, $start, $end)
	{
	    $string = ' ' . $string;
	    $ini = strpos($string, $start);
	    if ($ini == 0) return '';
	    $ini += strlen($start);
	    $len = strpos($string, $end, $ini) - $ini;
	    return substr($string, $ini, $len);
	}

	public function getTechnology()
	{
		$post = Post::with('category')->whereHas('category', function($q)
		{
		    $q->where('ms_category_id', '14'); // category 14 = edukasi
		})->get();

		$arr = array();
		foreach ($post as $row) 
		{
			$arr[] = array(
				'id' => $row->id,
				'user' => $row->user->name,
				'judul' => $row->title,
				'date' => date('Y-m-d', strtotime($row->created_at)),
				'foto' => asset('upload/media/'.$row->mediaPost->file),
				'content' => strip_tags($row->description)
				);
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}
	
	public function getMitraCity($id)
	{
		$mitra = User::where('ms_privilige_id', '2')->where('active', '1')->get();
		$arr = array();
		
		foreach($mitra as $row)
		{
			if($row->detail->ms_city_id != $id)
				continue;
			$arr[] = array(
				'id' => $row->id,
				'enagic_id' => $row->detail->enagic_id,
				'nama' => $row->name,
				'url' => '#',
				'nohp' => $row->detail->nohp,
				'notelp' => $row->detail->notelp,
				'alamat' =>	preg_replace('/[^A-Za-z0-9\-]/', ' ', $row->detail->address),
				'kota' =>	$row->detail->city->name,
				'facebook' =>	$row->detail->facebook,
				'email' =>	$row->email,
				'pin_bb' => $row->detail->pin_bb,
				'sn_mesin' => $row->detail->sn_mesin,
				'jenis_mesin' => $row->detail->jenis_mesin,
				'foto' => asset('upload/media/'.$row->detail->media->file),
				'koordinat' => $row->detail->latitude.",".$row->detail->longitude
				);
		}
		echo json_encode($arr);
	}
	
	public function getLevelUk()
	{	
		
		$products= Products::where('ms_categories_id', 2)->get();
		//$products = Products::all();
		$arr = array();
		
		foreach($products as $row)
		{
			//if($row->ms_categories_id != '2') continue;
			$arr[] = array(
				'id' => $row->id,
				'nama' => $row->name,
				'deskripsi' =>	preg_replace('/[^A-Za-z0-9\-]/', ' ', $row->description),
				'categori' => $row->ms_categories_id,
				'foto' => asset('upload/media/'.$row->media->file)
				);
		}
		echo json_encode($arr);
	}
	public function getTestimoniKesehatan()
	{		
		$post = Post::with('category')->whereHas('category', function($q)
		{
		    $q->where('ms_category_id', '13'); // category 13 = testimoni kesehatan
		})->get();

		$arr = array();
		foreach ($post as $row) 
		{
			if($row->user == null)
				continue;
			$arr[] = array(
				'id' => $row->id,
				'id_user' => $row->user->id,
				'nama_user' => $row->user->name,
				'lokasi' => 'Surabaya',//$row->user->detail->city->name,
				'date' => date('Y-m-d', strtotime($row->created_at)),
				'content' => strip_tags($row->description),
				'gambar' => asset('upload/media/'.$row->mediaPost->file)
				);
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	public function getTestimoniBisnis()
	{		
		$post = Post::with('category')->whereHas('category', function($q)
		{
		    $q->where('ms_category_id', '12'); // category 12 = testimoni bisnis
		})->get();

		$arr = array();
		foreach ($post as $row) 
		{
			if($row->user == null)
				continue;
			$arr[] = array(
				'id' => $row->id,
				'id_user' => $row->user->id,
				'nama_user' => $row->user->name,
				'lokasi' => 'Surabaya',//$row->user->detail->city->name,
				'date' => date('Y-m-d', strtotime($row->created_at)),
				'content' => strip_tags($row->description),
				'gambar' => asset('upload/media/'.$row->mediaPost->file)
				);
		}
		// $json = array();
		// $json['status'] = "200";
		// $json['message'] = "getPhotoSuccess";
		// $json['data'] = $arr;
		echo json_encode($arr);
	}

	public function getCredit()
	{
		$general = General::find(1);
		$contact = Contact::where('featured','1')->first();

		$arr[] = array(
			'about' => $general->about_community,
			'address' => $contact->address,
			'logo' => asset('upload/media/'.$general->file_credit)
			);
		echo json_encode($arr);
	}

	public function getPhotoAlbum()
	{
		$album = Portfolio::where('ms_categories_id', 1)->get();

		$arr = array();
		foreach ($album as $row) 
		{
			$arr[] = array(
				'id' => $row->id,
				'nama' => $row->title
				);
		}
		echo json_encode($arr);
	}

	public function getNotification()
	{
		$notification = Notification::orderBy('created_at','desc')->get();

		$arr = array();
		foreach ($notification as $row) 
		{
			$arr[] = array(
				'id' => $row->id,
				'title' => $row->title,
				'description' => $row->description,
				'date' => $row->created_at
				);
		}
		echo json_encode($arr);
	}
}
