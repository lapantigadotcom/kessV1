<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Category;
use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\MenuDetailRequest;
use App\MenuDetail;
use App\MenuType;
use App\Portfolio;
use App\PortfolioCategory;
use App\Post;
use App\Rss;
use App\Products;
use App\User;
use Illuminate\Http\Request;
use Session;
use Input;

class MenuDetailController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(MenuDetailRequest $request)
	{
		$data = MenuDetail::create($request->all());
		$col = MenuDetail::where('ms_menu_id', $data->ms_menu_id)->where('parent_id',$data->parent_id)->get();
		$data->order = ($col->count())+1;
		$data->save();
		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));

		return redirect()->route('vrs-admin.menu.show',[$data->ms_menu_id]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = MenuDetail::with('menu','menu.menuDetail')->find($id);
		$data['menu_type'] = MenuType::all();
		switch ($data['content']->ms_menu_type_id) {
			case '1':
				$data['detail'] = Category::all();
				break;
			case '2':
				$data['detail'] = Post::all();
				break;
			case '3':
				$data['detail'] = Portfolio::all();
				break;
			case '4':
				$data['detail'] = Rss::all();
				break;
			case '5':
				$data['detail'] = "this is link type";
				break;
			case '6':
				break;
			case '7':
				break;
			case '8':
				break;
			case '9':
				$data['detail'] = PortfolioCategory::all();
				break;
			
			default:
				# code...
				break;
		}
		return view('page.menu.detail-edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(MenuDetailRequest $request,$id)
	{
		$data = MenuDetail::find($id);
		$oldParent = $data->parent_id;
		$data->parent_id = $request->input('parent_id');
		$data->ms_menu_type_id = $request->input('ms_menu_type_id');
		$data->custom = $request->input('custom');
		$data->title = $request->input('title');
		$data->code = $request->input('code');
		$data->save();
		$order = $request->input('order');
		if($oldParent != $request->parent_id || $data->order != $order)
		{
			$col = MenuDetail::with('parent', 'parent.submenu')->find($id);
			if($col->parent()->count() > 0)
			{
				foreach ($col->parent->submenu as $row) {
					if($row->order > $order)
					{
						$row->order = $row->order + 1;
						$row->save();
					}
				}
			}
			
			$col->order = intval($order) + 1;
			$col->save();
		}
		

		$log = array();
		$log['action'] = 'update';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));
		return redirect()->route('vrs-admin.menu.show',$data->ms_menu_id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = MenuDetail::find($id);
		$id = $data->ms_menu_id;
		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.menu.show',$id);
	}

	public function getjson()
	{
		$id = Input::get('parent_id');
		$data = MenuDetail::with('submenu')->find($id);
		return response()->json($data->submenu->toArray());
	}

}
