<?php namespace App\Http\Controllers;
use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\SoalRequest;
use Illuminate\Http\Request;
use App\Soal;
use Session;
use DB;
use Input;
class SoalController extends BasicController {
	public function index(){
		$data['content'] = DB::table('ms_soal')->orderBy('id', 'desc')->get();
		$data['ujian'] = DB::table('ms_ujian')->get();
		return view('page.soal.index',compact('data'));
	}
	public function store(SoalRequest $request)
	{
		//$data = soal::create($request->all());
		$data = DB::table('ms_soal')->insert([
				'ms_ujian_id' => $request->ujian,
				'pertanyaan' => $request->pertanyaan,
				'jawaban_a' => $request->jawaban_a,
				'jawaban_b' => $request->jawaban_b,
				'jawaban_c' => $request->jawaban_c,
				'jawaban_d' => $request->jawaban_d,
				'jawaban_benar' => $request->jawaban_benar,
				'ms_media_id' => $request->ms_media_id
			]);
		/*$log = array();
		$log['action'] = 'insert';
		$log['name'] = $request->input('title');
		$data->logs()->save($this->log($log));*/

		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.soal.index');
	}

	public function edit($id)
	{
		$data['ujian'] = DB::table('ms_ujian')->get();
		$data['content'] = DB::table('ms_soal')->where('id', $id)->first();
		return view('page.soal.edit',compact('data'));
	}

	public function Update(SoalRequest $request, $id){
		//dd(Input::all());
		DB::table('ms_soal')
			->where('id', $id)
			->update([
				'ms_ujian_id' => $request->ujian,
				'pertanyaan' => $request->pertanyaan,
				'jawaban_a' => $request->jawaban_a,
				'jawaban_b' => $request->jawaban_b,
				'jawaban_c' => $request->jawaban_c,
				'jawaban_d' => $request->jawaban_d,
				'jawaban_benar' => $request->jawaban_benar,
				'ms_media_id' => $request->ms_media_id
			]);

		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('vrs-admin.soal.index');
	}

	public function Destroy($id){
		DB::table('ms_soal')->where('id', $id)->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.soal.index');
	}
}
