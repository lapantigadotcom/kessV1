<?php namespace App\Http\Controllers;
use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\UjianRequest;
use Illuminate\Http\Request;
use App\Theme;
use App\Ujian;
use Session;
use DB;
use Input;
use DateTime;

class UjianController extends BasicController {
	protected $theme;

	public function __construct($value=null)
	{
		$theme_t = Theme::where('active','1')->first();
		$this->theme = $theme_t->code;
	}
	public function index(){
		$data['content'] = DB::table('ms_ujian')->orderBy('id', 'desc')->get();
		return view('page.ujian.index',compact('data'));
	}
	private function convertToCode($string){
		$string = preg_replace('/[^A-Za-z0-9\-]/', ' ', $string);
		$string = str_replace(" ", "-", $string);
		//$time = strtotime(date());
		$stringtime = new DateTime();
		//echo $stringtime->getTimestamp();
		return $stringtime->getTimestamp()."-".$string;
	}
	public function store(UjianRequest $request)
	{
		//$data = Ujian::create($request->all());
		
		$data = DB::table('ms_ujian')->insert([
				'title' => $request->title,
				'description' => $request->description,
				'ms_media_id' => $request->ms_media_id,
				'code' => $this->convertToCode($request->title)
			]);
		
		/*$log = array();
		$log['action'] = 'insert';
		$log['name'] = $request->input('title');
		$data->logs()->save($this->log($log));*/

		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.ujian.index');
	}

	public function edit($id)
	{
		$data['content'] = DB::table('ms_ujian')->where('id', $id)->first();
		return view('page.ujian.edit',compact('data'));
	}

	public function Update(UjianRequest $request, $id){
		
		DB::table('ms_ujian')
			->where('id', $id)
			->update(['title' => $request->title, 
					'description' => $request->description,
					'ms_media_id' => $request->ms_media_id
					]);
		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('vrs-admin.ujian.index');
	}

	public function Destroy($id){
		DB::table('ms_ujian')->where('id', $id)->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.ujian.index');
	}

	public function ujian($code){
		$data['ujian'] = DB::table('ms_ujian')->where('code', $code)->first();

		if (count($data['ujian']) > 0)
			$data['soal'] = DB::table('ms_soal')->where('ms_ujian_id', $data['ujian']->id)->get();
		else 
			$data['soal'] = null;
		//print_r($data['ujian']);
		return view('theme.'.$this->theme.'.ujian',compact('data'));
	}
}
