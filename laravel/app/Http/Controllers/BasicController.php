<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Log;
use Illuminate\Http\Request;
use Auth;

class BasicController extends Controller {

	protected $modelName;

	public function __construct() {
		
	}
	public function log($data = null){
		$log = new Log;
		$log->ms_user_id = Auth::id();
		$log->action = $data['action'];
		$log->name = $data['name'];
		$log->date = date('Y-m-d H:i:s');
		$data = Log::all();
		if($data->count() > 49)
		{
			$lebih = $data->count()-49;
			$hapus = Log::orderBy('id','desc')->get()->take($lebih);
			foreach ($hapus as $row) {
				$row->delete();
			}

		}
		return $log;
	}
}
