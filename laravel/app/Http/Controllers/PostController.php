<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Category;
use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\PostRequest;
use App\MenuDetail;
use App\Portfolio;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class PostController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->fillMeta();
		$data['content'] = \App\Post::with('user','category','tag')->orderBy('updated_at','desc')->get();
		return view('page.post.index')->with('data',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function create()
	{
		$data['categories'] = Category::all();
		$data['tags'] = Tag::all();
		$data['portfolio'] = Portfolio::all();

		return view('page.post.create',compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(PostRequest $request)
	{
		$data=Post::create($request->all());

		$postTitle = preg_replace("/[^A-Za-z0-9 ]/", '-', $data->title);
		$postTitle = str_replace(" ", "-", $postTitle);
		$postDate = $data->created_at;
		$data->seo = strtotime($data->created_at).'-'.$postTitle;

		$data->ms_user_id = Auth::id();
		$data->ms_status_posts_id = 2;
		if($request->meta_description == "")
			$data->meta_description = strip_tags($data->description);
		$data->save();
		if(is_array($request->categories) and count($request->categories) > 0)
			foreach ($request->categories as $row) {
				$data->category()->attach($row);
			}
		if(is_array($request->tags) and count($request->tags) > 0)
			foreach ($request->tags as $row) {
				$data->tag()->attach($row);
			}
		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.post.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		echo "string";
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['categories'] = Category::all();
		$data['tags'] = Tag::all();
		$data['portfolio'] = Portfolio::all();

		$data['content'] = Post::with('mediaPost','category','tag')->find($id);
		return view('page.post.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(PostRequest $request,$id)
	{
		

		$data=Post::find($id);
		if(empty($request->show_comment))
		{
			$data->show_comment = '';
			$data->save();
		}
		$data->update($request->all());
		if($request->meta_description == "")
		{
			$data->update(array(
				"meta_description" => strip_tags($request->description)
				));
		}
		$data->category()->detach();
		$data->tag()->detach();
		if(is_array($request->categories) and count($request->categories) > 0)
		foreach ($request->categories as $row) {
			$data->category()->attach($row);
		}
		if(is_array($request->tags) and count($request->tags) > 0)
		foreach ($request->tags as $row) {
			$data->tag()->attach($row);
		}
		$log = array();
		$log['action'] = 'update';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('vrs-admin.post.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data=Post::find($id);
		if($data->category()->count() > 0)
		$data->category()->detach();
		$data->tag()->detach();

		$menu = MenuDetail::where('ms_menu_type_id',2)->where('custom',$id)->get();
		foreach ($menu as $row) {
			$row->delete();
		}

		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));

		$data->delete();

		

		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.post.index');
	}

	public function getAllAjax()
	{
		$data = Post::all();
		return response()->json($data);
	}
	public function featured($id)
	{
		$data = Post::find($id);
		if(empty($data))
		{
			return;
		}
		if($data->featured == '1')
		{
			$data->featured = '0';
		}else{
			$data->featured = '1';
		}
		$data->save();
		return;
	}
	function fillMeta()
	{
		//strip_tags
		$post = Post::all();
		foreach ($post as $row) 
		{
			if($row->meta_description == "")
			{
				$row->update(array(
					"meta_description" => strip_tags($row->description)
					));
				$row->save();
			}
		}
	}
}
