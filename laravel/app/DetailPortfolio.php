<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPortfolio extends Model {

	protected $table = 'tr_detail_portfolios';
	protected $fillable = array('type','ms_media_id','ms_portfolio_id','caption','description','link');
	public $timestamps = false;

	public function media()
    {
        return $this->belongsTo('App\Media','ms_media_id','id');
    }

    public function portfolio()
    {
        return $this->belongsTo('App\Portfolio','ms_portfolio_id','id');
    }
    public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}
