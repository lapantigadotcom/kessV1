<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FilePdf extends Model {

	protected $table = 'ms_files';
	protected $fillable = array('file','mime','title','description','isumum', 'ms_media_id');
	public $timestamps = true;

    public function media()
    {
        return $this->hasOne('\App\Media', 'id', 'ms_media_id');
    }
}
