<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table = 'ms_categories';
	protected $fillable = array('title','parent_id','description','ms_user_id');
	public $timestamps = true;

	public function user()
	{
		return $this->belongsTo('App\User','ms_user_id','id');
	}
	public function subcategory()
	{
		return $this->hasMany('App\Category','parent_id','id');
	}
	public function post()
    {
        return $this->belongsToMany('App\Post','tr_category_posts','ms_category_id','ms_post_id');
    }
    public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}
