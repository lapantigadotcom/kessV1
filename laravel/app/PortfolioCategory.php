<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PortfolioCategory extends Model {

	protected $table = 'ms_portfolio_category';
    protected $fillable = array('name');
	public $timestamps = false;

}
